<?php

define('Message_Limit', 10);


function send_mail($to,$subject,$message) {

	//$to = 'mahmoud.salem@media-sci.com' ;

	// To send HTML mail, the Content-type header must be set
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	// Additional headers
	//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
	$headers .= 'From: MediaSCI <info@media-sci.com>' . "\r\n".
            'X-Mailer: PHP/' . phpversion();
	//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
	//$headers .= 'Bcc: '. implode(",",$user_emails) . "\r\n";


	// Mail it
	mail($to, $subject, $message, $headers);
}
function pixel(){
      $im=imagecreate(1,1);

    // Set the background colour
    $white=imagecolorallocate($im,255,255,255);

    // Allocate the background colour
    imagesetpixel($im,1,1,$white);

    // Set the image type
    header("content-type:image/jpg");

    // Create a JPEG file from the image
    imagejpeg($im);

    // Free memory associated with the image
    imagedestroy($im);

  }

function assets($file = "") {
	return URL::to("assets/dashboard/" . $file);
}

function uploads_url($file = "") {
	return url('/') . "/" . UPLOADS . "/" . $file;
}

function uploads_path($file = "") {
	return public_path() . "/" . UPLOADS . "/" . $file;
}

function get_user_photo() {
	return Session::get("user_photo");
}

/*
 * Lang block
 */

function langs($func = false) {
	$langs = \Mediasci\Cms\Models\Languages::all();
	// $langs = Config::get("cms::app.locales");
	$lang = Config::get("app.locale");

	if ($func) {
		foreach ($langs as $code => $title) {
			?>
			<span class="lang_area <?php echo $code ?>" <?php if ($lang != $code) { ?> style="display:none;" <?php } ?>>
				<?php $func($code, $title->lang); ?>
			</span>
			<?php
		}
	} else {
		?>
		<div class="btn-group lang_block">
			<?php foreach ($langs as $code => $title->lang) { ?>
				<button class="btn <?php if ($lang == $code) { ?>btn-primary btn-labeled<?php } else { ?>btn-outline<?php } ?>" type="button" rel="<?php echo $code; ?>"><?php echo $title->lang; ?></button>
			<?php } ?>
		</div>
		<script>
			$(document).ready(function () {
				$(".lang_block button").click(function () {
					var base = $(this);
					var lang = base.attr("rel");
					$(".lang_block button").removeClass("btn-primary btn-labeled btn-outline");
					base.addClass("btn-primary btn-labeled");
					$(".lang_area").hide();
					$(".lang_area." + lang).show();


				});
			});
		</script>
		<?php
	}
}

/*
 * Generate slug
 */



/**
 * Convert int size to formatted file size
 *
 * @param  int
 * @param  string
 * @return string
 */
function format_file_size($size, $type = "KB") {
	switch ($type) {
		case "KB":
			$filesize = $size * .0009765625; // bytes to KB
			break;
		case "MB":
			$filesize = ($size * .0009765625) * .0009765625; // bytes to MB
			break;
		case "GB":
			$filesize = (($size * .0009765625) * .0009765625) * .0009765625; // bytes to GB
			break;
	}

	if ($filesize < 0) {
		return $filesize = 'unknown file size';
	} else {
		return round($filesize, 2) . ' ' . $type;
	}
}

function countNotEmpty($array) {//counts the non-empty items of an array
	$counter = 0;
	foreach ($array as $item) {
		if (!empty($item)) {
			$counter++;
		}
	}
	return $counter;
}

// Media functions

function get_youtube_video_id($url) {

	if (strstr($url, "/v/")) {
		$array = parse_url($url);
		$path = trim($array["path"], "/");
		if ($parts = @explode("/", $path)) {
			if (isset($parts[1])) {
				return $parts[1];
			}
		}
	}
	$url_string = parse_url($url, PHP_URL_QUERY);
	parse_str($url_string, $args);
	return isset($args['v']) ? $args['v'] : false;
}

function get_extension($mime = "") {

	$types = array(
		'csv' => array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel'),
		'pdf' => array('application/pdf', 'application/x-download'),
		'ai' => 'application/postscript',
		'eps' => 'application/postscript',
		'xls' => array('application/excel', 'application/vnd.ms-excel', 'application/msexcel'),
		'ppt' => array('application/powerpoint', 'application/vnd.ms-powerpoint'),
		'swf' => 'application/x-shockwave-flash',
		'zip' => array('application/x-zip', 'application/zip', 'application/x-zip-compressed'),
		'mp3' => array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
		'wav' => array('audio/x-wav', 'audio/wave', 'audio/wav'),
		'bmp' => array('image/bmp', 'image/x-windows-bmp'),
		'gif' => 'image/gif',
		'jpg' => array('image/jpeg', 'image/pjpeg'),
		'png' => array('image/png', 'image/x-png'),
		'txt' => 'text/plain',
		'rtx' => 'text/richtext',
		'rtf' => 'text/rtf',
		'mpg' => 'video/mpeg',
		'mov' => 'video/quicktime',
		'avi' => 'video/x-msvideo',
		'doc' => 'application/msword',
		'docx' => array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip'),
		'xlsx' => array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip'),
		'word' => array('application/msword', 'application/octet-stream'),
		'xl' => 'application/excel'
	);

	foreach ($types as $extension => $mimes) {
		if ((is_array($mimes) and in_array($mime, $mimes)) or ( !is_array($mimes) and $mime == $mimes)) {
			return $extension;
		}
	}

	return false;
}

/*
  function get_audio_duration($url) {
  http://soundcloud.com/oembed?format=js&url=' . $url . '&iframe=true
  //Get the JSON data of song details with embed code from SoundCloud oEmbed
  $getValues = file_get_contents('http://soundcloud.com/oembed?format=js&url=' . $url . '&iframe=true');
  //Clean the Json to decode
  $decodeiFrame = substr($getValues, 1, -2);
  //json decode to convert it as an array
  $jsonObj = json_decode($decodeiFrame);

  $pattern = '/<iframe[^>]*\ssrc="[^"]*\?visual=true&url=[^&]*tracks%2F(\d+)[^&]*&/';
  preg_match($pattern, $jsonObj->html, $matches);

  $ch = curl_init('');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_URL, "http://api.soundcloud.com/tracks/$matches[1].json?client_id=YOUR_CLIENT_ID");
  // Execute
  $result = curl_exec($ch);
  // Close connection
  curl_close($ch);

  $json_res = json_decode($result, true);

  return $json_res['duration'] / 1000;
  }
 */

//function get_youtube_video_details($id) {
//
//    $row = new stdClass();
//    $row->title = "";
//    $row->content = "";
//    $row->keywords = "";
//    $row->length = "";
//    $row->image = "";
//
//    $feedURL = 'http://gdata.youtube.com/feeds/api/videos/' . $id;
//    $entry = simplexml_load_file($feedURL);
//    $obj = new stdClass;
//    $media = $entry->children('http://search.yahoo.com/mrss/');
//    $row->title = (string) $media->group->title;
//    $row->description = (string) strip_tags($media->group->description);
//    $row->keywords = (string) $media->group->keywords;
//    $yt = $media->children('http://gdata.youtube.com/schemas/2007');
//    $attrs = $yt->duration->attributes();
//    $row->length = (string) $attrs['seconds'];
//    $row->image = "http://img.youtube.com/vi/" . $id . "/0.jpg";
//
//    return $row;
//}

function get_youtube_video_details($id) {

	$row = new stdClass();
	$row->title = "";
	$row->description = "";
	$row->content = "";
	$row->keywords = "";
	$row->length = "";
	$row->image = "";

	$url = "https://www.youtube.com/watch?v=" . $id;
	$youtube = "http://www.youtube.com/oembed?url=" . $url . "&format=json";
	$curl = curl_init($youtube);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$return = json_decode(curl_exec($curl));
	curl_close($curl);
	if (isset($return->title)) {
		$row->title = $return->title;
		$row->description = $return->title;
	}

	if (isset($return->thumbnail_url))
		$row->image = $return->thumbnail_url;
	else
		$row->image = "http://img.youtube.com/vi/" . $id . "/default.jpg";

	return $row;
}

function get_soundcloud_track_details($url = "") {

	if ($content = @file_get_contents("http://api.soundcloud.com/resolve.json?url=" . $url . "&client_id=203ed54b9fd03054e1aa2b2cae337eae")) {
		$data = json_decode($content);
		$row = new \stdClass();
		$row->id = $data->id;
		$row->title = $data->title;
		$row->description = $data->description;
		$row->length = round($data->duration / 1000);
		$row->link = $data->permalink_url;
		if ($data->artwork_url != null) {
			$row->image = $data->artwork_url;
		} else {
			$row->image = "";
		}

		return $row;
	}
}

function format_duration($init) {
	$hours = floor($init / 3600);
	$minutes = floor(($init / 60) % 60);
	$seconds = $init % 60;
	$string = "";
	if ($hours != 0) {
		$string .= $hours . ":";
	}

	if (strlen($hours) == 1) {
		$hours = "0" . $hours;
	}
	if (strlen($minutes) == 1) {
		$minutes = "0" . $minutes;
	}
	if (strlen($seconds) == 1) {
		$seconds = "0" . $seconds;
	}

	$string .= "$minutes:$seconds";
	return $string;
}

function thumbnail($file = "", $size = "thumbnail", $default = "img/file.jpg") {
	$parts = explode(".", $file);
	$ext = end($parts);
	if (in_array(strtolower($ext), array("jpg", "jpeg", "png", "bmp", "gif"))) {
		if ($size == "full") {
			return uploads_url($file);
		} else {
			return uploads_url( $file);
		}
	} else {
		if (File::exists(public_path("packages/admin/files/" . strtolower($ext) . ".png"))) {
			return assets("files/" . strtolower($ext) . ".png");
		} else {
			return assets($default);
		}
	}
}

/* ==========================Adnan========================= */

/*
  |--------------------------------------------------------------------------
  | to array
  |--------------------------------------------------------------------------
  |
 */

function to_array($objs, $vars = true) {
	$temp = array();
	$arr = array();
	if ($vars) {
		foreach ($objs as $key => $obj) {
			foreach (get_object_vars($obj) as $key => $value) {
				$temp[$key] = $value;
			}
			$arr[] = $temp;
		}
	} else {
		foreach ($objs as $key => $obj) {
			foreach (get_object_vars($obj) as $key => $value) {
				$arr[] = $value;
			}
		}
	}
	return $arr;
}

/*
  |--------------------------------------------------------------------------
  | get post details
  |--------------------------------------------------------------------------
  |
 */

function get_post_details($post_slug, $site, $from_slug = '') {
	$link = "";
	$sites = Config::get("cms::app.sites");
	foreach ($sites as $key => $value) {
		if ($key == $site) {
			if ($site == 3) {
				$link = $value . "details/featured-" . $from_slug . '/' . $post_slug;
			} else {
				$link = $value . "details/" . $post_slug;
			}
		}
	}
	return $link;
}

/**
 * get all parent ategories
 */
function get_cats($args = []) {
	$cats = Mediasci\Cms\Models\Category::get_cats(['lang' => App::getLocale(), 'parent' => $args['parent'], 'conn' => $args['conn']]);
	return $cats;
}

/**
 * get all parent ategories
 */
function get_site($database) {
	$sites = Config::get('cms::app.site_databases');

	foreach ($sites as $key => $site) {
		if ($key == $database) {
			return $site;
		}
	}
}

/**
 * Build tree
 */
function buildtree($src_arr, $parent_id = 0, $tree = array()) {
	foreach ($src_arr as $idx => $row) {
		if ($row['parent'] == $parent_id) {
			foreach ($row as $k => $v)
				$tree[$row['id']][$k] = $v;
			unset($src_arr[$idx]);


			$child = buildtree($src_arr, $row['id']);
			if ($child) {
				$tree[$row['id']]['children'] = $child;
			}
		}
	}
	//ksort($tree);
	return $tree;
}

$output = '';

/**
 * Draw tree
 */
function cats_tree($tree, $recursionDepth = 0, $maxDepth, $args = [], $excep = []) {
	global $output;

	if ($maxDepth && ($recursionDepth == $maxDepth))
		return;

	if ($recursionDepth == 0) {
		$output = '';
	}

	$output .= "<ul>";

	foreach ($tree as $k => $v) {
		if (!in_array($tree[$k]['id'], $excep)) {

			$output .= "<li><div class='checkbox'><label><input type='checkbox' class='px' name='" . $args['checkbox_name'] . "[]' value='" . $tree[$k]['id'] . "'> <span class='lbl'>" . $tree[$k]['name'] . "</span></label></div>";

			if (isset($tree[$k]['children']))
				cats_tree($tree[$k]['children'], $recursionDepth + 1, $maxDepth, $args, $excep);

			$output .= "</li>";
		}
	}

	$output .= "</ul>";


	return $output;
}

$parent = '';
$space = '';

/**
 * Draw tree
 */
function parent_tree($tree, $recursionDepth = 0, $maxDepth, $class = '', $del = '', $excep = []) {
	global $parent, $space;

	if ($maxDepth && ($recursionDepth == $maxDepth))
		return;

	if ($recursionDepth == 0) {
		$parent = '';
		$space = '';
	}

	foreach ($tree as $k => $v) {
		if (!in_array($tree[$k]['id'], $excep)) {
			$parent .= "<option value='" . $tree[$k]['id'] . "' class='" . $class . "'>" . $del . $tree[$k]['name'] . "</option>";

			if (isset($tree[$k]['children'])) {
				$space .= '&nbsp;&nbsp;&nbsp;';
				parent_tree($tree[$k]['children'], $recursionDepth + 1, $maxDepth, $class, $space);
			}
		}
	}


	return $parent;
}

/**
 * create slug
 */
function create_slug($str, $options = array()) {
	/* $slug = preg_replace('/[^A-Za-z0-9-أإآابتثجحخدذرزسشصضطظعغفقكلمنهولالأل‘ىيئةءؤدً]+/', '-', mb_strtolower($string));
	  return $slug; */

	$str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

	$defaults = array(
		'delimiter' => '-',
		'limit' => null,
		'lowercase' => true,
		'replacements' => array(),
		'transliterate' => false,
	);

// Merge options
	$options = array_merge($defaults, $options);

	$char_map = array(
// Latin
		'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
		'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
		'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
		'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
		'ß' => 'ss',
		'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
		'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
		'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
		'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
		'ÿ' => 'y',
// Latin symbols
		'©' => '(c)',
// Greek
		'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
		'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
		'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
		'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
		'Ϋ' => 'Y',
		'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
		'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
		'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
		'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
		'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
// Turkish
		'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
		'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
// Russian
		'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
		'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
		'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
		'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
		'Я' => 'Ya',
		'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
		'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
		'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
		'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
		'я' => 'ya',
// Ukrainian
		'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
		'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
// Czech
		'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
		'Ž' => 'Z',
		'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
		'ž' => 'z',
// Polish
		'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
		'Ż' => 'Z',
		'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
		'ż' => 'z',
// Latvian
		'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
		'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
		'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
		'š' => 's', 'ū' => 'u', 'ž' => 'z'
	);

// Make custom replacements
	$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

// Transliterate characters to ASCII
	if ($options['transliterate']) {
		$str = str_replace(array_keys($char_map), $char_map, $str);
	}

// Replace non-alphanumeric characters with our delimiter
	$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

// Remove duplicate delimiters
	$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

// Truncate slug to max. characters
	$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

// Remove delimiter from ends
	$str = trim($str, $options['delimiter']);

	return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}

function setNullWhenEmpty($var) {
	if (empty($var)) {
		return null;
	} else {
		return $var;
	}
}

/*
  |--------------------------------------------------------------------------
  | add block post
  |--------------------------------------------------------------------------
  |
 */

function add_block_post($block_id, $post_id, $count = 8) {
	//  block folder
	//$block_dir = Config::get("cms::app.dbDriver");
	$block_dir = Session::get('conn');
	if (!$block_dir) {
		$block_dir = Config::get("cms::app.dbDriver");
	}

	$filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
	$arr = array();

	if (file_exists($filePath)) {
		$objData = file_get_contents($filePath);
		$arr = (array) json_decode($objData);
		if (count($arr) < $count) {
			if (!in_array($post_id, $arr)) {
				$arr[] = $post_id;
			}
		} else {
			array_insert($arr, $post_id, $count);
		}
	} else {
		$arr[] = $post_id;
	}

	file_put_contents($filePath, json_encode($arr));
}

/*
  |--------------------------------------------------------------------------
  | add block post
  |--------------------------------------------------------------------------
  |
 */

function append_block_post($block_id, $post_id, $conn, $count = 8) {
	//  block folder
	$block_dir = $conn;

	$filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
	$arr = array();

	if (file_exists($filePath)) {
		$objData = file_get_contents($filePath);
		$arr = (array) json_decode($objData);
		array_insert($arr, $post_id, 0);
	} else {
		$arr[] = $post_id;
	}

	file_put_contents($filePath, json_encode($arr));
}

/*
  |--------------------------------------------------------------------------
  | create block order file
  |--------------------------------------------------------------------------
  |
 */

function create_block_file($block_id) {
	//  block folder
	//$block_dir = Config::get("cms::app.dbDriver");
	$block_dir = Session::get('conn');
	if (!$block_dir) {
		$block_dir = Config::get("cms::app.dbDriver");
	}

	$filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
	$arr = array();

	file_put_contents($filePath, json_encode($arr));
}

/*
  |--------------------------------------------------------------------------
  | save block
  |--------------------------------------------------------------------------
  |
 */

function save_block($block_id, $posts) {
	//  block folder
	//$block_dir = Config::get("cms::app.dbDriver");
	$block_dir = Session::get('conn');
	if (!$block_dir) {
		$block_dir = Config::get("cms::app.dbDriver");
	}

	$temp = array();

	$filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
	$count = count($posts);

	$objData = file_get_contents($filePath);
	$arr = (array) json_decode($objData);
	$arr1 = array_slice($arr, $count - 1, count($arr) - 1);

	foreach ($arr1 as $key => $value) {
		if (!in_array($value, $posts)) {
			$temp[] = $value;
		}
	}

	$block_posts = array_merge($posts, $temp);

	file_put_contents($filePath, json_encode($block_posts));
}

/*
  |--------------------------------------------------------------------------
  | get block posts
  |--------------------------------------------------------------------------
  |
 */

function get_block_posts($block_id, $post_id, $count = 8) {
	//  block folder
	//$block_dir = Config::get("cms::app.dbDriver");
	$block_dir = Session::get('conn');
	if (!$block_dir) {
		$block_dir = Config::get("cms::app.dbDriver");
	}

	$filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";

	$arr = array();
	$temp = array();

	if (file_exists($filePath)) {
		$objData = file_get_contents($filePath);
		$arr = (array) json_decode($objData);
		//$arr = array_slice($arr, 0, $count-1);

		foreach ($arr as $key => $value) {
			if ($value != $post_id) {
				$temp[] = $value;
			}
		}

		$arr = $temp;
		$arr = array_slice($arr, 0, $count - 1);
	} else {
		file_put_contents($filePath, '');
	}


	return $arr;
}

/*
  |--------------------------------------------------------------------------
  | delete post blocks
  |--------------------------------------------------------------------------
  |
 */

function delete_post_block($block_id, $post_id) {
	//  block folder
	//$block_dir = Config::get("cms::app.dbDriver");
	$block_dir = Session::get('conn');
	if (!$block_dir) {
		$block_dir = Config::get("cms::app.dbDriver");
	}

	$filePath = getcwd() . DIRECTORY_SEPARATOR . "blocks" . DIRECTORY_SEPARATOR . $block_dir . DIRECTORY_SEPARATOR . $block_id . ".json";
	$arr = array();
	$temp = array();

	if (file_exists($filePath)) {
		$objData = file_get_contents($filePath);
		$arr = (array) json_decode($objData);

		foreach ($arr as $key => $value) {
			if ($value != $post_id) {
				$temp[] = $value;
			}
		}

		file_put_contents($filePath, json_encode($temp));
	}
}

/*
  |--------------------------------------------------------------------------
  | Insert element at specific Index of Array.
  |--------------------------------------------------------------------------
  |
 */

function array_insert(&$array, $element, $position = null) {
	if (count($array) == 0) {
		$array[] = $element;
	} elseif (is_numeric($position) && $position < 0) {
		if ((count($array) + position) < 0) {
			$array = array_insert($array, $element, 0);
		} else {
			$array[count($array) + $position] = $element;
		}
	} elseif (is_numeric($position) && isset($array[$position])) {
		$part1 = array_slice($array, 0, $position, true);
		$part2 = array_slice($array, $position, null, true);
		$array = array_merge($part1, array($position => $element), $part2);
		foreach ($array as $key => $item) {
			if (is_null($item)) {
				unset($array[$key]);
			}
		}
	} elseif (is_null($position)) {
		$array[] = $element;
	} elseif (!isset($array[$position])) {
		$array[$position] = $element;
	}
	$array = array_merge($array);
	return $array;
}

function denied() {
	return "Permission denied";
}

/*==========================Adnan=========================*/

function is_admin()
{
	return \Session::has('admin_user_id');
}

?>
