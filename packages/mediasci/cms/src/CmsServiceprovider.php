<?php

namespace Mediasci\Cms;

/**
 * 
 * @author mostafa elbardeny <mostafa.elbardeny@media-sci.com>
 */
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

use Mediasci\Cms\Http\Controllers\CmsController;

class CmsServiceprovider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot() {
        define("UPLOADS", "uploads");
		$this->publishes([
			__DIR__ . '/config/app.php' => config_path('app.php'),
		]);
		include __DIR__.'/helpers.php';

		$this->setupRoutes($this->app->router);
		$this->loadViewsFrom(realpath(__DIR__ . '/../resources/views'), 'cms');
		$this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'cms');
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function setupRoutes(Router $router) {
		$router->middleware('backend', 'Mediasci\Cms\Http\Middleware\Backend');

		$router->group(['prefix' => \Config::get("app.ADMIN"), 'namespace' => 'Mediasci\Cms\Http\Controllers', 'middleware' => ['web']], function($router) {
			require __DIR__ . '/Http/routes.php';
		});
	}

	public function register() {
		$this->mergeConfigFrom(__DIR__ . '/config/app.php', 'app');

		//setting up the aliases
		$loader = \Illuminate\Foundation\AliasLoader::getInstance();
		//Helpers
		$loader->alias('Helpers', 'App\Http\Controllers\Common\Helpers');
		//parent controller
		$loader->alias('CmsController', '\Mediasci\Cms\Http\Controllers\CmsController');
		//models
		$loader->alias('User', 'Mediasci\Cms\Models\User');
		$loader->alias('Sites', 'Mediasci\Cms\Models\Sites');
		$loader->alias('Categories', 'Mediasci\Cms\Models\Categories');
		$loader->alias('Groups', 'Mediasci\Cms\Models\Groups');
		$loader->alias('Roles', 'Mediasci\Cms\Models\Roles');
		$loader->alias('Routes', 'Mediasci\Cms\Models\Routes');
		$loader->alias('RolesRoutes', 'Mediasci\Cms\Models\RolesRoutes');
		$loader->alias('Tags', 'Mediasci\Cms\Models\Tags');
		$loader->alias('Comments', 'Mediasci\Cms\Models\Comments');
		$loader->alias('Blocks', 'Mediasci\Cms\Models\Blocks');
		$loader->alias('BlocksPosts', 'Mediasci\Cms\Models\BlocksPosts');
		$loader->alias('Messages', 'Mediasci\Cms\Models\Messages');
		$loader->alias('Sms', 'Mediasci\Cms\Models\Sms');
		$loader->alias('UserMessages', 'Mediasci\Cms\Models\UserMessages');
		$loader->alias('UserSms', 'Mediasci\Cms\Models\UserSms');
		$loader->alias('Posts', 'Mediasci\Cms\Models\Posts');
		$loader->alias('Media', 'Mediasci\Cms\Models\Media');
		$loader->alias('Services', 'Mediasci\Cms\Models\Services');
		$loader->alias('ServicesLangs', 'Mediasci\Cms\Models\ServicesLangs');
		$loader->alias('Cities', 'Mediasci\Cms\Models\Cities');
		$loader->alias('CitiesLangs', 'Mediasci\Cms\Models\CitiesLangs');
		$loader->alias('Setting', 'App\Models\Setting');
		$loader->alias('QuestionsAnswers', 'Mediasci\Cms\Models\QuestionsAnswers');
		$loader->alias('QuestionsAnswersLangs', 'Mediasci\Cms\Models\QuestionsAnswersLangs');
	}

}
