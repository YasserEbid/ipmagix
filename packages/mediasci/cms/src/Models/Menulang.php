<?php
namespace Mediasci\Cms\Models;

class Menulang extends BaseModel {
    protected $table='menus_langs';
    
       protected $fillable=['menu_id','name','lang'];
       function menu(){
           return $this->belongsTo('Mediasci\Cms\Models\Menu','menu_id');
       }
}
