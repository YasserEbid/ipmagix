<?php

namespace Mediasci\Cms\Models;

class Blocks extends CmsModel {

	public $table = 'blocks';

	public static function get_attributes_names() {
		$names = [
			'block_name' => trans('cms::blocks.block_name'),
			'site_id' => trans('cms::blocks.site_name'),
			'max_posts' => trans('cms::blocks.max_posts'),
		];

		return $names;
	}

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function get_all($args = []) {
		$query = self::init();

		if (isset($args['q'])) {
			$query->where('block_name', 'like', '%' . $args['q'] . '%');
		}
		if (isset($args['site_id'])) {
			$query->where('site_id', $args['site_id']);
		}

		if (isset($args['per_page'])) {
			$tags = $query->paginate($args['per_page']);
		} else {
			$tags = $query->get();
		}

		return $tags;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('block_id', $id)->update($data);

		return $result;
	}

	public static function add($data) {
		$id = self::init()->insertGetId($data);

		return $id;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();

		return $result;
	}

}
