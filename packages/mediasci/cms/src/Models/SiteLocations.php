<?php

namespace Mediasci\Cms\Models;

class SiteLocations extends BaseModel {
    public $table = 'site_locations';


	public function locationslang($e){
		return $this->hasMany('Mediasci\Cms\Models\LocationsLangs','location_id')->where('lang',$e)->first();
		//return $this->hasMany('Mediasci\Cms\Models\ServicesLangs','service_id');
	}

//	public static function modify_service_lang($data, $args) {
//		$result = \DB::table('services_langs')->where($args)->update($data);
//		return $result;
//	}
}
