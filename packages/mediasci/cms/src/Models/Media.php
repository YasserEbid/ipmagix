<?php
namespace Mediasci\Cms\Models;
use Illuminate\Database\Eloquent\Model ;

class Media extends BaseModel
{
    
    public $table = 'media';
    protected $primaryKey = 'media_id';
  
    public $allowedMimes = array(
        'image/png',
        'image/jpeg',
        'image/gif',
    );
    public function doctor(){
        return $this->belongsTo('Mediasci\Cms\Models\Doctor','media_id','media_id');
    }

    public function getImageURL($size = 'full') {
        $sizeName = ($size == 'full') ? '' : $size . '-';
        return URL::to('/') . '/uploads/' . $sizeName . $this->media_path;
    }

    public function getLocalPath($size = 'full') {
        $sizeName = ($size == 'full') ? '' : $size . '-';
        return public_path('uploads/' . $sizeName . $this->media_path);
    }

    public function posts() {
        return $this->hasMany('Post', 'post_image_id', 'media_id');
    }

    public function users() {
        return $this->belongsToMany('Author');
    }

    public function galleries() {
        return $this->belongsToMany('Gallery', 'galleries_media', 'media_id', 'gallery_id');
    }

    public function uploadFile($imageContent, $extension) {
        $fileName = str_random(40) . '.' . $extension;
        $filePath = public_path() . '/uploads/' . $fileName;
//        $im = imagecreatefromstring($imageContent);
//        dd($im);
        File::put($filePath, base64_decode($imageContent));
//        dd($a);
//        $mimeParts = explode("/", $file->getMimeType());
//        $mimeType = $mimeParts[0];
        $mimeType = mime_content_type($filePath);
        $this->media_path = $fileName;
        $this->media_type = $mimeType;
        $this->media_title = '';
        $this->media_created_date = date("Y-m-d H:i:S");
        self::set_sizes($fileName);
        return $this;
    }

    public static function set_sizes($filename) {
        // resize
        $sizes = Config::get("cms::app.sizes");
        foreach ($sizes as $size => $dimensions) {
            /*
              \Image::make(UPLOADS . "/" . $filename)->resize(null, $dimensions[1], function ($constraint) {
              $constraint->aspectRatio();
              $constraint->upsize();
              })->save(UPLOADS . "/" . $size . "-" . $filename);



             * 
             */

            $width = \Image::make(UPLOADS . "/" . $filename)->width();
            $height = \Image::make(UPLOADS . "/" . $filename)->height();
            /*
              if($width > $height){
              $new_width = $width;
              $new_height = null;
              }else{
              $new_width = null;
              $new_height = $height;
              } */
            if ($width > $height) {


                $new_width = null;
                $new_height = $dimensions[0];
            } else {
                $new_width = $dimensions[1];
                $new_height = null;
            }

            \Image::make(UPLOADS . "/" . $filename)
                    ->resize($new_height, $new_width, function ($constraint) {
                        $constraint->aspectRatio();
                        // $constraint->upsize();
                    })
//                    ->resizeCanvas($dimensions[0], $dimensions[1], 'center', false, "#000000")
                    ->save(UPLOADS . "/" . $size . "-" . $filename);
        }
    }

}