<?php

namespace Mediasci\Cms\Models;

class Sms extends CmsModel {

	public $table = 'sms';

	public static function get_attributes_names() {
		$names = [
			'sms_message' => trans('cms::sms.sms_message'),
			'sent_date' => trans('cms::sms.sent_date'),
			'users' => trans('cms::sms.users'),
		];

		return $names;
	}

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function get_all($args = []) {
		$query = self::init();

		if (isset($args['q'])) {
			$query->where('sms_message', 'like', '%' . $args['q'] . '%');
		}

		if (isset($args['site_id'])) {
			$query->where('site_id', $args['site_id']);
		}
	
		if (isset($args['per_page'])) {
			$sms = $query->paginate($args['per_page']);
		} else {
			$sms = $query->get();
		}

		return $sms;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('sms_id', $id)->update($data);

		return $result;
	}

	public static function add($data) {
		$id = self::init()->insertGetId($data);

		return $id;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();

		return $result;
	}
	
	public static function cron(){
		return self::init()->join('users_sms', 'sms.sms_id', '=', 'users_sms.sms_id')
						->join('users', 'users_sms.user_id', '=', 'users.user_id')
						->where('sms.finsihed','=',0)
						->where('sms.sent_date','<=', date("Y-m-d H:i:s"))
						->where('users_sms.sent','=',0)
						->take(SMS_Limit)
						->select([ 'users.email' , 'users.full_name' ,'users.mobile', 'users.user_id' , 'sms.sms_id' , 'sms.sms_message'])
						->orderBy('sms.sms_id', 'ASC')
						->get();
	}

}
