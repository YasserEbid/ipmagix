<?php
namespace Mediasci\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends BaseModel
{
    protected $table='volunteers';

    public function countryName(){
        return $this->hasOne('Mediasci\Cms\Models\CountriesNames','country')->first();
    }
}
