<?php
namespace Mediasci\Cms\Models;

class General_setting extends BaseModel {
    protected $table='general_settings';
    protected $fillable=['name','value'];
}