<?php

namespace Mediasci\Cms\Models;

class UserSms extends CmsModel {

	public $table = 'users_sms';


	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->get();
		return $result;
	}

	public static function change_status($msg_id,$user_ids,$status) {
		return self::init()->where('sms_id', $msg_id)->whereIn('user_id',$user_ids)
					->update([ 'sent' => $status ]);
	}

	public static function add($data,$id) {
		self::init()->where('sms_id',$id)->delete();
		foreach ($data as $one){
			$conditions=['sms_id'=>$id,'user_id'=>$one];
			self::init()->insertGetId($conditions);
		}
		return true;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();

		return $result;
	}

	public static function get_sent_users($conditions) {
		$result = self::init()->where($conditions)->get();

		return $result;
	}

}
