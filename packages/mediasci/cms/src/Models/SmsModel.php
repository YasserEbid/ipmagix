<?php

namespace Mediasci\Cms\Models;

class SmsModel extends BaseModel {
    
    protected $table='new_sms';
    protected $guarded=['id'];
    public $timestamps=false;
    public $rules=[
        'message'=>'required'
    ];
}
