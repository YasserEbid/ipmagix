<?php

namespace Mediasci\Cms\Models;

class CitiesLangs extends BaseModel {

	protected $table='cities_langs';
	
	public function city(){
		return $this->belongsTo('Mediasci\Cms\Models\Cities','city_id');
	}
}
