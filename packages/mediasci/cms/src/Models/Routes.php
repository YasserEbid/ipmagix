<?php

namespace Mediasci\Cms\Models;

class Routes extends CmsModel {

	public $table = 'routes';

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function get_by_group_id($id) {
		$result = self::init()->where('group_id', $id)->get();
		return $result;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('route_id', $id)
				->update($data);
		return $result;
	}
	
	public static function is_general($route_name){
		$result = self::init()->where('route_name', $route_name)->select('route_type')->first();
		
		return ($result && $result->route_type == 1) ? true : false;
	}

	public static function add($data) {
		$result = self::init()->insert($data);
		return $result;
	}

}