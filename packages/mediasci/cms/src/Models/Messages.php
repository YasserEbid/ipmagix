<?php

namespace Mediasci\Cms\Models;

class Messages extends CmsModel {

	public $table = 'messages';

	public static function get_attributes_names() {
		$names = [
			'messages_title' => trans('cms::messages.messages_title'),
			'messages_content' => trans('cms::messages.messages_content'),
			'sent_date' => trans('cms::messages.sent_date'),
			'users' => trans('cms::messages.users'),
		];

		return $names;
	}

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function get_all($args = []) {
		$query = self::init();

		if (isset($args['q'])) {
			$query->where('message_title', 'like', '%' . $args['q'] . '%');
		}

		if (isset($args['site_id'])) {
			$query->where('site_id', $args['site_id']);
		}
	
		if (isset($args['per_page'])) {
			$messages = $query->paginate($args['per_page']);
		} else {
			$messages = $query->get();
		}

		return $messages;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('message_id', $id)->update($data);

		return $result;
	}

	public static function add($data) {
		$id = self::init()->insertGetId($data);

		return $id;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();

		return $result;
	}
	
	public static function cron(){
		return self::init()->join('users_messages', 'messages.message_id', '=', 'users_messages.message_id')
						->join('users', 'users_messages.user_id', '=', 'users.user_id')
						->where('messages.finsihed','=',0)
						->where('messages.sent_date','<=', date("Y-m-d H:i:s"))
						->where('users_messages.sent','=',0)
						->take(Message_Limit)
						->select([ 'users.email' , 'users.full_name' , 'users.user_id' , 'messages.message_id' , 'messages.message_title' , 'messages.message_content'])
						->orderBy('messages.message_id', 'ASC')
						->get();
	}

}
