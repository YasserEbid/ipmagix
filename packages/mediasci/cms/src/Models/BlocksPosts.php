<?php

namespace Mediasci\Cms\Models;

class BlocksPosts extends CmsModel {

	public $table = 'blocks_posts';

	public static function get_attributes_names() {
		$names = array(
			'full_name' => trans('cms::users.full_name')
		);

		return $names;
	}
	public static function get_posts_by_block_id($block_id) {
		$query = self::init();

		$query->join('blocks', 'blocks_posts.block_id', '=', 'blocks.block_id');
		
		$query->join('posts', 'blocks_posts.post_id', '=', 'posts.post_id');
		
		$query->where('blocks_posts.block_id',$block_id);
		
		$query->addSelect('blocks_posts.post_id', 'posts.title' , 'blocks.block_name');
		
		$query->orderBy('blocks_posts.post_order', 'ASC');
		
		return $query->get();
		
	}
	
	public static function add($data,$id){
		self::remove($id);
		
		for($i=0;$i<count($data);$i++){
			$condition=['block_id'=>$id,
					'post_id'=>$data[$i],
					'post_order'=>$i+1
				  ];
			 self::init()->insert($condition);
		}
		return true;
	}
	public static function remove($id) {
		$result = self::init()->where('block_id',$id)->delete();

		return $result;
	}

}