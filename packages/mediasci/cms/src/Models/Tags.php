<?php

namespace Mediasci\Cms\Models;

class Tags extends CmsModel {

	public $table = 'tags';

	public static function get_attributes_names() {
		$names = [
			'tag_name' => trans('cms::tags.tag_name'),
		];

		return $names;
	}

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function get_all($args = []) {
		$query = self::init();

		if (isset($args['q'])) {
			$query->where('tag_name', 'like', '%' . $args['q'] . '%');
		}

		if (isset($args['per_page'])) {
			$tags = $query->paginate($args['per_page']);
		} else {
			$tags = $query->get();
		}

		return $tags;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('tag_id', $id)->update($data);

		return $result;
	}

	public static function add($data) {
		$id = self::init()->insertGetId($data);

		return $id;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();

		return $result;
	}

}
