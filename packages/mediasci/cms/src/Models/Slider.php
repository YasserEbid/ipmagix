<?php
namespace Mediasci\Cms\Models;

class Slider extends BaseModel {
    protected $table='sliders';
    protected $fillable=['media_id','link','country_id'];
     public function media(){
       return $this->hasOne('Mediasci\Cms\Models\Media','media_id','media_id') ;
    }
    public function sliderlang($lang){
        return $this->hasMany('Mediasci\Cms\Models\Slider_lang', 'slider_id')->where('lang',$lang)->first();
    }
}
