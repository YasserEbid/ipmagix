<?php
namespace Mediasci\Cms\Models;

class Menulink extends BaseModel {
    protected $table='menu_links';
protected $fillable=['menu_id','parent_id','active','order','link_url','open_windows','country_id'];

function menulinklang($lang){
    return $this->hasMany('Mediasci\Cms\Models\Menulinks_lang','menu_link_id')->where('lang',$lang)->first();
}
function mainmenu(){
    return $this->hasMany('Mediasci\Cms\Models\Menulink', 'parent_id');
}
function submenu(){
      return $this->belongsTo('Mediasci\Cms\Models\Menulink', 'parent_id');
}
function menu(){
   return $this->belongsTo('Mediasci\Cms\Models\Menu', 'menu_id');
}
}
