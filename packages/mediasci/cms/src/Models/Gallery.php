<?php
namespace Mediasci\Cms\Models;
use Illuminate\Database\Eloquent\Model as Eloquent, DB;

class Gallery extends CmsModel
{
        protected $table = 'galleries';
    
    public function scopeLang($query, $lang) {
        $query->join("galleries_langs", "galleries.gallery_id", "=", "galleries_langs.gallery_id")
                ->where("galleries_langs.lang", $lang);
        return $query;
    }

   /*==========================Adnan=========================*/
    /**
     * Get galleries
     */
    public function scopeGet_galleries( $query, $args = []){
    	$galleries = $query->join('galleries_langs', 'galleries_langs.gallery_id', '=', 'galleries.id')
                ->where('lang', '=', $args['lang'])
    			->orderBy('galleries.id', 'DESC')
                ->select('galleries.id', 'gallery_name')
                ->get();

        return $galleries;
    } 

    /**
     * Get post gallery
     */
    public function scopeGet_post_gallery( $query, $id){
        $gallery = DB::table('posts_galleries')->where('post_id', '=', $id)
                ->select('post_gallery_id')
                ->first();

        return $gallery;
    } 

    /**
     * delete post gallery
     */
    public function scopeDelete_post_gallery( $query, $args = []){
        DB::table('posts_galleries')->where('post_id', '=', $args['post_id'])->delete();
    
    } 

    /**
     * Add Posts Gallery
     */
    public function scopeAdd_post_gallery( $query, $args = [] ){

        DB::table('posts_galleries')->insert(
            array( 'post_id' => $args['post_id'], 'post_gallery_id' => $args['gallery_id'] )
        );

        
    }
    /*==========================Adnan=========================*/

}

