<?php

namespace Mediasci\Cms\Models;
use DB;

class QuestionsAnswersLangs extends BaseModel {

	protected $table='questions_answers_langs';
	
	public function questionAnswer(){
		return $this->belongsTo('Mediasci\Cms\Models\QuestionsAnswers','question_answer_id');
	}
}
