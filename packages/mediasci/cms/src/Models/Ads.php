<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Models;
use Mediasci\Cms\Models\Traits\Media;
/**
 * Description of Ads
 *
 * @author sayed
 */
class Ads extends BaseModel{
    use Media;
    protected $table='ads';
    protected $guarded=['id'];
    public $timestamps=false;
}
