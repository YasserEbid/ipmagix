<?php

namespace Mediasci\Cms\Models;

class AppointmentsRequests extends BaseModel {

	public $table = 'appointments_requests';

	public function city(){
		return $this->belongsTo('Mediasci\Cms\Models\Cities','city_id');
	}
	public function product(){
		return $this->belongsTo('Mediasci\Cms\Models\Posts','product_id','post_id');
	}

}
