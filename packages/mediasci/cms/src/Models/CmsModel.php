<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Models;

/**
 * Description of CmsModel
 *
 * @author Darsh
 */
abstract class CmsModel extends \DB {

    public $table = '';

    public static function init() {
		$obj = new static();
        $table = $obj->table;
        return \DB::table($table);
    }

}