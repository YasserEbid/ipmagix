<?php

namespace Mediasci\Cms\Models;

class Career extends BaseModel {
    protected $table='careers';
    protected $fillable=['media_id'];
    public function media(){
        return $this->hasOne('Mediasci\Cms\Models\Media','media_id','media_id');

    }
    public function careerlang($lang){
        return $this->hasMany('Mediasci\Cms\Models\Career_lang','career_id')->where('lang',$lang)->first();
    }
    public function category(){
        return $this->belongsTo('Mediasci\Cms\Models\Career_category','cat_id');
    }
    public function jobcv(){
        return $this->hasMany('Mediasci\Cms\Models\Job_cv', 'career_id');
    }

    public function categories()
    {
        return $this->belongsToMany('Mediasci\Cms\Models\Career_category', 'career_careers_category', 'career_id', 'careers_category_id');
    }
}
