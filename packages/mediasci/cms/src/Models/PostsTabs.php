<?php
namespace Mediasci\Cms\Models;

class PostsTabs extends BaseModel {
     protected $table='posts_tabs';
     protected $fillable=['slug','post_id'];
             
     function tablang($lang){
         return $this->hasMany('Mediasci\Cms\Models\PostsTabslangs', 'post_tab_id')->where('lang',$lang)->first();
     }
}

