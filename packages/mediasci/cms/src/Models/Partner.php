<?php
namespace Mediasci\Cms\Models;


class Partner extends BaseModel
{
  protected $table='partners';
  protected $fillable = ['media_id','type','country_id'];
   public function media(){
     return $this->hasOne('Mediasci\Cms\Models\Media','media_id','media_id') ;
  }
  public function partnerlang($lang){
      return $this->hasMany('Mediasci\Cms\Models\Partner_lang', 'partner_id')->where('lang',$lang)->first();
  }
}
