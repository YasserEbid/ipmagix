<?php

namespace Mediasci\Cms\Models;

class Comments extends CmsModel {

	public $table = 'comments';

	public static function get_attributes_names() {
		$names = array(
			'full_name' => trans('cms::comments.full_name'),
			'email' => trans('cms::comments.email'),
			'content' => trans('cms::comments.content'),
			'post_id' => trans('cms::comments.post_id'),
		);

		return $names;
	}

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();

		return $result;
	}

	public static function get_all($args = []) {
		$query = self::init();

		$query->join('posts_langs', 'comments.post_lang_id', '=', 'posts_langs.id');

		if (isset($args['post_id'])) {
			$query->where('comments.post_id', $args['post_id']);
		}

		$query->join('statuses', 'comments.comment_status', '=', 'statuses.status_id');

		if (isset($args['status'])) {
			$query->where('statuses.status_id', $args['status']);
		} else {
			$query->where('statuses.status_id', '!=', 4);
		}

		if (isset($args['site_id'])) {
			$query->where('comments.site_id', $args['site_id']);
		}

		if (isset($args['q']) && $args['q'] != '') {
			$query->leftJoin('users', 'users.user_id', '=', 'comments.user_id');
			$query->where(function ($query) use($args) {
				$query->orwhere('comments.comment_content', 'like', '%' . $args['q'] . '%');
				$query->orwhere('comments.name', 'like', '%' . $args['q'] . '%');
				$query->orwhere('comments.email', 'like', '%' . $args['q'] . '%');
				$query->orwhere('users.email', 'like', '%' . $args['q'] . '%');
				$query->orwhere('users.full_name', 'like', '%' . $args['q'] . '%');
			});
		}

		if (isset($args['per_page'])) {
			$comments = $query->addSelect('posts_langs.post_id', 'posts_langs.post_title', 'comments.comment_content', 'comments.comment_id', 'comments.site_id', 'comments.comment_date', 'statuses.status_name', 'statuses.status_id')->paginate($args['per_page']);
		}

		return $comments;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('comment_id', $id)
				->update($data);
		return $result;
	}

	public static function add($data) {
		$result = self::init()->insertGetId($data);
		return $result;
	}

	public static function change_status($conditions, $status_id) {
		$result = self::init()->where($conditions)
				->update(['comment_status' => $status_id]);
		return $result;
	}

	public static function hard_delete($conditions) {
		$result = self::init()->where($conditions)
				->delete();
		return $result;
	}

}
