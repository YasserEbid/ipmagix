<?php

namespace Mediasci\Cms\Models;

class MailMessages extends BaseModel {

    public $table = 'mail_messages';
    public $timestamps=false;
    protected $guarded=['id'];
    public $rules=[
        'subject'=>'required',
        'content'=>'required'
    ]; 
    
    function theme(){
        return $this->hasOne('Mediasci\Cms\Models\MailThemes','id', 'theme_id');
    }

	
}
