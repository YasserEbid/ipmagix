<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Models;

/**
 * Description of Categories
 *
 * @author Darsh
 */
class Categories extends CmsModel {

	public $table = 'categories';

	public static function get_attributes_names() {
		$names = array(
			'site_id' => trans('cms::categories.site_id'),
			'parent_id' => trans('cms::categories.parent_id'),
			'cat_slug'=>trans('cms::categories.cat_slug'),
			'banner_id'=>trans('cms::categories.banner_id')
		);

		return $names;
	}

	public static function get_all($args = []) {
		$query = self::init()->Join('categories_langs as c_langs', 'categories.cat_id', '=', 'c_langs.cat_id');
		$query->leftJoin('categories as pc', 'categories.parent_id', '=', 'pc.cat_id');
		$query->leftJoin('categories_langs as pc_langs', 'pc.cat_id', '=', 'pc_langs.cat_id');
		if (isset($args['parent_id'])) {
			$query->where('categories.parent_id', $args['parent_id'] ? $args['parent_id'] : null);
		}

		if (isset($args['q'])) {
			$query->where('c_langs.name', 'like', '%' . $args['q'] . '%');
		}

		if (isset($args['site_id'])) {
			$query->where('categories.site_id', $args['site_id']);
		}
		if (isset($args['country_id'])) {
			$query->where('categories.country_id', $args['country_id']);
		}

		$query->select(['categories.*', 'pc_langs.name as parent_name','c_langs.name']);
		$query->groupBy('categories.cat_id');
		$query->where('c_langs.lang',\Config::get("app.locale"));


		if (isset($args['per_page'])) {
			$categories = $query->paginate($args['per_page']);
		} else {
			$categories = $query->get();
		}

		return $categories;
	}

	public static function add($args) {
		$id = self::init()->insertGetId($args);

		return $id;
	}

	public static function modify($id, $args) {
		self::init()->where('cat_id', $id)->update($args);
	}

	public static function find($id) {
		$category = self::init()->where('cat_id', $id)->where("country_id", session('country'))->first();

		return $category;
	}

	public static function remove($id) {
		$result = self::init()->where('cat_id', $id)->delete();
		\DB::table('posts_categories')->where('cat_id', $id)->delete();

		return $result;
	}


}
