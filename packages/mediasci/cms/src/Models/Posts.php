<?php

namespace Mediasci\Cms\Models;

class Posts extends CmsModel {

    public $table = 'posts';

    public static function get_attributes_names() {

        $names = [
            'post_id' => trans('cms::posts.post_id'),
            'site_id' => trans('cms::posts.site_id'),
            'created_by' => trans('cms::posts.created_by'),
            'title' => trans('cms::posts.title'),
            'tags' => trans('cms::posts.tags'),
            'sub_title' => trans('cms::posts.sub_title'),
            'excerpt' => trans('cms::posts.excerpt'),
            'created_date' => trans('cms::posts.created_date'),
            'meta_title' => trans('cms::posts.meta_title'),
            'meta_keywords' => trans('cms::posts.meta_keywords'),
            'meta_excerpt' => trans('cms::posts.meta_excerpt'),
            'status' => trans('cms::posts.status'),
            'template_id' => trans('cms::posts.template_id'),
        ];

        return $names;
    }

    public static function get_statuses() {
        $statuses = [
            '1' => trans('cms::posts.published'),
            '2' => trans('cms::posts.draft'),
            '3' => trans('cms::posts.deleted'),
        ];
        return $statuses;
    }

    public static function find_by($conditions) {
        $query = self::init();

        $query->join('posts_langs', 'posts_langs.post_id', '=', 'posts.post_id');
        $result = $query->where($conditions)->first();
        return $result;
    }

    public static function find_post_categories($id, $keys = false) {
        $query = \DB::table('categories');

        $query->join('posts_categories', 'categories.cat_id', '=', 'posts_categories.cat_id');
        $query->where('posts_categories.post_id', $id);
        if ($keys) {
            $result = Array();
            $categories = $query->select('categories.cat_id')->get();
            foreach ($categories as $key => $cat) {
                $result[] = $cat->cat_id;
            }
        } else {
            $result = $query->get();
        }
        return $result;
    }

    public static function get_all($args = []) {
        $query = self::init();

        $query->join('posts_langs', 'posts_langs.post_id', '=', 'posts.post_id');
        $query->join('users', 'users.user_id', '=', 'posts.created_by');
        $query->leftJoin('posts_categories', 'posts_langs.post_id', '=', 'posts_categories.post_id');
        $query->join('categories', 'posts_categories.cat_id', '=', 'categories.cat_id');
        $query->join('categories_langs','categories.cat_id','=','categories_langs.cat_id');
        $query->orderBy('posts.created_date', 'desc');

        if (isset($args['url_type'])) {
            if ($args['url_type'] == 'news') {
              $query->where('categories.cat_slug', '=', $args['url_type']);
              // $query->where('categories_langs.name', '=', 'News');
            }else {
              $query->where('categories.cat_slug', '<>', 'news');
            }
        }
        if (isset($args['lang'])) {
            $query->where('posts_langs.lang', '=', $args['lang']);
        }
        if (isset($args['title']) && \Illuminate\Support\Facades\Input::get('sort') == 'title') {
            $query->orderBy('posts_langs.post_title', $args['lang']);
        }
        if (isset($args['q'])) {
            $query->where('posts_langs.post_title', 'like', '%' . $args['q'] . '%');
        }

        if (isset($args['site_id'])) {
            $query->where('posts.site_id', $args['site_id']);
        }

        if (isset($args['country_id'])) {
            $query->where('posts.country_id', $args['country_id']);
        }

        if (isset($args['per_page'])) {
            $posts = $query->paginate($args['per_page']);
        } else {
            $posts = $query->get();
        }

        return $posts;
    }

    public static function modify($data, $id) {
        $categories = [];
        if (array_key_exists('categories', $data)) {
          $categories = $data['categories'];
          unset($data['categories']);
        }

        self::save_post_categories($categories, $id);
        // dd($categories);

        if (isset($data['slug'])) {
            self::save_post_slug($data['slug'], $id);
            unset($data['slug']);
        }
        $result = self::init()->where('post_id', $id)->update($data);

        return $result;
    }

    public static function modify_post_lang($data, $args) {
        $result = \DB::table('posts_langs')->where($args)->update($data);
        if (isset($data['tags'])) {
            $data['id'] = $args['id'];
            $data['post_id'] = $args['post_id'];
            self::save_post_tags($data);
        }

        return $result;
    }

    public static function add($data) {
        $categories = [];

        if (array_key_exists('categories', $data)) {
            $categories = $data['categories'];
            unset($data['categories']);
        }
        $id = self::init()->insertGetId($data);

        self::save_post_categories($categories, $id);

        if (isset($data['slug'])) {
            self::save_post_slug($data['slug'], $id);
            unset($data['slug']);
        }

        return $id;
    }

    public static function add_post_lang($data) {
        $id = \DB::table('posts_langs')->insertGetId($data);
        if ($id && isset($data['tags'])) {
            $data['id'] = $id;
            self::save_post_tags($data);
        }

        return $id;
    }

    public static function save_post_tags($data) {
        $where = ['post_id' => $data['post_id'], 'post_lang_id' => $data['id']];
        \DB::table('posts_tags')->where($where)->delete();

        $posts_tags = Array();
        $tags = array_filter(array_unique(array_map('trim', explode(',', $data['tags']))));
        foreach ($tags as $key => $tag_name) {
            $tag = \DB::table('tags')->where('tag_name', $tag_name)->first();
            if ($tag) {
                $tag_id = $tag->tag_id;
                $where = ['tag_id' => $tag_id];
                $tag_data = ['count' => (int) $tag->count + 1];
                \DB::table('tags')->where($where)->update($tag_data);
            } else {
                $tag_data = ['count' => 1,
                    'tag_name' => $tag_name,
                ];
                $tag_id = \DB::table('tags')->insertGetId($tag_data);
            }
            $posts_tags[] = Array('post_id' => $data['post_id'],
                'post_lang_id' => $data['id'],
                'tag_id' => $tag_id,
            );
        }
        \DB::table('posts_tags')->insert($posts_tags);
    }

    public static function save_post_categories($categories, $id) {
      // dd([$categories, $id]);
        $query = \DB::table('posts_categories')->where('post_id', $id)->get();
        $old_categories = json_decode(json_encode($query), TRUE);

        \DB::table('posts_categories')->where('post_id', $id)->delete();
        if ($categories) {
            $categories = \DB::table('categories')->whereIn('cat_id', $categories)->get();
            $post_cats = array();
            foreach ($categories as $key => $cat) {
                $post_cat = array('post_id' => $id, 'cat_id' => $cat->cat_id);
                // get old sort
                $key = array_search($cat->cat_id, array_column($old_categories, 'cat_id'));
                if ($key !== false) {
                    $post_cat['sort'] = $old_categories[$key]['sort'];
                }

                $post_cats[] = $post_cat;
            }
            \DB::table('posts_categories')->insert($post_cats);
        }
    }

    public static function save_post_slug($slug, $id) {
        $slug = str_replace(' ', '_', $slug);
        $post = self::find_by(['slug' => $slug]);
        if ($post && $post->post_id != $id) {
            $last_c = substr($slug, -1);
            if (substr($slug, -1) > 0) {
                $slug = substr($slug, 0, -1) . ++$last_c;
            }
            $slug = $slug . '_1';
        }
        \DB::table('posts')->where(['post_id' => $id])->update(['slug' => $slug]);

        return $slug;
    }

    public static function remove($conditions) {
        \DB::table('posts_categories')->where($conditions)->delete();
        \DB::table('posts_langs')->where($conditions)->delete();
        $result = self::init()->where($conditions)->delete();

        return $result;
    }


}
