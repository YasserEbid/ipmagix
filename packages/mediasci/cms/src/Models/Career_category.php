<?php
namespace Mediasci\Cms\Models;

class Career_category extends BaseModel {
    protected $table='careers_categories';
    protected $fillable=['media_id','country_id'];
      public function media(){
       return $this->hasOne('Mediasci\Cms\Models\Media','media_id','media_id') ;
    }
    public function career(){
        return $this->hasMany('Mediasci\Cms\Models\Career', 'cat_id');

    }
    public function catlang($lang){
        return $this->hasMany('Mediasci\Cms\Models\Careers_category_lang','cat_id')->where('lang',$lang)->first();
    }
}
