<?php

namespace Mediasci\Cms\Models;
use DB;

class Cities extends BaseModel {
	
	protected $table='cities';
	
	public static function get_attributes_names() {

		$names = [
			'city_id' => trans('cms::cities.city_id'),
			'name' => trans('cms::cities.title'),
			'created_at' => trans('cms::cities.created_at'),
			'active' => trans('cms::cities.active'),
		];

		return $names;
	}
	
	public function cityLang($e){
		return $this->hasMany('Mediasci\Cms\Models\CitiesLangs','city_id')->where('lang',$e)->first();
		//return $this->hasMany('Mediasci\Cms\Models\ServicesLangs','service_id');
	}
	
	public static function modify_city_lang($data, $args) {
		$result = \DB::table('cities_langs')->where($args)->update($data);
		return $result;
	}
	
	public static function get_active_cities(){
		return DB::table('cities')
			->join('cities_langs', 'cities.id', '=', 'cities_langs.city_id')
			->select('cities.*', 'cities_langs.name')
			->where('active','1')
			->where('lang',\Config::get("app.locale"))
			->orderBy('cities.id', 'asc')
		->get();
	}
}
