<?php
namespace Mediasci\Cms\Models;

class Slider_lang extends BaseModel {
    protected $table='slider_langs';
    protected $fillable=['title','button_name','description','slider_id','lang'];
    public function slider(){
        return $this->belongsTo('Mediasci\Cms\Models\Slider','slider_id');
    }
}
