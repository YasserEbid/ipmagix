<?php

namespace Mediasci\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends BaseModel {

    protected $table = 'countries';
    public $timestamps = false;
    protected $primaryKey = 'id';

    function Lang() {
        return $this->hasOne('Mediasci\Cms\Models\Countrylang', 'country_id', 'id');
    }

}
