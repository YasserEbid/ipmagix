<?php

namespace Mediasci\Cms\Models;
use DB;

class FAQCategories extends BaseModel {
	
	protected $table='faq_categories';
	
	public static function get_attributes_names() {

		$names = [
			'active' => trans('cms::faqCategories.active'),
			'created_at' => trans('cms::faqCategories.created_at'),
			'updated_at'=> trans('cms::faqCategories.updated_at'),
		];

		return $names;
	}
	
	public function faqCategoryLang($e){
		return $this->hasMany('Mediasci\Cms\Models\FAQCategoriesLangs','category_id')->where('lang',$e)->first();
	}
	
	public static function modify_faq_categories_lang($data, $args) {
		$result = \DB::table('faq_categories_langs')->where($args)->update($data);
		return $result;
	}
}
