<?php

namespace Mediasci\Cms\Models;
use Illuminate\Database\Eloquent\Model as Eloquent, DB;

class Galleries_lang extends CmsModel
{
        protected $table = "galleries_langs";
        
           public function scopeLang($query, $lang) {
        $query->where("lang", $lang);
        return $query;
        
    }

}