<?php
namespace Mediasci\Cms\Models;

class Careers_category_lang extends BaseModel {
    protected $table='careers_category_langs';
    protected $fillable=['title','lang','cat_id'];
    public function careercat(){
        return $this->belongsTo('Mediasci\Cms\Models\Career_category','cat_id');
    }
}