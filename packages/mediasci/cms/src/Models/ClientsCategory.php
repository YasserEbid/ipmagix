<?php
namespace Mediasci\Cms\Models;


class ClientsCategory extends BaseModel
{
protected $table='clientscategory';
  public function clients(){
     return $this->hasMany('Mediasci\Cms\Models\Client','category_id','id');
  }
  public function clientCategoryLangs(){
		 return $this->hasMany('Mediasci\Cms\Models\ClientsCategoryLangs','clientCategory_id','id');
	}
}
