<?php

namespace Mediasci\Cms\Models;

class Career_lang extends BaseModel {
    protected $table='career_langs';
    protected $fillable=['title','summery','description','career_id','lang','location'];
    public function career(){
        return $this->belongsTo('Mediasci\Cms\Models\Career','career_id');
    }
}
