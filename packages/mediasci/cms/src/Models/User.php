<?php

namespace Mediasci\Cms\Models;

class User extends CmsModel {

	public $table = 'users';

	public static function get_attributes_names() {
		$names = array(
			'full_name' => trans('cms::users.full_name'),
			'email' => trans('cms::users.email'),
			'password' => trans('cms::users.password'),
			'role_id' => trans('cms::users.role_id'),
			'mobile' => trans('cms::users.mobile'),
		);

		return $names;
	}

	public static function login($email, $password) {
		$conditions = [
			'users.email' => $email,
			'users.password' => md5($password),
			'roles.access_dashboard' => '1'
		];

		$user = self::find_by($conditions);

		if ($user) {
			\Session::put('admin_user_id', $user->user_id);
		}

		return $user;
	}

	public static function can_access($route_name, $site_id) {
		$check = self::init()->join('roles', 'users.role_id', '=', 'roles.role_id')
				->join('roles_routes', 'roles_routes.role_id', '=', 'roles.role_id')
				->join('routes', 'roles_routes.route_id', '=', 'routes.route_id')
				->where('routes.route_name', $route_name)
				->where('users.user_id', \Session::get('admin_user_id'))
				->where(function ($query) use($site_id) {
					$query->where('roles_routes.site_id', $site_id);
					$query->orWhereNull('roles_routes.site_id');
				})
				->where('roles_routes.can_access', '1')
				->exists();

		return $check;
	}

	public static function find_by($conditions) {
		$result = self::init()->join('roles', 'users.role_id', '=', 'roles.role_id')
						->where($conditions)->select(['users.*', 'roles.role_name'])->first();

		return $result;
	}

	public static function get_all($args = []) {
		$query = self::init();
		
		if (isset($args['q'])) {
			$query->where('full_name', 'like', '%' . $args['q'] . '%');
		}
		
		if (isset($args['per_page'])) {
			$users = $query->paginate($args['per_page']);
		} else {
			$users = $query->get();
		}
		
		return $users;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('user_id', $id)
				->update($data);
		return $result;
	}

	public static function add($data) {
		$result = self::init()->insert($data);
		return $result;
	}

	public static function remove($conditions) {
		self::init()->where($conditions)->delete();
	}

}
