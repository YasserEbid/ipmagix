<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Models;

/**
 * Description of Sites
 *
 * @author Darsh
 */
class Sites extends CmsModel {

	public $table = 'sites';

	public static function get_all(){
		$sites = self::init()->where('status', '1')->get();

		return $sites;
	}
}
