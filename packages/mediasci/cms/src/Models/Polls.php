<?php

namespace Mediasci\Cms\Models;
use DB;

class Polls extends BaseModel {

    public $table = 'polls';
    public $timestamps=false;
    protected $guarded=['id'];

    function lang(){
        return $this->hasMany('Mediasci\Cms\Models\Pollslang','poll_id');
    }

    function options(){
        return $this->hasMany('Mediasci\Cms\Models\PollsOptions','poll_id');
    }

    function optionsConcat($lang){
        return DB::table('polls_options_lang')->select(DB::raw("GROUP_CONCAT(`option`) as options" ))
                            ->join('polls_options','polls_options.id','=','polls_options_lang.option_id')
                            ->join('polls','polls.id','=','polls_options.poll_id')
                            ->where('polls.id',$this->id)->where('polls_options_lang.lang',$lang)->get()[0]->options;
    }
    function optionsConcatEn(){
        return DB::table('polls_options_lang')->select(DB::raw("GROUP_CONCAT('option') as options" ))
                            ->join('polls_options','polls_options.id','=','polls_options_lang.option_id')
                            ->join('polls','polls.id','=','polls_options.poll_id')
                            ->where('polls.id',$this->id)->where('lang','en')->get()[0]->options;
    }




}
