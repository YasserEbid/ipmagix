<?php

namespace Mediasci\Cms\Models;
use DB;

class QuestionsAnswers extends BaseModel {
	
	protected $table='questions_answers';
	
	public static function get_attributes_names() {

		$names = [
			'city_id' => trans('cms::faq.city_id'),
			'question' => trans('cms::faq.question'),
			'answer'=> trans('cms::faq.answer'),
			'created_at' => trans('cms::faq.created_at'),
			'active' => trans('cms::faq.active'),
			'updated_at'=> trans('cms::faq.updated_at'),
		];

		return $names;
	}
	
	public function questionAnswerLang($e){
		return $this->hasMany('Mediasci\Cms\Models\QuestionsAnswersLangs','question_answer_id')->where('lang',$e)->first();
	}
	
	public static function modify_question_answer_lang($data, $args) {
		$result = \DB::table('questions_answers_langs')->where($args)->update($data);
		return $result;
	}
}
