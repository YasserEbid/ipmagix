<?php


namespace Mediasci\Cms\Models;

class Specialisty extends BaseModel {
    protected $table='specialisty';
    public function doctorlang(){
        return $this->hasMany('Mediasci\Cms\Models\Doctor_lang');
        
    }
    public static function special($lang) {
   return self::where('lang',$lang)->get();
    }
}

