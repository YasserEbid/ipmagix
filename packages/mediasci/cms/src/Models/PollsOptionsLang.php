<?php

namespace Mediasci\Cms\Models;
use Mediasci\Cms\Models\Traits\Languages;

class PollsOptionslang extends BaseModel {
    use Languages;
    
    public $table = 'polls_options_lang';
    public $timestamps=false;
    protected $guarded=['id'];
    
}
