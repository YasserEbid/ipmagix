<?php

namespace Mediasci\Cms\Models;

use Mediasci\Cms\Models\Routes;
use Mediasci\Cms\Models\RolesRoutes;

class Groups extends CmsModel {

	public $table = 'routes_groups';

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();
		return $result;
	}

	public static function get_all() {
		$result = self::init()->get();
		return $result;
	}

	public static function get_all_herically($id = 0) {
		if ($id > 0) {
			$routes_access = RolesRoutes::role_access($id);
			$accessable = array();
			foreach ($routes_access as $access) {
				$accessable[] = $access->route_id;
			}
			$results = self::init()->get();
			$data = array();
			foreach ($results as $result) {
				$obj = new \stdClass();
				$obj->parent_name = $result->group_name;
				$routes = Routes::get_by_group_id($result->group_id);
				$route_arr = array();
				foreach ($routes as $route) {
					$route_obj = new \stdClass();
					$route_obj->id = $route->route_id;
					$route_obj->name = str_replace(".", "_", $route->route_name);
					;
					$route_obj->naming = $route->conventional_name;
					if (in_array($route->route_id, $accessable)) {
						$route_obj->checked = 'checked';
					} else {
						$route_obj->checked = '';
					}
					$route_arr[] = $route_obj;
				}
				$obj->child = $route_arr;
				$data[] = $obj;
			}
			return $data;
		} else {
			$results = self::init()->get();
			$data = array();
			foreach ($results as $result) {
				$obj = new \stdClass();
				$obj->parent_name = $result->group_name;
				$routes = Routes::get_by_group_id($result->group_id);
				$route_arr = array();
				foreach ($routes as $route) {
					$route_obj = new \stdClass();
					$route_obj->id = $route->route_id;
					$route_obj->name = str_replace(".", "_", $route->route_name);
					;
					$route_obj->naming = $route->conventional_name;
					$route_obj->checked = '';
					$route_arr[] = $route_obj;
				}
				$obj->child = $route_arr;
				$data[] = $obj;
			}
			return $data;
		}
	}

	public static function modify($data, $id) {
		$result = self::init()->where('group_id', $id)
				->update($data);

		return $result;
	}

	public static function add($data) {
		$result = self::init()->insert($data);

		return $result;
	}

}
