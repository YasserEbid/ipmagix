<?php

namespace Mediasci\Cms\Models;
use DB;

class FAQCategoriesLangs extends BaseModel {

	protected $table='faq_categories_langs';
	
	public function faqCategory(){
		return $this->belongsTo('Mediasci\Cms\Models\FAQCategories','category_id');
	}
}
