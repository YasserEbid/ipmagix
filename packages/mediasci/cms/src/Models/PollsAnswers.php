<?php

namespace Mediasci\Cms\Models;

class PollsAnswers extends BaseModel {

    public $table = 'polls_answers';
    public $timestamps=false;
    protected $guarded=['id'];
    
}
