<?php

namespace Mediasci\Cms\Models;

use Mediasci\Cms\Models\RolesRoutes;

class Roles extends CmsModel {

	public $table = 'roles';

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function get_all() {
		$result = self::init()->get();
		return $result;
	}

	public static function modify($data, $id) {
		$result = self::init()->where('role_id', $id)
				->update($data);
		return $result;
	}

	public static function add($data) {
		$id = self::init()->insertGetId($data);
		return $id;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();
		$result = RolesRoutes::remove($conditions);
		return $result;
	}

}
