<?php

namespace Mediasci\Cms\Models;

class Menu extends BaseModel {
    protected $table='menus';
    protected $fillable=['active'];
    function menulang($lang){
        return $this->hasMany('Mediasci\Cms\Models\Menulang','menu_id')->where('lang',$lang)->first();
    }
    function menulinks(){
        return $this->hasMany('Mediasci\Cms\Models\Menulink', 'menu_id');
    }
   
}
