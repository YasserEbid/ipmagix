<?php

namespace Mediasci\Cms\Models\Traits;

/**
 * Description of lang
 *
 * @author Elsayed Nofal
 */
trait Media {
    
    function media(){
        return $this->belongsTo('Mediasci\Cms\Models\Media','media_id');
    }

}
