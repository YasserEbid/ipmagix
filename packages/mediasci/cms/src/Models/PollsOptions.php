<?php

namespace Mediasci\Cms\Models;

class PollsOptions extends BaseModel {

    public $table = 'polls_options';
    public $timestamps=false;
    protected $guarded=['id'];

    function lang(){
        return $this->hasMany('Mediasci\Cms\Models\PollsOptionsLang','option_id');
    }
}
