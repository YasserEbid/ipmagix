<?php

namespace Mediasci\Cms\Models;

class RolesRoutes extends CmsModel {

	public $table = 'roles_routes';

	public static function find_by($conditions) {
		$result = self::init()->where($conditions)->first();
		return $result;
	}

	public static function role_access($id) {
		$result = self::init()->where('role_id', $id)->get();
		return $result;
	}

	public static function addRoles($data, $id) {
		self::init()->where('role_id', $id)->delete();
		$routeCollection = \Route::getRoutes();

		foreach ($routeCollection as $value) {

			$check_name = str_replace(".", "_", $value->getName());

			if (isset($data[$check_name]) && $data[$check_name]) {
				$result = self::init()->insert([
					['role_id' => $id,
						'route_id' => $data[$check_name],
						'can_access' => 1]
				]);
			}
		}
		return $result;
	}

	public static function remove($conditions) {
		$result = self::init()->where($conditions)->delete();
		return $result;
	}

}
