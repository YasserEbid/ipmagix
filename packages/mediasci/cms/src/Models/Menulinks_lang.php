<?php
namespace Mediasci\Cms\Models;

class Menulinks_lang extends BaseModel {
    protected $table='menu_links_langs';
    
       protected $fillable=['menu_link_id','name','lang'];
       function menulink(){
           return $this->belongsTo('Mediasci\Cms\Models\Menulink','menu_link_id');
       }
       
}
