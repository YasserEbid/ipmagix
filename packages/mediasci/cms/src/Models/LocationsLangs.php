<?php

namespace Mediasci\Cms\Models;

class LocationsLangs extends BaseModel {
    protected $table='site_locations_lang';
    protected $fillable=['office','discription','location_id','lang'];
    public function location(){
        return $this->belongsTo('Mediasci\Cms\Models\SiteLocations','location_id');
    }
}
