<?php
namespace Mediasci\Cms\Models;

class PostsTabslangs extends BaseModel {
      protected $table='posts_tabs_langs';
      protected $fillable=['title','description','lang','post_tab_id'];
              function tab(){
          return $this->belongsTo('Mediasci\Cms\Models\PostsTabs', 'post_tab_id');
      }
}