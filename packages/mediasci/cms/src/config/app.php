<?php

return [
	//admin url prefix
	"ADMIN" => "admin",
    'sizes' => array(
        'large' => array(1000, 1000),
        'medium' => array(500, 500),
        'small' => array(250, 250),
        'thumbnail' => array(150, 150)
    ),

    // Languages
    'locales' => array('en', 'ar'),
    "domain"=>'http://localhost/cms/public/',
    'locale' => 'en',
];
