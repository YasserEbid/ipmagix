<?php

namespace Mediasci\Cms\Http\Middleware;

use Closure;

class Backend {

	/**
	 * Run the request filter.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		// handle app cache flush
		$this->app_cache();

		return $this->admin_auth($request, $next);
	}

	public function admin_auth($request, Closure $next)
	{
		if (\Session::has('admin_user_id')) {
			$route_name = \Request::route()->getName();

			$user = \User::find_by(['users.user_id' => \Session::get('admin_user_id')]);

			$site_id = $request->input('site_id');
			
			if (!$site_id && !\Routes::is_general($route_name)) {
				$site_id = '1';
				$request->merge(['site_id' => $site_id]);
			}

			// super admin
			$super_admin = $user->user_id == 1;
			if ($super_admin || \User::can_access($route_name, $site_id) && $user) {
				\CmsController::set_user($user);
				return $next($request);
			} else {
				return response()->view('cms::errors.403');
			}
		}
		return redirect()->route('auth.login');
	}

	public function app_cache()
	{
		$route_name = explode('.', \Request::route()->getName());
		$controller = reset($route_name);
		$action = end($route_name);
		if ($controller && $action) {
			$appcache_names = \Config::get('appcache.names');
			$cache_names = [];
			foreach ($appcache_names as $key => $names) {
				if ($controller == $key) {
					if (\Request::isMethod('post') && in_array($action, ['create','edit','update'])) {
						$cache_names = explode(',', $names);
					} else {
						if (in_array($action, ['delete'])) {
							$cache_names = explode(',', $names);
						}
					}
				}
			}
			if (count($cache_names)) {
				foreach ($cache_names as $cache_name) {
					\Cache::forget($cache_name);
				}
			}
		}
	}

}
