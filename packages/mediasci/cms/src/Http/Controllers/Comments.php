<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

/**
 * Description of Groups
 *
 * @author Mahmoud
 */
class Comments extends \CmsController {

	public $data = [];

	private function filter() {
		$args = [
			'post_id' => $this->request->input('post_id') ? $this->request->input('post_id') : 0,
			'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
			'status' => $this->request->input('status') ? $this->request->input('status') : 0,
			'site_id' => $this->request->input('site_id') ? $this->request->input('site_id') : 0,
			'q' => $this->request->input('q') ? $this->request->input('q') : '',
		];

		$this->data['comments'] = \Comments::get_all($args);
	}

	public function index() {
		$this->filter();

		$this->data['sites'] = \Sites::get_all();

		return view('cms::comments.index', $this->data);
	}

	public function create() {
		if ($this->request->isMethod('post')) {

			$rules = [ 'email' => 'required|email',
				'full_name' => 'required',
				'post_id' => 'required',
				'content' => 'required'
			];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Comments::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->route('comments.create')->withErrors($validator)->withInput();
			}

			$data = ['post_id' => $this->request->input('post_id'),
				'email' => $this->request->input('email'),
				'name' => $this->request->input('full_name'),
				'comment_content' => $this->request->input('content'),
				'site_id' => $this->request->input('site_id'),
				'comment_status' => 2
			];
			$id = \Comments::add($data);

			return redirect()->route('comments.edit', ['id' => $id, 'post_id' => $this->request->input('post_id')])->with('message', trans('cms::comments.created'));
		}

		return view('cms::comments.create', ['current_comment' => null]);
	}

	public function edit($id) {
		if ($this->request->isMethod('post')) {

			$rules = [
				'email' => 'required|email',
				'post_id' => 'required',
				'full_name' => 'required',
				'content' => 'required'
			];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Comments::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			$data = [
				'post_id' => $this->request->input('post_id'),
				'email' => $this->request->input('email'),
				'name' => $this->request->input('full_name'),
				'comment_content' => $this->request->input('content'),
				'edited' => 1
			];

			\Comments::modify($data, $id);

			return redirect()->route('comments.edit', ['id' => $id])->with('message', trans('cms::users.updated'));
		}

		$conditions = ['comment_id' => $id];
		$current_comment = \Comments::find_by($conditions);
		
		return view('cms::comments.create', ['current_comment' => $current_comment]);
	}

	public function soft_delete($id) {
		$conditions = ['comment_id' => $id,];
		
		\Comments::change_status($conditions, 4);
		
		return redirect()->back()->with('message', trans('cms::comments.deleted'));
	}

	public function spam($id) {
		$conditions = ['comment_id' => $id];
		
		\Comments::change_status($conditions,3);
		
		return redirect()->back()->with('message', trans('cms::comments.success_spam'));
	}

	public function publish($id) {
		$conditions = ['comment_id' => $id];
		
		\Comments::change_status($conditions, 1);
		
		return redirect()->back()->with('message', trans('cms::comments.publish'));
	}

	public function hard_delete($id) {
		$conditions = ['comment_id' => $id,];
		
		\Comments::hard_delete($conditions, 4);
		
		return redirect()->back()->with('message', trans('cms::comments.deleted'));
	}

}
