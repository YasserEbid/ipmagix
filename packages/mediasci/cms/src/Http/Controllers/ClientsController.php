<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Mediasci\Cms\Models\Client;
use Mediasci\Cms\Models\ClientsCategory;
use Mediasci\Cms\Models\ClientsLangs;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

/**
 * Description of Groups
 *
 * @author abeer elhout
 */
class ClientsController extends CmsController {

    /** get all clients * */
    public function getIndex() {
        $current_lang = \Config::get("app.locale");
        $clients = new Client;
        if (isset($_GET['sort'])) {
            $clients = $clients::orderBy($_GET['sort'], $_GET['dir']);
        }
        $clients = $clients->join("clients_langs", "clients.id", "=", "clients_langs.client_id")->where("clients_langs.lang", $current_lang)
                ->select("clients.id", "clients.media_id", "clients.category_id", "clients_langs.name")
                ->where('clients.country_id', session('country'))
                ->paginate(8);
        return view('cms::clients.index')->withclients($clients);
    }

    /*     * ************ create new client && Save ********************* */

    public function getCreate() {
        if ($this->request->isMethod('post')) {
            $client = new Client;
            $client->country_id = session('country');
            $client->media_id = Input::get('featured_image');
            $client->category_id = Input::get('category_id');
            $client->Save();
            foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                $client_lang = new ClientsLangs;
                $client_lang->name = Input::get('name_' . $lang->lang);
                $client_lang->brief = Input::get('brief_' . $lang->lang);
                $client_lang->description = Input::get('description_' . $lang->lang);
                $client_lang->lang = $lang->lang;
                $client_lang->client_id = $client->id;
                $client_lang->Save();
            }
            session()->push('client_created', "New Client has been added seccessfully.");
            return redirect('admin/clients');
        } else {
            $data['categories'] = ClientsCategory::
                            join("clientscategory_langs", "clientscategory.id", "=", "clientscategory_langs.clientcategory_id")
                            ->where("clientscategory_langs.lang", \Config::get("app.locale"))
                            ->where('clientscategory.country_id', session('country'))->get();
            return view('cms::clients.create', $data);
        }
    }

    /*     * ************ delete client ********************* */

    public function destroy($id) {
        $client = Client::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($client) && $client->count() > 0) {
            $client->delete();
        }
        return redirect('admin/clients');
    }

    /*     * *************** update client ************************** */

    public function update($id, Request $request) {
        if ($this->request->isMethod('post')) {
            // dd($_POST);
            $client = Client::find($id);
            $input = $request->all();
            $client->category_id = $input['category_id'];
            if (!empty($input['featured_image'])) {
                $client->media_id = $input['featured_image'];
            }
            $client->Save();
            foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                $client_lang = ClientsLangs::where('client_id', $client->id)->where('lang', $lang->lang)->first();
                if (is_object($client_lang) && $client_lang->count() > 0) {
                    $client_lang->name = Input::get('name_' . $lang->lang);
                    $client_lang->brief = Input::get('brief_' . $lang->lang);
                    $client_lang->description = Input::get('description_' . $lang->lang);
                    $client_lang->Save();
                } else {
                    $client_lang = new ClientsLangs();
                    $client_lang->name = Input::get('name_' . $lang->lang);
                    $client_lang->client_id = $client->id;
                    $client_lang->brief = Input::get('brief_' . $lang->lang);
                    $client_lang->description = Input::get('description_' . $lang->lang);
                    $client_lang->Save();
                }
            }
            return redirect('admin/clients');
        } else {
            $data['client'] = $client = Client::where("id", $id)->where('country_id', session('country'))->first();
            if (is_object($client) && $client->count() > 0) {
                $data['client_lang'] = ClientsLangs::where('client_id', $data['client']->id)->get();
                $data['categories'] = ClientsCategory::
                                join("clientscategory_langs", "clientscategory.id", "=", "clientscategory_langs.clientcategory_id")
                                ->where("clientscategory_langs.lang", \Config::get("app.locale"))->where('clientscategory.country_id', session('country'))->get();
                $data['client_banner'] = \Media::find($client->media_id);
                return view('cms::clients.edit', $data);
            } else {
                return redirect('admin/clients');
            }
        }
    }

////////// end of class
}

?>
