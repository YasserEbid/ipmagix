<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use DB;

/**
 * Description of Groups
 *
 * @author Elsayed Nofal
 */
class WorkflowController extends CmsController {

    function index(){
        return view('cms::workflow.index')
                ->with('workflows', \Mediasci\Cms\Models\Workflow::all());
    }

    function create(){
        if(Input::has('title')){
            try{
                DB::beginTransaction();
                $workflow=  new \Mediasci\Cms\Models\Workflow();
                $workflow->fill(Input::all());
                unset($workflow->level);
                $workflow->save();
              //  $level=Input::get('level');
              //  dd($level["'name'"]);
                foreach(Input::get('level')["'name'"] as $key=>$value){
                    $wf_level=new \Mediasci\Cms\Models\WorkflowLevels();
                    $wf_level->name=$value;
                    $wf_level->url=Input::get('level')["'url'"][$key];
                    $wf_level->workflow_id=$workflow->id;
                    $wf_level->save();
                }
                DB::commit();
                \Illuminate\Support\Facades\Session::flash('message','one row inseted');
                return redirect()->route('workflow');
            }catch(\Exception $e){
                DB::rollBack();
                // dd($e);
            }
        }
        return view('cms::workflow.create');
    }

    function delete($id){
        if(is_object($workflow=  \Mediasci\Cms\Models\Workflow::find($id))){
            $workflow->delete();
            \Illuminate\Support\Facades\Session::flash('message','one row deleted');
        }else{
            \Illuminate\Support\Facades\Session::flsah('error','can not delete row with id=#'.$id);
        }
        return redirect()->route('workflow');
    }

    function update($id){
        $workflow=  \Mediasci\Cms\Models\Workflow::find($id);
        if(Input::has('title')){
            DB::beginTransaction();
            try{
                //saving new inputs to workflow
                $workflow->fill(Input::all());
                unset($workflow->level);
                $workflow->save();
                //delete old levels
                DB::table('workflow_levels')->where('workflow_id',$id)->delete();
                //inserts new levels
                foreach(Input::get('level')["'name'"] as $key=>$value){
                    $wf_level=new \Mediasci\Cms\Models\WorkflowLevels();
                    $wf_level->name=$value;
                    $wf_level->url=Input::get('level')["'url'"][$key];
                    $wf_level->workflow_id=$workflow->id;
                    $wf_level->save();
                }
                //commit all pervious queries
                DB::commit();
                \Illuminate\Support\Facades\Session::flash('message','one row updated');
                return redirect()->route('workflow');
            }catch(\Exception $e){
                // roll back all changes and dump errors
                DB::rollBack();
                // dd($e);
                \Illuminate\Support\Facades\Session::flash('error','somthing went wrong');
            }
        }
        $data['workflow']=$workflow;
        return view('cms::workflow.update',$data);
    }

    function show($id){
        return view('cms::workflow.show')->with('workflow', \Mediasci\Cms\Models\Workflow::find($id));
    }

}
