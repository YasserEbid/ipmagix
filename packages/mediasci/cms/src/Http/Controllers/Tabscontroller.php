<?php

namespace Mediasci\Cms\Http\Controllers;

use DB;
use Mediasci\Cms\Models\PostsTabs;
use Mediasci\Cms\Models\PostsTabslangs;
use Illuminate\Support\Facades\Input;
class Tabscontroller extends \CmsController {

    public function show($id) {
        $data['post'] = DB::table('posts')->where('post_id', $id)->first();
        $data['posts_langs'] = DB::table('posts_langs')->where('post_id', $id)->first();

        if (!isset($data['post']) || !isset($data['posts_langs'])) {
            return redirect('admin/posts');
        }

        $data['tabs'] = PostsTabs::where('post_id', $data['post']->post_id)->paginate(6);

        return view('cms::tabs.index', $data);
    }

    public function anyCreate($id) {
        $data['post'] = DB::table('posts')->where('post_id', $id)->first();
        $data['id'] = $data['post']->post_id;
        if (Input::get('en_title')) {
// dd(Input::all());
            DB::transaction(function () {
                $tab = PostsTabs::create(['slug'=> Input::get('slug'),
                    'post_id'=> Input::get('post_id')]);
                foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
                  $tab_lang = PostsTabslangs::create(['title'=>Input::get($lang->lang.'_title'),
                      'description'=>Input::get($lang->lang.'_desc'),
                      'lang'=>$lang->lang,
                      'post_tab_id'=>$tab->id]);
                    }
               //
              //  $entab = PostsTabslangs::create(['title'=>Input::get('en_title'),
              //       'description'=>Input::get('en_desc'),
              //       'lang'=>'en',
              //       'post_tab_id'=>$tab->id]);

           });
           return redirect('admin/posts/tabs/'.$id);
        }
        return view('cms::tabs.create', $data);
    }
     public function anyUpdate($id) {
        $data['tab']= PostsTabs::find($id);

        if ($this->request->isMethod('post')) {
        $tab= PostsTabs::find($id);
        $post_id=$tab->post_id;
        foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
          $tab_lang= PostsTabslangs::where('post_tab_id',$tab->id)->where('lang',$lang->lang)->first();
          $tab_lang->title = Input::get($lang->lang.'_title');
          $tab_lang->description = Input::get($lang->lang.'_desc');
          $tab_lang->save();
            }
        // $artab= PostsTabslangs::where('post_tab_id',$tab->id)->where('lang','ar')->first();
        // $entab= PostsTabslangs::where('post_tab_id',$tab->id)->where('lang','en')->first();
        $tab->slug=Input::get('slug');
        // $artab->title = Input::get('ar_title');
        // $artab->description = Input::get('ar_desc');
        // $entab->title = Input::get('en_title');
        // $entab->description = Input::get('en_desc');
        $tab->save();
        // $artab->save();
        // $entab->save();

        return redirect('admin/posts/tabs/'.$post_id);

        }
        return view('cms::tabs.update', $data);
    }
    public function anyDelete($id){
         $tab= PostsTabs::find($id);
         $post_id=$tab->post_id;
         $tabs= PostsTabslangs::where('post_tab_id',$tab->id)->delete();
         $tab->delete();
         return redirect('admin/posts/tabs/'.$post_id);
    }
}
