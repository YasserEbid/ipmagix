<?php

namespace Mediasci\Cms\Http\Controllers\component;


use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Input;

class AjaxController extends Controller {

	function anyUpload() {

		$rules = ['file' => 'required|image'];
		//dd($_FILES);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			$data['status'] = 'error';
			$data['data'] = $validator->errors()->all();
		} else {
			$destinationPath = 'uploads/temp'; // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
			$fileName = random_int(1, 5000) * microtime() . '.' . $extension; // renameing image
			Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
			//$thumb=  Helpers::createThumb('./'.$destinationPath, $fileName, './uploads/thumbs');
			$data['status'] = 'ok';
			$data['data'] = './' . $destinationPath . '/' . $fileName;
			$data['file'] = $fileName;
		}
		return json_encode($data);
	}

	function anyEditorupload() {
		$rules = ['file' => 'required|image'];
		//dd($_FILES);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			$response['status'] = 'error';
			$response['data'] = $validator->errors()->all();
		} else {
			$destinationPath = 'uploads/editor'; // upload path
			$extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
			$fileName = random_int(1, 9) * time() . '.' . $extension; // renameing image
			Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
			$thumb= Helpers::makeThumb('./'.$destinationPath.'/'.$fileName, './'.$destinationPath.'/thumbs/'.$fileName,200);
			// Generate response.
			$response = new \StdClass;
			$response->link = './' . $destinationPath . '/' . $fileName;
		}
		return json_encode($response);
	}

	function anyRemoveimage() {
		if (isset($_POST['src'])) {
			if (strstr($_POST['src'], '/uploads/')) {
				unlink(getcwd() . $_POST['src']);
				unlink(getcwd() . str_replace('editor/', 'editor/thumbs/', $_POST['src']));
			}
		}
	}
        
    function anyBrowse(){
        $directory = "./uploads/editor/";
        $images = glob($directory.'*');
        foreach($images as $image)
        {
            
            if(@is_array(getimagesize($image))){
                $image_properties['url']=$image;
                $image_properties['thumb']=  str_replace('editor/', 'editor/thumbs/', $image);
                $image_properties['tag']='general';
                $data[]=$image_properties;
            }
        }
        return json_encode($data);
    }
        
}
