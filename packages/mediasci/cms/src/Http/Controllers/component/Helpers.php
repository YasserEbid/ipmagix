<?php

namespace Mediasci\Cms\Http\Controllers\component;


use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
//use Illuminate\Support\Facades\Input;

class Helpers extends Controller {

    public static function selected($option1, $option2) {
        if ($option1 == $option2) {
            return "selected='selected'";
        }
    }

    public static function checked($option1,$option2){
        if($option1==$option2){
            return "checked='checked'";
        }
    }

    public static function makeThumb($src, $dest, $desired_width , $desired_height='auto'){
            //if (!file_exists($src))
            //    return false ;

        // image type
        $ext = exif_imagetype($src);

        /* read the source image */
        //if($ext!="JPG" || $ext!="JPEG" || $ext!="jpg")
        if ($ext == '1' ) //GIF
            $source_image = @imagecreatefromgif($src);
        elseif($ext == "2" ) //jpg
            $source_image = @imagecreatefromjpeg($src);
        elseif ($ext == "3" ) //png
            $source_image = @imagecreatefrompng($src);
        else
            $source_image = $src;
        $width = @imagesx($source_image);
        $height = @imagesy($source_image);
            if ($desired_height=='auto')
            {
                    /* find the "desired height" of this thumbnail, relative to the desired width  */
                    $desired_height = @floor($height * ($desired_width / $width));
            }

        /* create a new, "virtual" image */
        $virtual_image = @imagecreatetruecolor($desired_width, $desired_height);
        /* copy source image at a resized size */
        @imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        /* create the physical thumbnail image to its destination */
        @imagejpeg($virtual_image, $dest);
    }

    static function sort($label , $column){
        $url =  \Request::url();
        $fa = '' ;
        $params = $_GET ;
        $params['sort'] = $column ;
        if(isset($params['dir']) && $params['dir']=='desc'  )
            $params['dir'] = 'asc' ;
        else
            $params['dir'] = 'desc' ;
        $href= $url.'?';
        foreach($params as $key=>$value)
        {
            $href.=$key.'='.$value.'&' ;
        }

        if((isset($_GET['sort']))&&($_GET['sort']==$column))
        {
            if(($params['dir'] =='asc') )
                $fa = 'up' ;
            else
                $fa = 'down' ;
        }
        $href= substr($href, 0, -1);
        return "<a href='".$href."'>".$label."&nbsp;<i class='fa fa-arrow-".$fa."'></i></a>";
    }
    static function export($data){

          dd($data);
          return Excel::create('report', function($excel) use($data) {
          $excel->setTitle(' report ');
          // Chain the setters
          $excel->setCreator('Icldc')->setCompany('ICLDC');
          $excel->setDescription(' report ');
          $excel->sheet('Sheetname', function($sheet) use($data) {
          $sheet->fromArray($data);
        });
      })->export('xls');
    }
}
