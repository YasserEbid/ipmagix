            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::diabetes($object->diabetes_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::Types_visitors($object->type_visitor_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::gender($object->gender_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::lifestyle($object->lifestyle_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::location($object->location_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::nationality($object->nationality_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::p_Interests($object->p_interests_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::s_Interests($object->s_interests_id)?>
            </div>
            <br/>
            <div class="form-group col-md-6">
                <?=  \Mediasci\Cms\Http\Controllers\component\Component::patientsCase($object->case_id)?>
            </div>