<?php

namespace Mediasci\Cms\Http\Controllers\component;


use Illuminate\Support\Facades\Input;

class Component{

    public static function gender($value=''){
        $html='<label>Gender</label>';
        $html.='<select name="gender_id" class="form-control">';
        $html.='<option value="">Choose One</option>';
        foreach(\Mediasci\Cms\Models\UsersProperties::where('parent','Gender')->get() as $row){
            if(Input::has('gender_id')){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, Input::get('gender_id')).' >'.ucfirst($row->name).'</option>';
            }else if($value!=''){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, $value).' >'.ucfirst($row->name).'</option>';
            }else{
                $html.='<option value="'.$row->id.'" >'.ucfirst($row->name).'</option>';
            }
        }
        $html.='</select>';
        return $html;
    }

    public static function lifestyle($value=''){
        $html='<label>Lifestyle Segments</label>';
        $html.='<select name="lifestyle_id" class="form-control">';
        $html.='<option value="">Choose One</option>';
        foreach(\Mediasci\Cms\Models\UsersProperties::where('parent','Lifestyle segments')->get() as $row){
            if(Input::has('lifestyle_id')){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, Input::get('lifestyle_id')).' >'.$row->name.'</option>';
            }else if($value!=''){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, $value).' >'.$row->name.'</option>';
            }else{
                $html.='<option value="'.$row->id.'" >'.$row->name.'</option>';
            }
        }
        $html.='</select>';
        return $html;
    }

    public static function location($value=''){
        $html='<label>Location</label>';
        $html.='<select name="location_id" class="form-control">';
        $html.='<option value="">Choose One</option>';
        foreach(\Mediasci\Cms\Models\UsersProperties::where('parent','Location')->get() as $row){
            if(Input::has('location_id')){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, Input::get('lifestyle_id')).' >'.$row->name.'</option>';
            }else if($value!=''){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, $value).' >'.$row->name.'</option>';
            }else{
                $html.='<option value="'.$row->id.'" >'.$row->name.'</option>';
            }
        }
        $html.='</select>';
        return $html;
    }

    public static function nationality($value=''){
        $html='<label>Nationality</label>';
        $html.='<select name="nationality_id" class="form-control">';
        $html.='<option value="">Choose One</option>';
        foreach(\Mediasci\Cms\Models\UsersProperties::where('parent','Nationality')->get() as $row){
            if(Input::has('nationality_id')){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, Input::get('lifestyle_id')).' >'.$row->name.'</option>';
            }else if($value!=''){
                $html.='<option value="'.$row->id.'" '.Helpers::selected($row->id, $value).' >'.$row->name.'</option>';
            }else{
                $html.='<option value="'.$row->id.'" >'.$row->name.'</option>';
            }
        }
        $html.='</select>';
        return $html;
    }


    public static function Filters($update=false){
        if($update){
            include (__DIR__."/html/filter_update.php");
        }else{
            include (__DIR__."/html/filters.php");
        }

    }

}
