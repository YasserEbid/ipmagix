<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Support\Facades\Input;
use DB;
use Mediasci\Cms\Models\General_setting;

class settingController extends \CmsController {
    public function getIndex(){
        $settings=  General_setting::paginate(8);
          return view('cms::setting.index')->withsettings($settings);
    }
    public function getCreate(){
       return view('cms::setting.create'); 
    }
    public function postCreate(){
       $setting=  new General_setting;
       $setting->name=Input::get('name');
       $setting->value=Input::get('value');
       $setting->save();
        $message = trans('cms::setting.success');
        session()->push('m', $message);
        return redirect('admin/setting');
    }
    public function getUpdate($id){
            $setting=  General_setting::find($id);
        return view('cms::setting.update')->withsetting($setting);
    }
    public function postUpdate(){
        $id=Input::get('set_id');
        $setting=  General_setting::find($id);
        $setting->name=Input::get('name');
       $setting->value=Input::get('value');
       $setting->save();
        $message = trans('cms::setting.success');
        session()->push('m', $message);
        return redirect('admin/setting');
    }
    public function getDelete($id){
                $setting=  General_setting::find($id);
$setting->delete();
 $message = trans('cms::setting.success');
        session()->push('n', $message);
        return redirect('admin/setting');
    }
}

