<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\Career;
use Mediasci\Cms\Models\Career_lang;
use Mediasci\Cms\Models\Career_category;
use Mediasci\Cms\Models\Careers_category_lang;
use DB;

class CareerController extends \CmsController {

    public function getIndex() {
        $careers = new Career;
        if (isset($_GET['sort'])) {
            $careers = $careers->join('career_langs', 'careers.id', '=', 'career_langs.career_id')->where('career_langs.lang', "en")->orderBy('career_langs.' . $_GET['sort'], $_GET['dir']);
            $careers = $careers->select('careers.*');
        }
        $careers = $careers->paginate(10);
        return view('cms::careers.index')->withcareers($careers);
    }

    public function getCreate() {
        $category = Career_category::where('country_id',session('country'))->get();

        return view('cms::careers.create')->withcategory($category);
    }

    public function postCreate() {
        if (empty(Input::get('cat_id'))) {
            return redirect(\URL::current())->with('validate_errors', ["category require"]);
        }
        DB::transaction(function () {
            $languages = \Mediasci\Cms\Models\Languages::all();
            $career = Career::create([
                        'status' => '1', "cat_id" => Input::get('cat_id')
            ]);
            foreach ($languages as $language) {
                $careerlan = Career_lang::create([
                            'title' => Input::get($language->lang . '_title'),
                            'summery' => Input::get($language->lang . '_summery'),
                            'description' => Input::get($language->lang . '_description'),
                            'location' => Input::get($language->lang . '_location'),
                            'lang' => $language->lang,
                            'career_id' => $career->id
                ]);
            }
        });
        $message = trans('cms::careers.success');
        session()->push('m', $message);
        return redirect('admin/careers');
    }

    public function getUpdate($id) {
        $data['career'] = Career::where("id", $id)->first();
        if (is_object($data["career"]) && $data["career"]->count() > 0) {
            $data['category'] = Career_category::where('country_id', session('country'))->get();
            return view('cms::careers.update', $data);
        } else {
            return redirect('admin/careers');
        }
    }

    public function postUpdate($id) {
        $career = Career::where("id", $id)->first();
        if (is_object($career) && $career->count() > 0) {
            $career->cat_id = Input::get('cat_id');
            $career->save();

            $languages = \Mediasci\Cms\Models\Languages::all();
            foreach ($languages as $language) {
                $career = Career_lang::where('career_id', $id)->where('lang', $language->lang)->first();
                $career->title = Input::get($language->lang . '_title');
                $career->summery = Input::get($language->lang . '_summery');
                $career->description = Input::get($language->lang . '_description');
                $career->location = Input::get($language->lang . '_location');
                $career->save();
            }
            $message = trans('cms::careers.success');
            session()->push('m', $message);
        }
        return redirect('admin/careers');
    }

    public function getDelete($id) {
        $career = Career::find($id);
        $cartr = Career_lang::where('career_id', $id)->get();
        foreach ($cartr as $careerlang) {
            $careerlang->delete();
        }
        $career->delete();
        $message = trans('cms::careers.success');
        session()->push('n', $message);
        return redirect('admin/careers');
    }

    public function getCreatecat() {
        return view('cms::careerscategory.create');
    }

    public function postCreatecat() {
        DB::transaction(function () {
            $cat = Career_category::create(['media_id' => Input::get('featured_image'),'country_id' => session('country')]);
            $languages = \Mediasci\Cms\Models\Languages::all();
            foreach ($languages as $language) {
                $arcat = Careers_category_lang::create(['title' => Input::get($language->lang . '_title'),
                            'lang' => $language->lang,
                            'cat_id' => $cat->id]);
            }
        });
        $message = trans('cms::careers.success');
        session()->push('m', $message);
        return redirect('admin/careercategory');
    }

    public function getUpdatecat($id) {
        $category = Career_category::where("id", $id)->first();
        if (is_object($category) && $category->count() > 0) {
            return view('cms::careerscategory.update')->withcategory($category);
        } else {
            return redirect('admin/careercategory');
        }
    }

    public function postUpdatecat($id) {
        $category = Career_category::find($id);
        if (empty($media_id)) {

        } else {
            $category->media_id = Input::get('featured_image');
        }
        $category->save();
        $languages = \Mediasci\Cms\Models\Languages::all();
        foreach ($languages as $language) {
            $arcat = $category->catlang($language->lang);
            $arcat->title = Input::get($language->lang . '_title');
            $arcat->save();
        }
        $message = trans('cms::careers.success');
        session()->push('m', $message);
        return redirect('admin/careercategory');
    }

    public function getDeletecat($id) {
        $category = Career_category::find($id);
        $languages = \Mediasci\Cms\Models\Languages::all();
        foreach ($languages as $language) {
            $arcat = $category->catlang($language->lang);
            $arcat->delete();
        }
        $category->delete();
        $message = trans('cms::careers.success');
        session()->push('m', $message);
        return redirect('admin/careercategory');
    }

    public function getCategory() {
        $category = new Career_category;
        if (isset($_GET['sort'])) {
            $category = $category->join('careers_category_langs', 'careers_categories.id', '=', 'careers_category_langs.cat_id')->where('careers_category_langs.lang', "en")->orderBy('careers_category_langs.' . $_GET['sort'], $_GET['dir']);
            $category = $category->select('careers_categories.*');
        }
        $category = $category->where('country_id', \Session::get('country'))->paginate(7);
        return view('cms::careerscategory.index')->withcategory($category);
    }

}
