<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\AppointmentsRequests;
use Mediasci\Cms\Models\Posts;
use DB;

class AppointmentsRequestsController extends \CmsController {

	public function getIndex() {
    $appointmentsRequests=new AppointmentsRequests();
		$products = [];
    if(isset($_GET['sort'])){
        $appointmentsRequests=$appointmentsRequests->orderBy($_GET['sort'],$_GET['dir']);
    }
		$appointmentsRequests=$appointmentsRequests->join("posts",'appointments_requests.product_id','=','posts.post_id')
		->leftJoin('posts_langs','posts.post_id','=','posts_langs.post_id')
    ->select("appointments_requests.*",'posts_langs.post_title as product_title')
		->paginate(10);

		return view('cms::appointmentsRequests.index',['appointmentsRequests'=>$appointmentsRequests]);
	}

	public function getDelete($id) {
		$appointment = AppointmentsRequests::find($id);
		$appointment->delete();
		$message = trans('cms::appointmentsRequests.success');
		session()->push('success', $message);
		return redirect('admin/demoRequests');
	}

}
