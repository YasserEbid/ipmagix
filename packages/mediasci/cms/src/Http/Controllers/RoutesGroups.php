<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Description of Groups
 *
 * @author Mahmoud
 */
class RoutesGroups extends \CmsController {

	public function index() {
		$groups = \Groups::get_all();
		
		return view('cms::routes_groups.index', ['groups' => $groups,'current_group'=>null]);
	}

	public function create() {

		if ($this->request->isMethod('post')) {
			$data = ['group_name' => $this->request->input('group_name')];
			\Groups::add($data);
			return redirect()->route('routes_groups.index')->with('message', trans('cms::routes_groups.created'));
		}
	}

	public function edit($id) {

		if ($this->request->isMethod('post')) {
			$data = ['group_name' => $this->request->input('group_name')];
			\Groups::modify($data, $id);
			return redirect()->route('routes_groups.index')->with('message', trans('cms::routes_groups.updated'));
		}
		$groups = \Groups::get_all();
		$conditions = ['routes_groups.group_id' => $id];
		$current_group = \Groups::find_by($conditions);
		if(!$current_group){
			return redirect()->route('routes_groups.index')->with('message', trans('cms::routes_groups.no_routes_groups'));
		}
		return view('cms::routes_groups.index', ['current_group' => $current_group,'groups'=>$groups]);
	}

	public function remove($id) {
		$conditions = ['routes_groups.group_id' => $id];
		\Groups::remove($conditions);
		return redirect()->route('routes_groups.index');
	}

}
