<?php

namespace Mediasci\Cms\Http\Controllers;

use Mediasci\Cms\Models\User,
    Mediasci\Cms\Models\UserPermission,
    Mediasci\Cms\Models\Role,
    Mediasci\Cms\Models\Media,
    Mediasci\Cms\Models\Gallery,
    Mediasci\Cms\Models\GalleryLang,
    Mediasci\Cms\Models\GalleryMedia,
    Config,
    View,
    Request,
    Illuminate\Support\Facades\Input,
    Auth,
    Validator,
    Hash,
    URL,
    Str,
    Mail,
    DB,
    File,
    Response,
    Image,
    Redirect;

class MediaController extends \CmsController {

    public $data = array();

    function unique($filename = false) {

        if ($filename) {
            if (File::exists(UPLOADS . "/" . $filename)) {
                $parts = explode(".", $filename);
                $extension = end($parts);
                $name = str_slug(basename($filename, "." . $extension));
                $filename = $name . "-" . time() . "." . $extension;
                return $filename;
            }
            return $filename;
        }
    }

    function youtube($link = "") {
        $id = get_youtube_video_id($link);
        $details = get_youtube_video_details($id);

        $data["media_provider"] = "youtube";
        $data["media_provider_id"] = $id;
        $data["media_provider_image"] = $details->image;
        $data["media_type"] = "video";
        $data["media_path"] = "https://www.youtube.com/watch?v=" . $id;
        $data["media_title"] = $details->title;
        $data["media_description"] = $details->description;
        $data["media_duration"] = $details->length;
        $data["media_created_date"] = date("Y-m-d H:i:S");

        DB::table('media')->insert([$data]);

        $media_id = DB::getPdo()->lastInsertId();

        // Saving the file to database
        $row = new \stdClass();
        $row->error = false;
        $row->id = $media_id;
        $row->name = $data["media_title"];
        $row->size = "";
        $row->duration = $details->length;
        $row->url = "https://www.youtube.com/watch?v=" . $id;
        $row->thumbnail = $details->image;
        $row->html = View::make("cms::media.index", array(
                    "files" => array(0 => (object) array(
                            "media_id" => $media_id,
                            "media_provider" => "youtube",
                            "media_provider_id" => $id,
                            "media_url" => $row->url,
                            "media_thumbnail" => $row->thumbnail,
                            "media_size" => $row->size,
                            "media_path" => $row->name,
                            "media_duration" => format_duration($row->duration),
                            "media_type" => "video",
                            "media_title" => $data["media_title"],
                            "media_description" => $data["media_description"],
                            "media_created_date" => date("Y-m-d H:i:s")
                        ))
                ))->render();
        return Response::json($row, 200);
    }

    function soundcloud($link = "") {

        $details = get_soundcloud_track_details($link);
        $link = $details->link;

        $data["media_provider"] = "soundcloud";
        $data["media_provider_id"] = $details->id;
        $data["media_provider_image"] = $details->image;
        $data["media_type"] = "audio";
        $data["media_path"] = $link;
        $data["media_title"] = $details->title;
        $data["media_description"] = $details->description;
        $data["media_duration"] = $details->length;
        $data["media_created_date"] = date("Y-m-d H:i:S");

        DB::table('media')->insert($data);

        $media_id = DB::getPdo()->lastInsertId();

        // Saving the file to database
        $row = new \stdClass();
        $row->error = false;
        $row->id = $media_id;
        $row->name = $details->title;
        $row->size = "";
        $row->duration = $details->length;
        $row->url = $link;

        if ($details->image != "") {
            $row->thumbnail = $details->image;
        } else {
            $row->thumbnail = assets("default/soundcloud.png");
        }

        $row->html = View::make("cms::media.index", array(
                    "files" => array(0 => (object) array(
                            "media_id" => $media_id,
                            "media_provider" => "soundcloud",
                            "media_provider_id" => $details->id,
                            "media_url" => $link,
                            "media_thumbnail" => $row->thumbnail,
                            "media_size" => $row->size,
                            "media_path" => $link,
                            "media_duration" => format_duration($row->duration),
                            "media_type" => "audio",
                            "media_title" => $details->title,
                            "media_description" => $details->description,
                            "media_created_date" => date("Y-m-d H:i:s")
                        ))
                ))->render();
        return Response::json($row, 200);
    }

    function link() {

        $data = array(
            "media_provider" => 0,
            "media_path" => "",
            "media_type" => "",
            "media_title" => "",
            "media_description" => ""
        );

        if ($link = Input::get("link")) {

            // Detecting link
            if (strstr($link, "youtube.") and get_youtube_video_id($link)) {
                return $this->youtube($link);
            } else if (strstr($link, "soundcloud.")) {
                return $this->soundcloud($link);
            } else {
                $name = md5($link);
                if (copy($link, UPLOADS . "/temp/" . $name)) {
                    $mime = strtolower(mime_content_type(UPLOADS . "/temp/" . $name));

                    // check allowed types
                    $media_extension = get_extension($mime);
                    if (!$media_extension) {
                        $row->error = "Invalid link file type";
                        return Response::json($row, 200);
                    }

                    $mime_parts = explode("/", $mime);
                    $media_type = $mime_parts[0];

                    $remote_file_parts = explode("/", ($link));
                    $data["media_title"] = $remote_file_name = urldecode(basename(end($remote_file_parts), "." . $media_extension));

                    $slug = str_slug(str_replace("." . $media_extension, "", urldecode($remote_file_name)));
                    $filename = $this->unique($slug) . "." . $media_extension;

                    $filename = time() * rand() . "." . strtolower($media_extension);

                    if (copy(UPLOADS . "/temp/" . $name, UPLOADS . "/" . $filename)) {

                        if (in_array($media_extension, array("jpg", "jpeg", "gif", "png", "bmp"))) {
                            $this->set_sizes($filename);
                        }

                        $data["media_type"] = $media_type;
                        $data["media_path"] = $filename;

                        \Media::create($data);

                        $media_id = DB::getPdo()->lastInsertId();

                        // Saving the file to database
                        $row = new \stdClass();
                        $row->error = false;
                        $row->id = $media_id;
                        $row->name = $filename;
                        $row->size = format_file_size(filesize(UPLOADS . "/" . $filename));
                        $row->url = uploads_url($filename);
                        $row->thumbnail = thumbnail($filename);

                        $row->html = View::make("cms::media.index", array(
                                    "files" => array(0 => (object) array(
                                            "media_id" => $media_id,
                                            "media_provider" => "",
                                            "media_provider_id" => "",
                                            "media_url" => $row->url,
                                            "media_thumbnail" => $row->thumbnail,
                                            "media_size" => $row->size,
                                            "media_path" => $row->name,
                                            "media_duration" => "",
                                            "media_type" => "",
                                            "media_title" => $data["media_title"],
                                            "media_description" => "",
                                            "media_created_date" => date("Y-m-d H:i:s")
                                        ))
                                ))->render();

                        return Response::json($row, 200);
                    }
                } else {
                    return "Cannot copy this file";
                }
            }
        }
    }

    public function upload() {
        //dd($_FILES);
        $file = Input::file('files')[0];

        $rules = array(
            'files.0' => "mimes:jpg,png,jpeg,gif,doc,docx,txt,pdf,mp4|max:166666666",
        );

        $size = $file->getSize();

        $filename = $this->unique($file->getClientOriginalName());
        $parts = explode(".", $file->getClientOriginalName());
        $extension = strtolower(end($parts));


        // rename file with timestamp
        $filename = time() * rand() . "." . strtolower($extension);

        $mime_parts = explode("/", $file->getMimeType());
        $mime_type = $mime_parts[0];


        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return \Response::json(array(
                        "error" => join("|", str_replace("files.0", $file->getClientOriginalName(), $errors["files.0"]))
                            ), 200);
        }

        try {
            $operation = $file->move(UPLOADS, $filename);
        } catch (Exception $exception) {
            // Something went wrong. Log it.
            $error = array(
                'name' => $file->getClientOriginalName(),
                'size' => $size,
                'error' => $exception->getMessage(),
            );

            // Return error
            return \Response::json($error, 400);
        }


        if ($operation) {

            // creating image size
            if (in_array($extension, array("jpg", "jpeg", "gif", "png", "bmp"))) {
                $this->set_sizes($filename);
            }

            $video_id = null;
            $media_provider = "";
            $thum = thumbnail($filename);

            if ($mime_type == "video") {
                $video_id = $this->bc_upload($filename);
                $media_provider = "brightcove";
                $thum = URL::to('images/url.jpg');
            }

            DB::table('media')->insert([
                "media_provider" => $media_provider,
                "media_path" => $filename,
                "media_type" => $mime_type,
                "media_title" => basename($file->getClientOriginalName(), "." . $extension),
                "media_created_date" => date("Y-m-d H:i:S"),
                "video_id" => $video_id
            ]);

            $media_id = DB::getPdo()->lastInsertId();

            // Saving the file to database
            $row = new \stdClass();
            $row->id = $media_id;

            $row->name = $filename;
            $row->title = basename($file->getClientOriginalName(), "." . $extension);
            $row->size = format_file_size($size);
            $row->url = uploads_url($filename);
            $row->thumbnail = thumbnail($filename);
            $row->html = View::make("cms::media.index", array(
                        "files" => array(0 => (object) array(
                                "media_id" => $media_id,
                                "media_provider" => "",
                                "media_provider_id" => "",
                                "media_type" => $mime_type,
                                "media_url" => uploads_url($filename),
                                "media_thumbnail" => $thum,
                                "media_size" => format_file_size($size),
                                "media_path" => $filename,
                                "media_duration" => "",
                                "media_title" => basename($file->getClientOriginalName(), "." . $extension),
                                "media_description" => "",
                                "media_created_date" => date("Y-m-d H:i:s")
                            ))
                    ))->render();

            return Response::json(array('files' => array($row)), 200);
        } else {
            return Response::json('Error', 400);
        }
    }

    public function bc_upload($filename = 0) {
        if ($filename) {
            include(app_path() . '/libraries/bcapi/bc-mapi.php');

            $read_token = Config::get('app.bc_read_token');
            $write_token = Config::get('app.bc_write_token');

            $bc = new \BCMAPI($read_token, $write_token);

            $metaData = array(
                'name' => $filename,
                'shortDescription' => $filename
            );

            $file_location = public_path() . '/uploads/' . $filename;
            # Send video to Brightcove
            $options = [
                'create_multiple_renditions' => true,
            ];
            $id = $bc->createMedia('video', $file_location, $metaData, $options);

            unlink($file_location);

            return $id;
        }
    }

    function save_gallery() {

        $name = Input::get("name");
        $slug = str_slug($name);

        Gallery::create(array(
            "gallery_slug" => $slug,
            "gallery_author" => ""
        ));

        $gallery_id = DB::getPdo()->lastInsertId();

        GalleryLang::create(array(
            "gallery_id" => $gallery_id,
            "lang" => LANG,
            "gallery_name" => $name
        ));

        // insert gallery media
        if ($ids = Input::get("content")) {

            $i = 1;
            foreach ($ids as $media_id) {

                DB::table("media")->where("media_id", $media_id)->update(array(
                    "media_description" => $name . "-" . $i
                ));

                \GalleryMedia::create(array(
                    "gallery_id" => $gallery_id,
                    "media_id" => $media_id
                ));

                $i++;
            }
        }

        $media_type = DB::table("media")->where("media_id", $ids[0])->pluck("media_type");

        echo json_encode(array("gallery_id" => $gallery_id, "gallery_type" => $media_type));
    }

    function set_sizes($filename) {


        if (file_exists(UPLOADS . "/" . $filename)) {

            $sizes = array(
                // 'large' => array(1000, 1000),
                'medium' => array(653, 345),
                'small' => array(450, 331),
                'thumbnail' => array(150, 150),
                'solitem'=>array(600,393)
            );

            $width = \Image::make(UPLOADS . "/" . $filename)->width();
            $height = \Image::make(UPLOADS . "/" . $filename)->height();

            foreach ($sizes as $size => $dimensions) {

                if ($width > $height) {
                    $new_width = null;
                    $new_height = $dimensions[0];
                } else {
                    $new_width = $dimensions[1];
                    $new_height = null;
                }

                /*
                  \Image::make(UPLOADS . "/" . $filename)
                  ->resize($new_height, $new_width, function ($constraint) {
                  $constraint->aspectRatio();
                  })
                  ->save(UPLOADS . "/" . $size . "-" . $filename);
                 */



                \Image::make(UPLOADS . "/" . $filename)
//                        ->resize($dimensions[0], $dimensions[1])
                        ->fit($dimensions[0], $dimensions[1])
                        ->save(UPLOADS . "/" . $size . "-" . $filename);



                /*
                  \Image::make(UPLOADS . "/" . $filename)

                  //$img->grab($dimensions[1], $dimensions[0]);
                  ->fit($dimensions[1], $dimensions[0], function ($constraint) {
                  $constraint->upsize();
                  })

                  ->save(UPLOADS . "/" . $size . "-" . $filename); */
            }
        } else {

            return "Image Not found";
        }
    }

    /*
      function set_sizes($filename) {
      $sizes = Config::get("cms::app.sizes");
      foreach ($sizes as $size => $dimensions) {

      $width = \Image::make(UPLOADS . "/" . $filename)->width();
      $height = \Image::make(UPLOADS . "/" . $filename)->height();

      if($width > $height){
      $new_width = null;
      $new_height = $dimensions[0];
      }else{
      $new_width = $dimensions[1];
      $new_height = null;
      }

      \Image::make(UPLOADS . "/" . $filename)
      ->resize($new_height, $new_width, function ($constraint) {
      $constraint->aspectRatio();
      })
      ->resizeCanvas($dimensions[0], $dimensions[1], 'center', false, "#000000")
      ->save(UPLOADS . "/" . $size . "-" . $filename);

      }
      }
     */

    public function index($page = 1, $type = "all", $q = "") {

        $limit = 25;
        $offset = ($page - 1) * $limit;

        $query = DB::table('media')->orderBy('media_created_date', 'desc');



        if ($type != "all") {

            if ($type == "pdf" or $type == "swf") {
                $query->where("media_path", "LIKE", "%" . $type . "%");
            } else {
                $query->Where("media_type", "=", $type);
            }
        }

        if ($q != "") {
            $query->where("media_title", "LIKE", "%" . $q . "%");
            $query->orWhere("media_description", "LIKE", "%" . $q . "%");
        }



        $files = $query->limit($limit)->skip($offset)->get();

        $new_files = array();
        foreach ($files as $file) {

            $row = new \stdClass();
            $row = $file;

            if ($file->media_provider == "") {
                $row->media_thumbnail = thumbnail($file->media_path);
                $row->media_size = (File::exists(uploads_path($row->media_path))) ? format_file_size(File::size(uploads_path($row->media_path))) : "";
                $row->media_duration = "";
                $row->media_url = uploads_url($file->media_path);
            } else {

                if ($file->media_provider_image != NULL) {
                    $row->media_thumbnail = $file->media_provider_image;
                } else {
                    $row->media_thumbnail = assets("default/soundcloud.png");
                }

                $row->media_url = $file->media_path;
                $row->media_size = "";
                $row->media_duration = format_duration($file->media_duration);
            }

            $new_files[] = $row;
        }


        $this->data["files"] = $new_files;

        $this->data["q"] = $q;
        return View::make("cms::media.index", $this->data);
    }

    public function save() {

        if (Request::isMethod("post")) {
            $media_id = Input::get("file_id");
            DB::table("media")->where("media_id", "=", $media_id)->update(array(
                "media_title" => Input::get("file_title"),
                "media_description" => Input::get("file_description")
            ));
        }
    }

    public function delete() {

        if (Request::isMethod("post")) {

            $media_id = Input::get("media_id");
            DB::table("media")->where("media_id", "=", $media_id)->delete();

            $media_path = Input::get("media_path");
            @unlink(uploads_path($media_path));

            // delete all sizes
            $parts = explode(".", $media_path);
            $extension = end($parts);
            if (in_array(strtolower($extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
                // resize
                $sizes = \Config::get("cms::app.sizes");

                foreach ($sizes as $size => $dimensions) {
                    if (File::exists(uploads_path($size . "-" . $media_path))) {
                        @unlink(uploads_path($size . "-" . $media_path));
                    }
                }
            }
        }
    }

    public function cropImage() {
        $input = Request::all();
        $file_id = $input["file_id"];
        $image = Media::find($file_id)->media_path;
        $parts = explode(".", $image);
        $extension = end($parts);
        if (in_array(strtolower($extension), array("jpg", "jpeg", "gif", "png", "bmp"))) {
            $imgData = explode(',', $input['imgData']);
            $imgData_base_code = base64_decode($imgData[1]);
//            $filename = md5(uniqid(rand(), true));
            unlink("uploads/".$input['resize_type'].$image);
//            $filepath = "uploads/" . $filename . "." . strtolower($extension); // or image.jpg
            // if ($input['resize_type'] == 'free') {
            //   $filepath = "uploads/".$image; // or image.jpg
            // }else {
              $filepath = "uploads/".$input['resize_type'].$image; // or image.jpg
            // }
            file_put_contents($filepath, $imgData_base_code);
            // Media::where("media_id", $file_id)->update(array("media_path" => $image));
            return $filepath;
        }
    }

}
