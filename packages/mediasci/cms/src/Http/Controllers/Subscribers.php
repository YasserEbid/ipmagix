<?php
namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\MailList;
use Mediasci\Cms\Models\UsersProperties;
use DB;
class Subscribers extends \CmsController{
    function index(){
    $data['subscribers']=new MailList;
      if(isset($_GET['sort']))                       {
                  $data['subscribers']=  $data['subscribers']->orderBy($_GET['sort'],$_GET['dir']);
            }
          $data['subscribers']=$data['subscribers']->paginate(10);
    $data['proberty']=new UsersProperties;
    // dd($data['subscribers']);
    return view('cms::subscribe.index',$data);
    }
    function show($id){
        $data['subscribe']=  MailList::find($id);
         $proberty=new UsersProperties;
    return view('cms::subscribe.show',$data);
    }
    function delete($id){
        $subscribe=  MailList::find($id);
        $subscribe->delete();
            session()->push('m',"One Item has been deleted successfully .");
        return redirect('admin/subscribers');
    }

}
