<?php

namespace Mediasci\Cms\Http\Controllers;
use Mediasci\Cms\Models\User,
	Mediasci\Cms\Models\UserPermission,
        Mediasci\Cms\Models\Poll,
        Mediasci\Cms\Models\Pollslang
        ;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;


class PollsController extends \CmsController  {


    public function getIndex(){
        $data= Poll::question();


      return view("cms::polls.index")->withdata($data);

    }
    public function getCreate(){
        return view("cms::polls.create");
    }
    public function postCreate(){

        $poll=new Poll;
        $poll_lang=new Pollslang;
        $lang=Input::get('lang');
        $poll->parent_id=null;
        $poll->save();
        $poll_lang->content=  Input::get('content');
        $poll_lang->lang=$lang;
                $poll_lang->poll_id=$poll->id;
                $poll_lang->save();
////
                foreach(Input::get('options') as $option){
                   if($option!=''){
                    $polls=new Poll;
                $pols_lang=new Pollslang;
                   $polls->parent_id=$poll->id;
                   $polls->save();
                $pols_lang->poll_id=$polls->id;
                   $pols_lang->lang=$lang;
                  $pols_lang->content=$option;
                  $pols_lang->save();
////             ]
                }}

               session()->push('m','تم ادخال الاستفتاء  بنجاح   ');
                return redirect('admin/polls');
////
    }
    public function getEdit($id){
        $poll =Poll::find($id);
        return view('cms::polls.update')->withpoll($poll);
    }
    public function postUpdate(){

    }
    public function getDelete($id){
        $poll=Poll::find($id);
        foreach($poll->answers()->get() as $answer){
            $polslang=  Pollslang::where('poll_id',$answer->id);
            foreach($polslang as $lang){
                $lang->delete();
            }
            $answer->delete();
        }
        $polllang =  Pollslang::where('poll_id',$id);
        foreach ($polllang as $pol){

           $pol->delete();
        }
        $poll->delete();
          session()->push('n','تم المسح  بنجاح');
        return redirect()->back();
    }
}
