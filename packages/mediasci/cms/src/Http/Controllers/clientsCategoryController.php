<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Mediasci\Cms\Models\ClientsCategory;
use Mediasci\Cms\Models\ClientsCategoryLangs;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

/**
 * Description of Groups
 *
 * @author abeer elhout
 */
class clientsCategoryController extends CmsController {

    /** get all Clients catgeory * */
    public function getIndex() {
        $categories = new ClientsCategory;
        if (isset($_GET['sort'])) {
            $categories = $categories::orderBy($_GET['sort'], $_GET['dir']);
        }
        $categories = $categories->where('country_id', session('country'))->paginate(8);
        return view('cms::clientsCategory.index')->withcategories($categories);
    }

    /*     * ************ create new category && Save ********************* */

    public function getCreate() {
        if ($this->request->isMethod('post')) {
            $category = new ClientsCategory;
            $category->country_id = session('country');
            $category->Save();
            foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                $clientCatsLangs = new ClientsCategoryLangs;
                $clientCatsLangs->name = Input::get('name_' . $lang->lang);
                $clientCatsLangs->lang = $lang->lang;
                $clientCatsLangs->clientCategory_id = $category->id;
                $clientCatsLangs->Save();
            }
            session()->push('clientCategory_created', "New Category has been added seccessfully.");
            return redirect('admin/clientsCategory');
        } else {
            return view('cms::clientsCategory.create');
        }
    }

    /*     * ************ delete category ********************* */

    public function destroy($id) {
        $category = ClientsCategory::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($category) && $category->count() > 0) {
            $category->delete();
        }
        return redirect('admin/clientsCategory');
    }

    /*     * *************** update ategory ************************** */

    public function update($id, Request $request) {
        if ($this->request->isMethod('post')) {
            foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                $clientCatsLangs = ClientsCategoryLangs::where('clientCategory_id', $id)->where('lang', $lang->lang)->first();
                $clientCatsLangs->name = Input::get('name_' . $lang->lang);
                $clientCatsLangs->Save();
            }
            return redirect('admin/clientsCategory');
        } else {
            $data['category'] = $category = ClientsCategory::where("id", $id)->where('country_id', session('country'))->first();
            if (is_object($category) && $category->count() > 0) {
                return view('cms::clientsCategory.edit', $data);
            } else {
                return redirect('admin/clientsCategory');
            }
        }
    }

////////// end of class
}
