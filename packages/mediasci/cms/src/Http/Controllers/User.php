<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Description of Groups
 *
 * @author Mahmoud
 */
class User extends \CmsController {

    public $data = [];

    private function filter() {
        $args = [
            'q' => $this->request->input('q'),
            'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
        ];

        $this->data['users'] = \User::get_all($args);
    }

    public function index() {

        $this->filter();

        return view('cms::users.index', $this->data);
    }

    public function create() {
        if ($this->request->isMethod('post')) {

            $rules = ['email' => 'required|email|unique:users',
                'role_id' => 'required',
                'full_name' => 'required',
                'mobile' => 'required|numeric|digits_between:11,15'
            ];

            $validator = \Validator::make($this->request->all(), $rules);
            $validator->setAttributeNames(\User::get_attributes_names());

            if ($validator->fails()) {
                return redirect()->route('user.create')->withErrors($validator)->withInput();
            }

            $data = ['full_name' => $this->request->input('full_name'),
                'email' => $this->request->input('email'),
                'role_id' => $this->request->input('role_id'),
                'mobile' => $this->request->input('mobile'),
                'password' => md5($this->request->input('password'))
            ];
            \User::add($data);
            return redirect()->route('user.index')->with('message', trans('cms::users.created'));
        }
        $roles = \Roles::get_all();
        return view('cms::users.create', ['roles' => $roles]);
    }

    public function edit($id) {
        if ($this->request->isMethod('post')) {
            $rules = [
                'email' => 'required|unique:users,email,' . $id . ',user_id',
                'role_id' => 'required',
                'full_name' => 'required',
                'mobile' => 'required|numeric|digits_between:11,15|unique:users,mobile,' . $id . ',user_id'
            ];
            $validator = \Validator::make($this->request->all(), $rules);
            $validator->setAttributeNames(\User::get_attributes_names());

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if ($this->request->input('password') != '') {
                $data = ['full_name' => $this->request->input('full_name'),
                    'email' => $this->request->input('email'),
                    'role_id' => $this->request->input('role_id'),
                    'mobile' => $this->request->input('mobile'),
                    'password' => md5($this->request->input('password'))
                ];
            } else {
                $data = ['full_name' => $this->request->input('full_name'),
                    'email' => $this->request->input('email'),
                    'role_id' => $this->request->input('role_id'),
                    'mobile' => $this->request->input('mobile')
                ];
            }

            \User::modify($data, $id);

            return redirect()->route('user.index')->with('message', trans('cms::users.updated'));
        }

        $conditions = ['user_id' => $id];
        $current_user = \User::find_by($conditions);
        $roles = \Roles::get_all();
        return view('cms::users.edit', ['roles' => $roles, 'current_user' => $current_user]);
    }

    public function remove($id) {
        $conditions = ['user_id' => $id];
        if ($id != 1) {
            \User::remove($conditions);
        }
        return redirect()->route('user.index');
    }

    public function transfer() {
            $to = $_GET["to"];
            $count = $_GET["count"];
            \Illuminate\Support\Facades\DB::table("users")->where("user_id", $to)->increment('points', $count);
            \Illuminate\Support\Facades\DB::table("users")->where("user_id", session("admin_user_id"))->decrement('points', $count);
            return redirect()->route('user.index');
    }

}
