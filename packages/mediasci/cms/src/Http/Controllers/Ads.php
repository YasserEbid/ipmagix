<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use DB;

/**
 * Description of Groups
 *
 * @author Elsayed Nofal
 */
class Ads extends CmsController {
    
    function index(){
        $data['ads']=  \Mediasci\Cms\Models\Ads::all();
        return view('cms::ads.index',$data);
    }
    
    function create(){
        $data=[];
        if(Input::has('type')){
           $ads=new \Mediasci\Cms\Models\Ads();
           $ads->fill(Input::all());
           $ads->save();
           $data['sucess']=true;
           return redirect()->route('ads');
        }
        return view('cms::ads.create',$data);
    }
    
    function delete($id){
        \Mediasci\Cms\Models\Ads::find($id)->delete();
        return redirect()->route('ads');
    }
    
    function update($id){
        $ads=  \Mediasci\Cms\Models\Ads::find($id);
        if(Input::has('subject')){
            $ads->fill(Input::all());
            $ads->save();
            return redirect()->route('ads');
        }
        $data['ads']=$ads;
        return view('cms::ads.update',$data);
    }
    
    
 
}
