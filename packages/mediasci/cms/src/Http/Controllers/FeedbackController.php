<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Mediasci\Cms\Models\Feedback;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Mediasci\Cms\Models\Feedback_lang;
use DB;

/**
 * Description of Groups
 *
 * @author abeer elhout
 */
class FeedbackController extends CmsController {

    /** get all partners * */
    public function getIndex() {
        $feedbacks = new Feedback();
        if (isset($_GET['sort'])) {
            $feedbacks = $feedbacks::orderBy($_GET['sort'], $_GET['dir']);
        }
        $data["feedbacks"] = $feedbacks->where('country_id', session('country'))->paginate(8);


        return view('cms::feedback.index', $data);
    }

    /*     * ************ create new feedback && Save ********************* */

    public function getCreate() {
      // dd(session("country"));
        if (Input::has('name_' . \Config::get("app.locale"))) {
            DB::transaction(function () {
                $languages = \Mediasci\Cms\Models\Languages::all();
                $feedback = Feedback::create(['media_id' => Input::get('featured_image'), 'country_id' => session('country')]);
                foreach ($languages as $language) {
                    $feedback_lang = new Feedback_lang();
                    $feedback_lang->feedback_id = $feedback->id;
                    $feedback_lang->name = Input::get('name_' . $language->lang);
                    $feedback_lang->description = Input::get('description_' . $language->lang);
                    $feedback_lang->job = Input::get('job_' . $language->lang);
                    $feedback_lang->lang = $language->lang;
                    $feedback_lang->save();
                }
            });
            session()->push('feedback_created', "New Feedback has been added seccessfully.");
            return redirect('admin/feedback');
        }
        return view('cms::feedback.create');
    }

    /*     * ************ delete feedback ********************* */

    public function destroy($id) {
        $feedback = Feedback::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($feedback) && $feedback->count() > 0) {
            $feedbacklang = Feedback_lang::where('feedback_id', $id)->delete();
            $feedback->delete();
        }
        return redirect('admin/feedback');
    }

    /*     * *************** edit feedback ********************* */

    public function edit($id) {
        $data["feedback"] = $feedback = Feedback::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($feedback) && $feedback->count() > 0) {
            $data["feedback_image"] = \Media::find($feedback->media_id);
            $data["feedback_langs"] = Array();
            foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
                $data["feedback_langs"][$lang->lang] = DB::table('feedback_lang')->where(['feedback_id' => $id, 'lang' => $lang->lang])->first();
            }
            return view('cms::feedback.edit', $data);
        } else {
            return redirect('admin/feedback');
        }
    }

    /*     * *************** update feedback ************************** */

    public function update($id) {
        DB::transaction(function () use ($id) {
            $languages = \Mediasci\Cms\Models\Languages::all();
            $feedback = Feedback::find($id);
            $feedback->country_id = session('country');
            $feedback->media_id = Input::get('featured_image');
            $feedback->save();

            foreach ($languages as $language) {
                $feedback_lang = Feedback_lang::where("feedback_id", $id)->where("lang", $language->lang)->first();
                $feedback_lang->name = Input::get('name_' . $language->lang);
                $feedback_lang->description = Input::get('description_' . $language->lang);
                $feedback_lang->job = Input::get('job_' . $language->lang);
                $feedback_lang->save();
            }
        });
        session()->push('m', "This feedback has been updated seccessfully.");
        return redirect('admin/feedback');
    }

/////////////// end of class
}
