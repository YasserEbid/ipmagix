<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;


/**
 * Description of Groups
 *
 * @author Elsayed Nofal
 */
class TrendsController extends CmsController {
    
    function index(){
        if(Input::has('date')){
            $data['trends']=  \Mediasci\Cms\Models\Trends::where('date',Input::get('date'))->first();
        }else{
            $data['trends']=  \Mediasci\Cms\Models\Trends::where('date',date('Y-m-d',time()))->first();
        }
        //dd($data);
        return view('cms::trends.index',$data);
    }
}
