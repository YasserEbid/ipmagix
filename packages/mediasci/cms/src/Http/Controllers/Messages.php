<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

/**
 * Description of Groups
 *
 * @author Mahmoud
 */
class Messages extends \CmsController {

	public $data = [];

	private function filter() {
		$args = [
			'q' => $this->request->input('q'),
			'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
			'site_id' => $this->request->input('site_id') ? $this->request->input('site_id') : 1,
		];

		$this->data['messages'] = \Messages::get_all($args);

		$this->data['current_message'] = null;
	}

	public function index() {
		$this->filter();

		return view('cms::messages.index', $this->data);
	}

	public function create() {
		if ($this->request->isMethod('post')) {

			$date = new \DateTime($this->request->input('sent_date'));
			$formated_date = $date->format('Y-m-d H:i:s');

			$site_id = $this->request->input('site_id') ? $this->request->input('site_id') : 1;

			$rules = ['messages_title' => 'required',
				'messages_content' => 'required',
				'sent_date' => 'required',
				'users' => 'required',
				'site_id' => 'required|numeric',
			];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Messages::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$data = ['message_title' => $this->request->input('messages_title'),
				'message_content' => $this->request->input('messages_content'),
				'sent_date' => $formated_date,
				'site_id' => $site_id,
				'created_by' => \Session('admin_user_id')
			];

			$id = \Messages::add($data);
			\UserMessages::add($this->request->input('users'), $id);

			return redirect()->route('messages.index', ['site_id' => $site_id])->with('message', trans('cms::messages.message_created'));
		}

		$this->data['users'] = \User::get_all();
		$this->data['current_message'] = null;
		$this->data['selected_users'] = [];

		return view('cms::messages.create', $this->data);
	}

	public function edit($id) {
		if ($this->request->isMethod('post')) {

			$date = new \DateTime($this->request->input('sent_date'));
			$formated_date = $date->format('Y-m-d H:i:s');

			$site_id = $this->request->input('site_id') ? $this->request->input('site_id') : 1;

			$rules = ['messages_title' => 'required',
				'messages_content' => 'required',
				'sent_date' => 'required',
				'users' => 'required',
				'site_id' => 'required|numeric',
			];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Messages::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$data = ['message_title' => $this->request->input('messages_title'),
				'message_content' => $this->request->input('messages_content'),
				'sent_date' => $formated_date,
					//'site_id' => $site_id
			];

			\Messages::modify($data, $id);
			\UserMessages::add($this->request->input('users'), $id);
			return redirect()->route('messages.index', ['site_id' => $site_id])->with('message', trans('cms::messages.message_updated'));
		}

		$this->filter();

		$conditions = ['message_id' => $id];
		$this->data['current_message'] = \Messages::find_by($conditions);
		$this->data['users'] = \User::get_all();
		$selected_users = \UserMessages::find_by(['message_id' => $id]);

		$this->data['selected_users'] = [];
		foreach ($selected_users as $user) {
			$this->data['selected_users'] [] = $user->user_id;
		}

		if (!$this->data['current_message']) {
			return redirect()->route('messages.index')->with('message', trans('cms::messages.no_messages'));
		}

		return view('cms::messages.create', $this->data);
	}

	public function remove($id) {
		$conditions = ['message_id' => $id];

		\Messages::remove($conditions);

		return redirect()->route('messages.index');
	}

	public function cron() {
		$messages = \Messages::cron();
		if (count($messages)) {
			$users_ids = array();
			$users_emails = array();
			$message_id = 0;
			for ($i = 0; $i < count($messages); $i++) {
				if ($messages[$i]->message_id == $message_id || $message_id == 0) {
					$users_ids[] = $messages[$i]->user_id;
					$users_emails[] = $messages[$i]->email;
					$message_id = $messages[$i]->message_id;
				}

				if (($i == (count($messages) - 1)) || ($messages[$i]->message_id != $message_id && $message_id != 0)) {
					send_mail($users_emails, $messages[$i]->message_title, $messages[$i]->message_content);
					\UserMessages::change_status($message_id, $users_ids, 1);
					$users_ids = array();
					$users_emails = array();
					$message_id = $messages[$i]->message_id;
				}
			}
		} else {
			die('there is no messages');
		}
	}

}
?>
