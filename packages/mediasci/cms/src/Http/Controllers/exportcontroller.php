<?php


namespace Mediasci\Cms\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use Mediasci\Cms\Models\Event;
use Illuminate\Support\Facades\Input;
class exportcontroller extends \CmsController {
    function event($id=Null){
        if($id){
                $data['event']=\Mediasci\Cms\Models\EventRegistration::orderBy('id', 'DESC')->where('event_id',$id)->get();
        }
        else{
            $data['event']=\Mediasci\Cms\Models\EventRegistration::orderBy('id', 'DESC')->get();
        }
        Excel::create('event', function($excel) use($data) {
        $excel->setTitle('Event registered report ');
        // Chain the setters
        $excel->setCreator('Icldc')->setCompany('ICLDC');
        $excel->setDescription('Event registered report ');
        $excel->sheet('Sheetname', function($sheet) use($data) {
        $sheet->fromArray($data['event']);
      });
      })->export('xls');
    }
    function subscribe(){
        $data['subscribe']=\Mediasci\Cms\Models\MailList::orderBy('id', 'DESC')->select('name','email','phone','company','country','designation')->get();
        Excel::create('subscribers', function($excel) use($data) {
        $excel->setTitle('subscribers  report ');
        // Chain the setters
        $excel->setCreator('Icldc')->setCompany('ICLDC');
        $excel->setDescription('subscribers report ');
        $excel->sheet('Sheetname', function($sheet) use($data) {
        $sheet->fromArray($data['subscribe']);
      });
      })->export('xls');
    }
    function request(){
        $data['appoint']=\Mediasci\Cms\Models\AppointmentsRequests::orderBy('appointments_requests.id', 'DESC')
        ->join("posts",'appointments_requests.product_id','=','posts.post_id')
        ->leftJoin('posts_langs','posts.post_id','=','posts_langs.post_id')
        ->select('appointments_requests.name','appointments_requests.email','posts_langs.post_title as product','appointments_requests.subject','appointments_requests.comment','appointments_requests.phone','appointments_requests.created_at as time')->get();
        Excel::create('Demo Requests', function($excel) use($data) {
        $excel->setTitle('Demo Requests  report 2016');
        // Chain the setters
        $excel->setCreator('Icldc')->setCompany('ICLDC');
        $excel->setDescription('Demos report ');
        $excel->sheet('Sheetname', function($sheet) use($data) {
        $sheet->fromArray($data['appoint']);
      });
      })->export('xls');
    }
    function volunteer(){
        $data['volunteer']=\App\Models\Volunteer::all();
        Excel::create('Be Partner', function($excel) use($data) {
        $excel->setTitle('Be Partner report ');
        // Chain the setters
        $excel->setCreator('Icldc')->setCompany('ICLDC');
        $excel->setDescription('Be Partner report ');
        $excel->sheet('Sheetname', function($sheet) use($data) {
        $sheet->fromArray($data['volunteer']);
        });
      })->export('xls');
    }
    function cv(){
        $data['cv']=  \Mediasci\Cms\Models\Job_cv::orderBy('id', 'DESC')->get();
        Excel::create('cv', function($excel) use($data) {
        $excel->setTitle('CVS report ');
        // Chain the setters
        $excel->setCreator('Icldc')->setCompany('ICLDC');
        $excel->setDescription('CVS report ');
        $excel->sheet('Sheetname', function($sheet) use($data) {
        $sheet->fromArray($data['cv']);
      });
      })->export('xls');
    }
    function info(){
            $data['info']=  \App\Models\Registered_information::orderBy('id', 'DESC')->get();
            Excel::create('info', function($excel) use($data) {
            $excel->setTitle('event informations report ');
                // Chain the setters
            $excel->setCreator('Icldc')->setCompany('ICLDC');
            $excel->setDescription('event informations report ');
            $excel->sheet('Sheetname', function($sheet) use($data) {
            $sheet->fromArray($data['info']);
        });
        })->export('xls');
    }

    function contact_leads(){
        $data['contacts']=  \App\Models\Contactus::orderBy('id', 'DESC')->get();
        Excel::create('Contact-us-Leads', function($excel) use($data) {
        $excel->setTitle('Contact Us Leads report');
            // Chain the setters
        $excel->setCreator('Ipmagix')->setCompany('Ipmagix');

        $excel->setDescription('Contact Us Leads report');
        $excel->sheet('Sheetname', function($sheet) use($data) {
        $sheet->fromArray($data['contacts']);
      });
      })->export('xls');
    }
}
