<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use DB;
use Thujohn\Twitter\Facades\Twitter;
use Alaouy\Youtube\Facades\Youtube;

/**
 * Description of Groups
 *
 * @author Elsayed Nofal
 */
class Cronjob extends CmsController {
    
    function getTwittertrend(){
        $data=Twitter::getTrendsPlace(['id'=>1940119]);
        $i=0;
        foreach ($data[0]->trends as $row){
            if($i>=9)break;
            $trend_row['name']=$row->name;
            $trend_row['url']=$row->url;
            $output[]=$trend_row;
            $i++;
        }
        if(!is_object($trend=  \Mediasci\Cms\Models\Trends::where('date',date('Y-m-d',time()))->first())){
            $trend=new \Mediasci\Cms\Models\Trends();
            $trend->date=date('Y-m-d',time());
        }
        $trend->twitter=  json_encode($output);
        $trend->save();
    }
    
    function getYoutube(){
        // echo date('Y-m-d');die;
        $videoList = Youtube::getPopularVideos('ae');
        foreach($videoList as $row){
            $trend_row['title']=$row->snippet->title;
            $trend_row['id']=$row->id;
            $output[]=$trend_row;
        }
        $date=date('Y-m-d');
        if(!is_object($trend=  \Mediasci\Cms\Models\Trends::where('date',$date)->first())){
            $trend=new \Mediasci\Cms\Models\Trends();
            $trend->date=$date;
        }
        $trend->youtube=  json_encode($output);
        $trend->save();
    }
    
}
