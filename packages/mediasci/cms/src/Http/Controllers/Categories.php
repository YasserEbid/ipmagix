<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;

/**
 * Description of Categories
 *
 * @author Darsh
 */
class Categories extends \CmsController {

    public $data = [];

    private function filter() {
        $args = [
            'site_id' => $this->request->input('site_id'),
                // 'parent_id' => null,
        ];

        $this->data['parent_categories'] = \Categories::get_all($args);

        $args = [
            'site_id' => $this->request->input('site_id'),
            'q' => $this->request->input('q'),
            "country_id" => session('country'),
            'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
        ];

        $this->data['categories'] = \Categories::get_all($args);

        $this->data['category'] = null;
    }

    public function getIndex() {
        $this->filter();
        // dd($this->data);
        return view('cms::categories.index', $this->data);
    }

    public function anyCreate() {
        if ($this->request->isMethod('post')) {
            //echo "<pre>";print_r($_POST);die;
            $rules = [];
            $rules['site_id'] = 'required';
            //$rules['parent_id']= 'present';
            $rules['cat_slug'] = 'required|present';
            //$rules['banner_id']= 'present';
            foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                $rules ['name_' . $lang->lang] = 'required';
            }
            $validator = \Validator::make($this->request->all(), $rules);
            $validator->setAttributeNames(\Categories::get_attributes_names());
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $reow = DB::table('categories')
                    ->where('cat_slug', $this->request->input('cat_slug'))
                    ->where('site_id', $this->request->input('site_id'))
                    ->get();

            if ($reow) {
                session()->push('fail', trans('cms::categories.category_exist'));
                //return redirect('admin/categories');
                return redirect()->route('categories.index', ['site_id' => $this->request->input('site_id')]);
            }

            DB::transaction(function () {
                //$id = \Categories::add(array_filter($this->request->except(['_token'])));
                $cat_id = DB::table('categories')->insertGetId([
                    'site_id' => Input::get('site_id'),
                    'parent_id' => Input::get('parent_id'),
                    "country_id" => session('country'),
                    'cat_slug' => Input::get('cat_slug'),
                    'banner_id' => Input::get('banner_id'),
                    'icon_id' => Input::get('icon_id'),
                    'icon_id_active' => Input::get('icon_id_active'),
                    'video_id' => Input::get('video_id'),
                    'video_cover_id' => Input::get('video_cover_id')
                ]);

                foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                    DB::table('categories_langs')->insert([
                        'name' => Input::get('name_' . $lang->lang),
                        'sub_title' => Input::get('sub_title_' . $lang->lang),
                        'description' => Input::get('description_' . $lang->lang),
                        'lang' => $lang->lang,
                        'cat_id' => $cat_id
                    ]);
                }
            });

            $parents = \Categories::get_all();
            // dd($parents);
            return redirect()->route('categories.index', ['site_id' => $this->request->input('site_id')])
                            ->with('success', trans('cms::categories.category_added'));
        }

        $this->filter();
        $this->data['category_banner'] = null;
        $this->data['category_icon'] = null;
        $this->data['category_icon_active'] = null;
        $this->data['post_video'] = null;
        $this->data['video_cover'] = null;
        return view('cms::categories.form', $this->data);
    }

    public function anyUpdate($id) {
        if ($this->request->isMethod('post')) {
            $rules = [
                'cat_name' => 'required',
                'site_id' => 'required',
            ];

            $validator = \Validator::make($this->request->all(), $rules);
            $validator->setAttributeNames(\Categories::get_attributes_names());

            if ($validator->fails()) {
                return redirect()->route('categories.edit', ['id' => $id])->withErrors($validator)->withInput();
            }

            \Categories::modify($id, array_filter($this->request->except(['_token'])));

            return redirect()->route('categories.edit', ['id' => $id])->with('message', trans('cms::categories.category_edited'));
        }

        $this->filter();

        $this->data['category'] = \Categories::find($id);

        if (!$this->data['category']) {
            return redirect()->route('categories.index')->with('message', trans('cms::categories.category_not_found'));
        }

        return view('cms::categories.index', $this->data);
    }

    public function anyEdit($id) {
        $category = \Categories::find($id);
        if ($this->request->isMethod('post')) {
            //echo "<pre>";print_r($_POST);die;
            $rules['site_id'] = 'required';
            $rules['parent_id'] = "parent_cat:$id"; //pass$idas $parameters[0]
            $rules['cat_slug'] = 'present';
            //$rules['banner_id']= 'present';

            foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                $rules ['name_' . $lang->lang] = 'required';
            }
            //create parent_cat validation rule
            \Validator::extend('parent_cat', function($field, $value, $parameters) {
                return $value != $parameters[0];
            });
            $messages = array(
                'parent_cat' => 'Parent category can not equal to category'
            );
            $validator = \Validator::make($this->request->all(), $rules, $messages);
            $validator->setAttributeNames(\Categories::get_attributes_names());
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $reow = DB::table('categories')
                    ->where('cat_slug', Input::get('cat_slug'))
                    ->where('site_id', Input::get('site_id'))
                    ->where("country_id", session('country'))
                    ->where('cat_id', '!=', $id)
                    ->get();
            if ($reow) {
                session()->push('fail', trans('cms::categories.category_exist'));
                //return redirect('admin/categories');
                return redirect()->route('categories.index', ['site_id' => Input::get('site_id')]);
            }

            DB::transaction(function($id) use ($id) {
                //$id = \Categories::add(array_filter($this->request->except(['_token'])));
                DB::table('categories')->where('cat_id', $id)->update([
                    'site_id' => Input::get('site_id'),
                    'parent_id' => Input::get('parent_id'),
                    "country_id" => session('country'),
                    'cat_slug' => Input::get('cat_slug'),
                    'banner_id' => Input::get('banner_id'),
                    'icon_id' => Input::get('icon_id'),
                    'icon_id_active' => Input::get('icon_id_active'),
                    'video_id' => Input::get('video_id'),
                    'video_cover_id' => Input::get('video_cover_id')
                ]);

                $affectedRows = DB::table('categories_langs')->where('cat_id', $id)->delete();
                foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                    DB::table('categories_langs')->insert([
                        'name' => Input::get('name_' . $lang->lang),
                        'sub_title' => Input::get('sub_title_' . $lang->lang),
                        'description' => Input::get('description_' . $lang->lang),
                        'lang' => $lang->lang,
                        'cat_id' => $id
                    ]);
                }
            });

            return redirect()->route('categories.index', ['site_id' => Input::get('site_id')])->with('success', trans('cms::categories.category_edited'));
        }
        if (is_object($category) && count($category) > 0) {
            $category_langs = Array();
            foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
                $category_langs[$lang->lang] = DB::table('categories_langs')->where(['cat_id' => $id, 'lang' => $lang->lang])->first();
            }
            $this->filter();

            $this->data['category'] = $category;
            $this->data['category_langs'] = $category_langs;
            $this->data['category_banner'] = \Media::find($category->banner_id);
            $this->data['category_icon'] = \Media::find($category->icon_id);
            $this->data['category_icon_active'] = \Media::find($category->icon_id_active);
            $this->data['post_video'] = \Media::find($category->video_id);
            $this->data['video_cover'] = \Media::find($category->video_cover_id);

            if (!$category || !$category_langs)
                return redirect()->route('categories.index', ['site_id' => Input::get('site_id')])->with('message', trans('cms::categories.category_not_found'));

            return view('cms::categories.form', $this->data);
        }else {
            return redirect()->route('categories.index');
        }
    }

    public function anuDelete($id) {
        $affectedRows = DB::table('categories_langs')->where('cat_id', $id)->delete();
        $res = \Categories::remove($id);

        if ($res && $affectedRows) {
            return redirect()->route('categories.index')->with('message', trans('cms::categories.category_deleted'));
        } else {
            return redirect()->route('categories.index')->with('message', trans('cms::categories.category_not_found'));
        }
    }

}
