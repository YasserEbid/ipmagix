<?php
namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\Job_cv;
use DB;
class CvsController extends \CmsController{
    public function getIndex(){
        $cvs=new Job_cv;
          if(isset($_GET['sort'])){
                $cvs=$cvs->orderBy($_GET['sort'],$_GET['dir']);
            }
        $cvs=  $cvs->paginate(15);
        return view('cms::cvs.index')->withcvs($cvs);

    }
    public function getShow($id){
        $cv=  Job_cv::find($id);
          return view('cms::cvs.show')->withcv($cv);
    }
    public function delete($id){
        $cv=  Job_cv::find($id);
        $cv->delete();
            session()->push('m',"One Item has been deleted successfully .");
        return redirect('admin/jobcv');
    }
}
