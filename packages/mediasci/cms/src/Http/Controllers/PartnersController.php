<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Mediasci\Cms\Models\Partner;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Mediasci\Cms\Models\Partner_lang;
use DB;

/**
 * Description of Groups
 *
 * @author abeer elhout
 */
class PartnersController extends CmsController {

    /** get all partners * */
    public function getIndex() {
        $partners = new Partner();
        if (isset($_GET['sort'])) {
            $partners = $partners::orderBy($_GET['sort'], $_GET['dir']);
        }
        $partners = $partners->where('country_id', session('country'))->paginate(8);

        return view('cms::partners.index')->withpartners($partners);
    }

    /*     * ************ create new partner && Save ********************* */

    public function getCreate() {

        if (Input::has('type')) {
            DB::transaction(function () {
                $languages = \Mediasci\Cms\Models\Languages::all();
                $partner = Partner::create(['media_id' => Input::get('featured_image'), 'type' => Input::get('type'), 'country_id' => session('country')]);
                foreach ($languages as $language) {
                    $partner_lang = new Partner_lang();
                    $partner_lang->partner_id = $partner->id;
                    $partner_lang->name = Input::get('name_' . $language->lang);
                    $partner_lang->description = Input::get('description_' . $language->lang);
                    $partner_lang->time = time();
                    $partner_lang->lang = $language->lang;
                    $partner_lang->save();
                }
            });
            Session()->push('partner_created', "New Partner has been added seccessfully.");
            return redirect('admin/partners');
        }
        return view('cms::partners.create');
    }

    /*     * ************ delete partner ********************* */

    public function destroy($id) {
        $partner = Partner::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($partner) && $partner->count() > 0) {
            $partnerlang = Partner_lang::where('partner_id', $id)->delete();
            $partner->delete();
        }
        return redirect('admin/partners');
    }

    /*     * *************** edit partner ********************* */

    public function edit($id) {
        $data["partner"] = $partner = Partner::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($partner) && $partner->count() > 0) {
            $data["partner_image"] = \Media::find($partner->media_id);
            $data["partner_langs"] = Array();
            foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
                $data["partner_langs"][$lang->lang] = DB::table('partners_lang')->where(['partner_id' => $id, 'lang' => $lang->lang])->first();
            }
            return view('cms::partners.edit', $data);
        } else {
            return redirect('admin/partners');
        }
    }

    /*     * *************** update partner ************************** */

    public function update($id) {
        $partner = Partner::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($partner) && $partner->count() > 0) {
            DB::transaction(function () use ($id) {
                $languages = \Mediasci\Cms\Models\Languages::all();
                $partner = Partner::where("id", $id)->where('country_id', session('country'))->first();
                $partner->media_id = Input::get('featured_image');
                $partner->type = Input::get('type');
                $partner->save();

                foreach ($languages as $language) {
                    $partner_lang = Partner_lang::where("partner_id", $id)->where("lang", $language->lang)->first();
                    $partner_lang->name = Input::get('name_' . $language->lang);
                    $partner_lang->description = Input::get('description_' . $language->lang);
                    $partner_lang->save();
                }
            });
            session()->push('m', "This partner has been updated seccessfully.");
        }
        return redirect('admin/partners');
    }

/////////////// end of class
}
