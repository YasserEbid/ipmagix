<?php

namespace Mediasci\Cms\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Lang;

class LocationsController extends \CmsController {

    public $data = [];

    private function filter() {
        $args = [
            'q' => $this->request->input('q'),
            'lang' => \Config::get("app.locale"),
            'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
        ];

        $this->data['current_location'] = null;
    }

    private function validator() {
        // $lang = $this->request->input('lang');
        $app_locales = \Mediasci\Cms\Models\Languages::all();
        // dd($app_locales);
// dd($this->request->all());
        $rules = [];
        $rules['longitude'] = 'required';
        $rules['latitude'] = 'required';
        foreach ($app_locales as $key => $locale) {
          // dd($locale);
            $rules['city_' .    $locale->lang] = 'required';
            $rules['address_' . $locale->lang] = 'required';
            $rules['phone_' . $locale->lang] = 'required';
            $rules['email_' . $locale->lang] = 'required';
        }

        $validator = \Validator::make($this->request->all(), $rules);
        // $validator->setAttributeNames(\Posts::get_attributes_names());
        return $validator;
    }

    public function getIndex() {
        $this->data['locations'] = \Mediasci\Cms\Models\SiteLocations::
        leftJoin("site_locations_lang","site_locations.id","=",'site_locations_lang.location_id')
        ->where("site_locations_lang.lang",(\Config::get("app.locale")))
        ->select("site_locations_lang.city","site_locations_lang.email","site_locations_lang.address","site_locations_lang.phone","site_locations.*")
        ->paginate(10);
        return view('cms::locations.index', $this->data);
    }

    public function anyCreate() {
        if ($this->request->isMethod('post')) {

            $validator = $this->validator();
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $reow = \Mediasci\Cms\Models\SiteLocations::where('longitude', $this->request->input('longitude'))
                            ->where('latitude', $this->request->input('latitude'))->get();

            if ($reow->count()) {
                session()->push('fail', trans('cms::locations.service_exist'));
                return redirect('admin/locations');
            }
            $location = new \Mediasci\Cms\Models\SiteLocations;
            $location->longitude = $this->request->input('longitude');
            $location->latitude = $this->request->input('latitude');

            if ($location->save()) {
                foreach (\Mediasci\Cms\Models\Languages::all() as $key => $locale) {
                    $locationsLangs = new \Mediasci\Cms\Models\LocationsLangs;
                    $locationsLangs->location_id = $location->id;
                    $locationsLangs->lang = $locale->lang;
                    $locationsLangs->city = $this->request->input('city_' . $locale->lang);
                    $locationsLangs->address = $this->request->input('address_' . $locale->lang);
                    $locationsLangs->phone = $this->request->input('phone_' . $locale->lang);
                    $locationsLangs->email = $this->request->input('email_' . $locale->lang);
                    $locationsLangs->save();
                }
            }

            session()->push('success', trans('cms::locations.service_created'));
            return redirect('admin/locations');
        }

        $this->data['current_location'] = null;

        return view('cms::locations.form', $this->data);
    }

    public function anyUpdate($id) {
        $location = \Mediasci\Cms\Models\SiteLocations::find($id);
        $location_langs = \Mediasci\Cms\Models\LocationsLangs::where('location_id', $id)->get();
        if ($this->request->isMethod('post')) {

            $validator = $this->validator();
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $reow = \Mediasci\Cms\Models\SiteLocations::where('longitude', $this->request->input('longitude'))
                    ->where('latitude', $this->request->input('latitude'))
                    ->where('id', '!=', $id)
                    ->get();

            if ($reow->count()) {
                session()->push('fail', trans('cms::locations.service_exist'));
                return redirect('admin/locations');
            }

            $location->longitude = $this->request->input('longitude');
            $location->latitude = $this->request->input('latitude');
            $location->save();

            $affectedRows = \Mediasci\Cms\Models\LocationsLangs::where('location_id', $id)->delete();
            foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                $locationsLangs = new \Mediasci\Cms\Models\LocationsLangs;
                $locationsLangs->location_id = $location->id;
                $locationsLangs->lang = $lang->lang;
                $locationsLangs->city = $this->request->input('city_' . $lang->lang);
                $locationsLangs->address = $this->request->input('address_' . $lang->lang);
                $locationsLangs->phone = $this->request->input('phone_' . $lang->lang);
                $locationsLangs->email = $this->request->input('email_' . $lang->lang);
                $locationsLangs->save();
            }

            session()->push('success', trans('cms::locations.service_updated'));
            return redirect('admin/locations');
        }

        $current_location = \Mediasci\Cms\Models\SiteLocations::where('id',$id)->first();
// dd($current_location);
        $location_langs = Array();
        foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
//			$conditions = ['posts.post_id' => $id,
//				'posts_langs.lang' => $lang,
//			];
            $location_langs[$lang->lang] = \Mediasci\Cms\Models\LocationsLangs::where(['location_id' => $id, 'lang' => $lang->lang])->first();
        }

        $this->data['current_location'] = $current_location;
        $this->data['locations_langs'] = $location_langs;


        if (!$current_location || !$location_langs) {
            session()->push('fail', trans('cms::locations.no_services'));
            return redirect('admin/locations');
        }

        return view('cms::locations.form', $this->data);
    }

    public function getDelete($id) {
        $location = \Mediasci\Cms\Models\SiteLocations::find($id);
        $locationLangs = \Mediasci\Cms\Models\LocationsLangs::where('location_id', $id)->get();
        foreach ($locationLangs as $locationLang) {
            $locationLang->delete();
        }
        $location->delete();
        $message = trans('cms::locations.success');
        session()->push('success', $message);
        return redirect('admin/locations');
    }

}
