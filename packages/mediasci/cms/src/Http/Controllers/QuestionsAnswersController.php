<?php

namespace Mediasci\Cms\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\QuestionsAnswers;
use Mediasci\Cms\Models\QuestionsAnswersLangs;
use DB;
use Lang;

class QuestionsAnswersController extends \CmsController {

    public $data = [];

    private function filter() {
        $args = [
            'q' => $this->request->input('q'),
            'lang' => \Config::get("app.locale"),
            'country_id' => session('country'),
            'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
        ];

        $this->data['questionsAnswers'] = \QuestionsAnswers::all($args);
        $this->data['current_questionAnswer'] = null;
    }

    private function validator() {
        $lang = $this->request->input('lang');
        $site_langs = array();
        $languages = \Mediasci\Cms\Models\Languages::select('lang')->get();
        foreach ($languages as $key => $value) {
            array_push($site_langs, $value->lang);
        }
        $app_locales = $lang ? [$lang] : $site_langs;

        foreach ($app_locales as $key => $locale) {
            $rules['question_' . $locale] = 'required';
            $rules['answer_' . $locale] = 'required';
        }

        $validator = \Validator::make($this->request->all(), $rules);
        $validator->setAttributeNames(QuestionsAnswers::get_attributes_names());
        return $validator;
    }

    public function index() {
        $this->data['questionsAnswers'] = new QuestionsAnswers;
        if (isset($_GET['sort'])) {


            $this->data['questionsAnswers'] = $this->data['questionsAnswers']->join('questions_answers_langs', 'questions_answers.id', '=', 'questions_answers_langs.question_answer_id')->where('questions_answers_langs.lang', "en")->orderBy('questions_answers_langs.' . $_GET['sort'], $_GET['dir']);
            $this->data['questionsAnswers'] = $this->data['questionsAnswers']->select('questions_answers.*');
        }
        $this->data['questionsAnswers'] = $this->data['questionsAnswers']->where('country_id', session('country'))->paginate(10);
        return view('cms::questionsAnswers.index', $this->data);
    }

    public function create() {

        if ($this->request->isMethod('post')) {

            $validator = $this->validator();
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $site_langs = array();
            $languages = \Mediasci\Cms\Models\Languages::select('lang')->get();
            foreach ($languages as $key => $value) {
                array_push($site_langs, $value->lang);
            }
            foreach ($site_langs as $key => $locale) {
                $reow = DB::table('questions_answers')
                        ->join('questions_answers_langs', 'questions_answers_langs.question_answer_id', '=', 'questions_answers.id')
                        ->where('question', $this->request->input('question_' . $locale))
                        ->where('lang', $locale)
                        ->get();

                if ($reow) {
                    session()->push('fail', trans('cms::questionsAnswers.questionAnswer_exist'));
                    return redirect('admin/questionsAnswers');
                }
            }

            DB::transaction(function () {
                $question_answer_id = DB::table('questions_answers')->insertGetId([
                    'active' => (Input::get('active')) ? Input::get('active') : 0,
                    'country_id' => session('country'),
                ]);
                foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                    DB::table('questions_answers_langs')->insert([
                        'question' => Input::get('question_' . $lang->lang),
                        'answer' => Input::get('answer_' . $lang->lang),
                        'lang' => $lang->lang,
                        'question_answer_id' => $question_answer_id
                    ]);
                }
            });

            session()->push('success', trans('cms::questionsAnswers.questionAnswer_created'));
            return redirect('admin/questionsAnswers');
        }

        $this->data['current_questionAnswer'] = null;

        return view('cms::questionsAnswers.form', $this->data);
    }

    public function update($id) {
        $questionAnswer = QuestionsAnswers::where("id", $id)->where('country_id', session('country'))->first();
        //$questionAnswerLangs = QuestionsAnswersLangs::where('question_answer_id', $id)->get();
        $site_langs = array();
        $languages = \Mediasci\Cms\Models\Languages::select('lang')->get();
        foreach ($languages as $key => $value) {
            array_push($site_langs, $value->lang);
        }
        if ($this->request->isMethod('post')) {

            $validator = $this->validator();
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            foreach ($site_langs as $key => $locale) {
                $reow = DB::table('questions_answers')
                        ->join('questions_answers_langs', 'questions_answers_langs.question_answer_id', '=', 'questions_answers.id')
                        ->where('question', $this->request->input('question_' . $locale))
                        ->where('lang', $locale)
                        ->where('questions_answers.id', '!=', $id)
                        ->get();

                if ($reow) {
                    session()->push('fail', trans('cms::questionsAnswers.questionAnswer_exist'));
                    return redirect('admin/questionsAnswers');
                }
            }

            DB::transaction(function($id) use ($id) {
                DB::table('questions_answers')->where('id', $id)->update([
                    'active' => (Input::get('active')) ? Input::get('active') : 0,
                ]);

                $affectedRows = DB::table('questions_answers_langs')->where('question_answer_id', $id)->delete();
                foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
                    DB::table('questions_answers_langs')->insert([
                        'question' => Input::get('question_' . $lang->lang),
                        'answer' => Input::get('answer_' . $lang->lang),
                        'lang' => $lang->lang,
                        'question_answer_id' => $id
                    ]);
                }
            });

            session()->push('success', trans('cms::questionsAnswers.questionAnswer_updated'));
            return redirect('admin/questionsAnswers');
        }
        if (is_object($questionAnswer) && $questionAnswer->count() > 0) {
            $current_questionAnswer = QuestionsAnswersLangs::where(['question_answer_id' => $id, 'lang' => \Config::get("app.locale")])->first();

            $questionAnswer_langs = Array();
            foreach ($site_langs as $lang) {
                $questionAnswer_langs[$lang] = QuestionsAnswersLangs::where(['question_answer_id' => $id, 'lang' => $lang])->first();
            }

            $this->data['current_questionAnswer'] = $current_questionAnswer;
            $this->data['questionAnswer_langs'] = $questionAnswer_langs;

            if (!$current_questionAnswer || !$questionAnswer_langs) {
                session()->push('fail', trans('cms::questionsAnswers.no_questionAnswer'));
                return redirect('admin/questionsAnswers');
            }

            return view('cms::questionsAnswers.form', $this->data);
        } else {
            return redirect('admin/questionsAnswers');
        }
    }

    public function delete($id) {
        $questionAnswer = QuestionsAnswers::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($questionAnswer) && $questionAnswer->count() > 0) {
            $questionsAnswersLangs = QuestionsAnswersLangs::where('question_answer_id', $id)->get();
            foreach ($questionsAnswersLangs as $questionAnswerLangs) {
                $questionAnswerLangs->delete();
            }
            $questionAnswer->delete();
            $message = trans('cms::questionsAnswers.success');
            session()->push('success', $message);
        }
        return redirect('admin/questionsAnswers');
    }

}
