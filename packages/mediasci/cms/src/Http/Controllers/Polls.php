<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\PollsOptionsLang;
use Mediasci\Cms\Models\Pollslang;
use DB;

/**
 * Description of Groups
 *
 * @author Elsayed Nofal
 */
class Polls extends CmsController {

    function index(){
        $data['polls']=  \Mediasci\Cms\Models\Polls::paginate(10);
        return view('cms::polls.index',$data);
    }
    function create(){
         if($this->request->isMethod('post')){
          //  dd($_POST);
            DB::beginTransaction();
            try{
                // insert poll
                $poll=new \Mediasci\Cms\Models\Polls();
                $poll->time=time();
                $poll->date=date('Y-m-d');
                 $poll->save();
                //insert poll option
                foreach($_POST as $key=>$value){
                    // insert poll lang
                    // dd($value);
                    $question=$value['question'];
                    $poll_lang= Pollslang::create([
                        'question'=>$question,
                        'poll_id'=>$poll->id,
                        'lang'=>$key
                    ]);
                    //insert poll option lang
                    $options=  explode(',', $value['options']);
                    foreach($options as $i=>$option){
                        if($i==0){
                          $poll_option[$i]=\Mediasci\Cms\Models\PollsOptions::create(['poll_id'=>$poll->id]);
                        }
                        $poll_option_lang= PollsOptionsLang::create([
                               'option'=>$option,
                               'option_id'=>$poll_option[0]->id,
                               'lang'=>$key
                        ]);
                    }
                }
                DB::commit();
            }catch(\Exception $e){
                DB::rollBack();
              return redirect('admin/polls')->with('m', trans('cms::polls.updatedm'));
            }
            return redirect('admin/polls')->with('n', trans('cms::polls.created'));
        }
           return view('cms::polls.create');
    }

    function update($id){
        $poll=  \Mediasci\Cms\Models\Polls::find($id);
        if($this->request->isMethod('post')){
            DB::beginTransaction();
            try{
                // DELTE OPTIONS
                DB::table('polls_options')->where('poll_id',$id)->delete();
                // delete old options lang
                //                DB::table('polls_options_lang')
                //                    ->join('polls_options','polls_options.id','=','polls_options_lang.option_id')
                //                    ->join('polls','polls.id','=','polls_options.poll_id')
                //                    ->where('polls.id',$id)->delete();

                foreach($_POST as $key=>$value){
                    // update question
                    $poll_lang=$poll->allLang()->lang()->where('lang',$key)->first();
                    $poll_lang->question=$value['question'];
                    $poll_lang->save();

                    //insert poll option lang
                    $options=  explode(',', $value['options']);
                    foreach($options as $i=>$option){
                        if($i==0){
                          $poll_option[$i]=\Mediasci\Cms\Models\PollsOptions::create(['poll_id'=>$poll->id]);
                        }
                        $poll_option_lang= PollsOptionsLang::create([
                                       'option'=>$option,
                                       'option_id'=>$poll_option[0]->id,
                                       'lang'=>$key
                        ]);
                    }

                }
                    DB::commit();
            }catch(\Exception $e){
                DB::rollBack();
              return redirect('admin/polls')->with('m', trans('cms::polls.updatedm'));
            }
                return redirect('admin/polls')->with('n', trans('cms::polls.updated'));
        }

        $data['poll']=$poll;
        return view('cms::polls.update',$data);
    }

    function delete($id){
        \Mediasci\Cms\Models\Polls::find($id)->delete();
        return redirect()->route('polls.index');
    }

    function result($id){
        $poll=  \Mediasci\Cms\Models\Polls::find($id);
        $poll_answer=DB::table('polls_answers')->select(DB::Raw('poll_id,option_id,count(*) as count'))
                ->where('poll_id',$id)
                ->groupBy(['poll_id','option_id'])->get();
        $total=DB::table('polls_answers')->select(DB::Raw('count(*) as total'))
                ->where('poll_id',$id)->groupBy('poll_id')->get();
        $poll_options = [];
        foreach ($poll->options as $key => $option) {
          $poll_options[$key] = ($option->lang()->where('lang',\Config::get("app.locale"))->get())->toArray();
        }
        // dd($poll_options);
        $data=[
            'poll'=>$poll,
            'answers'=>$poll_answer,
            'poll_options'=>$poll_options
        ];
		if(count($total)>0)
			$data['total']=$total[0]->total;
        return view('cms::polls.result',$data);
    }
}
