<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class CmsController extends BaseController {

	use AuthorizesRequests,
	 DispatchesJobs,
	 ValidatesRequests;

	private static $user;
	public $request;

	public function __construct(Request $request) {
             
            if(!isset($_SESSION['country']))
            {
                $_SESSION['country'] = 1 ;
            }
            
		view()->share('current_user', self::get_user());
		view()->share('request', $request);
		view()->share('sites', \Sites::get_all());
		view()->share('languages', \Mediasci\Cms\Models\Languages::all());
		view()->share('countries', \Mediasci\Cms\Models\Country::all());

		$this->request = $request;
	}

	public static function set_user($user) {
		self::$user = $user;
	}

	public static function get_user() {
		return self::$user;
	}

}
