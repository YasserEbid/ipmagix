<?php

namespace Mediasci\Cms\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\Cities;
use Mediasci\Cms\Models\CitiesLangs;
use DB;
use Lang;

class CitiesController extends \CmsController {

	public $data = [];

	private function filter() {
		$args = [
			'q' => $this->request->input('q'),
			'lang' => \Config::get("app.locale"),
			'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
		];

		$this->data['cities'] = \Cities::all($args);
		$this->data['current_city'] = null;
	}

	private function validator(){
		$langarray = array();
		foreach(\Mediasci\Cms\Models\Languages::all() as $onelang){
			$langarray[] = $onelang->lang;
		}
		$lang = $this->request->input('lang');
		$app_locales = $lang ? [$lang] : $langarray;



		foreach ($app_locales as $key => $locale) {
			$rules['name_'.$locale] = 'required';
		}

		$validator = \Validator::make($this->request->all(), $rules);
		$validator->setAttributeNames(Cities::get_attributes_names());
		return $validator;
	}

	public function index() {
		$this->data['cities'] = Cities::paginate(10);
		return view('cms::cities.index', $this->data);
	}

	public function create() {
		if ($this->request->isMethod('post')) {

			$validator = $this->validator();
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			foreach (\Mediasci\Cms\Models\Languages::all() as $key => $locale) {
				$reow=DB::table('cities')
					->join('cities_langs', 'cities_langs.city_id','=','cities.id')
					->where('name',$this->request->input('name_'.$locale->lang))
					->where('lang',$locale->lang)
					->get();

				if($reow){
					session()->push('fail', trans('cms::cities.city_exist'));
					return redirect('admin/cities');
				}
			}

			DB::transaction(function () {
				$city_id=  DB::table('cities')->insertGetId([
					'active'=>(Input::get('active'))?Input::get('active'):0,
				]);

				foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
					DB::table('cities_langs')->insert([
						'name'=>Input::get('name_'.$lang->lang),
						'lang'=>$lang->lang,
						'city_id'=> $city_id
					]);
				}
			});

			session()->push('success', trans('cms::services.city_created'));
			return redirect('admin/cities');
		}

		$this->data['current_city'] = null;

		return view('cms::cities.form', $this->data);
	}

	public function update($id) {
		$city = Cities::find($id);
		$city_langs = CitiesLangs::where('city_id', $id)->get();

		if ($this->request->isMethod('post')) {

			$validator = $this->validator();
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			foreach (\Mediasci\Cms\Models\Languages::all() as $key => $locale) {
				$reow=DB::table('cities')
					->join('cities_langs', 'cities_langs.city_id','=','cities.id')
					->where('name',$this->request->input('name_'.$locale->lang))
					->where('lang',$locale->lang)
					->where('cities.id','!=',$id)
					->get();

				if($reow){
					session()->push('fail', trans('cms::cities.city_exist'));
					return redirect('admin/cities');
				}
			}

			DB::transaction(function($id) use ($id) {
				DB::table('cities')->where('id', $id)->update([
					'active'=>(Input::get('active'))?Input::get('active'):0,
				]);

				$affectedRows = DB::table('cities_langs')->where('city_id', $id)->delete();
				foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
					DB::table('cities_langs')->insert([
						'name'=>Input::get('name_'.$lang->lang),
						'lang'=>$lang->lang,
						'city_id'=> $id
					]);
				}
			});

			session()->push('success', trans('cms::cities.city_updated'));
			return redirect('admin/cities');
		}

		$current_city = CitiesLangs::where(['city_id'=> $id,'lang'=>\Config::get("app.locale")])->first();

		$city_langs = Array();
		foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
			$city_langs[$lang->lang] = CitiesLangs::where(['city_id'=> $id,'lang'=>$lang->lang])->first();
		}

		$this->data['current_city'] = $current_city;
		$this->data['city_langs'] = $city_langs;

		if (!$current_city || !$city_langs) {
			session()->push('fail', trans('cms::cities.no_city'));
			return redirect('admin/cities');
		}

		return view('cms::cities.form', $this->data);
	}

	public function delete($id) {
		$city = Cities::find($id);
		$citiesLangs = CitiesLangs::where('city_id', $id)->get();
		foreach ($citiesLangs as $cityLangs){
			$cityLangs->delete();
		}
		$city->delete();
		$message = trans('cms::cities.success');
		session()->push('success', $message);
		return redirect('admin/cities');
	}

}
