<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

/**
 * Description of Groups
 *
 * @author Mahmoud
 */
class Tags extends \CmsController {

	public $data = [];

	private function filter() {
		$args = [
			'q' => $this->request->input('q'),
			'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
		];

		$this->data['tags'] = \Tags::get_all($args);

		$this->data['current_tag'] = null;
	}

	public function index() {
		$this->filter();

		return view('cms::tags.index', $this->data);
	}

	public function create() {
		if ($this->request->isMethod('post')) {

			$rules = ['tag_name' => 'required|unique:tags'];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Tags::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->route('tags.index')->withErrors($validator)->withInput();
			}

			$data = [
				'tag_name' => $this->request->input('tag_name')
			];

			\Tags::add($data);

			return redirect()->route('tags.index')->with('message', trans('cms::tags.tag_created'));
		}
	}

	public function edit($id) {
		if ($this->request->isMethod('post')) {
			$rules = [
				'tag_name' => 'required|unique:tags,tag_name,' . $id . ',tag_id'
			];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Tags::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$data = ['tag_name' => $this->request->input('tag_name')];

			\Tags::modify($data, $id);

			return redirect()->route('tags.index')->with('message', trans('cms::tags.tag_updated'));
		}

		$this->filter();

		$conditions = ['tag_id' => $id];
		$this->data['current_tag'] = \Tags::find_by($conditions);

		if (!$this->data['current_tag']) {
			return redirect()->route('tags.index')->with('message', trans('cms::tags.no_tag'));
		}

		return view('cms::tags.index', $this->data);
	}

	public function remove($id) {
		$conditions = ['tag_id' => $id];
		
		\Tags::remove($conditions);
		
		return redirect()->route('tags.index');
	}

}
