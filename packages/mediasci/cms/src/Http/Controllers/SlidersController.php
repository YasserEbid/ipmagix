<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\Slider;
use Mediasci\Cms\Models\Slider_lang;
use DB;

class SlidersController extends \CmsController {

    public function getIndex() {
        $sliders = Slider::where("country_id", session('country'))->paginate(7);
        return view('cms::sliders.index')->withsliders($sliders);
    }

    public function getCreate() {
        return view('cms::sliders.create');
    }

    public function postCreate() {
        DB::transaction(function () {
            $slider = Slider::create(['media_id' => Input::get('featured_image'),
                        'link' => Input::get('link'), "country_id" => session('country')]);

            foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
                $slider_lang = Slider_lang::create(['slider_id' => $slider->id,
                            'title' => Input::get($lang->lang . '_title'),
                            'description' => Input::get($lang->lang . '_description'),
                            'button_name' => Input::get($lang->lang . '_button'),
                            'lang' => $lang->lang]);
            }
        });
        $message = trans('cms::sliders.success');
        session()->push('m', $message);
        return redirect('admin/sliders');
    }

    public function getUpdate($id) {
        $slider = Slider::where("country_id", session('country'))->where("id", $id)->first();
        if (is_object($slider) && $slider->count() > 0) {
            return view('cms::sliders.update')->withslider($slider);
        } else {
            return redirect('admin/sliders');
        }
    }

    public function postUpdate($id) {
        $slider = Slider::find($id);
        $media_id = Input::get('featured_image');
        $slider->link = Input::get('link');

        if (empty($media_id)) {

        } else {
            $slider->media_id = $media_id;
        }

        foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
            $lanslider = $slider->sliderlang($lang->lang);
            if (is_object($lanslider) && $lanslider->count() > 0) {
                $lanslider->title = Input::get($lang->lang . '_title');
                $lanslider->button_name = Input::get($lang->lang . '_button');
                $lanslider->description = Input::get($lang->lang . '_description');
                $lanslider->save();
            }
        }
        $slider->save();
        $message = trans('cms::sliders.success');
        session()->push('m', $message);
        return redirect('admin/sliders');
    }

    public function getDelete($id) {
        $slider = Slider::where("country_id", session('country'))->where("id", $id)->first();
        if (is_object($slider) && $slider->count() > 0) {
            $sliderlang = Slider_lang::where('slider_id', $id)->delete();
            $slider->delete();
        }
        return redirect()->back();
    }

}
