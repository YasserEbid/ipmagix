<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Support\Facades\Input;
/**
 * Description of Groups
 *
 * @author Elsayed Nofal
 */
class SmsController extends \CmsController {

    function index(){
        return view('cms::sms_view.index')
        ->with('smss', \Mediasci\Cms\Models\SmsModel::all());
    }

    function create(){
        if(Input::has('message')){
            $sms=  new \Mediasci\Cms\Models\SmsModel();
            if(!$sms->validate(Input::all())){
                \Illuminate\Support\Facades\Session::flash('errors',$sms->errors());
            }else{
                $sms->create(Input::all());
                \Illuminate\Support\Facades\Session::flash('message','“SMS inserted successfully”');
                return redirect()->route('sms');
            }
        }
        return view('cms::sms_view.create');
    }

    function update($id){
        $sms=  \Mediasci\Cms\Models\SmsModel::find($id);
        if(Input::has('message')){
            if(!$sms->validate(Input::all())){
                \Illuminate\Support\Facades\Session::flash('errors',$sms->errors());
            }else{
                $sms->update(Input::all());
                \Illuminate\Support\Facades\Session::flash('message','SMS updated successfully');
                return redirect()->route('sms');
            }
        }
        return view('cms::sms_view.update')->with('sms',$sms);
    }

    function delete($id){
        $sms=  \Mediasci\Cms\Models\SmsModel::find($id);
        if($sms){
            $sms->delete();
            \Illuminate\Support\Facades\Session::flash('message','SMS deleted successfully');
        }else{
            \Illuminate\Support\Facades\Session::flash('message','error occure try again');
        }
        return redirect()->route('sms');
    }
    function send(){
        $smses= \Mediasci\Cms\Models\SmsModel::where('completed',0)->get();

       if($smses){

               $mails=\Mediasci\Cms\Models\MailList::whereNotNull('phone')->where('phone','!=','')->get();
              $cout=0;
//               foreach($mails as $mail ){

                   $message='hi salma this is atest message sorry for annoying you';
                   $phone = '971507348636';
                   //http://78.108.164.69:8080/websmpp/websms?user=sci&pass=sci123&sid=ICLDC&mno=971507348636&type=1&text=Test
                   $link='http://78.108.164.69:8080/websmpp/websms?user=sci&pass=sci123&sid=ICLDC&mno='.$phone.'&type=1&text='.$message;
                   $data=file_get_contents($link);
                   echo $data .'<br/>';
                   die;

                   $cout=$cout+1;
//               }
               $mail->count=$cout;
               $mail->completed=1;
               $mail->save();

       }
    }
    function get_ip() {
		//Just get the headers if we can or else use the SERVER global
		if ( function_exists( 'apache_request_headers' ) ) {
			$headers = apache_request_headers();

		} else {
			$headers = $_SERVER;
		}
		//Get the forwarded IP if it exists
		if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
			$the_ip = $headers['X-Forwarded-For'];
                        dd('h');
		} elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
		) {
			$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
		} else {

			$the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
		}
		return $the_ip;
	}
}
