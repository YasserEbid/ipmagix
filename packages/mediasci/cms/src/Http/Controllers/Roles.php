<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

/**
 * Description of Groups
 *
 * @author Mahmoud
 */
class Roles extends \CmsController {

	public function index() {
		$roles = \Roles::get_all();
		return view('cms::roles.index', ['roles' => $roles]);
	}

	public function create() {

		if ($this->request->isMethod('post')) {
			$roles_data = ['role_name' => $this->request->input('role_name')];
			$rol_id = \Roles::add($roles_data);
			\RolesRoutes::addRoles($this->request->all(), $rol_id);
			return redirect()->route('roles.index')->with('message', trans('cms::role.created'));
		}

		$groups = \Groups::get_all_herically();
		return view('cms::roles.create', ['groups' => $groups]);
	}

	public function edit($id) {

		if ($this->request->isMethod('post')) {
			$roles_data = ['role_name' => $this->request->input('role_name')];
			\Roles::modify($roles_data, $id);
			\RolesRoutes::addRoles($this->request->all(), $id);
			return redirect()->route('roles.index')->with('message', trans('cms::role.updated'));;
		}

		$conditions = ['roles.role_id' => $id];
		$current_role = \Roles::find_by($conditions);
		$groups = \Groups::get_all_herically($id);
		return view('cms::roles.edit', ['current_role' => $current_role, 'groups' => $groups]);
	}

	public function remove($id) {
		$conditions = ['role_id' => $id];
		if ($id != 1) {
			\Roles::remove($conditions);
		}
		return redirect()->route('roles.index');
	}

}
