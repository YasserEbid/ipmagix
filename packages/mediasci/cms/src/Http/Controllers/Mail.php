<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use DB;

/**
 * Description of Groups
 *
 * @author Elsayed Nofal
 */
class Mail extends CmsController {

    function themes(){
        if(isset($_POST['header']) && isset($_POST['footer'])){
            $theme=new \Mediasci\Cms\Models\MailThemes();
            unset($_POST['site_id']);
            $theme->fill($_POST);
            $theme->save();
        }
        $data['themes']=  \Mediasci\Cms\Models\MailThemes::all();
        return view('cms::mail.themes',$data);
    }
    function create_theme(){
        if(isset($_POST['header']) && isset($_POST['footer'])){
            $theme=new \Mediasci\Cms\Models\MailThemes();
            unset($_POST['site_id']);
            $theme->fill($_POST);
            $theme->save();
              return redirect(route('mail.themes'));
        }
        $data[]=  '';
        return view('cms::mail.create_theme',$data);
    }

    function showtheme($id){
        $data['data']=  \Mediasci\Cms\Models\MailThemes::find($id);
        return view('cms::mail.show_theme',$data);
    }

    function updatetheme($id){
        $theme=  \Mediasci\Cms\Models\MailThemes::find($id);
        if(Input::has('name') && Input::has('header') && Input::has('footer')){
            $theme->name=  Input::get('name');
            $theme->header=  Input::get('header');
            $theme->save();
            return redirect(route('mail.themes'));
        }
        $data['data']=$theme;
        return view('cms::mail.update_theme',$data);
    }

    function deletetheme($id){
        \Mediasci\Cms\Models\MailThemes::find($id)->delete();
        return redirect(route('mail.themes'));
    }

    //------------------------------Message Area ------------------------------

    function createMeaage(){
        $data=[];
        if(Input::has('content')){
            $mail_message=new \Mediasci\Cms\Models\MailMessages();
            if($mail_message->validate(Input::all())){
               $mail_message->fill(Input::all());
               unset($mail_message->nationality_id);
                unset($mail_message->files);
               $mail_message->save();
              $mail= \Mediasci\Cms\Models\MailMessages::find($mail_message->id);
              $text=$mail->content ;
                  $test= preg_match_all('/(http|https|ftp|ftps)(\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3})(\/[a-zA-Z0-9\-\_\/]*)?/', $mail->content ,$matches);
                  foreach($matches[0] as $match){
                      $text= str_replace($match,$match.'?$mail_id='.$mail->id.'&mail='.$mail->id , $text);
                  }
              $mail->content=$text;
                  $mail->save();

               return redirect(route('mail.messages'));
            }else{
                $data['v_errors']=$mail_message->errors();
           }
        }
       return view('cms::mail.create_message',$data);
    }

    function messagesHome(){
        $data['messages']=  \Mediasci\Cms\Models\MailMessages::where('is_finished',0)->get();
        return view('cms::mail.messages_home',$data);
    }

    function updateMessage($id){
        $message=  \Mediasci\Cms\Models\MailMessages::find($id);
        if(Input::has('subject')){
            if($message->validate(Input::all())){
                $message->fill(Input::all());
                $message->save();
                              $mail= \Mediasci\Cms\Models\MailMessages::find($message->id);
                              $text=$mail->content ;
                  $test= preg_match_all('/(http|https|ftp|ftps)(\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3})(\/[a-zA-Z0-9\-\_\/]*)?/', $mail->content ,$matches);
                  foreach($matches[0] as $match){
                      $text= str_replace($match,$match.'?$mail_id='.$mail->id.'&mail='.$mail->id , $text);
                  }
              $mail->content=$text;
                  $mail->save();
                return redirect(route('mail.messages'));
            }else{
                $data['v_errors']=$message->errors();
            }
        }
        $data['message']=$message;
        return view('cms::mail.update_message',$data);
    }

    function deleteMessage($id){
        $mail_message=  \Mediasci\Cms\Models\MailMessages::find($id)->delete();
        return redirect(route('mail.messages'));
    }

    // sending message
    function send(){
             $mail_limit=100;

        $domain_name = url('/');
        $message=  \Mediasci\Cms\Models\MailMessages::where('is_finished',0)
                ->orderBy('id','asc')->get();
        if(count($message->toArray())==0)
            die;
        $message=$message[0];
        if($message->start==0){
            $message->start=time();
            $message->save();
        }
        /*
        * 'diabetes_id','type_visitor_id',,'nationality_id','p_interests_id','s_interests_id',
        * 'case_id'
        */
        $mail_list=  \Mediasci\Cms\Models\MailList::where('status',0);
        $properties=['gender_id','lifestyle_id',
            'location_id'];
        foreach($properties as $key){
            if($message->$key!=0){
                $mail_list->where($key,$message->$key);
            }
        }
        $mail_count=$mail_list->count();

        if($mail_count==0){
            $message->end=time();
            $message->is_finished=1;
            $message->save();
            die();
        }
        if($mail_count!=0 && $mail_count<$mail_limit){
            $message->end=time();
            $message->is_finished=1;
            $message->save();
        }
        foreach($mail_list->get() as $email){
          $content=$this->genrateTrackingCode($message->id, $email->id);
          $message->content =  str_replace('__username__', $email->name, $message->content);
            $content.=$message->theme->header."<br/>".$message->content."<br/>".$message->theme->footer;
            $content=  str_replace('./', $domain_name, $content);
            //echo $content;die;
            $headers  = 'MIME-Version: 1.0' . "\r\n";

            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            $headers .= 'From: info@icldc.ae' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            echo $email->email."<br/>";
            echo $email->subject."<br/>";
            echo $content."<br/>";
            echo "<hr><br/>";
            $mail=mail($email->email, $message->subject, $content,$headers);
            if($mail){
                echo "message sent succssfuly";
            }
            //send_mail($email->email, $message->subject, $content);
            $message->sent_count+=1;
            $message->save();
            $message->content =  str_replace($email->name,'__username__' , $message->content);
            $email->status=1;
            $email->save();
        }
        if($mail_count<$mail_limit){
            DB::table('mail_list')->update(['status'=>0]);
        }

    }

    private function genrateTrackingCode($message_id,$email_id){
        return "<img style='display:none' src='".url('/admin/mail/track?message_id=$message_id&email_id=$email_id')."' width='1'/>";
    }

    function track(){
        if(isset($_GET['message_id']) && isset($_GET['email_id'])){
            $mail_track=new \Mediasci\Cms\Models\MailTrack();
            $mail_track->message_id=$_GET['message_id'];
            $mail_track->email_id=$_GET['email_id'];
            $mail_track->time=time();
            $mail_track->save();
        }

        pixel();die;
    }

    // component for mail
    public static function themesSelect($value=''){
        $html='<select name="theme_id" class="form-control" required>';
        $html.='<option value="">choose one</option>';
        foreach(\Mediasci\Cms\Models\MailThemes::all() as $row){
            if($value!=''){
                $html.='<option value="'.$row->id.'" '.component\Helpers::selected($row->id, $value).'>'.$row->name.'</option>';
            }else{
                $html.='<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        }
        $html.='</select>';
        return $html;
    }


    //statistics
    function statistics($id){
        $sent=  \Mediasci\Cms\Models\MailMessages::find($id);


        $opend_count= \Mediasci\Cms\Models\MailTrack::where('message_id',$id)
                ->groupBy('message_id')->count();
        return view('cms::mail.statistic')
                ->with('sent', $sent)
                ->with('opend_count', $opend_count);
    }


}
