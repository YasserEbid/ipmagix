<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\Contactus;
use DB;

class contactRequestsController extends \CmsController {

  public function allRequests(){
    $all_requests = Contactus::all();
    return view('cms::contact-us.contact-us')->with('all_requests',$all_requests);
  }

  public function deleteRequest($id) {
    $contact_request = Contactus::find($id);
    $contact_request->delete();
    $message = trans('cms::contact_requests.success');
    session()->push('success', $message);
    return redirect('admin/contact-us');
  }

}
