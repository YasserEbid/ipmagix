<?php

namespace Mediasci\Cms\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Mediasci\Cms\Models\FAQCategories;
use Mediasci\Cms\Models\FAQCategoriesLangs;
use DB;
use Lang;

class FAQCategoriesController extends \CmsController {

	public $data = [];

	public function index() {
		$this->data['faqCategories'] = FAQCategories::paginate(10);
		return view('cms::faqCategories.index', $this->data);
	}

	public function create() {
		if ($this->request->isMethod('post')) {

			$validator = $this->validator();
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			foreach (\Mediasci\Cms\Models\Languages::all() as $key => $locale) {
				$reow=DB::table('faq_categories')
					->join('faq_categories_langs', 'faq_categories_langs.category_id','=','faq_categories.id')
					->where('name',$this->request->input('name_'.$locale->lang))
					->where('lang',$locale->lang)
					->get();

				if($reow){
					session()->push('fail', trans('cms::faqCategories.faqCategories_exist'));
					return redirect('admin/faqCategories');
				}
			}

			DB::transaction(function () {
				$category_id=  DB::table('faq_categories')->insertGetId([
					'active'=>(Input::get('active'))?Input::get('active'):0,
				]);

				foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
					DB::table('faq_categories_langs')->insert([
						'name'=>Input::get('name_'.$lang->lang),
						'lang'=>$lang->lang,
						'category_id'=> $category_id
					]);
				}
			});

			session()->push('success', trans('cms::faqCategories.faqCategories_created'));
			return redirect('admin/faqCategories');
		}

		$this->data['current_category'] = null;

		return view('cms::faqCategories.form', $this->data);
	}

	public function update($id) {
		$faqCategory = FAQCategories::find($id);

		if ($this->request->isMethod('post')) {

			$validator = $this->validator();
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			foreach (\Mediasci\Cms\Models\Languages::all() as $key => $locale) {
				$reow=DB::table('faq_categories')
					->join('faq_categories_langs', 'faq_categories_langs.category_id','=','faq_categories.id')
					->where('name',$this->request->input('name_'.$locale->lang))
					->where('lang',$locale->lang)
					->where('faq_categories.id','!=',$id)
					->get();

				if($reow){
					session()->push('fail', trans('cms::faqCategories.faqCategories_exist'));
					return redirect('admin/faqCategories');
				}
			}

			DB::transaction(function($id) use ($id) {
				DB::table('faq_categories')->where('id', $id)->update([
					'active'=>(Input::get('active'))?Input::get('active'):0,
				]);

				$affectedRows = DB::table('faq_categories_langs')->where('category_id', $id)->delete();
				foreach (\Mediasci\Cms\Models\Languages::all() as $key => $lang) {
					DB::table('faq_categories_langs')->insert([
						'name'=>Input::get('name_'.$lang->lang),
						'lang'=>$lang->lang,
						'category_id'=> $id
					]);
				}
			});

			session()->push('success', trans('cms::faqCategories.faqCategories_updated'));
			return redirect('admin/faqCategories');
		}

		$current_category = FAQCategoriesLangs::where(['category_id'=> $id,'lang'=>\Config::get("app.locale")])->first();

		$category_langs = Array();
		foreach (\Mediasci\Cms\Models\Languages::all() as $lang) {
			$category_langs[$lang->lang] = FAQCategoriesLangs::where(['category_id'=> $id,'lang'=>$lang->lang])->first();
		}

		$this->data['current_category'] = $current_category;
		$this->data['category_langs'] = $category_langs;

		if (!$current_category || !$category_langs) {
			session()->push('fail', trans('cms::faqCategories.no_faqCategory'));
			return redirect('admin/faqCategories');
		}

		return view('cms::faqCategories.form', $this->data);
	}

	public function delete($id) {
		$faqCategory = FAQCategories::find($id);
		$faqCategoryLangs = FAQCategoriesLangs::where('category_id', $id)->get();
		foreach ($faqCategoryLangs as $faqCategoryLang){
			$faqCategoryLang->delete();
		}
		$faqCategory->delete();
		$message = trans('cms::faqCategories.success');
		session()->push('success', $message);
		return redirect('admin/faqCategories');
	}

	private function validator(){
		$langarray = array();
		foreach(\Mediasci\Cms\Models\Languages::all() as $onelang){
			$langarray[] = $onelang->lang;
		}
		$lang = $this->request->input('lang');
		$app_locales = $lang ? [$lang] : $langarray;



		foreach ($app_locales as $key => $locale) {
			$rules['name_'.$locale] = 'required';
		}

		$validator = \Validator::make($this->request->all(), $rules);
		$validator->setAttributeNames(FAQCategories::get_attributes_names());
		return $validator;
	}

}
