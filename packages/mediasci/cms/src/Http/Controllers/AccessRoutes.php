<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;


/**
 * Description of Routes
 *
 * @author Mahmoud
 */
class AccessRoutes extends \CmsController {

	public function index() {
		$routeCollection= \Route::getRoutes();
		$groups=\Groups::get_all();
		
		if ($this->request->isMethod('post')) {
			
			for($i=0;$i<count($this->request->input('route'));$i++){
				
				$check_name=str_replace(".","_",$this->request->input('route')[$i]);
				$checked=$this->request->input($check_name)==1?1:0;
				$group_id=$this->request->input($check_name.'_select');
				$conventional_name=$this->request->input('route_conventional_name')[$i];
				$route_name=$this->request->input('route')[$i];
				
				$conditions = ['routes.route_name' =>$route_name ];
				$result = \Routes::find_by($conditions);
				
				if($result){
					$data=['conventional_name' =>$conventional_name ,
						'group_id'=>$group_id,
						'route_type'=>$checked];
					\Routes::modify($data,$result->route_id);
				} else{
					$data=['route_name' => $route_name,
							'conventional_name' => $conventional_name,
							 'group_id'=>$group_id,
							 'route_type'=>$checked];
					\Routes::add($data);
				}
				
			}
			return redirect()->route('routes.index')->with('message', trans('cms::routes.updated'));;
		}
		return view('cms::routes.index',['routeCollection' => $routeCollection,'groups'=>$groups]);
	}

}
