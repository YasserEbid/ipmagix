<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

/**
 * Description of Groups
 *
 * @author Mahmoud
 */
class Blocks extends \CmsController {

	public $data = [];

	private function filter() {
		$args = [
			'q' => $this->request->input('q'),
			'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
			'site_id' => $this->request->input('site_id') ? $this->request->input('site_id') : 1,
		];

		$this->data['blocks'] = \Blocks::get_all($args);

		$this->data['current_block'] = null;
	}

	public function index() {
		$this->filter();

		return view('cms::blocks.index', $this->data);
	}

	public function create() {
		if ($this->request->isMethod('post')) {
			
			$site_id=$this->request->input('site_id') ? $this->request->input('site_id') : 1;
			
			$rules = ['block_name' => 'required|unique:blocks',
					  'max_posts' => 'required|numeric', 
					  'site_id' => 'required|numeric', 
					 ];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Blocks::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->route('blocks.index', ['site_id' => $site_id ] )->withErrors($validator)->withInput();
			}

			$data = ['block_name' => $this->request->input('block_name'),
					 'max_posts' => $this->request->input('max_posts'),
				     'site_id' => $this->request->input('site_id'),
					];

			\Blocks::add($data);

			return redirect()->route('blocks.index', ['site_id' => $site_id ])->with('message', trans('cms::blocks.block_created'));
		}
	}

	public function edit($id) {
		if ($this->request->isMethod('post')) {
			$site_id=$this->request->input('site_id') ? $this->request->input('site_id') : 1;
			$rules = [
				'block_name' => 'required|unique:blocks,block_name,' . $id . ',block_id',
				'max_posts' => 'required|numeric', 
				'site_id' => 'required|numeric', 
			];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Blocks::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$data = ['block_name' => $this->request->input('block_name'),
					 'max_posts' => $this->request->input('max_posts'),
				     'site_id' => $this->request->input('site_id'),
					];

			\Blocks::modify($data, $id);

			return redirect()->route('blocks.index', ['site_id' => $site_id ] )->with('message', trans('cms::blocks.block_updated'));
		}

		$this->filter();

		$conditions = ['block_id' => $id];
		$this->data['current_block'] = \Blocks::find_by($conditions);

		if (!$this->data['current_block']) {
			return redirect()->route('blocks.index')->with('message', trans('cms::blocks.no_block'));
		}

		return view('cms::blocks.index', $this->data);
	}

	public function remove($id) {
		$conditions = ['block_id' => $id];
		
		\Blocks::remove($conditions);
		
		return redirect()->route('blocks.index');
	}
	
	public function posts($id){
		if ($this->request->isMethod('post')) {
			$rules = [
				'posts_ids' => 'required'
			];

			$validator = \Validator::make($this->request->all(), $rules);
			$validator->setAttributeNames(\Blocks::get_attributes_names());

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$id=$this->request->input('block_id');
			$data = explode(",",$this->request->input('posts_ids'));
			\BlocksPosts::add($data, $id);

			return redirect()->route('blocks.index')->with('message', trans('cms::blocks.block_posts_updated'));
		}
		$this->data['current_block_posts']=\BlocksPosts::get_posts_by_block_id($id);
		
		$conditions = ['block_id' => $id];
		$this->data['current_block'] = \Blocks::find_by($conditions);

		return view('cms::blocks.create', $this->data);
	}

}
