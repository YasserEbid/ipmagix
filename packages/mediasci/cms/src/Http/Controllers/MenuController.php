<?php

namespace Mediasci\Cms\Http\Controllers;

use App\Http\Requests;
use Request;
use Illuminate\Support\Facades\Input;
use DB;
use Mediasci\Cms\Models\Menu;
use Mediasci\Cms\Models\Menulang;
use Mediasci\Cms\Models\Menulink;
use Mediasci\Cms\Models\Menulinks_lang;

class MenuController extends \CmsController {

    function index() {
        $data['menus'] = Menu::where('country_id', session('country'))->get();
        return view('cms::menus.allmenus', $data);
    }

    function show($id) {
        $data['menus'] = Menu::where("id", $id)->where('country_id', session('country'))->first();
        $data['mastermenus'] = Menulink::where('country_id', session('country'))->where('menu_id', $id)->where('parent_id', Null)->get();
        return view('cms::menus.menulinks', $data);
    }

    function showsub($id) {
        $data['menu'] = Menulink::where("id", $id)->where('country_id', session('country'))->first();
        $data['mainmenus'] = Menulink::where('country_id', session('country'))->where('parent_id', $id)->get();
        return view('cms::menus.submenus', $data);
    }

    function create($id1 = '', $id2 = '') {
        $data['id1'] = $id1;
        $data['id2'] = $id2;
        if ($id2) {

        } else {
            //  $date['id2']='';
        }
        $data['menus'] = Menu::where('country_id', session('country'))->get();
        $data['mainmenus'] = Menulink::where('country_id', session('country'))->get();
        if (Request::isMethod('post')) {
            //    dd(Input::get('mainmenu'));
            $mainmenu_id = Input::get('mainmenu');
            $menu_id = Input::get('menu');
            DB::transaction(function () {
                $languages = \Mediasci\Cms\Models\Languages::all();
                if (input::get('active') == Null) {
                    $active = 0;
                } else {
                    $active = 1;
                }
                $parent = Input::get('mainmenu');
                $menulink = Menulink::create([
                            'menu_id' => Input::get('menu'),
                            'parent_id' => $parent ? $parent : null,
                            'country_id' => session('country'),
                            'active' => $active,
                            'order' => Input::get('order'),
                            'link_url' => Input::get('link')
                ]);
                foreach ($languages as $lang) {
                    $langmenulink = Menulinks_lang::create([
                                'name' => Input::get($lang->lang . '_name'),
                                'lang' => $lang->lang,
                                'menu_link_id' => $menulink->id
                    ]);
                }
            });
            if ($mainmenu_id == '') {
                return redirect('admin/menus/' . $menu_id);
            } else {
                return redirect('admin/menus/show/' . $mainmenu_id);
            }
        }
        return view('cms::menus.create', $data);
    }

    function update($id) {
        $data['menus'] = Menu::where('country_id', session('country'))->get();
        $languages = \Mediasci\Cms\Models\Languages::all();
        $data['mainmenus'] = Menulink::where('country_id', session('country'))->get();
        $data['menu'] = Menulink::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($data["menu"]) && $data["menu"]->count() > 0) {
            if (input::get('en_name')) {
                $mainmenu_id = Input::get('mainmenu');
                $menu_id = Input::get('menu');
                if (input::get('active') == Null) {
                    $active = 0;
                } else {
                    $active = 1;
                }
                $parent = Input::get('mainmenu');
                $menu = Menulink::find($id);
                $menu->menu_id = Input::get('menu');
                $menu->parent_id = $parent ? $parent : null;
                $menu->active = $active;
                $menu->order = Input::get('order');
                $menu->link_url = Input::get('link');
                foreach ($languages as $lang) {
                    $langmenu = $menu->menulinklang($lang->lang);
                    if (is_object($langmenu) && $langmenu->count() > 0) {
                        $langmenu->name = Input::get($lang->lang . '_name');
                        $langmenu->save();
                    } else {
                        $langmenulink = Menulinks_lang::create([
                                    'name' => Input::get($lang->lang . '_name'),
                                    'lang' => $lang->lang,
                                    'menu_link_id' => $id
                        ]);
                    }
                }
                $menu->save();
                if ($mainmenu_id == '') {
                    return redirect('admin/menus/' . $menu_id);
                } else {
                    return redirect('admin/menus/show/' . $mainmenu_id);
                }
            }
            // dd($data);
            return view('cms::menus.update', $data);
        } else {
            return redirect('admin/menus/' . $id);
        }
    }

    function delete($id) {
        $menu = Menulink::where("id", $id)->where('country_id', session('country'))->first();
        if (is_object($menu) && $menu->count() > 0) {
            $menu_id = $menu->menu_id;
            $parent_id = $menu->parent_id;
            if ($menu->parent_id == null) {
                if ($menu->submenu) {
                    foreach ($menu->submenu as $menulink) {
                        $menulinklang = Menulinks_lang::where('menu_link_id', $menulink->id)->delete();
                        $menulink->delete();
                    }
                }
            } else {

            }
            $menulang = Menulinks_lang::where('menu_link_id', $menu->id)->delete();
            $menu->delete();
            if ($parent_id) {
                return redirect('admin/menus/show/' . $parent_id);
            } else {
                return redirect('admin/menus/' . $menu_id);
            }
        }else{
            return redirect('admin/menus/show/' . $id);
        }
    }

}
