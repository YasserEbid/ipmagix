<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Http\Request;
use DB;

/**
 * Description of User
 *
 * @author Darshevo
 */
class Home extends \CmsController {

    public $data = [];

    public function index() {
        // posts
        $args = [
            'lang' => \Config::get("app.locale"),
            "country_id" => $_SESSION["country"],
            'per_page' => 5,
        ];
        $args1 = [
            'lang' => \Config::get("app.locale"),
            "country_id" => session('country')
        ];
        $this->data['recent_posts'] = \Posts::get_all($args);
        $this->data['recent_users'] = \User::get_all($args);
        $totposts = \Posts::get_all($args1);
        $this->data['total_posts'] = count($totposts);
        $this->data['total_users'] = \User::init()->count();

        $this->data['dashboard_sort_items'] = DB::table('dashboard_sort_items')->orderBy('sort')->get();
        ;

        return view('cms::home.index', $this->data);
    }

    public function dashboardSorting(Request $request) {
        if ($request->ajax()) {
            //echo "<pre>";print_r($_POST['items']);die;
            foreach ($_POST['items'] as $key => $item) {
                //echo $item;die;
                $sort = $key + 1;
                $tem_exist = DB::table('dashboard_sort_items')->where('item', $item)->first();
                if ($tem_exist) {
                    DB::table('dashboard_sort_items')->where('item', $item)->update([
                        'sort' => $sort
                    ]);
                } else {
                    DB::table('dashboard_sort_items')->insert([
                        'item' => $item,
                        'sort' => $sort
                    ]);
                }
            }
        }
    }

    public function chartbeat() {
        return view('cms::home.chartbeat');
    }

    public function changeCountry($id) {
        $check = \Mediasci\Cms\Models\Country::where("id", $id)->first();
        if (is_object($check) && $check->count() > 0) {
            // $_SESSION["country"] = $id;
            \Session::put('country', $id);            
        }
        return redirect()->route('dashboard');
    }

}
