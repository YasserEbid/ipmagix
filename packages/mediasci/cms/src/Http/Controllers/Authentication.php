<?php

namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Description of User
 *
 * @author Darshevo
 */
class Authentication extends \CmsController {

    public function login(Request $request) {

        if (\Session::has('admin_user_id')) {
            // $_SESSION["country"] = 1;
            \Session::put('country', 1);
            return redirect()->route('dashboard');
        }

        if ($request->isMethod('post')) {
            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];

            $this->validate($request, $rules);

            $user = \User::login($request->input('email'), $request->input('password'));

            if ($user) {
                // $_SESSION["country"] = 1;
                \Session::set('country', 1);
                return redirect()->route('dashboard');
            } else {
                return back()->withInput($request->except('password'))->with('message', "this user doesn't exist");
            }
        }

        return view('cms::auth.login');
    }

    public function logout() {
        \Session::forget('admin_user_id');
        // unset($_SESSION["country"]);
        \Session::forget('country');
        return redirect()->route('auth.login');
    }

}
