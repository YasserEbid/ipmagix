<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mediasci\Cms\Http\Controllers;

use App\Models\posts_langs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Request as rquest;
/**
 * Description of Groups
 *
 * @author Ali
 */
class Posts extends \CmsController {

    public $data = [];
    public $site_langs = array();
    private $languages;

    public function __construct(Request $request) {
        parent::__construct($request);
        $this->site_id = $this->request->input('site_id') ? $this->request->input('site_id') : 1;
        $this->languages = \Mediasci\Cms\Models\Languages::select('lang')->get();
        foreach ($this->languages as $key => $value) {
            array_push($this->site_langs, $value->lang);
        }
    }

    private function filter() {
        $args = [
            'q' => $this->request->input('q'),
            'lang' => \Config::get("app.locale"),
            'country_id' => session('country'),
            'per_page' => $this->request->input('per_page') ? $this->request->input('per_page') : 10,
            'site_id' => $this->request->input('site_id') ? $this->request->input('site_id') : 1,
            'title' => \Illuminate\Support\Facades\Input::get('sort') ? \Illuminate\Support\Facades\Input::get('dir') : Null,
        ];
        $url_type = 'products';
        if (rquest::is('admin/news')) {
          $url_type = 'news';
          // dd(rquest::url());
        }
        $args['url_type'] = $url_type;
        $this->data['posts'] = \Posts::get_all($args);
        $this->data['current_post'] = null;
    }

    private function validator() {
        $lang = $this->request->input('lang');
        $app_locales = $lang ? [$lang] : $this->site_langs;

        $rules = ['site_id' => 'required|numeric'];

        foreach ($app_locales as $key => $locale) {
            $rules['post_title_' . $locale] = 'required';
            $rules['post_subtitle_' . $locale] = 'required';
            $rules['post_brief_' . $locale] = 'required';
            $rules['post_content_' . $locale] = 'required';
        }

        $validator = \Validator::make($this->request->all(), $rules);
        $validator->setAttributeNames(\Posts::get_attributes_names());
        return $validator;
    }

    public function index() {
        $this->filter();
        if (rquest::is('admin/news')) {
          return view('cms::news.index', $this->data);
        }else {
          return view('cms::posts.index', $this->data);
        }
    }

    public function create() {
        if ($this->request->isMethod('post')) {
            $validator = $this->validator();
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $post = [
                'site_id' => $this->site_id,
                'status' => $this->request->input('post_status'),
                'slug' => $this->request->input('post_slug'),
                //  'is_link' => $this->request->input('is_link') ? 1 : 0,
                'image_id' => $this->request->input('featured_image_id'),
                'intro_image_id' => $this->request->input('intro_image_id'),
                'country_id' => session('country'),
                'created_date' => Carbon::now(),
                'created_by' => \Session('admin_user_id'),
                'categories' => $this->request->input('categories')
            ];
            if (Input::get('url_type') == 'news') {
              $post['home_image_id'] = $this->request->input('home_image_id');
            }elseif (Input::get('url_type') == 'posts') {
              $post['internal_image_id'] = $this->request->input('internal_image_id');
              $post['logo_id']            = $this->request->input('logo_id');
              $post['video_id']           = $this->request->input('featured_video_id');
              $post['video_cover_id']     = $this->request->input('video_cover_id');
            }
            $id = \Posts::add($post);
            if ($id) {
                foreach ($this->site_langs as $key => $locale) {
                    $post_lang = [
                        'post_id' => $id,
                        'lang' => $locale,
                        'post_title' => $this->request->input('post_title_' . $locale),
                        'post_subtitle' => $this->request->input('post_subtitle_' . $locale),
                        'post_excerpt' => $this->request->input('post_brief_' . $locale),
                        'post_content' => $this->request->input('post_content_' . $locale),
                        'tags' => $this->request->input('post_tags_' . $locale),
                        'meta_title' => $this->request->input('meta_title_' . $locale),
                        'meta_keywords' => $this->request->input('meta_keywords_' . $locale),
                        'meta_description' => $this->request->input('meta_description_' . $locale),
                    ];
                    \Posts::add_post_lang($post_lang);
                }
            }
            if (Input::get('url_type') == 'news') {
              return redirect('admin/news');
            }
            return redirect('admin/posts/tabs/' . $id);
        }

        $this->data['all_categories'] = \Categories::get_all(['site_id' => $this->site_id, 'country_id' => session('country')]);
        $this->data['post_statuses'] = \Posts::get_statuses();
        $this->data['post_categories'] = null;
        $this->data['current_post'] = null;
        $this->data['post_langs'] = null;
        $this->data['post_image'] = null;
        $this->data['intro_post_image'] = null;
        $this->data['internal_post_image'] = null;
        $this->data['home_post_image'] = null;
        $this->data['logo_image'] = null;
        $this->data['video_cover'] = null;
        $this->data['post_video'] = null;
        return view('cms::posts.create', $this->data);
    }

    public function edit($id) {
        if ($this->request->isMethod('post')) {
          // dd( $this->request->all());
            $validator = $this->validator();
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $post = [
                'status' => $this->request->input('post_status'),
                'slug' => $this->request->input('post_slug'),
                //  'is_link' => $this->request->input('is_link') ? 1 : 0,
                'image_id' => $this->request->input('featured_image_id'),
                'intro_image_id' => $this->request->input('intro_image_id'),
                'categories' => $this->request->input('categories'),
                'country_id' => session('country'),
                'seo' => $this->seo($this->request->input('post_content_en'))
            ];
            if (Input::get('url_type') == 'news') {
              $post['home_image_id'] = $this->request->input('home_image_id');
            }elseif (Input::get('url_type') == 'posts') {
              $post['internal_image_id'] = $this->request->input('internal_image_id');
              $post['logo_id']            = $this->request->input('logo_id');
              $post['video_id']           = $this->request->input('featured_video_id');
              $post['video_cover_id']     = $this->request->input('video_cover_id');
            }

            \Posts::modify($post, $id);

            foreach ($this->site_langs as $key => $lang) {
                $post_lang = [
                    'post_title' => $this->request->input('post_title_' . $lang),
                    'post_subtitle' => $this->request->input('post_subtitle_' . $lang),
                    'post_excerpt' => $this->request->input('post_brief_' . $lang),
                    'post_content' => $this->request->input('post_content_' . $lang),
                    'tags' => $this->request->input('post_tags_' . $lang),
                    'meta_title' => $this->request->input('meta_title_' . $lang),
                    'meta_keywords' => $this->request->input('meta_keywords_' . $lang),
                    'meta_description' => $this->request->input('meta_description_' . $lang),
                ];
                $args = ['post_id' => $id,
                    'id' => $this->request->input('post_lang_id_' . $lang),
                ];

                \Posts::modify_post_lang($post_lang, $args);
            }
            if (Input::get('url_type') == 'news') {
              // return redirect('admin/news');
              return redirect()->route('news.index', ['site_id' => $this->site_id])->with('message', trans('cms::news.news_updated'));

            }
            return redirect()->route('posts.index', ['site_id' => $this->site_id])->with('message', trans('cms::posts.post_updated'));
        }

        $post = \Posts::find_by(['posts.post_id' => $id, 'country_id' => session('country')]);
        if (is_object($post) && count($post) > 0) {
            $post_langs = Array();
            foreach ($this->site_langs as $lang) {
                $conditions = ['posts.post_id' => $id,
                    'posts_langs.lang' => $lang,
                ];
                $post_langs[$lang] = \Posts::find_by($conditions);
            }

            $site_id = $post->site_id ? $post->site_id : $this->site_id;
            $this->data['current_post'] = $post;
            $this->data['post_langs'] = $post_langs;
            $this->data['post_statuses'] = \Posts::get_statuses();
            $this->data['all_categories'] = \Categories::get_all(['site_id' => $site_id, 'country_id' => session('country')]);
            $this->data['post_categories'] = \Posts::find_post_categories($id, true);
            $this->data['post_image'] = \Media::find($post->image_id);
            $this->data['intro_post_image'] = \Media::find($post->intro_image_id);
            $this->data['internal_post_image'] = \Media::find($post->internal_image_id);
            $this->data['logo_image'] = \Media::find($post->logo_id);
            $this->data['video_cover'] = \Media::find($post->video_cover_id);
            $this->data['post_video'] = \Media::find($post->video_id);
            $this->data['home_post_image'] = \Media::find($post->home_image_id);

            if (!$post) {
                return redirect()->route('posts.index')->with('mssage', trans('cms::posts.no_post'));
            }
            return view('cms::posts.create', $this->data);
        } else {
            return redirect()->route('posts.index');
        }
    }

    public function remove($id) {
        $conditions = ['post_id' => $id];
        $post = \Posts::find_by(['posts.post_id' => $id, 'country_id' => session('country')]);
        if (is_object($post) && count($post) > 0) {
            \Posts::remove($conditions);
            return redirect()->route('posts.index')->with('message', trans('cms::posts.deleted'));
        }
            return redirect()->route('posts.index');
    }

    function getUpdateContent() {
        if (Input::get('postID')) {
            $post = posts_langs::find(Input::get('postID'));
            $post->post_content = Input::get('postContents');
            $post->save();
            return $post->post_content;
        }
    }

    public function seorequest() {

        $posts = \App\Models\posts::all();
        foreach ($posts as $post) {
            if ($post->lang) {
                $content = preg_replace("/\n\s+/", "\n", rtrim(html_entity_decode(strip_tags($post->Lang->post_content))));
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, "http://termextract.fivefilters.org/extract.php");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "text_or_url=" . $content . "&output=json");

                // in real life you should use something like:
                // curl_setopt($ch, CURLOPT_POSTFIELDS,
                //          http_build_query(array('postvar1' => 'value1')));
                // receive server response ...
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close($ch);
                $post->seo = $server_output;

                $post->save();
            }
        }



        //echo $server_output;
        // further processing ....
    }

    public function seo($text) {

        $content = preg_replace("/\n\s+/", "\n", rtrim(html_entity_decode(strip_tags($text))));
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "http://termextract.fivefilters.org/extract.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "text_or_url=" . $content . "&output=json");

        // in real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
        //          http_build_query(array('postvar1' => 'value1')));
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);
        return $server_output;
    }

}

?>
