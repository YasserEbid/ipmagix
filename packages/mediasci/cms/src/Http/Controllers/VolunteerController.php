<?php
namespace Mediasci\Cms\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Models\Volunteer;
use DB;
class VolunteerController extends \CmsController{
    public function index(){
        $volunteers = new Volunteer;
          if(isset($_GET['sort'])){
                 $volunteers= $volunteers->orderBy($_GET['sort'],$_GET['dir']);
            }
            $volunteers =$volunteers->paginate(9);
            foreach ($volunteers as $key => $volunteer) {
              $volunteer->countryName = DB::table('countries_names')->where('id',$volunteer->country)->first()->name;
            }
        return view('cms::volunter.index')->withvolunteers($volunteers);
    }
    public function show($id){
        $volunteer=  Volunteer::find($id);
        $volunteer->countryName = DB::table('countries_names')->where('id',$volunteer->country)->first()->name;
         return view('cms::volunter.volunteer')->withvolunteer($volunteer);
    }
    public function delete($id){
           $volunteer=  Volunteer::find($id);
           $volunteer->delete();
           session()->push('n','One item has been deleted successfully.');
           return redirect('admin/partnership-requests');
    }
}
