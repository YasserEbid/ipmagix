<?php
namespace Mediasci\Cms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use App\Models\MailSystem;
class MailsystemController extends \CmsController {
    public function index(){
        $date['mails']=  MailSystem::paginate(10);
        return view('cms::mailsystem.index',$date);
    }
    public function update($id){
        $mail=  MailSystem::find($id);
        if(Input::get('title')){
         $mail ->title=  Input::get('title') ;
         $mail->message=  Input::get('message');
         $mail->save();
         return redirect('admin/mailsystem');
        }
         return view('cms::mailsystem.update')->withmail($mail);
    }
}