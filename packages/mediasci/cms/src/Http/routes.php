<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::any('login', ['as' => 'auth.login', 'uses' => 'Authentication@login']);
Route::controller('ajax', 'component\AjaxController');
Route::controller('cronjob', 'Cronjob');
Route::get('youtube1', ['as' => 'youtube', 'uses' => 'Cronjob@getYoutube']);
Route::get('mail/send', 'Mail@send');
Route::get('sms/send', 'SmsController@send');
Route::get('post/seorequest', 'Posts@seorequest');


Route::get('mail/track', 'Mail@track');

Route::group(['middleware' => ['backend']], function () {

    /*
     * authentication protected routes
     */
    Route::any('logout', ['as' => 'auth.logout', 'uses' => 'Authentication@logout']);
    /*
     * home
     */
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Home@index']);
    Route::post('dashboard/sort', ['as' => 'dashboard.sort', 'uses' => 'Home@dashboardSorting']);
    Route::get('dashboard/chartbeat', ['as' => 'dashboard.chartbeat', 'uses' => 'Home@chartbeat']);
    Route::get('dashboard/changecountry/{id}', ['as' => 'dashboard.changecountry', 'uses' => 'Home@changeCountry']);

    /*
     * categories
     */
    Route::get('categories', ['as' => 'categories.index', 'uses' => 'Categories@getIndex']);
    Route::any('categories/create', ['as' => 'categories.create', 'uses' => 'Categories@anyCreate']);
    Route::any('categories/{id}/edit', ['as' => 'categories.edit', 'uses' => 'Categories@anyEdit']);
    Route::any('categories/{id}/delete', ['as' => 'categories.delete', 'uses' => 'Categories@anuDelete']);
    /*
     * Routes
     */
    Route::any('routes', ['as' => 'routes.index', 'uses' => 'AccessRoutes@index']);
    /*
     * RoutesGroupsController
     */
    Route::any('routes_groups', ['as' => 'routes_groups.index', 'uses' => 'RoutesGroups@index']);
    Route::any('routes_groups/{id}/edit', ['as' => 'routes_groups.edit', 'uses' => 'RoutesGroups@edit']);
    Route::any('routes_groups/{id}/delete', ['as' => 'routes_groups.delete', 'uses' => 'RoutesGroups@remove']);
    Route::any('routes_groups/create', ['as' => 'routes_groups.create', 'uses' => 'RoutesGroups@create']);
    // //polls
    // Route::get('polls', ['as' => 'polls.index', 'uses' => 'PollsController@getIndex']);
    // Route::get('polls/create', ['as' => 'polls/create', 'uses' => 'PollsController@getCreate']);
    // Route::get('polls', ['as' => 'polls.index', 'uses' => 'PollsController@getIndex']);
    // Route::get('polls/create', ['as' => 'polls/create', 'uses' => 'PollsController@getCreate']);
    /*
     * Sliders
     */
    Route::get('sliders/create', ['as' => 'sliders.create', 'uses' => 'SlidersController@getCreate']);
    Route::post('sliders/create', ['as' => 'sliders.create', 'uses' => 'SlidersController@postCreate']);
    Route::get('sliders/update/{id}', ['as' => 'sliders.update', 'uses' => 'SlidersController@getUpdate']);
    Route::post('sliders/update/{id}', ['as' => 'sliders.update', 'uses' => 'SlidersController@postUpdate']);
    Route::get('sliders/', ['as' => 'sliders.index', 'uses' => 'SlidersController@getIndex']);
    Route::get('sliders/delete/{id}', ['as' => 'sliders.delete', 'uses' => 'SlidersController@getDelete']);

    /*
     * careers, careers category
     */
    Route::get('careers/create', ['as' => 'careers.create', 'uses' => 'CareerController@getCreate']);
    Route::post('careers/create', ['as' => 'careers.create', 'uses' => 'CareerController@postCreate']);
    Route::get('careers/update/{id}', ['as' => 'careers.update', 'uses' => 'CareerController@getUpdate']);
    Route::post('careers/update/{id}', ['as' => 'careers.update', 'uses' => 'CareerController@postUpdate']);
    Route::get('careers/', ['as' => 'careers.index', 'uses' => 'CareerController@getindex']);
    Route::get('careers/delete/{id}', ['as' => 'careers.delete', 'uses' => 'CareerController@getDelete']);
    Route::get('careercategory/create', ['as' => 'careercategory.create', 'uses' => 'CareerController@getCreatecat']);
    Route::post('careercategory/create', ['as' => 'careercategory.create', 'uses' => 'CareerController@postCreatecat']);
    Route::get('careercategory/update/{id}', ['as' => 'careercategory.update', 'uses' => 'CareerController@getUpdatecat']);
    Route::post('careercategory/update/{id}', ['as' => 'careercategory.update', 'uses' => 'CareerController@postUpdatecat']);
    Route::get('careercategory/', ['as' => 'careercategory.index', 'uses' => 'CareerController@getCategory']);
    Route::get('careercategory/delete/{id}', ['as' => 'careercategory.delete', 'uses' => 'CareerController@getDeletecat']);
    /*
     * cvs
     */
    Route::get('jobcv', ['as' => 'jobcv.index', 'uses' => 'CvsController@getIndex']);
    Route::get('jobcv/{id}', ['as' => 'jobcv.show', 'uses' => 'CvsController@getShow']);
    Route::get('jobcv/delete/{id}', ['as' => 'jobcv.delete', 'uses' => 'CvsController@delete']);
    /*
     * setting
     */
    Route::get('setting', ['as' => 'setting.index', 'uses' => 'settingController@getIndex']);
    Route::get('setting/create', ['as' => 'setting.create', 'uses' => 'settingController@getCreate']);
    Route::get('setting/update/{id}', ['as' => 'setting.update', 'uses' => 'settingController@getUpdate']);
    Route::get('setting/delete/{id}', ['as' => 'setting.delete', 'uses' => 'settingController@getdelete']);
    Route::post('setting/create', ['as' => 'setting.create', 'uses' => 'settingController@postCreate']);
    Route::post('setting/update', ['as' => 'setting.update', 'uses' => 'settingController@postUpdate']);


    /*
     * Roles
     */

    Route::any('roles', ['as' => 'roles.index', 'uses' => 'Roles@index']);
    Route::any('roles/create', ['as' => 'roles.create', 'uses' => 'Roles@create']);
    Route::any('roles/{id}/edit', ['as' => 'roles.edit', 'uses' => 'Roles@edit']);
    Route::any('roles/{id}/delete', ['as' => 'roles.delete', 'uses' => 'Roles@remove']);
    /*
     * Users
     */
    Route::any('user', ['as' => 'user.index', 'uses' => 'User@index']);
    Route::any('user/create', ['as' => 'user.create', 'uses' => 'User@create']);
    Route::any('user/{id}/edit', ['as' => 'user.edit', 'uses' => 'User@edit']);
    Route::any('user/transfer', ['as' => 'user.transfer', 'uses' => 'User@transfer']);
    Route::any('user/{id}/delete', ['as' => 'user.delete', 'uses' => 'User@remove']);
    /*
     * Tags
     */
    Route::any('tags', ['as' => 'tags.index', 'uses' => 'Tags@index']);
    Route::any('tags/create', ['as' => 'tags.create', 'uses' => 'Tags@create']);
    Route::any('tags/{id}/edit', ['as' => 'tags.edit', 'uses' => 'Tags@edit']);
    Route::any('tags/{id}/delete', ['as' => 'tags.delete', 'uses' => 'Tags@remove']);
    /*
     * Comments
    */
    // Route::any('comments', ['as' => 'comments.index', 'uses' => 'Comments@index']);
    // Route::any('comments/create', ['as' => 'comments.create', 'uses' => 'Comments@create']);
    // Route::any('comments/{id}/edit', ['as' => 'comments.edit', 'uses' => 'Comments@edit']);
    // Route::any('comments/{id}/spam', ['as' => 'comments.spam', 'uses' => 'Comments@spam']);
    // Route::any('comments/{id}/publish', ['as' => 'comments.publish', 'uses' => 'Comments@publish']);
    // Route::any('comments/{id}/soft_delete', ['as' => 'comments.soft_delete', 'uses' => 'Comments@soft_delete']);
    // Route::any('comments/{id}/hard_delete', ['as' => 'comments.hard_delete', 'uses' => 'Comments@hard_delete']);
    /*
     * Blocks
     */
    // Route::any('blocks', ['as' => 'blocks.index', 'uses' => 'Blocks@index']);
    // Route::any('blocks/create', ['as' => 'blocks.create', 'uses' => 'Blocks@create']);
    // Route::any('blocks/{id}/edit', ['as' => 'blocks.edit', 'uses' => 'Blocks@edit']);
    // Route::any('blocks/{id}/delete', ['as' => 'blocks.delete', 'uses' => 'Blocks@remove']);
    // Route::any('blocks/posts/{id}', ['as' => 'blocks.posts', 'uses' => 'Blocks@posts']);
    /*
     * messages
     */
    Route::any('messages', ['as' => 'messages.index', 'uses' => 'Messages@index']);
    Route::any('messages/create', ['as' => 'messages.create', 'uses' => 'Messages@create']);
    Route::any('messages/{id}/edit', ['as' => 'messages.edit', 'uses' => 'Messages@edit']);
    Route::any('messages/{id}/delete', ['as' => 'messages.delete', 'uses' => 'Messages@remove']);
    Route::any('messages/cron', ['as' => 'messages.cron', 'uses' => 'Messages@cron']);

    // Media

    Route::any('media/get/{offset?}/{type?}/{q?}', ["as" => "media.index", "uses" => "MediaController@index"]);
    Route::any('media/save_gallery', ["as" => "media.save_gallery", "uses" => "MediaController@save_gallery"]);
    Route::any('media/save', ["as" => "media.save", "uses" => "MediaController@save"]);
    Route::any('media/delete', ["as" => "media.delete", "uses" => "MediaController@delete"]);
    Route::any('media/upload', ["as" => "media.upload", "uses" => "MediaController@upload"]);
    Route::any('media/link', ["as" => "media.link", "uses" => "MediaController@link"]);
    Route::any('media/crop', ["as" => "media.crop", "uses" => "MediaController@cropImage"]);
    /*
     * Sms
     */

    Route::get('sms', ['as' => 'sms', 'uses' => 'SmsController@index']);
    Route::any('sms/create', ['as' => 'sms.create', 'uses' => 'SmsController@create']);
    Route::any('sms/update/{id}', ['as' => 'sms.update', 'uses' => 'SmsController@update']);
    Route::get('sms/delete/{id}', ['as' => 'sms.delete', 'uses' => 'SmsController@delete']);
    /*
     * news
     */
    Route::any('news', ['as' => 'news.index', 'uses' => 'Posts@index']);
    Route::any('news/create', ['as' => 'news.create', 'uses' => 'Posts@create']);
    Route::any('news/{id}/edit', ['as' => 'news.edit', 'uses' => 'Posts@edit']);
    /*
     * Posts
     */
    Route::any('posts', ['as' => 'posts.index', 'uses' => 'Posts@index']);
    Route::any('posts/create', ['as' => 'posts.create', 'uses' => 'Posts@create']);
    Route::any('posts/{id}/edit', ['as' => 'posts.edit', 'uses' => 'Posts@edit']);
    Route::any('posts/{id}/delete', ['as' => 'posts.delete', 'uses' => 'Posts@remove']);
    Route::any('posts/updateContent', ['as' => 'posts.updateContent', 'uses' => 'Posts@getUpdateContent']);

    Route::any('posts/cron', ['as' => 'posts.cron', 'uses' => 'Posts@cron']);

    Route::any('posts/tabs/{id}', ['as' => 'tabs.index', 'uses' => 'Tabscontroller@show']);
    Route::any('poststabs/create/{id}', ['as' => 'tabs.create', 'uses' => 'Tabscontroller@anyCreate']);
    Route::any('poststabs/update/{id}', ['as' => 'tabs.update', 'uses' => 'Tabscontroller@anyUpdate']);
    Route::any('poststabs/delete/{id}', ['as' => 'tabs.delete', 'uses' => 'Tabscontroller@anyDelete']);


    /*
     * mail markting
     */
    // themes
    Route::any('mail/themes/delete/{id}', ['as' => 'mail.themes.delete', 'uses' => 'Mail@deletetheme']);
    Route::any('mail/themes/update/{id}', ['as' => 'mail.themes.update', 'uses' => 'Mail@updatetheme']);
    Route::any('mail/themes/create', ['as' => 'mail.themes.create', 'uses' => 'Mail@create_theme']);
    Route::any('mail/themes/show/{id}', ['as' => 'mail.thems.show', 'uses' => 'Mail@showtheme']);
    Route::any('mail/themes', ['as' => 'mail.themes', 'uses' => 'Mail@themes']);

    // messages
    Route::any('mail/message', ['as' => 'mail.messages', 'uses' => 'Mail@messagesHome']);
    Route::any('mail/message/delete/{id}', ['as' => 'mail.messages.delete', 'uses' => 'Mail@deleteMessage']);
    Route::any('mail/message/update/{id}', ['as' => 'mail.messages.update', 'uses' => 'Mail@updateMessage']);
    Route::any('mail/statistics/{id}', ['as' => 'mail.messages.statistics', 'uses' => 'Mail@statistics']);
    Route::any('mail/message/create', ['as' => 'mail.messages.create', 'uses' => 'Mail@createMeaage']);

    //trends
    Route::get('trends', ['as' => 'trends', 'uses' => 'TrendsController@index']);

    /*
     * careers
     */
    // Route::get('careers/create', ['as' => 'careers.create', 'uses' => 'CareerController@getCreate']);
    // Route::post('careers/create', ['as' => 'careers.create', 'uses' => 'CareerController@postCreate']);
    // Route::get('careers/update/{id}', ['as' => 'careers.update', 'uses' => 'CareerController@getUpdate']);
    // Route::post('careers/update/{id}', ['as' => 'careers.update', 'uses' => 'CareerController@postUpdate']);
    // Route::get('careers/', ['as' => 'careers.index', 'uses' => 'CareerController@getindex']);
    // Route::get('careers/delete/{id}', ['as' => 'careers.delete', 'uses' => 'CareerController@getdelete']);

    //Polls
    Route::any('/polls', ['as' => 'polls.index', 'uses' => 'Polls@index']);
    Route::any('/polls/create', ['as' => 'polls.create', 'uses' => 'Polls@create']);
    Route::any('/polls/update/{id}', ['as' => 'polls.update', 'uses' => 'Polls@update']);
    Route::any('/polls/delete/{id}', ['as' => 'polls.delete', 'uses' => 'Polls@delete']);
    Route::any('/polls/result/{id}', ['as' => 'polls.result', 'uses' => 'Polls@result']);

    //ads
    Route::get('/ads', ['as' => 'ads', 'uses' => 'Ads@index']);
    Route::any('/ads/create', ['as' => 'ads.create', 'uses' => 'Ads@create']);
    Route::get('/ads/delete/{id}', ['as' => 'ads.delete', 'uses' => 'Ads@delete']);
    Route::any('/ads/update/{id}', ['as' => 'ads.update', 'uses' => 'Ads@update']);

    //workflow
    Route::get('/journeys', ['as' => 'workflow', 'uses' => 'WorkflowController@index']);
    Route::any('/journeys/create', ['as' => 'workflow-create', 'uses' => 'WorkflowController@create']);
    Route::any('/journeys/update/{id}', ['as' => 'workflow-update', 'uses' => 'WorkflowController@update']);
    Route::get('/journeys/delete/{id}', ['as' => 'workflow-delete', 'uses' => 'WorkflowController@delete']);
    Route::get('/journeys/show/{id}', ['as' => 'workflow-show', 'uses' => 'WorkflowController@show']);

    Route::get('demoRequests/', ['as' => 'appointmentsRequests.index', 'uses' => 'AppointmentsRequestsController@getindex']);
    Route::get('demoRequests/delete/{id}', ['as' => 'appointmentsRequests.delete', 'uses' => 'AppointmentsRequestsController@getdelete']);

    // Route::any('cities', ['as' => 'cities.index', 'uses' => 'CitiesController@index']);
    // Route::any('cities/create', ['as' => 'cities.create', 'uses' => 'CitiesController@create']);
    // Route::any('cities/update/{id}', ['as' => 'cities.update', 'uses' => 'CitiesController@update']);
    // Route::any('cities/delete/{id}', ['as' => 'cities.delete', 'uses' => 'CitiesController@delete']);

    // //faq categories
    // Route::get('/faqCategories', ['as' => 'faqCategories.index', 'uses' => 'FAQCategoriesController@index']);
    // Route::any('/faqCategories/create', ['as' => 'faqCategories.create', 'uses' => 'FAQCategoriesController@create']);
    // Route::any('/faqCategories/update/{id}', ['as' => 'faqCategories.update', 'uses' => 'FAQCategoriesController@update']);
    // Route::get('/faqCategories/delete/{id}', ['as' => 'faqCategories.delete', 'uses' => 'FAQCategoriesController@delete']);

    //faq
    Route::get('/questionsAnswers', ['as' => 'questionsAnswers.index', 'uses' => 'QuestionsAnswersController@index']);
    Route::any('/questionsAnswers/create', ['as' => 'questionsAnswers.create', 'uses' => 'QuestionsAnswersController@create']);
    Route::any('/questionsAnswers/update/{id}', ['as' => 'questionsAnswers.update', 'uses' => 'QuestionsAnswersController@update']);
    Route::get('/questionsAnswers/delete/{id}', ['as' => 'questionsAnswers.delete', 'uses' => 'QuestionsAnswersController@delete']);

    Route::get('/partnership-requests', ['as' => 'volunteer.index', 'uses' => 'VolunteerController@index']);
    Route::get('/partnership-requests/{id}', ['as' => 'volunteer.create', 'uses' => 'VolunteerController@show']);
    Route::get('/partnership-requests/delete/{id}', ['as' => 'volunteer.delete', 'uses' => 'VolunteerController@delete']);

    /*
     * Menus
     */
    Route::any('/menus/create/{id1?}/{id2?}', ['as' => 'menus.create', 'uses' => 'MenuController@create']);
    Route::any('/menus/update/{id}', ['as' => 'menus.update', 'uses' => 'MenuController@update']);
    Route::any('/menus/delete/{id}', ['as' => 'menus.delete', 'uses' => 'MenuController@delete']);

    Route::any('/menus/show/{id}', ['as' => 'menus.showsub', 'uses' => 'MenuController@showsub']);
    Route::any('/menus/{id}', ['as' => 'menus.show', 'uses' => 'MenuController@show']);
    Route::any('/menus', ['as' => 'menus.index', 'uses' => 'MenuController@index']);
    /*
     * Subscribers
     */
    Route::any('/subscribers', ['as' => 'subscribers.index', 'uses' => 'Subscribers@index']);
    Route::any('/subscribers/{id}', ['as' => 'subscribers.find', 'uses' => 'Subscribers@show']);
    Route::any('/subscribers/delete/{id}', ['as' => 'subscribers.delete', 'uses' => 'Subscribers@delete']);

    /*
     * mailsystem
     */
    Route::any('/mailsystem', ['as' => 'mailsystem.index', 'uses' => 'MailsystemController@index']);
    Route::any('/mailsystem/update/{id}', ['as' => 'mailsystem.update', 'uses' => 'MailsystemController@update']);
    /*
     * Exportation
     */
    Route::any('/exportevent/{id?}', ['as' => 'exportevent', 'uses' => 'exportcontroller@event']);
    Route::any('/exportsubecribe', ['as' => 'exportsubecribe', 'uses' => 'exportcontroller@subscribe']);
    Route::any('/exportrequest', ['as' => 'exportrequest', 'uses' => 'exportcontroller@request']);
    Route::any('/exportvolunteer', ['as' => 'exportvolunteer', 'uses' => 'exportcontroller@volunteer']);
    Route::any('/exportcv', ['as' => 'exportcv', 'uses' => 'exportcontroller@cv']);
    Route::any('/exportinfo', ['as' => 'exportinfo', 'uses' => 'exportcontroller@info']);
    Route::any('/exportcontact-us', ['as' => 'exportcontact', 'uses' => 'exportcontroller@contact_leads']);

    /*
    * Partners
    */
    Route::any('/partners', ['as' => 'partners.index', 'uses' => 'PartnersController@getIndex']);
    Route::any('/partners/create', ['as' => 'partners.create', 'uses' => 'PartnersController@getCreate']);
    Route::get('/partners/delete/{id}', ['as' => 'partners.delete', 'uses' => 'PartnersController@destroy']);
    Route::get('/partners/edit/{id}', ['as' => 'partners.edit', 'uses' => 'PartnersController@edit']);
    Route::post('/partners/update/{id}', ['as' => 'partners.update', 'uses' => 'PartnersController@update']);
    /*
    * feedback
    */
    Route::get('/feedback', ['as' => 'feedback.index', 'uses' => 'FeedbackController@getIndex']);
    Route::any('/feedback/create', ['as' => 'feedback.create', 'uses' => 'FeedbackController@getCreate']);
    Route::get('/feedback/delete/{id}', ['as' => 'feedback.delete', 'uses' => 'FeedbackController@destroy']);
    Route::get('/feedback/edit/{id}', ['as' => 'feedback.edit', 'uses' => 'FeedbackController@edit']);
    Route::post('/feedback/update/{id}', ['as' => 'feedback.update', 'uses' => 'FeedbackController@update']);
    /*
    * Clients
    */
    Route::any('/clients', ['as' => 'clients.index', 'uses' => 'ClientsController@getIndex']);
    Route::any('/clients/create', ['as' => 'clients.create', 'uses' => 'ClientsController@getCreate']);
    Route::get('/clients/delete/{id}', ['as' => 'clients.delete', 'uses' => 'ClientsController@destroy']);
    Route::any('/clients/update/{id}', ['as' => 'clients.update', 'uses' => 'ClientsController@update']);
    /*
    * Clients Category
    */
    Route::any('/clientsCategory', ['as' => 'clientsCategory.index', 'uses' => 'clientsCategoryController@getIndex']);
    Route::any('/clientsCategory/create', ['as' => 'clientsCategory.create', 'uses' => 'clientsCategoryController@getCreate']);
    Route::get('/clientsCategory/delete/{id}', ['as' => 'clientsCategory.delete', 'uses' => 'clientsCategoryController@destroy']);
    Route::any('/clientsCategory/update/{id}', ['as' => 'clientsCategory.update', 'uses' => 'clientsCategoryController@update']);

    //locations
    Route::get('/locations', ['as' => 'locations', 'uses' => 'LocationsController@getIndex']);
    Route::any('/locations/create', ['as' => 'locations.create', 'uses' => 'LocationsController@anyCreate']);
    Route::get('/locations/delete/{id}', ['as' => 'locations.delete', 'uses' => 'LocationsController@getDelete']);
    Route::any('/locations/update/{id}', ['as' => 'locations.update', 'uses' => 'LocationsController@anyUpdate']);

    // contact us
    Route::get('/contact-us',['as' => 'contact.allRequests', 'uses' => 'contactRequestsController@allRequests']);
    Route::get('/contact-us/delete/{id}', ['as' => 'contact.delete', 'uses' => 'contactRequestsController@deleteRequest']);


});
