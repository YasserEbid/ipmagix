<?php

return [
    'navigation' => 'Navigation',
    'navigations' => 'Navigations',
    'all' => 'All',
    'add_new' => 'Add New',
    'create' => 'Add',
    'menu_name' => 'Menu Name',
    'edit' => 'Edit',
    'items' => 'Items',
    'menu_data' => 'Menu Data',
    'new_navigation' => 'New Navigation',
    'save' => 'Save',
    'posts' => 'Posts',
    'pages' => 'Pages',
    'categories' => 'Categories',
    'url' => 'URL',
    'search' => 'Search',
    'add' => 'Add',
    'text' => 'Text',
    'filter' => 'Filter',
    'save_order' => 'Save Order'
];
