<?php

return [
    "galleries" => "Galleries",
    "add_new" => "Add New",
    "per_page" => "per page",
    "bulk_actions" => "Bulk actions",
    "apply" => "Apply",
    "delete" => "Delete",
    "name" => "Gallery name",
    "actions" => "Action",
    "sure_delete" => "you are about to delete ... continue?",
    "page" => "Page",
    "add_new_gallery" => "Add New Gallery",
    "add_media" => "Add Media",
    "gallery_author" => "Gallery author",
    "no_media" => "No media has been added",
    "save_gallery" => "Save gallery",
    "is_not_valid_image" => "is not an image",
    "of" => "of",
    "search_galleries" => "seach galleries ..",
    "files" => "files",
    "bulk_actions" => "Bulk actions"
];
