<?php

return [
	//attributes
	'group_id' => 'Group ID',
	'group_name' => 'Group Name',
	'actions' => 'Actions',
	'index_title' => 'Routes Groups',
	'save' => 'Save',
	'create' => 'Add new Routes Group',
	'edit' => 'Edit',
	'created' => 'Added Successfully',
	'updated' => 'Edited Successfully',
	'no_routes_groups'=>'No routes groups',
];
