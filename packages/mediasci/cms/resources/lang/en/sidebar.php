<?php

return [
	'routesGroups' => 'RoutesGroups',
	'routesGroups_view' => 'view',
	'routesGroups_add' => 'Add',
	'privilege' => 'privilege',
	'privilege' => 'Roles',
	'privilege_view' => 'View',
	'privilege_add' => 'Add',
	'routes' => 'Routes',
	'users' => 'Admin Users Management',
	'contact' => 'Contact us Requests',
    'user'=>'Users',
	'users_view' => 'View',
	'users_add' => 'Add',
	'categories' => 'Categories',
	'tags' => 'Tags',
	'comments' => 'Comments',
	'blocks' => 'Blocks',
	'messages' => 'Messages',
	'sms' => 'Sms',
	'posts' => 'Posts',
	'doctors' => 'Doctors',
	'themes' => 'Themes',
	'email_marketing' => 'Email Marketing',
	'careers' => 'Careers',
	'events' => 'Events',
	'trends' => 'Trends',
	'services' => 'Services',
	'event_registered' => 'Events Registered People',
	'promo_code' => 'Promo Codes',
	'eventcat' => 'Event Category',
	'payment' => 'Payment',
	'locations' => 'Site Locations',
	'categories' => 'Categories',
	'tags'=>'Tags',
	'comments'=>'Comments',
	'blocks'=>'Blocks',
	'messages'=>'Messages',
	'sms'=>'SMS',
	'posts'=>'Posts',
	'news'=>'News',
	'doctors'=>'Doctors',
	'eventcat'=>'Event Category',
	'appointmentsRequests'=>'Demo Requests',
	'cities'=>'Cities',
    'themes'=>'Themes',
    'email_marketing' => 'Email Marketing',
    'careers' => 'Careers',
    'events_managment' => 'Events Managment',
    'events' => 'Events',
    'trends' => 'Trends',
    'services'=>'Services',
    'event_registered'=>'Events Registered',
    'promo_code'=>'Promo Codes',
    'eventcat'=>'Event Category',
    'chartbeat'=>'Chartbeat',
    'questionsAnswers'=>'FAQs',
    'site_settings'=>'Site Settings',
    'sliders'=>'Homepage Banner',
    'locations'=>'Locations',
    'careersmanagement'=>'Career Management',
    'career'=>'Careers',
    'careercategory'=>'Career Category',
    'faqCategories'=>'FAQs Categories',
    'workflow'=>'Journeys',
    'polls'=>'Polls',
    'volunteer'=>'Partnership Requests',
    'ads'=>'Ads',
    'media'=>'Media',
    'packages'=>'Packages',
    'campaigns'=>'Campaigns',
    'content_managment'=>'Content Management',
    'user_requests' => 'User Requests',
    'moniter'=>'Site Monitoring',
    'menu_managment'=>'Menu Management',
    'menu'=>'Menus',
    'jobcv'=>'Job CVs',
    'subscribe'=>"Mail Subscribers",
    'registerinfo'=>'Event Registeration Informaion',
		'partners'=>'Partners',
		'clients'=>'Clients',
		'clients_category'=>'Clients Category',
		'feedback'=>'Feedback',
];
