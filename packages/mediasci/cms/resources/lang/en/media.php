<?php

return [
	'upload_files' => 'Upload File',
	'files' => 'Files',
	'galleries' => 'Galleries',
	'settings' => 'Settings',
	'search_media' => 'Search Files',
	'drop_files_here' => 'Drop Files Here',
	'select_files' => 'Select Files',
	'add_external_link' => 'Add External Link',
	'save_to_media' => 'Save to Media',
	'grabbing' => 'Grabbing',
	 "upload_files" => "Upload Files",
    "media" => "Media",
    "search_media" => " Search Media ",
    "drop_files_here" => "Drop Files Here",
    "select_files" => "Select File ",
    "preview" => "Preview",
    "delete" => "delete",
    "title" => "Title",
    "description" => "Description",
    "save" => "save",
    "deleting_selected" => " Deleting Selected",
    "delete_selected" => "Delete Selected",
    "loading" => " loading",
    "select_media" => "Select Media",
    
  
    "embed_settings" => " Embed Settings ",
   
    "grabbing" => "Grabbing",
    "save_to_media" => "Save to Media",
    "add_external_link" => "Add External Link",
    "files" => "Files",
    'edit'=>'Edit',
];
