<?php

return [
	//other
	'index_title' => 'Events Registered',
	'name' => 'Name',
	'email' => 'Email',
	'phone' => 'Phone',
	'city' => 'City',
	'country' => 'Country',
	'package' => 'Package',
	'date' => 'Date',
	'promocode' => 'PromoCode',
	'amount' => 'Amount',
	'options' => 'Actions',
	'paid' => 'Paid',
	'notpaid' => 'Not Paid',
	'no_registered' => 'there are no registered yet .',
	'user_deleted' => 'this user was deleted successfully',
	'confirm_delete' => 'Are you sure you want to delete this user?',
	'show_per_page' => 'show',
	'selectone' => 'Select One',
];
