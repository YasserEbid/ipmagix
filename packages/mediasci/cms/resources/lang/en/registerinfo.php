<?php

return[
    'register'=>'Event Registeration Information',
    'name'=>'Name',
    'birth'=>"Birth Date",
    'gender'=>'Gender',
    'title'=>'Title',
    'email'=>'Email',
    'phone'=>'Phone',
    'langs'=>'Languages',
    'event'=>'Event',
    'cv'=>'CV Name',
    'certificate'=>'Certificate Name',
    'achieved'=>'Achieved Name',
    'down'=>'Download'
    
    
];
