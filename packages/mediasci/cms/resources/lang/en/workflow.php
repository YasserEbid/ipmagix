<?php

return[
    'title'=>'Title',

    'name'=>'Journeys',
    'new'=>'New Journey',
    'create'=>'Add Journey',

    'editmessage'=>'',
    'newmessage'=>'',
    'update'=>'Edit Journey',
    'show'=>'show',
    'input_level_name'=>'Journey Level Name',
    'input_level_url'=>'Journey Level URL',
    'add_new'=>'Add Journey'
];
