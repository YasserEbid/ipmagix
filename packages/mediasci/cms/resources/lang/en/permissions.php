<?php

return array(
    
    //permissions
    "manage_permissions" => "Manage permissions",
    
    // ِAuthors
    "manage_authors" => "Manage authors",

    //reporters
    "manage_reporters" => "Manage reporters",
    
    // users
    "manage_users" => "Manage users",
   
    "create_categories" => "Add categories",
    "edit_categories" => "Edit categories",
    "delete_categories" => "Delete categories",
    "create_tags" => "Add tags",
    "edit_tags" => "Edit tags",
    "delete_tags" => "Delete tags",
    "create_blocks" => "Add blocks",
    "edit_blocks" => "Editblocks",
    "delete_blocks" => "Delete blocks",
    "sort_blocks" => "Sort blocks",

    "create_posts" => "Add Posts",
    "edit_posts" => "Edit Posts",
    "delete_posts" => "Delete Posts",
    "trash_posts" => "Unapprove Posts",
    "order_posts" => "Order Posts",
    "restore_posts" => "Restore Posts",
    "published_posts" => "Approve Posts",
    "pending_posts" => "Report Posts",
    "reviewed_posts" => "Posts As Edited",
    "draft_posts" => "Pending Posts",
    "scheduled_posts" => "Schedule Posts",
    "move_posts" => "Move Posts",
    "rank_posts" => "Rank Posts",
    
    "create_groups" => "Add Groups",
    "edit_groups" => "Edit Groups",
    "delete_groups" => "Delete Groups",

    "create_pages" => "Add Pages",
    "edit_pages" => "Edit Pages",
    "delete_pages" => "Delete Pages",
    "trash_pages" => "Trash Pages",
    "restore_pages" => "Restore Pages",
    "published_pages" => "Publish Pages",
    "pending_pages" => "Pending Pages",
    "draft_pages" => "Draft Pages",

	"create_area" => "Add Area",
    "edit_area" => "Edit Area",
    "delete_area" => "Delete Area",

    "add_comments" => "Add Comments",
    "edit_comments" => "Edit Comments",
    "delete_comments" => "Delete Comments",
    "trash_comments" => "Trash Comments",
    "restore_comments" => "Restore Comments",
    "approve_comments" => "Approve Comments",
    "unapprove_comments" => "Unapprove Comments",
    "spam_comments" => "Spam Comments",
    "not_spam_comments" => "Not Spam Comments",
);