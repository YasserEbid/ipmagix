<?php

return [
    //attributes
    'post_id' => 'News ID',
    'site_id' => 'Site ID',
    'created_by' => 'Added By',
    'created_date' => 'Creation Date',
    'title' => 'Title',
    'subtitle' => 'Subtitle',
    'brief' => 'Brief',
    'slug' => 'News Slug',
    'newmessage' => '',
    'editmessage' => '',
    'post_content' => 'News Content',
    "post_tabs" => "News Tabs",
    'tags' => 'Tags',
    'lang' => 'Language',
    'user' => 'User',
    'new_tab' => 'New Tab',
    "update_tab" => "Update Tab",
    'full_name' => 'Full Name',
    'featured_image' => 'Featured Image',
    "internal_image" => "Internal Image",
    'home_image' => 'Home Image',
    "logo_image" => "Logo",
    'set_image' => 'Set Image',
    'featured_video' => 'Featured Video',
    'video_cover' => 'Video Cover Image',
    'set_video' => 'Set Video',
    'locale_en' => 'English',
    'locale_ar' => 'Arabic',
    'categories' => 'Select Category',
    'category' => 'Category',
    'status' => 'Select Status',
    'published' => 'Published',
    'draft' => 'Draft',
    'deleted' => 'Deleted',
    'nothing' => 'Nothing Selected',
    'actions' => 'Actions',
    'new' => 'Add News',
    'create' => 'Add News',
    'edit' => 'Edit News',
    'save' => 'Save',
    'index_title' => 'News',
    'no_posts' => 'No News',
    'show_per_page' => 'Show per page',
    'post_created' => 'News added successfully',
    'post_updated' => 'News edited successfully',
    'meta_title' => 'Meta Title',
    'meta_keywords' => 'Meta Keywords',
    'meta_description' => 'Meta Description',
    'is_link' => 'Is Link?',
    'intro_image' => 'Intro Image',
    'news_updated' => 'News edited successfully',

];
