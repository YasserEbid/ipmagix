<?php

return[
    'cvs'=>'Job CVs',
    'title'=>'Job Title',
    'operation'=>'Operation',
    'show'=>'Show','delete'=>'Delete',
    'field'=>'Field',
    'desc'=>'Description',
    'name'=>'Name',
    'email'=>'Email',
    'phone'=>'Phone',
    'cover_letter'=>'Cover Letter',
    'cv'=>'Job C.V',
    'download'=>'Download',
    'back'=>'Back',
    'job'=>'Job'
];
