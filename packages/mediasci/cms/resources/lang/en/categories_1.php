<?php

return array(
    "title" => "Categories",
    "add" => "Add category",
    "name" => "Name",
    "slug" => "URL",
    "parent" => "Parent",
    "top_categories" => "Top categories",
    "del" => "Delete",
    "img" => "Image",
    "edit" => "Edit",
    "child_categories" => "Child categories",
    "sure_delete" => "Delete category?",
    "search_categories" => "Search categories"
);
