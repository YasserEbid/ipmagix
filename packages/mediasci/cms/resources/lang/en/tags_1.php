<?php

return array(
    "title" => "Tags",
    "add" => "Add tag",
    "name" => "Name",
    "slug" => "Slug",
    "del" => "Delete",
    "edit" => "Edit",
    "done" => "Done",
    "sure_delete" => "Delete tag?"
);
