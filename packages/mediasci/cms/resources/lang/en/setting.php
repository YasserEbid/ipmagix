<?php
return[
    'setting'=>'Site Settings',
    'delete'=>'Delete',
    'update'=>'Edit', 
    'operation'=>'Operation',
    'confirm'=>'',
    'new'=>'Add Setting',
    'newmessage'=>'',
    'addnew'=>'Add Setting',
    'submit'=>'Save',
    'success'=>'operation success',
    'edit'=>'Edit Setting',
    'editmessage'=>'',
    'name'=>'Name',
    'value'=>'Value'
];
