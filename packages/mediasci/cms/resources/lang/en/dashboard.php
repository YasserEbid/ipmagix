<?php

return [

    "title" => "Dashboard",
    "statistics" => "Statistics",
    "visitors_today" => "Visitors Today",
    "total_posts" => "Total Posts",
    "recent_posts" => "Recent Posts",
    "total_comments" => "Total Comments",
    "recent_comments" => "Recent Comments",
    "total_users" => "Total Users",
    "recent_users" => "Recent Users",
    "news" => "News",
    "articles" => "Articles",
    "galleries" => "Galleries",
    "users" => "Users",
    "categories" => "Categories",
    "tags" => "Tags",
    "show_all" => "Show All",
    "by" => "by",
    "at" => "at",
    "on" => "on",
    "comments" => "Comments",
    "new_users" => "New Users",
    "photo" => "photo",
    "full_name" => "Full Name",
    "role" => "role",
    "pending_posts" => "Pending posts",
    "reviewed_posts" => "Reviewed posts",
    "no_posts" => "No Posts"
  
];
