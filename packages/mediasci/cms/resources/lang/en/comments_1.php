<?php

return [
    'home' => 'Home',
    'comments' => 'Comments',
    'comment' => 'Comment',
    'new' => 'Add New',
    'search' => 'Search...',
    'all' => 'All',
    'approve' => 'Approve',
    'approved' => 'Approved',
    's_approved' => 'Approved',
    's_unapproved' => 'Unapproved',
    'pending' => 'Pending',
    'unapprove' => 'Unapprove',
    'unapproved' => 'Unapproved',
    'spam' => 'Spam',
    'notspam' => 'Not Spam',
    'trash' => 'Trash',
    'trash_comment_q' => 'Are you sure to trash comment?',
    'delete_comment_q' => 'Are you sure to delete comment?',
    'edit' => 'Edit',
    'bulk' => 'Bulk Actions',
    'delete' => 'Delete Permanently',
    'restore' => 'Restore',
    'move_trash' => 'Move to Trash',
    'mark_spam' => 'Mark as Spam',
    'apply' => 'Apply',
    'per_page' => 'Per Page', 
    'author' => 'Author',
    'response_to' => 'In Response To',
    'submitted_on' => 'Submitted on',
    'no_comments' => 'No comments found!',
    'showing' => 'Showing',
    'to' => 'to',
    'of' => 'of',
    'comment' => 'comments',
	'like' => 'Like',
	'dislike' => 'Dislike',
	'comment_type' => 'Type',
	'filter_by' => 'Filter By',

    /*comment page*/

    'edit_comment' => 'Edit Comment',
    'name' => 'Name',
	'required' => 'This field is required.',
    'email' => 'Email',
    'well_done' => 'Well done!',
    'success_update' => 'You successfully edit this comment.',
    'oh_snap' => 'Oh snap!',
    'add' => 'Add!',
    'cancel' => 'Cancel',
    'preview_changes' => 'Preview Changes',
    'status' => 'Status',
    'ok' => 'Ok',
    'publish' => 'Save',
    ];