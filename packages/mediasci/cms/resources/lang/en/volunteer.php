<?php
return[
    'volunteer'=>'Partner Requests',
    'name'=>'Full Name',
    'show'=>'Show Volunteer',
    'operation'=>'Actions',
    'mail'=>'E-Mail',
    'phone'=>'Phone Number',
    'company_name'=>'Company Name',
    'message'=>'Message',
    'delete'=>'Delete',
    'back'=>'Back',
    'field'=>'Fields',
    'value'=>'Value',
    'gender'=>'Gender',
    'country'=>'Country',
    'birth'=>'Date of Birth'

];
