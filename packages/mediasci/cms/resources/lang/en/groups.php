<?php

return [
    'home' => 'Home',
    'groups' => 'Groups',
    'new' => 'Add New',
    'search' => 'Search...',
    'all' => 'All',
    'edit' => 'Edit',
    'bulk' => 'Bulk Actions',
    'delete' => 'Delete',
    'apply' => 'Apply',
    'per_page' => 'Per Page', 
    'name' => 'Name',
    'author' => 'Author',
    'last_updated' => 'Last Updated',
    'no_groups' => 'No groups found!',
    'showing' => 'Showing',
    'to' => 'to',
    'of' => 'of',
    'groups' => 'groups',

    /*page page*/

    'new_page' => 'Add New Group',
    'well_done' => 'Well done!',
    'success_update' => 'You successfully edited this group.',
    'oh_snap' => 'Oh snap!',
    'required' => 'This field is required.',
    'enter_name' => 'Enter name here',
    'related_pages' => 'Related Pages',
    'add' => 'Add!',
    'save' => 'Save',
    'updated_on' => 'Edited On',
    ];
