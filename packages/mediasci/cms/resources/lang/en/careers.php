<?php

return[
    'careers'=>'Careers',
    'delete'=>'Delete',
    'update'=>'Edit',
    'title'=>'Title',
     
    'operation'=>'Operation',
    'confirm'=>'',
    'new'=>'Add Career',
    'newmessage'=>'',
    'addnew'=>'Add Career',
    'submit'=>'Save',
    'success'=>'Operation success',
    'edit'=>'Edit Career',
    'editmessage'=>'',
    'category'=>'Category'
];

