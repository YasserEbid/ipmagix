<?php

return [
	//attributes
	'route_name' => 'Route Name',
	'conv_name' => 'Route Conventional name',
	'Required' => 'Required',
	'index_title' => 'Routes',
	'group_name' => 'Group Name',
	'plz_select' => 'please select group',
	'route_type'=>'Route Type',
	'save' => 'Save',
	'general_route'=>'General Route',
	'updated'=>'Edited Successfully',
];
