<?php

return [
    'index_title' => 'Users',
    'user_id' => 'User ID',
    'role_id' => 'User Role',
    'user_name' => 'User Name',
    'privilege' => 'Privilege',
    'full_name' => 'Full Name',
    'password' => 'Password',
    'email' => 'Email',
    'role' => 'Role',
    'save' => 'Save',
    'choose_role' => 'Choose Role',
	'create' => 'Add User',
	'edit' => 'Edit User',
	'no_users'=>'No Users',
	'actions'=>'Actions',
	'show_per_page'=>'Show Per Page',
	'created' => 'Added Successfully',
	'updated' => 'Edited Successfully',
	'mobile'=>'Mobile',
];
