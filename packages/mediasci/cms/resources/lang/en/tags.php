<?php

return [
	//attributes
	'tag_id' => 'Tag ID',
	'tag_name' => 'Tag Name',
	'actions' => 'Actions',
	'index_title' => 'Tags',
	'tag' => 'Tag',
	'name' => 'Name',
	'save' => 'Save',
	'create' => 'Add new tag',
	'edit' => 'Edit Tag',
	'no_tags'=>'No Tags Found',
	'confirm_delete'=>'Are you sure to delete this tag',
	'show_per_page'=>'Show per page',
	'tag_created'=>'Tag added successfully',
	'tag_updated'=>'Tag edited successfully',
];
