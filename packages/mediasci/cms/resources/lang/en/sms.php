<?php

return [
	//attributes
	'sms_id' => 'SMS ID',
	'sms_message' => 'SMS Message',
	'sent_date' => 'Sent Date',
	'users'=>'Users',
	'actions' => 'Actions',
	'index_title' => 'SMS',
	'sms' => 'SMS',
	'name' => 'Name',
	'site' => 'Site',
	'site_name' => 'Site Name',
	'choose_site' => 'choose site',
	'max_posts' => 'maxuimm number of posts',
	'save' => 'Save',
	'new' => 'Add SMS',
	'newmessage' => '',
	'edit' => 'Edit SMS',
	'editmessage' => '',
	'no_sms'=>'No SMS found',
	'confirm_delete'=>'Are you sure to delete this SMS',
	'show_per_page'=>'Show per page',
	'sms_created'=>'SMS added successfully',
	'sms_updated'=>'SMS edited successfully',
	'deleted' => 'SMS deleted successfully',
];
