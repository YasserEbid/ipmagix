<?php

return [
	//attributes
	'role_id' => 'Role ID',
	'role_name' => 'Role Name',
	'actions' => 'Actions',
	'index_title' => 'Roles',
	'create' => 'Add Role',
	'role' => 'Role',
	'name' => 'Name',
	'save' => 'Save',
	'create' => 'Add Role',
	'no_roles'=>'No Roles Found',
	'created' => 'Added Successfully',
	'updated' => 'Edited Successfully',
	'edit' => 'Edit Role',
];
