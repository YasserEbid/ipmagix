<?php

return [
	//attributes
	'block_id' => 'Block ID',
	'block_name' => 'Block Name',
	'actions' => 'Actions',
	'index_title' => 'Blocks',
	'block' => 'Block',
	'name' => 'Name',
	'site' => 'Site',
	'site_name' => 'Site Name',
	'choose_site' => 'choose site',
	'max_posts' => 'maxuimm number of posts',
	'save' => 'Save',
	'create' => 'Add new block',
	'edit' => 'Edit block',
	'no_blocks'=>'No blocks found',
	'confirm_delete'=>'Are you sure to delete this block',
	'show_per_page'=>'Show per page',
	'block_created'=>'Block Added successfully',
	'block_updated'=>'Block edited successfully',
	'bolck_posts_index_title'=>'Block Posts',
	'post_search'=>'Search for posts',
	'bolck_posts_index_title'=>'Block Posts',
	'block_posts_updated'=>'Posts Blocks added successfully',
];
