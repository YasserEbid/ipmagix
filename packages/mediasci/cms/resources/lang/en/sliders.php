<?php

return[
    'sliders'=>'Home Banners',
    'delete'=>'Delete',
    'update'=>'Edit',
    'title'=>'Title',
     'description'=>'Description',
    'operation'=>'Operation',
    'confirm'=>'',
    'new'=>'Add Home Banner',
    'newmessage'=>'',
    'addnew'=>'Add Home Banner',
    'edit'=>'Edit Home Banner',
    'submit'=>'Save',
    'success'=>'Operation Success',
    'editmessage'=>'',

];
