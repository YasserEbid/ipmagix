<?php

return array(
    "roles" => "Roles",
    "add_new" => "Add new",
    "role_name" => "Role name",
    "search_roles" => "Search roles...",
    "per_page" => "per page",
    "bulk_actions" => "Bulk actions",
    "delete" => "Delete",
    "apply" => "Apply",
    "name" => "Name",
    "actions" => "Actions",
    "sure_delete" => "You are about to delete.. continue?",
    "page" => "Page",
    "add_new_role" => "Add New Role",
    "add_new" => "Add new",
    "save" => "Save",
    "of" => "of",
    "role_created" => "Role added successfully",
    "role_updated" => "Role edited successfully",
    "role_deleted" => "Role deleted successfully",
    "bulk_actions" => "Bulk actions"
);