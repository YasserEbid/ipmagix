<?php

return [
    'all' => 'All',
    'topics' => 'Topics',
    'add_new' => 'Add New',
    'create' => 'Add',
    'new_topic' => 'New Topic',
    'save' => 'Save',
    'name' => 'Name',
    'description' => 'Description',
    'set_image' => 'Set Image',
    'create_message' => 'Topic Added',
    'image' => 'Image',
    'delete' => 'Delete',
    'delete_confirm'=>'Are you sure you want to delete this topic',
    'edit'=>'Edit'
];
