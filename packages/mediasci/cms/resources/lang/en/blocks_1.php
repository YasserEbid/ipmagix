<?php

return array(
    "title" => "Blocks",
    "add" => "Add block",
    "name" => "Name",
    "del" => "Delete",
    "edit" => "Edit",
    "done" => "Done",
    "block_posts" => "posts",
    "order" => "Order",
    "slug" => "Slug",
    "sure_delete" => "Delete block?",
    "max" => "Maximum posts"
);
