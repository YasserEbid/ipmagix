<?php

return [
    "users" => "Editors",
    "add_new" => "Add new",
    "search_users" => "Search editors ..",
    "per_page" => "Per page",
    "bulk_actions" => "Bulk actions",
    "delete" => "Delete",
    "apply" => "Apply",
    "photo" => "Photo",
    "name" => "Name",
    "created" => "Created date",
    "articles" => "articles",
    "role" => "Role",
    "actions" => "Actions",
    "sure_delete" => "You are about to delete editor.. continue?",
    "page" => "Page",
    "add_new_user" => "Add new editor",
    "confirm_password" => "Confirm password",
    "change" => "Change",
    "first_name" => "First name",
    "last_name" => "Last name",
    "about_me" => "About me",
    "is_not_an_image" => "is not an image",
    "role" => "Role",
    "activation" => "Activation",
    "language" => "Language",
    "special_permissions" => "Special permissions",
    "save_user" => "Save editor",
    "of" => "of",
    "activated" => "Activated",
    "deactivated" => "Deactivated",
     "reporter_created" => "Reporter added successfully",
    "reporter_updated" => "Reporter edited successfully",
    "reporter_deleted" => "Reporter deleted successfully",

];
