<?php

return [
    //attributes
    'post_id' => 'Post ID',
    'site_id' => 'Site ID',
    'created_by' => 'Added By',
    'created_date' => 'Creation Date',
    'title' => 'Title',
    'subtitle' => 'Subtitle',
    'brief' => 'Brief',
    'slug' => 'Post Slug',
    'newmessage' => '',
    'editmessage' => '',
    'post_content' => 'Post Content',
    "post_tabs" => "Post Tabs",
    'tags' => 'Tags',
    'lang' => 'Language',
    'user' => 'User',
    'new_tab' => 'New Tab',
    "update_tab" => "Update Tab",
    'full_name' => 'Full Name',
    'featured_image' => 'Featured Image',
    "internal_image" => "Internal Image",
    "logo_image" => "Logo",
    'set_image' => 'Set Image',
    'featured_video' => 'Featured Video',
    'video_cover' => 'Video Cover Image',
    'set_video' => 'Set Video',
    'locale_en' => 'English',
    'locale_ar' => 'Arabic',
    'categories' => 'Select Category',
    'category' => 'Category',
    'status' => 'Select Status',
    'published' => 'Published',
    'draft' => 'Draft',
    'deleted' => 'Deleted',
    'nothing' => 'Nothing Selected',
    'actions' => 'Actions',
    'new' => 'Add Post',
    'create' => 'Add Post',
    'edit' => 'Edit Post',
    'save' => 'Save',
    'index_title' => 'Posts',
    'no_posts' => 'No Posts',
    'show_per_page' => 'Show per page',
    'post_created' => 'Post added successfully',
    'post_updated' => 'Post edited successfully',
    'meta_title' => 'Meta Title',
    'meta_keywords' => 'Meta Keywords',
    'meta_description' => 'Meta Description',
    'is_link' => 'Is Link?',
    'intro_image' => 'Intro Image'
];
