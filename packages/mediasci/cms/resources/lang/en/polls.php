<?php

return[
    'index_title'=>'Polls',
    'featured_image'=>'Featured Image',
    'set_image'=>'Set Image',
     'polls'=>'Polls',
    'delete'=>'Delete',
    'update'=>'Edit',
    'title'=>'Title',
    'created'=>"New poll has been created successfully .",
    'updated'=>"This poll has been updated successfully .",
    'updatedm'=>"Sorry , It seems some thing went wrong . please try again.",

    'operation'=>'Operation',
    'confirm'=>'',
    'new'=>'Add Poll',
    'newmessage'=>'',
    'addnew'=>'Add Poll',
    'submit'=>'Save',
    'success'=>'Operation success',
    'edit'=>'Edit Poll',
    'editmessage'=>'',

];
