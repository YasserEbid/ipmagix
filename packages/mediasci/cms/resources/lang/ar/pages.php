<?php

return [
    'home' => 'الرئيسية',
    'pages' => 'الصفحات',
    'new' => 'أضف جديد',
    'search' => 'بحث...',
    'all' => 'الكل',
    'published' => 'منشور',
    'pending' => 'بانتظار المراجعة',
    'draft' => 'مسودة',
    'trash' => 'سلة المهملات',
    'edit' => 'تعديل',
    'preview' => 'معاينة',
    'bulk' => 'تنفيذ الأمر',
    'delete' => 'حذف بشكل دائم',
    'restore' => 'استعادة',
    'move_trash' => 'نقل الى سلة المهملات',
    'apply' => 'تنفيذ',
    'per_page' => 'لكل صفحة', 
    'title' => 'العنوان',
    'author' => 'الكاتب',
    'last_updated' => 'تاريخ اّخر تعديل',
    'no_pages' => 'لا يوجد صفحات!',
    'showing' => 'عرض',
    'to' => 'الى',
    'of' => 'من',
    'page' => 'صفحة',

    /*page page*/

    'new_page' => 'اضافة صفحة جديدة',
    'well_done' => 'تم!',
    'success_update' => 'لقد تم بنجاح تحديث الصفحة!',
    'oh_snap' => 'للاّسف!',
    'required' => 'هذا الحقل مطلوب.',
    'enter_title' => 'أكتب العنوان هنا',
    'add' => 'اضف!',
    'seo_options' => 'خيارات محركات البحث',
    'seo_title' => 'العنوان',
    'seo_keywords' => 'الكلمات المفتاحية',
    'seo_description' => 'وصف الميتا',
    'excerpt' => 'المقتطف',
    'excerpt_note' => 'المقتطف هو ملخّص يدوي اختياري للمحتوى.',
    'cancel' => 'الغاء',
    'publish' => 'حفظ',
    'preview_changes' => 'تعيين',
    'status' => 'الحالة',
    'ok' => 'موافق',
    'updated_on' => 'تم التحديث فى',
    'featured_image' => 'الصورة البارزة',
    'set_image' => 'تعيين صورة بارزة',
    'remove_image' => 'ازالة الصورة البارزة',
    'add_media' => 'أضف ملف وسائط'
    ];