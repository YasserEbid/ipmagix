<?php

return [
	
	"blocks" => "المربعات",
	"tags" => "الوسوم",
	"categories" => "التصنيفات",
	"comments" => "التعليقات",
	"pages" => "الصفحات",
	"posts" => "الأخبار",
	"users" => "الأعضاء",
	"authors" => "المؤلفين",
	"permissions" => "الصلاحيات",
	"reporters" => "المحررين",
	"groups" => "المجموعات",
	"areas"=>"المناطق"


];