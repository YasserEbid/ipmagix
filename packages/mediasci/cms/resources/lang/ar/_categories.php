<?php

return array(
    "title" => "التصنيفات",
    "add" => "إضافة تصنيف",
    "name" => "الإسم",
    "slug" => "الإسم اللطيف",
    "parent" => "الأب",
    "top_categories" => "التصنيفات الرئيسية",
    "del" => "حذف",
    "img" => "صورة",
    "edit" => "تعديل",
    "child_categories" => "التصنيفات الفرعية",
    "sure_delete" => "حذف التصنيف",
    "search_categories" => "بحث في التصنيفات"
);
