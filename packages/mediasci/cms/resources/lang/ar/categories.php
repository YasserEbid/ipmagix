<?php

return [
    "galleries" => "التصنيفات",
    "add_new" => "أضف جديد",
    "per_page" => "لكل صفحة",
    "bulk_actions" => "إختر أمر",
    "apply" => "حفظ",
    "delete" => "حذف",
    "name" => "إسم التصنيف",
    "actions" => "Action",
    "sure_delete" => "هل أنت متأكد من الحذف",
    "page" => "الصغحة",
    "add_new_gallery" => "إضافة تصنيف جديد",
    "no_cats" => "لا توجد تصنيفات",
    "save_category" => "حفظ التصنيف",
    "is_not_valid_image" => "ليست صورة",
    "of" => "من",
    "search_galleries" => "البحث فى التصنيفات",
    "bulk_actions" => "إختر الحدث",
    "cat_name" => "إسم التصنيف",
    "cat_slug" => "الإسم اللطيف",
    "cat_parent" => "التصنيف الرئيسى",
    "change" => "تغيير الصورة",
    "actions" => "تحكم",
    "is_not_an_image" => "ليست صورة"
    
    
];
