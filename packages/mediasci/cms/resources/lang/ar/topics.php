<?php

return [
    'all' => 'كل',
    'topics' => 'الموضوعات',
    'add_new' => 'اضافة جديد',
    'create' => 'انشاء',
    'new_topic' => 'موضوع جديد',
    'save' => 'حفظ',
    'name' => 'الاسم',
    'description' => 'الوصف',
    'set_image' => 'تعين صورة',
    'create_message' => 'تم اضافة الموضوع',
    'image' => 'صورة',
    'delete' => 'حذف',
    'delete_confirm' => 'حذف الموضوع؟',
    'edit' => 'تعديل'
];
