<?php

return [
    'students' => 'الطلاب',
    'search' => 'بحث...',
    'name' => 'الاسم',
    'email' => 'الايميل',
    'seat_no' => 'رقم الجلوس',
    'phone' => 'رقم التليفون',
    'govern' => 'المحافظة',
	'no_students' => 'لا يوجد طلاب',
    'showing' => 'عرض',
    'to' => 'الى',
    'of' => 'من',
    'students_pag' => 'طلاب',
    'per_page' => 'لكل صفحة', 
    'bulk' => 'تنفيذ الأمر',
    'apply' => 'تنفيذ',
    'delete' => 'حذف',
    ];