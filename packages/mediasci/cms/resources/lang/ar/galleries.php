<?php

return [
    "galleries" => "الألبومات",
    "add_new" => "أضف جديد",
    "per_page" => "لكل صفحة",
    "bulk_actions" => "Bulk actions",
    "apply" => "حفظ",
    "delete" => "حذف",
    "name" => "إسم الألبوم",
    "actions" => "Action",
    "sure_delete" => "هل أنت متأكد من الحذف",
    "page" => "الصغحة",
    "add_new_gallery" => "إضافة ألبوم جديد",
    "add_media" => "أضف صور",
    "gallery_author" => "إسم المحرر",
    "no_media" => "لا يوجد محتوى فى الألبوم",
    "save_gallery" => "حفظ الألبوم",
    "is_not_valid_image" => "ليست صورة",
    "of" => "من",
    "search_galleries" => "البحث فى الألبومات",
    "files" => "ملفات",
    "bulk_actions" => "إختر الحدث"
    
    
];
