<?php

return [
    "galleries" => "الوسوم",
    "add_new" => "أضف جديد",
    "per_page" => "لكل صفحة",
    "bulk_actions" => "إختر أمر",
    "apply" => "حفظ",
    "delete" => "حذف",
    "name" => "إسم الوسم",
    "actions" => "Action",
    "sure_delete" => "هل أنت متأكد من الحذف",
    "page" => "الصغحة",
    "add_new_gallery" => "إضافة وسم جديد",
    "no_cats" => "لا توجد وسوم",
    "save_category" => "حفظ الوسم",
    "is_not_valid_image" => "ليست صورة",
    "of" => "من",
    "search_galleries" => "البحث فى الوسوم",
    "bulk_actions" => "إختر الحدث",
    "cat_name" => "إسم الوسم",
    "cat_slug" => "الإسم اللطيف",
    "change" => "تغيير الصورة",
    "actions" => "تحكم",
    "is_not_an_image" => "ليست صورة"
    
    
];
