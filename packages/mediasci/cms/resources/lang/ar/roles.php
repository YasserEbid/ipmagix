<?php

return array(
    "roles" => "الصلاحيات",
    "add_new" => "أضف جديد",
    "role_name" => "الصلاحية",
    "search_roles" => "البحث فى الصلاحيات ..",
    "per_page" => "per page",
    "bulk_actions" => "Bulk actions",
    "delete" => "حذف",
    "apply" => "حفظ",
    "name" => "الإسم",
    "actions" => "الحدث",
    "sure_delete" => "هل أنت متأكد من الحذف",
    "page" => "الصفحة",
    "add_new_role" => "أضف صلاحية",
    "add_new" => "أضف جديد",
    "save" => "حفظ",
    "of" => "من",
    "bulk_actions" => "اختر أمر",
    "role_created" => "تم إضافة الصلاحية بنجاح",
    "role_updated" => "تم تعديل الصلاحية بنجاح",
    "role_deleted" => "تم حذف الصلاحية بنجاح",
);