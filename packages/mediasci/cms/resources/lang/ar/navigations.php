<?php

return [
    'navigation' => 'قائمة',
    'navigations' => 'قوائم',
    'all' => 'كل',
    'add_new' => 'أضافة جديد',
    'create' => 'انشاء',
    'menu_name' => 'الاسم',
    'edit' => 'تحرير',
    'items' => 'عناصر',
    'menu_data' => 'معلومات القائمة',
    'new_navigation' => 'قائمة جديدة',
    'save' => 'حفظ',
    'posts' => 'مقالات',
    'pages' => 'صفحات',
    'categories' => 'تصنيفات',
    'url' => 'رابط',
    'search' => 'بحث',
    'add' => 'اضافة',
    'text' => 'النص',
    'filter' => 'تصفية',
    'save_order' => 'حفظ الترتيب'
];
