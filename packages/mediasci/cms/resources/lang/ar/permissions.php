<?php

return array(
    
    //permissions
    "manage_permissions" => "التحكم بالصلاحيات",
    
    // ِAuthors
    "manage_authors" => "التحكم بالمؤلفين",

    //reporters
    "manage_reporters" => "التحكم بالمحررين",
    
    // users
    "manage_users" => "التحكم بالأعضاء",
   
    "create_categories" => "إضافة التصنيفا",
    "edit_categories" => "تعديل التصنيفات",
    "delete_categories" => "حذف التصنيفات",
    "create_tags" => "إضافة الوسوم",
    "edit_tags" => "تعديل الوسوم",
    "delete_tags" => "حذف الوسوم",
    "create_blocks" => "إضافة البلوكات",
    "edit_blocks" => "تعديل البلوكات",
    "delete_blocks" => "حذف البلوكات",
    "sort_blocks" => "ترتيب البلوكات",

    "create_posts" => "إضافة الأخبار",
    "edit_posts" => "تعديل الأخبار",
    "delete_posts" => "حذف الأخبار",
    "trash_posts" => "رفض الأخبار",
    "order_posts" => "ترتيب الأخبار",
    "restore_posts" => "إستعادة الأخبار",
    "published_posts" => "الموافقة على الأخبار",
    "pending_posts" => "التبليغ عن الأخبار",
    "reviewed_posts" => "تحرير الأخبار",
    "draft_posts" => "بانتظار الموافقة",
    "scheduled_posts" => "جدولة الأخبار",
    "move_posts" => "نقل الأخبار",
    "rank_posts" => "تقييم الأخبار",

    "create_pages" => "إضافة الصفحات",
    "edit_pages" => "تعديل الصفحات",
    "delete_pages" => "حذف الصفحات ",
    "trash_pages" => "النقل لسلة المهملات",
    "restore_pages" => "إستعادة الصفحات",
    "published_pages" => "نشر الصفحات",
    "pending_pages" => "الحفظ بانتظار المراجعة",
    "draft_pages" => "حفظ الصفحات كمسودة",

	"create_groups" => "اضافة مجموعات",
    "edit_groups" => "تعديل المجموعات",
    "delete_groups" => "حذف المجموعات",

	"create_area" => "إضافة منطقة",
    "edit_area" => "تعديل منطقة",
    "delete_area" => "حذف منطقة",

    "add_comments" => "إضافة التعليقات",
    "edit_comments" => "تعديل التعليقات",
    "delete_comments" => "حذف التعليقات",
    "trash_comments" => "النقل لسلة المهملات",
    "restore_comments" => "إستعادة التعليقات",
    "approve_comments" => "الموافقة على التعليقات",
    "unapprove_comments" => "رفض التعليقات",
    "spam_comments" => "نقل التعليقات كمزعج",
    "not_spam_comments" => "حفظ التعليقات كغير مزعج"
);