@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1><?= trans('cms::news.index_title'); ?></h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<!-- ./page title -->

<!-- data table -->
<div class="wrapper wrapper-white">
    <div class="form-group">
        <a href="<?php echo route('news.create', ['site_id' => $request->input('site_id')]); ?>" class="btn btn-primary"><?= trans('cms::news.new'); ?></a>
    </div>
    <div class="form-group">
        <?php if (session('message')) { ?>
            <div class="alert alert-success">
                <?php echo session('message'); ?>
            </div>
        <?php } ?>
        <?php session()->forget('message'); ?>
    </div>
    <div class="dataTables_length">
        <div>
            <label><?php echo trans('cms::news.show_per_page'); ?> </label>
            <select class='per_page form-control'>
                <option value="10" <?php echo ($request->input(['per_page']) == 10) ? 'selected' : '' ?> >10</option>
                <option value="25" <?php echo ($request->input(['per_page']) == 25) ? 'selected' : '' ?> >25</option>
                <option value="50" <?php echo ($request->input(['per_page']) == 50) ? 'selected' : '' ?> >50</option>
                <option value="100" <?php echo ($request->input(['per_page']) == 100) ? 'selected' : '' ?> >100</option>
            </select>
        </div>
    </div>

    <div class="dataTables_filter">
        <?php echo Form::open(['route' => ['news.index'], 'method' => 'get', 'id' => 'users_search_form']); ?>
        <label>Search:<input type="search" value="<?php echo $request->input('q'); ?>" name="q" class="form-control search_q"></label>
        <?php echo Form::close(); ?>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th><?= trans('cms::news.post_id'); ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Title', 'title') ?></th>
                    <th><?= trans('cms::news.category'); ?></th>
                    <th><?= trans('cms::news.created_date'); ?></th>
                    <th><?= trans('cms::news.actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($posts)) { ?>
                    <?php foreach ($posts as $one) { ?>
                        <tr>
                            <td><?php echo $one->post_id; ?></td>
                            <td><?php echo $one->post_title; ?></td>
                            <td><?php echo $one->name; ?></td>
                            <td><?php echo $one->created_date; ?></td>
                            <td>
                                <a href="<?php echo route('news.edit', ['id' => $one->post_id, 'lang' => $one->lang, 'site_id' => $request->input('site_id')]); ?>" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo route('posts.delete', ['id' => $one->post_id]); ?>" onclick="return confirm('<?php echo trans('cms::posts.confirm_delete'); ?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td style="text-align: center;" colspan="6"><?php echo trans('cms::news.no_posts'); ?></td>
                    </tr>
<?php } ?>
            </tbody>
        </table>
        <?php
        if ($posts) {
            echo $posts->appends($request->except('page'))->links();
        }
        ?>
    </div>
</div>
<!-- ./data table -->
<?php
$params_per_page = count($request->except(['per_page'])) ? http_build_query($request->except(['per_page'])) : '';

$params_search = count($request->except(['q'])) ? http_build_query($request->except(['q'])) : '';
?>
<script>
    $(document).ready(function () {
        $('.per_page').change(function () {
            location.href = '<?php echo route('posts.index'); ?>?<?php echo $params_per_page ? $params_per_page . '&' : ''; ?>per_page=' + $(this).val();
        });


        $('#users_search_form').submit(function (e) {
            var q = $(this).find('.search_q');

            if (q.length > 0) {
                if (q.val().trim() == '') {
                    location.href = '<?php echo route('posts.index'); ?><?php echo $params_search ? '?' . $params_search : ''; ?>';
                    return false;
                }
            } else {
                return false;
            }
        });

    });
</script>
@stop
