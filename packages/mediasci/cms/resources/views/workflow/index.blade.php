@extends('cms::layouts.master')

@section('content')
<div class="page-title">
    <h1>Journeys</h1>
   <ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

	</ul>
</div>
<div class="wrapper wrapper-white">
    <div class="row">
         <div class="form-group">

     </div>
        @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
        <?php session()->forget('message');?>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            <?php session()->forget('error');?>

        @endif
        
        <a href="./admin/journeys/create">
            <button class="btn btn-primary">{{trans('cms::workflow.add_new')}}</button>
        </a>
        <div class="table-responsive">
        <!--<table class="table table-bordered table-striped table-sortable">-->
        <?php if($workflows->count() > 0){ ?>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id='table-data'>
                    @foreach($workflows as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->title}}</td>

                        <td>

                            <a  class="btn default btn-xs red" href="./admin/journeys/update/{{$row->id}}">
                                <i class="fa fa-edit" type="button" ></i>
                            </a>
                            <a  class="btn default btn-xs red" href="./admin/journeys/delete/{{$row->id}}" onclick="return confirm('Are you sure you want to delete this journey?')">
                                <i class="fa fa-trash" type="button"></i>
                            </a>
                            <a class="btn default btn-xs red" href='./admin/journeys/show/{{$row->id}}'  >
                                <i class="fa fa-eye" type="button"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <?php }else{
              ?>
<h3>Sorry , there are no journeys yet .</h3>
              <?php
            }?>

        </div>
    </div>
</div>



@stop

@section('js')
<script type='text/javascript' src='./assets/dashboard/js/plugins/tags-input/jquery.tagsinput.min.js'></script>
@stop
