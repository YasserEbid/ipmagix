@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>{{trans('cms::workflow.show')}}</h1>
	<ul class="breadcrumb">
		<li><a href="<?= route('workflow'); ?>">{{trans('cms::workflow.name')}}</a></li>
	</ul>
</div>                        
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        
        <form method="post">
            @include('cms::partials.filter_update',['object'=>$workflow]);
            
            <div class="row">
                <label>{{trans('workflow.title')}}</label>
                <input type="text" name="title" value="{{$workflow->title}}" required="" class="form-control"/>
            </div>
            <br/>
            
            <div id='levels'>
                @foreach($workflow->levels()->get() as $level)
                    <div class="row level">
                        <div class="row" style="position: relative;border: 1px solid;border-radius: 5px;margin: 10px 0px;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('workflow.input_level_name')}}</label>
                                    <input type='text' name="level['name'][]" value="{{$level->name}}" class="name-level form-control" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>URL</label>
                                    <input type="url" name="level['url'][]" value='{{$level->url}}' class="form-control" required=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            
            <br style="clear:both"/>
            <a href="{{ route('workflow')}}">
                <button type="button" class=" btn btn-primary">Back</button>
            </a>
        </form>
        <div class="row level"  style="display: none;" id="new-level">
            <div class="row" style="position: relative;border: 1px solid;border-radius: 5px;margin: 10px 0px;">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{trans('workflow.input_level_name')}}</label>
                        <input type='text' name="level['name'][]" class="name-level form-control" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>URL</label>
                        <input type="url" name="level['url'][]" class="form-control" required=""/>
                    </div>
                </div>
                <a href='javscript:void(0)' class="delete-level" style="position: absolute;top: -6px;right: -3px;">
                    <img src='./assets/dashboard/img/close.png'  width="20"  />
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop

@section('js')
<script>
    $(document).ready(function(){
        $('input[type=text]').attr('readonly',true);
        $('input[type=url]').attr('readonly',true);
        $('select').attr('readonly',true);

        
    });
</script>
<style>
.disabled-div {
    pointer-events: none;
    opacity: 0.4;
}
</style>

@stop
