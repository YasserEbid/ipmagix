@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>{{trans('cms::workflow.update')}}</h1>
         <p>{{trans('cms::workflow.editmessage')}}</p>
	<ul class="breadcrumb">
            <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

		<li><a href="<?= route('workflow'); ?>">{{trans('cms::workflow.name')}}</a></li>
	</ul>
</div>
<!-- ./page title -->

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>
<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
        @endif
        <form method="post">
					@include("cms::filter_update",['gender_id' => $workflow->gender_id , 'lifestyle_id' => $workflow->lifestyle_id , 'location_id' => $workflow->location_id , 'nationality_id' => $workflow->nationality_id])

                <div class="col-md-6">
                                <div class="form-group">
            <div class="row">
                <label>{{trans('cms::workflow.title')}}</label>
                <input type="text" name="title" value="{{$workflow->title}}" required="" class="form-control"/>
            </div>
            </div></div>
            <br/>
            <button type="button" class="btn btn-primary" id="add-level">Add Level</button>
            <br/>
            <div id='levels'>
                @foreach($workflow->levels()->get() as $level)
                    <div class="row level">
                        <div class="row" style="position: relative;border: 1px solid;border-radius: 5px;margin: 10px 0px;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('cms::workflow.input_level_name')}}</label>
                                    <input type='text' name="level['name'][]" value="{{$level->name}}" class="name-level form-control" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('cms::workflow.input_level_url')}}</label>
                                    <input type="url" name="level['url'][]" value='{{$level->url}}' class="form-control" required=""/>
                                </div>
                            </div>
                            <a href='javscript:void(0)' class="delete-level" style="position: absolute;top: -6px;right: -3px;">
                                <img src='./assets/dashboard/img/close.png'  width="20"  />
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>

            <br style="clear:both"/>
            <input type="submit" style="display: none" />
            <button type="button" class="submit btn btn-primary">Save</button>

        </form>
        <div class="row level"  style="display: none;" id="new-level">
            <div class="row" style="position: relative;border: 1px solid;border-radius: 5px;margin: 10px 0px;">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{trans('cms::workflow.input_level_name')}}</label>
                        <input type='text' name="level['name'][]" class="name-level form-control" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{trans('cms::workflow.input_level_url')}}</label>
                        <input type="url" name="level['url'][]" class="form-control" required=""/>
                    </div>
                </div>
                <a href='javscript:void(0)' class="delete-level" style="position: absolute;top: -6px;right: -3px;">
                    <img src='./assets/dashboard/img/close.png'  width="20"  />
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop

@section('js')
<script>
    $(document).ready(function(){
        $('#add-level').click(function(){
           $('#levels').append($('#new-level').html());
           $(window).resize();
        });
        $('#levels').on('click','.delete-level',function(e){
            e.preventDefault();
           $(this).closest('div').remove();
        });
        $('.submit').click(function(){
           var level_length= $(".name-level").length;
           if(level_length<2){
               alert('you must add one level at least');
                return false;
           }
            $('input[type=submit]').trigger('click');
        });


    });
</script>
<style>
.disabled-div {
    pointer-events: none;
    opacity: 0.4;
}
</style>

@stop
