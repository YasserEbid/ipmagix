@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>{{trans('cms::workflow.create')}}</h1>
        <p>{{trans('cms::workflow.newmessage')}}</p>
	<ul class="breadcrumb">
            <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

		<li><a href="<?= route('workflow'); ?>">{{trans('cms::workflow.name')}}</a></li>
	</ul>
</div>
<style>
    /*.form-group {

    width: 50%;
    }*/
</style>
<!-- ./page title -->
<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>

<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        @if(Session::has('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
        @endif
        <form method="post" class="add">
						@include("cms::filters")
            <br/>
            <div class="col-md-6">
          <div class="form-group">
                <label>{{trans('cms::workflow.title')}}</label>
                <input type="text" name="title" required="required" class="form-control"/>
            </div>
					</div>
										<div class="col-md-6">
											<div class="form-group">
            <button type="button" class="btn btn-primary" id="add-level" style="margin-top: 30px;">add level</button>
					</div>
					</div>
            <div id='levels' style="margin-top:100px;"></div>
            <br style="clear:both"/>
            <input type="submit" style="display: none" />
            <button id="workflow_submit" type="button" class="submit btn btn-primary">Save</button>

        </form>
        <div class="row level"  style="display: none;" id="new-level">
            <div class="row" style="float: left;
    width: 100%;position: relative;border: 1px solid;border-radius: 5px;margin: 10px 0px;">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{trans('cms::workflow.input_level_name')}}</label>
                        <input type='text' name="level['name'][]" class="name-level form-control" required="required">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <br/> <label>{{trans('cms::workflow.input_level_url')}}</label>
                        <input type="url" name="level['url'][]" class="form-control" required=""/>
                    </div>
                </div>
                <a href='javscript:void(0)' class="delete-level" style="position: absolute;top: -6px;right: -3px;">
                    <img src='./assets/dashboard/img/close.png'  width="20"  />
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop

@section('js')
<script>
    $(document).ready(function(){
        $('#add-level').click(function(){
           $('#levels').append($('#new-level').html());
           $(window).resize();
        });
        $('#levels').on('click','.delete-level',function(e){
            e.preventDefault();
           $(this).closest('div').remove();
        });
        $('#workflow_submit').click(function(){
           var level_length= $(".name-level").length;
           if(level_length<2){
               alert('you must add one level at least');
                return false;
           }

					// $(this).parents().find('input[type=submit]').trigger('click');
            $(this).closest('form.add').find('input[type=submit]').trigger('click');
        });


    });
</script>
<style>
.disabled-div {
    pointer-events: none;
    opacity: 0.4;
}
</style>

@stop
