
@extends('cms::layouts.master')

@section('content')
@if(Session::has('fail'))
<?php $a = [];
$a = session()->pull('fail');
?>
<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
<?php session()->forget('fail'); ?>
@endif
@if(Session::has('success'))
<?php $a = [];
$a = session()->pull('success');
?>
<div class="alert alert-success" role="alert">{{$a[0]}} </div>
<?php session()->forget('success'); ?>
@endif
<div class="page-title">
    <h1>{{trans('cms::questionsAnswers.questionsAnswers')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>

</div>
<!-- page title -->
<div class="wrapper wrapper-white">
    @if(Session::has('fail'))
<?php $a = [];
$a = session()->pull('fail');
?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    @endif
    @if(Session::has('success'))
<?php $a = [];
$a = session()->pull('success');
?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    @endif

    <div class="form-group">
        <a href="{{URL('admin/questionsAnswers/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::questionsAnswers.new')}}</button></a>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Question', 'question') ?></th>
                    <th><?= trans('cms::questionsAnswers.active'); ?></th>
                    <th><?= trans('cms::questionsAnswers.created_at'); ?></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(empty($questionsAnswers))
                <tr>
                    <td>No questionsAnswers Found.</td>
                </tr>
                @else
                @foreach($questionsAnswers as $question)
                <tr>
                    <td>{{$question->questionAnswerLang('en')->question}}</td>
                    <td>{{($question->active)?"Yes":"No"}}</td>
                    <td>{{$question->created_at}}</td>
                    <td>
                        <a href="{{URL('admin/questionsAnswers/update/'.$question->id)}}"  class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a id="del" href="{{URL('admin/questionsAnswers/delete/'.$question->id)}}"  onclick="return confirm('Are you sure you want to delete this FAQ?')" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <div class="btn-group" role="group">
            @unless(empty($questionsAnswers))
            {!! $questionsAnswers->links()!!}
            @endunless
        </div>
    </div>
</div>
@endsection
