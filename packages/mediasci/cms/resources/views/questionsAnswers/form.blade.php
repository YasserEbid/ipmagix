@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo $current_questionAnswer ? trans('cms::questionsAnswers.edit') : trans('cms::questionsAnswers.create'); ?></h1>

        <p><?php echo $current_questionAnswer ? trans('cms::questionsAnswers.editmessage') : trans('cms::questionsAnswers.newmessage'); ?></p>
	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><a href="<?php echo url('admin/questionsAnswers'); ?>"><?php echo trans('cms::questionsAnswers.index_title'); ?></a></li>
		</ul>
</div>
<!-- ./page title -->
<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>

<!-- Horizontal Form -->
<div class="wrapper wrapper-white">

	<?php if (isset($errors)) { ?>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error) { ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php } ?>

	@if(Session::has('fail'))
		<?php $a = [];
		$a = session()->pull('fail'); ?>
		<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
	@endif
	@if(Session::has('success'))
	<?php $a = [];
		$a = session()->pull('success'); ?>
		<div class="alert alert-success" role="alert">{{$a[0]}} </div>
	@endif
	<?php //$form_route = ($current_questionAnswer ? ['questionsAnswers.update', $current_questionAnswer->id] : ['questionsAnswers.create']); ?>
	<?php echo Form::open( [/*'route' => $form_route,*/ 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'post_form']); ?>
	<?php if($current_questionAnswer ) : ?>
		<input type="hidden" value="<?php echo $current_questionAnswer->id; ?>" name="service_lang_id" />
		<input type="hidden" value="<?php echo $current_questionAnswer->lang; ?>" name="lang" />
	<?php endif; ?>

	<?php
		// $app_locales = Config::get("app.locales");
		$app_locales = $languages;
		$app_locale = Config::get("app.locale");
	?>

	<div class="wrapper-white col-sm-8 pull-left">
		<div class="widget-tabbed margin-top-30">
			<ul class="widget-tabs widget-tabs-three">
				<?php foreach($app_locales as $key => $locale) : ?>
					<?php $active_tab = ($locale['lang'] == $app_locale ? 'active' : ''); ?>
					<li class="<?php echo $active_tab; ?>"><a href="#question__<?php echo $locale['lang']; ?>"><?php echo trans('cms::questionsAnswers.locale_'.$locale['lang']); ?></a></li>
				<?php endforeach; ?>
			</ul>
			<?php foreach($app_locales as $key => $locale) : ?>
				<div class="widget-tab <?php echo $active_tab; ?>" id="question__<?php echo $locale['lang']; ?>">
					<div class="form-group">
						<label class="col-md-2 control-label" for="question_<?php echo $locale['lang']; ?>"><?php echo trans('cms::questionsAnswers.question'); ?></label>
						<div class="col-md-10">
							<input id="question_<?php echo $locale->lang; ?>" type='text'  name="question_<?php echo $locale->lang; ?>" class="form-control"
							value='<?php echo old('question_'.$locale->lang, (isset ($questionAnswer_langs) ? $questionAnswer_langs[$locale->lang]->question : '')); ?>' placeholder="<?php echo trans('cms::questionsAnswers.question'); ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="answer_<?php echo $locale->lang; ?>"><?php echo trans('cms::questionsAnswers.answer'); ?></label>
						<div class="col-md-10">
							<textarea id="answer_<?php echo $locale->lang; ?>" class="form-control summernote" rows="5" name="answer_<?php echo $locale->lang; ?>" placeholder="<?php echo trans('cms::questionsAnswers.answer'); ?>" >
								<?php echo old('answer_'.$locale->lang, (isset($questionAnswer_langs) ? $questionAnswer_langs[$locale->lang]->answer : '')); ?>
							</textarea>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="active"><?php echo trans('cms::questionsAnswers.active'); ?><span></span></label>
			<?= Form::checkbox('active', '1',(isset($current_questionAnswer->questionAnswer) && $current_questionAnswer->questionAnswer->active?true:false),['id' => 'active']);?>
		</div>
	</div>

	<div class="wrapper wrapper-white col-sm-8 pull-left">
		<div class="form-group">
			<div class="col-md-offset-1 col-md-8">
				<button type="submit" class="btn btn-primary"><?php echo trans('cms::questionsAnswers.save'); ?></button>
			</div>
		</div>
	</div>
	<?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop
