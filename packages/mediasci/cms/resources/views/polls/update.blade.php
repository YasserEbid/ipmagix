@extends('cms::layouts.master')

@section('content')
<?php
$current_lang = Config::get('app.locale');
?>
<div class="page-title">
    <h1>{{trans('cms::polls.edit')}}</h1>
    <p>{{trans('cms::polls.editmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="{{URL('admin/polls')}}">{{trans('cms::polls.polls')}}</a></li>

    </ul>
</div>

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>
<div class="wrapper wrapper-white">
    <div class="row">
        <form method="post">
            <div class="tabs">
                <ul class="nav nav-tabs nav-tabs-arrowed" role="tablist">
                  <?php foreach($languages as $language){?>
<li class="<?= ($language->lang == $current_lang)? "active":""?>"><a href="#tab1-{{$language->lang}}" role="tab" data-toggle="tab" aria-expanded="<?= ($language->lang == $current_lang)? "false":"true"?>">{{$language->name}}</a></li>
<?php }?>

                </ul>
                <div class="panel-body tab-content">
                  <?php foreach($languages as $language){?>
                    <div class="tab-pane <?= ($language->lang == $current_lang)? "active":""?>" id="tab1-{{$language->lang}}">
                            <div class="form-group">
                              <label>Question: <span></span></label>
                                <input class="form-control" type="text" name="{{$language->lang}}[question]" value="{{$poll->allLang()->lang()->where('lang',$language->lang)->first()->question}}" required />
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label>Question: <span></span></label>
                                        <input type="text" class="tags" name="{{$language->lang}}[options]" value="{{$poll->optionsConcat($language->lang)}}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Save</button>
        </form>
    </div>
</div>
@stop

@section('js')
    <script type='text/javascript' src='./assets/dashboard/js/plugins/tags-input/jquery.tagsinput.min.js'></script>
@stop
