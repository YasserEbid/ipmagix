@extends('cms::layouts.master')

@section('content')
<?php
$locale = \Config::get("app.locale");
 ?>

<div class="page-title">


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?= trans('cms::common.dashboard'); ?></a></li>
		<li><a href="<?php echo route('polls.index'); ?>"><?= trans('cms::polls.index_title'); ?></a></li>
	</ul>
</div>

<div class="wrapper wrapper-white">
    <div class="row">
        @if(isset($answers))
        <h1> QUESTION: {{$poll->lang()->first()->question}}</h1>
				<ul>
					<li> <h1>Options</h1>
						<ul>
              @foreach($poll_options as $key => $option)
							  @foreach($option as $key => $mini_option)
                  @if($mini_option)
  								  <li> <h5> {{$mini_option['option']}}  <h5></li>
                  @endif
                @endforeach
							@endforeach
						</ul>

					</li>
				</ul>

        <?php $colors = ['red' => '#F78C8C', 'green' => '#82b440', 'blue' => '#7BCAF1', 'yellow' => '#FFEB3B']; ?>
				@foreach($answers as $row)
        <?php
        $count = round(($row->count / $total) * 100);
        if ($count > 70) {
            $color = $colors['green'];
        } else if ($count > 35) {
            $color = $colors['blue'];
        } else if ($count > 15) {
            $color = $colors['yellow'];
        } else {
            $color = $colors['red'];
        }
        ?>
        <div class="col-md-4">
            <h4 style="color:{{$color}};font-weight: bold;">
                <?=  \Mediasci\Cms\Models\PollsOptions::find($row->option_id)->lang()->first()->option?> - ({{$row->count}})</h4>
            <input class="knob" data-width="100" data-min="0" data-fgColor="{{$color}}" data-displayPrevious=true value="<?= $count ?>" data-thickness=".1" data-linecap=round data-readOnly=true/>
        </div>

        @endforeach
        <br style="clear:both"/>

        @else
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>!</strong> NO result to been show , try again later.
        </div>
        @endif
    </div>
</div>




@stop

@section('js')
<script type="text/javascript" src="./assets/dashboard/js/plugins/knob/jquery.knob.min.js"></script>
@stop
