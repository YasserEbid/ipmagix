@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::polls.polls')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>

</div>
<!-- ./page title -->
<!-- page title -->
<div class="wrapper wrapper-white">
    <div class="row">
        <?php if (Session::has('n')) { ?>
            <div class="alert alert-success" role="alert">
                <?php echo session()->pull('n'); ?>
            </div>
            <?php session()->forget('n'); ?>
        <?php } else if (Session::has('m')) { ?>
            <div class="alert alert-danger" role="alert">
                <?php echo session()->pull('m'); ?>
            </div>
            <?php session()->forget('m'); ?>
        <?php } ?>

    </div>



    <div class="form-group">
        <a href="{{URL('admin/polls/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::polls.new')}}</button></a>
    </div>




    <div class="table-responsive">
        <table class="table table-bordered  table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id='table-data'>
                @foreach($polls as $row)
                <tr>
                    <td>{{$row->allLang()->lang()->where('lang','en')->first()->question}}</td>

                    <td>
                        <a href="./admin/polls/result/{{$row->id}}"  class="btn default btn-xs red">
                            <i class="fa fa-eye" type="button" ></i>
                        </a>
                        <a href="./admin/polls/update/{{$row->id}}"  class="btn default btn-xs red">
                            <i class="fa fa-edit" type="button" ></i>
                        </a>
                        <a href="./admin/polls/delete/{{$row->id}}"  class="btn default btn-xs red" onclick="return confirm('Are you sure you want to delete this Poll?')">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
            {!! $polls->links()!!}
        </div>
    </div>

    @endsection
