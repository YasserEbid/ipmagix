<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Mediasci CMS</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >

        <link rel="icon" href="{{URL::asset('assets/dashboard/img/favicon.ico')}}" type="image/x-icon" >
        <!-- /meta section -->        

        <!-- css styles -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/dashboard/css/default-blue-white.css')}}" id="dev-css">
        <!-- ./css styles -->                               

        <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/dashboard/css/dev-other/dev-ie-fix.css')}}">
        <![endif]-->

        <!-- javascripts -->
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/modernizr/modernizr.js')}}"></script>
        <!-- ./javascripts -->

        <style>.dev-page{visibility: hidden;}</style>
    </head>
    <body>

        <!-- page wrapper -->
        <div class="dev-page dev-page-login dev-page-login-v2">

            <div class="dev-page-login-block">
                <a class="dev-page-login-block__logo">MediaSci CMS</a>
                <div class="dev-page-login-block__form">
                    <div class="title"><strong>Welcome</strong>, please login</div>

					<?php if (isset($errors)) { ?>
						<?php if (count($errors) > 0) { ?>
							<div class="alert alert-danger">
								<ul>
									<?php foreach ($errors->all() as $error) { ?>
										<li><?php echo $error; ?></li>
									<?php } ?>
								</ul>
							</div>
						<?php } ?>
					<?php } ?>

					<?php if (session('message')) { ?>
						<div class="alert alert-danger">
							<?php echo session('message'); ?>
						</div>
					<?php } ?>

					<?php echo Form::open(['route' => 'auth.login', 'method' => 'post']); ?> 
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
							<input type="text" value="<?php echo old('email'); ?>" name="email" class="form-control" placeholder="Login">
						</div>
					</div>                        
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="password" name="password" class="form-control" placeholder="Password">
						</div>
					</div>
					<div class="form-group no-border margin-top-20">
						<button class="btn btn-success btn-block">Login</button>
					</div>                      
					<?php echo Form::close(); ?>

                </div>
                <div class="dev-page-login-block__footer">
                    © 2015 <strong>Mediasci</strong>. All rights reserved.
                </div>
            </div>

        </div>
        <!-- ./page wrapper -->                

        <!-- javascript -->
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/jquery/jquery.min.js')}}"></script>       
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/bootstrap/bootstrap.min.js')}}"></script>        
        <!-- ./javascript -->
    </body>
</html>






