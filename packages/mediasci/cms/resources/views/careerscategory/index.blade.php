@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title col-md-12">
	<h1>{{trans('cms::careercat.cats')}}</h1>

	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

	</ul>
</div>
<!-- ./page title -->

 <div class="wrapper wrapper-white">
   @if(Session::has('n'))
   <?php $a = [];
   $a = session()->pull('n'); ?>
   <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
	 <?php session()->forget('n');?>
   @endif @if(Session::has('m'))
   <?php $a = [];
   $a = session()->pull('m'); ?>
   <div class="alert alert-success" role="alert">{{$a[0]}} </div>
	 <?php session()->forget('m');?>
   @endif
	 
     <div class="form-group">
         <a href="{{URL('admin/careercategory/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::careercat.new')}}</button></a>
     </div>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>

                                            <th><?=\Mediasci\Cms\Http\Controllers\component\Helpers::sort('Title' , 'title')?></th>

                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                           @foreach($category as $cat)
                                            <td>{{$cat->catlang('en')->title}}</td>

                                            <td>
                                                    <a href="{{URL('admin/careercategory/update/'.$cat->id)}}" class="btn default btn-xs red">
                                                            <i class="fa fa-edit"></i>
                                                    </a>

                                                    <a id="del" onclick="return confirm('Are you sure you want to delete this Career Catecory?')" href="{{URL('admin/careercategory/delete/'.$cat->id)}}" class="btn default btn-xs red">
                                                            <i class="fa fa-trash"></i>
                                                    </a></td>
                                        </tr>
                                       @endforeach
                                </table>
                            </div>
     <div class="col-md-12"
     <div class="btn-group" role="group">
           <?php  $search_query = \Illuminate\Support\Facades\Input::except('page');
                    $category->appends($search_query); ?>
                                               {!! $category->links()!!}
                                            </div>
                        </div>

@endsection
