@extends('cms::layouts.master')
@section('content')
<?php
$current_lang = Config::get('app.locale');
?>
<div class="page-title">
    <h1>{{trans('cms::careercat.update')}}</h1>
    <p>{{trans('cms::careercat.editmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="{{URL('admin/dashboard')}}">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="{{URL('admin/careercategory')}}">{{trans('cms::careercat.cats')}}</a></li>

    </ul>
</div>

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careercat.confirm')}}
    </div></div>
        <div class="wrapper wrapper-white">
            <div class="wrapper">
            <div class="row">
                <form method="post" id="create_form" role="form" action="{{URL('admin/careercategory/update/'.$category->id)}}">
                    <div class='col-md-12'>

                        <div class="tabs">
                                        <ul class="nav nav-tabs nav-tabs-arrowed" role="tablist">
                                          <?php foreach($languages as $language){?>
                  <li class="<?= ($language->lang == $current_lang)? "active":""?>"><a href="#tab1-{{$language->lang}}" role="tab" data-toggle="tab" aria-expanded="<?= ($language->lang == $current_lang)? "false":"true"?>">{{$language->name}}</a></li>
                  <?php }?>

                                        </ul>
                                        <div class="panel-body tab-content">
                                          <?php foreach($languages as $language){?>
                                            <div class="tab-pane <?= ($language->lang == $current_lang)? "active":""?>" id="tab1-{{$language->lang}}">
                                                <div class="col-md-12" >


                                                 <div class="form-group">
                    <label>Title: <span></span></label>
                    <input type="text" class="form-control" name="{{$language->lang}}_title" required="" minlength="3" value="{{$category->catlang($language->lang)->title}}"/>
                </div>

                                                </div>


                                            </div>
                                        <?php }?>

                                        </div>


                                    </div>


                    </div>
                      <div class="col-md-12">

                        <div class="col-sm-4 pull-left">
            <div class="panel">

 @if ($category->media)
<a href="#" class="featured-preview set-post-thumbnail"><img src="{{URL('uploads/'.$category->media->media_path)}}"></a>
                @endif
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::polls.featured_image')}}</span>
                </div>
                <div class="panel-body" id="image-container">

                    <div class="row">
                        <a href="#" class="set-post-thumbnail">{{Lang::get('cms::polls.set_image')}}</a>
                    </div>

                </div>
            </div>
        </div>
                        </div>
                    <div class="pull-left margin-top-10">
                <!--<button class="btn btn-warning hide-prompts" type="button">Hide prompts</button>-->
                <button class="btn btn-primary" type="submit" id='btn-submit'>{{trans('cms::careercat.submit')}}</button>
            </div>

                </form></div>
            </div>
        </div>
          <script>
$(function() {
      $(".set-post-thumbnail").click(function(e) {
        e.preventDefault();
    });
    $(".set-post-thumbnail").filemanager({
        types: "png|jpg|jpeg|gif|bmp",
        done: function(files) {
            if (files.length) {
                var formContainer = $('#create_form');
                var imageContainer = $('#image-container');
                var file = files[0];
                imageContainer.find($('a.featured-preview')).remove();
                $('#featured_image_id').remove();
                formContainer.append($('<input>', {
                    'type': 'hidden',
                    'name': 'featured_image',
                    'id': 'featured_image_id',
                    'value': file.media_id
                }));
                imageContainer.prepend($('<a>', {
                    'href': '#',
                    'class': 'featured-preview set-post-thumbnail'
                }).append($('<img>', {
                    'src': file.media_thumbnail
                })));
            }
        },
        error: function(media_path) {
            alert(media_path + " <?php echo trans("cms::users.is_not_an_image") ?>");
        }
    });
});
</script>
        @endsection
