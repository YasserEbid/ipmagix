@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::ads.title')}}</h1>
    <ul class="breadcrumb">
        <li><a href="<?= url('/admin'); ?>">{{trans('cms::ads.home')}}</a></li>
    </ul>
</div>                        
<!-- ./page title -->

<div class="wrapper wrapper-white">
    <div class="row">
        <a href="./admin/ads/create">
            <button class="btn btn-primary">{{trans('cms::ads.new')}}</button>
        </a>
        <div class="table-responsive">
        <!--<table class="table table-bordered table-striped table-sortable">-->
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Actions</th>
                    </tr>
                </thead>                               
                <tbody id='table-data'>
                    @foreach($ads as $row)
                    <tr>
                        <td>{{$row->subject}}</td>

                        <td>

                            <a class="btn default btn-xs red" href="./admin/ads/update/{{$row->id}}">
                                <i class="fa fa-edit" type="button" ></i>
                            </a>
                            <a class="btn default btn-xs red" onclick="return confirm('Are you sure you want to delete this ad?')" href="./admin/ads/delete/{{$row->id}}">
                                <i class="fa fa-trash-o" type="button"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>



@stop

@section('js')
<script type='text/javascript' src='./assets/dashboard/js/plugins/tags-input/jquery.tagsinput.min.js'></script>
@stop


