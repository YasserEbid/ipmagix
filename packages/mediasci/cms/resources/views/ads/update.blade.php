@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::ads.edit_ads')}}</h1>
    <p>{{trans('cms::ads.editmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="<?= route('ads'); ?>">{{trans('cms::ads.title')}}</a></li>
    </ul>
</div>
<!-- ./page title -->

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>
<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        <form method="post">

          @include("cms::filter_update",['gender_id' => $ads->gender_id , 'lifestyle_id' => $ads->lifestyle_id , 'location_id' => $ads->location_id , 'nationality_id' => $ads->nationality_id])


            <div class="row">
                <label>Subject</label>
                <input type="text" value="{{$ads->subject}}"  name="subject" required="" class="form-control"/>
            </div>

            <div class="row">
                <label>Type of ad</label>
                <br/>
                <div class="col-md-6">
                    <input type="radio" name="type" value="1" checked="checked"/><span>Image & link</span>
                </div>
                <div class="col-md-6">
                    <input type="radio" name="type" value="2" /><span>HTML</span>
                </div>
            </div>
            <div class=" col-md-5 panel-body image" id="image-container" style="border: 1px solid #2196F3;padding: 5px;">
                            <div class="col-md-6 img-content" >
                            <?php if (is_object($ads->media)) : ?>
                                    <img class="featured-preview col-sm-12" src="./uploads/<?= $ads->media->media_path?>" width="300">
                            <?php endif; ?>
                                <input type="hidden" value="{{$ads->media_id}}" class="image-inputs" required="" name="media_id" id="featured_image_id" />
                            </div>
                            <div class="wrapper row">
                                <a href="#" class="btn btn-default set-post-thumbnail">{{Lang::get('cms::posts.set_image')}}</a>
                            </div>
                            <br/>
                            <div class="wrapper row">
                                <label>Link</label>
                                <input type="text" class="form-control image-inputs" value="{{$ads->link}}" name="link" required=""/>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-6 html disabled-div" style="border: 1px solid #2196F3; padding: 5px">
                            <label>HTML</label>
                            <textarea name="html" class="form-control html-inputs" >{{$ads->html}}</textarea>
                        </div>

                        <br style="clear:both"/>
                        <br/><br/>
            <button type="submit" class="btn btn-primary">Save</button>

        </form>
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop

@section('js')
<script>
    $('.set-post-thumbnail').click(function(e){
        e.preventDefault();
    });
    $(".set-post-thumbnail").filemanager({
        types: "png|jpg|jpeg|gif|bmp",
        done: function(files) {
            if (files.length) {
                var formContainer = $('.img-content');
                var imageContainer = $('#image-container');
                var file = files[0];
                formContainer.find('#featured_image_id').val(file.media_id);
                imageContainer.find('img.featured-preview').remove();
                imageContainer.prepend($('<img>', {
                	'class': 'featured-preview col-sm-12',
                    'src': file.media_thumbnail
                }));
                $(window).resize();
            }
        },
        error: function(media_path) {
            alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
        }
    });

    $(document).ready(function(){
        $('input[name=type]:radio').change(function(){
            var value=$(this).val();
            if(value==='1'){
                $('.html').addClass('disabled-div');
                $('.image').removeClass('disabled-div');
                $('.image-inputs').attr('required');
                $('.html-inputs').removeAttr('required');
            }else{
                $('.html').removeClass('disabled-div');
                $('.image').addClass('disabled-div');
                $('.image-inputs').removeAttr('required');
                $('.html-inputs').attr('required');
            }
        });
    });
</script>
<style>
.disabled-div {
    pointer-events: none;
    opacity: 0.4;
}
</style>

@stop
