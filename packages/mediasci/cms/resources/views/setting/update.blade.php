@extends('cms::layouts.master')
@section('content')
<div class="page-title">
    <h1>{{trans('cms::setting.edit')}}</h1>
    <p>{{trans('cms::setting.newmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="{{URL('admin/setting')}}">{{trans('cms::setting.setting')}}</a></li>
   
    </ul>
</div>

	<?php /*?><div class="col-md-12" style="">
	<div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::setting.confirm')}}
    </div>
 </div><?php */?>
        <div class="wrapper wrapper-white">
            <div class="wrapper">
            <div class="row">
                <form method="post" id="create_form" role="form" action="{{URL('admin/setting/update')}}">
                    <div class='col-md-12'>
                        <input type="hidden" name="set_id" value="{{$setting->id}}">
                   
                        <div class="col-md-12" >
                            <div class="col-md-12" > 
                                <div class="form-group">
                                    <div class="col-md-1" > 
                                        <label>{{trans('cms::setting.name')}}*:</label>
                                    </div>                                      
                                    
                                    <div class="col-md-6" > 
                                        <input type="text" class="form-control" name="name" required="" minlength="3" value="{{$setting->name}}"/>                                    
                                    </div>
                                </div>
                            </div>

                            <?php if ($setting->type == 'string') : ?>
                            <div class="col-md-12" > 
                                <div class="form-group">
                                    <div class="col-md-1" > 
                                        <label>{{trans('cms::setting.value')}}*:</label>
                                    </div>                                      
                                    
                                    <div class="col-md-6" > 
                                        <input type="text" class="form-control" name="value" required="" minlength="3" value="{{$setting->value}}"/>                                    
                                    </div>
                                </div>
                            </div>
                            <?php elseif($setting->type == 'text') : ?>
                            <div class="col-md-12" > 
                                <div class="form-group">
                                    <label>{{trans('cms::setting.value')}}*: <span></span></label>  
                                    <textarea class="form-control summernote" rows="5" name="value" placeholder="<?php echo trans('cms::setting.value'); ?>" ><?php echo old('value', ($setting ? $setting->value : '')); ?></textarea>     
                                </div>
                            </div>
                        <?php endif; ?>
                        </div>
                                                
                    </div>
                      
                    <div class="pull-left margin-top-10">
                        <!--<button class="btn btn-warning hide-prompts" type="button">Hide prompts</button>-->
                        <button class="btn btn-primary" type="submit" id='btn-submit'>{{trans('cms::setting.submit')}}</button>
                    </div>
                    
                </form></div>
            </div>
        </div>
       
        @endsection
