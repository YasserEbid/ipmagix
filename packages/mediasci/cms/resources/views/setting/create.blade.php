@extends('cms::layouts.master')
@section('content')
<div class="page-title">
    <h1>{{trans('cms::setting.new')}}</h1>
    <p>{{trans('cms::setting.newmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="{{URL('admin/setting')}}">{{trans('cms::setting.setting')}}</a></li>
     
    </ul>
</div>

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::setting.confirm')}}
    </div></div>
        <div class="wrapper wrapper-white">
            <div class="wrapper">
            <div class="row">
                <form method="post" id="create_form" role="form" action="{{URL('admin/setting/create')}}">
                    <div class='col-md-12'>
                   
                                       
                                                <div class="col-md-12" >
                                                    
                                                    <div class="col-md-6" > 
                                                 <div class="form-group">
                    <label>{{trans('cms::setting.name')}}: <span></span></label>                                      
                    <input type="text" class="form-control" name="name" required="" minlength="3" value=""/>                                    
                </div>
                                                    </div>
                                                      <div class="col-md-6" > 
                                                 <div class="form-group">
                    <label>{{trans('cms::setting.value')}}: <span></span></label>                                      
                    <input type="text" class="form-control" name="value" required="" minlength="3" value=""/>                                    
                </div>
                                                    </div>
                                                </div>
                                                
                                             
                                                    
                                       
                       
                       
                    </div>
                      
                    <div class="pull-left margin-top-10">
                <!--<button class="btn btn-warning hide-prompts" type="button">Hide prompts</button>-->
                <button class="btn btn-primary" type="submit" id='btn-submit'>{{trans('cms::setting.submit')}}</button>
            </div>
                    
                </form></div>
            </div>
        </div>
       
        @endsection
