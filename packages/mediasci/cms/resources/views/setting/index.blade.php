@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::setting.setting')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<div class="wrapper wrapper-white">
    @if(Session::has('n'))
    <?php $a = [];
    $a = session()->pull('n');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    <?php session()->forget('n');?>
    @endif @if(Session::has('m'))
    <?php $a = [];
    $a = session()->pull('m');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    <?php session()->forget('m');?>
    @endif
    
    <div class="page-subtitle">
        <h3></h3>
    </div>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>

                    <th>{{trans('cms::setting.name')}}</th>
                    <th>{{trans('cms::setting.value')}}</th>

                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($settings as $setting)
                    <td>{{$setting->name}}</td>
                    <td>{{$setting->value}}</td>

                    <td>
                        <a href="{{URL('admin/setting/update/'.$setting->id)}}"  class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>

                        </td>
                </tr>
                @endforeach
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
            {!! $settings->links()!!}
        </div>
    </div>

    @endsection
