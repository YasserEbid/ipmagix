@extends('cms::layouts.master')

@section('content')
<?php
$app_locales = $languages;
// dd($app_locales);
$app_locale = Config::get("app.locale");
?>
<!-- page title -->
<div class="page-title">
    <h1><?php echo $category ? trans('cms::categories.editcat') : trans('cms::categories.createcat'); ?></h1>
    <p><?php echo $category ? trans('cms::categories.editmessage') : trans('cms::categories.newmessage'); ?></p>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
        <li><a href="<?php echo route('categories.index'); ?>"><?php echo trans('cms::categories.index_title'); ?></a></li>
    </ul>
</div>

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
        {{trans('cms::careers.confirm')}}
    </div></div>
<!-- ./page title -->
<!-- form -->
<div class="wrapper wrapper-white">


    <div class="row">
        <?php if (isset($errors)) { ?>
            <?php if (count($errors) > 0) { ?>
                <div class="alert alert-danger">
                    <ul>
                        <?php foreach ($errors->all() as $error) { ?>
                            <li><?php echo $error; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        <?php } ?>

        @if(Session::has('fail'))
        <?php
        $a = [];
        $a = session()->pull('fail');
        ?>
        <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
        @endif
        @if(Session::has('success'))
        <?php
        $a = [];
        $a = session()->pull('success');
        ?>
        <div class="alert alert-success" role="alert">{{$a[0]}} </div>
        @endif
    </div>

    <?php //$form_route = ($category ? ['categories.edit', $category->cat_id] : ['categories.create', 'site_id' => $request->input('site_id')])   ?>
    <?php echo Form::open([/* 'route' => $form_route, */ 'method' => 'post', 'id' => 'category_form']); ?>
    <input type="hidden" value="<?php echo $request->input('site_id'); ?>" name="site_id" />
    <?php if ($category) : ?>
        <input type="hidden" value="<?php echo $category_banner ? $category_banner->media_id : ''; ?>" name="banner_id" id="banner_id" />
        <input type="hidden" value="<?php echo $category_icon ? $category_icon->media_id : ''; ?>" name="icon_id" id="icon_id" />
        <input type="hidden" value="<?php echo $category_icon_active ? $category_icon_active->media_id : ''; ?>" name="icon_id_active" id="icon_id_active" />
        <input type="hidden" value="<?php echo $post_video ? $post_video->media_id : ''; ?>" name="video_id" id="video_id" />
        <input type="hidden" value="<?php echo $video_cover ? $video_cover->media_id : ''; ?>" name="video_cover_id" id="video_cover_id" />
    <?php else: ?>
        <input type="hidden" value="" name="banner_id" id="banner_id" />
        <input type="hidden" value="" name="icon_id" id="icon_id" />
        <input type="hidden" value="" name="icon_id_active" id="icon_id_active" />
        <input type="hidden" value="" name="video_id" id="video_id" />
        <input type="hidden" value="" name="video_cover_id" id="video_cover_id" />
    <?php endif; ?>

    <div class="row">

        <div class="wrapper-white col-sm-12 pull-left">
            <div class="widget-tabbed margin-top-30">
                <ul class="widget-tabs widget-tabs-three">
                    <?php foreach ($app_locales as $key => $locale) : ?>
                        <?php $active_tab = ($locale['lang'] == $app_locale ? 'active' : ''); ?>
                        <li class="<?php echo $active_tab; ?>"><a href="#category__<?php echo $locale['lang']; ?>"><?php echo trans('cms::questionsAnswers.locale_' . $locale['lang']); ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php foreach ($app_locales as $key => $locale) : ?>
                    <div class="widget-tab <?php echo $active_tab; ?>" id="category__<?php echo $locale['lang']; ?>">
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="name_<?php echo $locale["lang"]; ?>"><?php
                                if ($locale["lang"] == "en") {
                                    echo trans('cms::categories.englishname');
                                } else {
                                    echo trans('cms::categories.arabicname');
                                }
                                ?></label>
                            <div class="col-md-10">
                                <input id="name_<?php echo $locale["lang"]; ?>" type='text'  name="name_<?php echo $locale["lang"]; ?>" class="form-control"
                                       value='<?php echo old('name_' . $locale["lang"], (isset($category_langs) ? $category_langs[$locale["lang"]]->name : '')); ?>' placeholder="<?php echo trans('cms::categories.name'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="subtitle_<?php echo $locale["lang"]; ?>"><?php
                                if ($locale["lang"] == "en") {
                                    echo trans('cms::categories.english_subtitle');
                                } else {
                                    echo trans('cms::categories.arabic_subtitle');
                                }
                                ?></label>
                            <div class="col-md-10">
                                <textarea id="subtitle_<?php echo $locale["lang"]; ?>" name="sub_title_<?php echo $locale["lang"]; ?>" class="form-control" rows="5"><?php echo old('sub_title_' . $locale["lang"], (isset($category_langs) ? $category_langs[$locale["lang"]]->sub_title : '')); ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="description_<?php echo $locale->lang; ?>"><?php
                                if ($locale["lang"] == "en") {
                                    echo trans('cms::categories.english_description');
                                } else {
                                    echo trans('cms::categories.arabic_description');
                                }
                                ?></label>
                            <div class="col-md-10">
                                <textarea id="description_<?php echo $locale->lang; ?>" class="form-control summernote" rows="5" name="description_<?php echo $locale->lang; ?>" placeholder="<?php echo trans('cms::categories.description'); ?>" >
                                    <?php echo old('description_' . $locale["lang"], (isset($category_langs) ? $category_langs[$locale["lang"]]->description : '')); ?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-6" style="padding: 0 10px 0 0;">
            <div class="form-group">
                <label for="cat_slug" ><?php echo trans('cms::categories.cat_slug'); ?></label>
                <input id="cat_slug" type='text'  name="cat_slug" class="form-control"  value='<?php echo old('cat_slug', (isset($category) ? $category->cat_slug : '')); ?>' placeholder="<?php echo trans('cms::categories.cat_slug'); ?>">
            </div>
        </div>

        <div class="col-md-6" style="padding: 0 10px 0 0;">
            <div class="form-group">
                <label><?php echo trans('cms::categories.parent'); ?></label>
                <select class="form-control" name="parent_id">
                    <option value="0">no parent</option>
                    <?php if ($parent_categories) { ?>
                        <?php foreach ($parent_categories as $cat) { ?>
                            <?php
                            $selected = '';
                            if (old('parent_id') == $cat->cat_id || ($category && $category->parent_id == $cat->cat_id)) {
                                $selected = 'selected';
                            }
                            ?>
                            <option <?php echo $selected; ?> value="<?php echo $cat->cat_id; ?>"><?php echo $cat->name; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-6" >
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::categories.banner_id')}}</span>
                </div>
                <div class="panel-body" id="image-container">
                    <?php if ($category_banner) : ?>
                        <img class="banner-preview col-sm-12" src="<?php echo thumbnail($category_banner->media_path) ?>">
                    <?php endif; ?>
                    <div class="wrapper row">
                        <a href="javascript:void(0)" class="btn btn-default set-category-thumbnail">{{Lang::get('cms::categories.set_image')}}</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-6 pull-right">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::posts.featured_video')}}</span>
                </div>
                <div class="panel-body" id="video-container">
                    <?php if ($post_video) : ?>
                        <img class="featured-preview col-sm-12" src="<?php echo $post_video->media_provider ? $post_video->media_provider_image : thumbnail($post_video->media_path); ?>">
                    <?php endif; ?>
                    <div class="wrapper row">
                        <a href="#" class="btn btn-default set-post-video">{{Lang::get('cms::posts.set_video')}}</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-6" >
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::categories.icon_id')}}</span>
                </div>
                <div class="panel-body" id="icon-container">
                    <?php if ($category_icon) : ?>
                        <img class="icon-preview col-sm-12" src="<?php echo thumbnail($category_icon->media_path) ?>">
                    <?php endif; ?>
                    <div class="wrapper row">
                        <a href="javascript:void(0)" class="btn btn-default set-category-icon">{{Lang::get('cms::categories.set_image')}}</a>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-sm-6 pull-right">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::categories.icon_id_active')}}</span>
                </div>
                <div class="panel-body" id="icon-container-active">
                    <?php if ($category_icon_active) : ?>
                        <img class="icon-active-preview col-sm-12" src="<?php echo thumbnail($category_icon_active->media_path) ?>">
                    <?php endif; ?>
                    <div class="wrapper row">
                        <a href="javascript:void(0)" class="btn btn-default set-category-icon-active">{{Lang::get('cms::categories.set_image')}}</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::categories.video_cover')}}</span>
                </div>
                <div class="panel-body" id="video-cover-container">
                    <?php if ($video_cover) : ?>
                        <img class="video-cover-preview col-sm-12" src="<?php echo thumbnail($video_cover->media_path) ?>">
                    <?php endif; ?>
                    <div class="wrapper row">
                        <a href="javascript:void(0)" class="btn btn-default set-video-cover">{{Lang::get('cms::categories.set_image')}}</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6" style="padding: 0 10px 0 0;">
            <button class="btn btn-primary" type="submit"><?php echo $category ? trans('cms::categories.update') : trans('cms::categories.add'); ?></button>
        </div>
    </div>
    <?php echo Form::close(); ?>
</div>
<!-- ./form -->
<script>
    $(function () {
        $(".set-category-thumbnail").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#category_form');
                    var imageContainer = $('#image-container');
                    var file = files[0];
                    formContainer.find('#banner_id').val(file.media_id);
                    imageContainer.find('img.banner-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'banner-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });


        $(".set-category-icon").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#category_form');
                    var imageContainer = $('#icon-container');
                    var file = files[0];
                    formContainer.find('#icon_id').val(file.media_id);
                    imageContainer.find('img.icon-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'icon-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });


        $(".set-category-icon-active").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#category_form');
                    var imageContainer = $('#icon-container-active');
                    var file = files[0];
                    formContainer.find('#icon_id_active').val(file.media_id);
                    imageContainer.find('img.icon-active-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'icon-active-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });


        $(".set-video-cover").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#category_form');
                    var imageContainer = $('#video-cover-container');
                    var file = files[0];
                    formContainer.find('#video_cover_id').val(file.media_id);
                    imageContainer.find('img.video-cover-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'video-cover-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });




        $('.set-post-video').click(function (e) {
            e.preventDefault();
        });
        $(".set-post-video").filemanager({
            types: "video",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#category_form');
                    var videoContainer = $('#video-container');
                    var file = files[0];
                    formContainer.find('#video_id').val(file.media_id);
                    videoContainer.find('img.featured-preview').remove();
                    videoContainer.prepend($('<img>', {
                        'class': 'featured-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_video") ?>");
            }
        });
    });
</script>
@stop
