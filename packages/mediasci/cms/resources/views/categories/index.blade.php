@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title col-md-12">
	<h1><?php echo trans('cms::categories.index_title'); ?></h1>

	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

	</ul>
</div>
<!-- ./page title -->

<script>
	$(document).ready(function(){
		$(".fgfg").filemanager({
            types: "image",
            done: function (files) {
                if(files.length){
//                    var file = files[0];
//					$("#image_wrap").show();
//					$("#set-post-thumbnail").hide();
//					$("#remove-post-thumbnail").show();
//                    $("#featured_image").val(file.media_id);
//                    $("#image_post").attr("src", file.media_thumbnail);
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::users.is_not_an_image") ?>");
            }
        });
	});
	</script>
<!-- data table -->
<div class="wrapper wrapper-white">
	@if(Session::has('fail'))
		<?php $a = [];
		$a = session()->pull('fail'); ?>
		<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
		<?php session()->forget('fail');?>
	@endif
	@if(Session::has('success'))
	<?php $a = [];
		$a = session()->pull('success'); ?>
		<div class="alert alert-success" role="alert">{{$a[0]}} </div>
		<?php session()->forget('success');?>
	@endif
	<div class="form-group">
		<a href="<?php echo route('categories.create', ['site_id' => $request->input('site_id')]); ?>"><button type="button" class="btn btn-primary">{{trans('cms::categories.add_new')}}</button></a>
	</div>

	<div class="dataTables_length">
		<div>
			<label><?php echo trans('cms::categories.show_per_page'); ?> </label>
			<select class='per_page form-control'>
				<option value="10" <?php echo ($request->input('per_page') == 10) ? 'selected' : ''; ?> >10</option>
				<option value="25" <?php echo ($request->input('per_page') == 25) ? 'selected' : ''; ?> >25</option>
				<option value="50" <?php echo ($request->input('per_page') == 50) ? 'selected' : ''; ?> >50</option>
				<option value="100" <?php echo ($request->input('per_page') == 100) ? 'selected' : ''; ?> >100</option>
			</select>

<!--			<label><?php // echo trans('cms::categories.from_site'); ?> </label>
			<select class='from_site form-control' style="width: auto !important;">
				<?php //if ($sites) { ?>
					<?php // foreach ($sites as $site) { ?>
						<option <?php // echo $request->input('site_id') == $site->site_id ? 'selected' : ''; ?> value="<?php // echo $site->site_id; ?>"><?php // echo $site->site_name; ?></option>
					<?php // } ?>
				<?php // } ?>
			</select>-->
		</div>
	</div>

	<div class="dataTables_filter">
		<?php echo Form::open(['route' => ['categories.index', 'site_id' => $request->input('site_id')], 'method' => 'get', 'id' => 'category_search_form']); ?>
		<label>Search:<input type="search" value="<?php echo $request->input('q'); ?>" name="q" class="form-control search_q"></label>
		<?php echo Form::close(); ?>
	</div>

	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><?=\Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name' , 'name')?></th> <!-- <?php echo trans('cms::categories.name'); ?>  -->
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($categories) { ?>
					<?php foreach ($categories as $cat) { ?>
						<tr>
							<td><?php echo $cat->name; ?></td>
							<td>
								<a href="<?php echo route('categories.edit', ['id' => $cat->cat_id,'site_id' => $request->input('site_id')]); ?>" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
								<a href="<?php echo route('categories.delete', ['id' => $cat->cat_id]); ?>" onclick="return confirm('<?php echo trans('cms::categories.confirm_delete'); ?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td style="text-align: center;" colspan="3"><?php echo trans('cms::categories.no_categories'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>

		<?php
		if ($categories) {
			echo $categories->appends($request->except('page'))->links();
		}
		?>
	</div>
</div>
<!-- ./data table -->

<?php
$params_per_page = count($request->except(['per_page'])) ? http_build_query($request->except(['per_page'])) : '';

$params_from_site = count($request->except(['site_id'])) ? http_build_query($request->except(['site_id'])) : '';

$params_search = count($request->except(['q'])) ? http_build_query($request->except(['q'])) : '';
?>

<script>
	$(document).ready(function () {
		$('.per_page').change(function () {
			location.href = '<?php echo route('categories.index'); ?>?<?php echo $params_per_page ? $params_per_page . '&' : ''; ?>per_page=' + $(this).val();
		});

		$('.from_site').change(function () {
			location.href = '<?php echo route('categories.index'); ?>?<?php echo $params_from_site ? $params_from_site . '&' : ''; ?>site_id=' + $(this).val();
		});

		$('#category_search_form').submit(function (e) {
			var q = $(this).find('.search_q');

			if (q.length > 0) {
				if (q.val().trim() == '') {
					location.href = '<?php echo route('categories.index'); ?><?php echo $params_search ? '?' . $params_search : ''; ?>';
					return false;
				}
			} else {
				return false;
			}
		});

	});
</script>
@stop
