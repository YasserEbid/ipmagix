@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>Edit Theme</h1>
	<ul class="breadcrumb">
		<li><a href="<?php echo route('mail.themes'); ?>">Themes</a></li>
	</ul>
</div>
<!-- ./page title -->
<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>

<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        <?//=  Mediasci\Cms\Http\Controllers\component\Froala\FroalaEditor::init_css()?>
        <form method="post">
            <div class="form-group">
                <label>Name :</label>
                <input name="name" type="text" class="form-control" value="{{$data->name}}" />
            </div>
            <br/>
            <div class="form-group">
                <label>Header</label>

                  <textarea class="form-control summernote" name="header"  cols="50" rows="6" id="header" >{{$data->header}}</textarea>
                <br/>
                <label>Footers</label>

                    <textarea class="form-control summernote" name="footer"  cols="50" rows="6" id="footer"  >{{$data->footer}}</textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>


    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop

@section('js')
<?//= Mediasci\Cms\Http\Controllers\component\Froala\FroalaEditor::init_js()?>
@stop
