@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>Show Theme</h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('mail.themes'); ?>">Themes</a></li>
	</ul>
</div>                        
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        <center><h1>{{$data->name}}</h1></center>
        <br/>
        
        <div class="row" style="width: 100%;border: 1px solid #77acfb;border-radius: 5px;">
            {!! $data->header !!}
        </div>
        <br style="clear: both"/>
        <br style="clear: both"/>
        <br style="clear: both"/>
        <div class="row" style="width: 100%;border: 1px solid #77acfb;border-radius: 5px;">
            {!! $data->footer !!}
        </div>
        
        
        
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop