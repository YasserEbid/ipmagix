@extends('cms::layouts.master')


@section('content')
<!-- page title -->
<div class="page-title">
    <h1>Add Message</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
        <li><a href="<?php echo route('mail.messages'); ?>">Messages</a></li>
    </ul>
</div>
<!-- ./page title -->

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>
<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        <form method="post">

            @include("cms::filters")

            <div class="form-group">
                <label>Subject</label>
                <input type='text' name='subject' id="subjectinput" class="form-control" required=""/>
            </div>
            <br/>
            <div class="form-group">
                 <label>Theme</label>
                <?= Mediasci\Cms\Http\Controllers\Mail::themesSelect() ?>
            </div>
            <br/>
            <div class="form-group">
              <button type="button" name="button" class="btn" id="addUserName"> Add User Name </button>
              <button type="button" name="button" class="btn" id="addImage"> Add User Image </button>
            </div>
            <div class="form-group">
                <? //=  Mediasci\Cms\Http\Controllers\component\Froala\FroalaEditor::init_editor('conntent')?>

                <textarea class="form-control summernote" name="content"  cols="50" rows="6"  id="messageContent"></textarea>

            </div>
            <br/>

            <button type="submit" class="btn btn-primary">Save</button>

        </form>
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop

@section('js')
<?//= \Mediasci\Cms\Http\Controllers\component\Froala\FroalaEditor::init_js() ?>
@stop
