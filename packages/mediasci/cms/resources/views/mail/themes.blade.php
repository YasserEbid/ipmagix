@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>Themes</h1>
  <ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		
	</ul>
	
</div>                        
<!-- ./page title -->
<div class="wrapper wrapper-white">
    <div class="row">
            <a href='<?= route('mail.themes.create') ?>' class="btn btn-primary">
			Add Theme
        </a>
        <div class="table-responsive">
        <!--<table class="table table-bordered table-striped table-sortable">-->
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th>Theme Name</th>
						<th>Actions</th>
					</tr>
				</thead>                               
				<tbody id='table-data'>
					@foreach($themes as $row)
					<tr>
						<td>{{$row->name}}</td>

						<td>
							<a class="btn default btn-xs red" href="./admin/mail/themes/show/{{$row->id}}">
								<i class="fa fa-eye" type="button" ></i>
							</a>
							<a class="btn default btn-xs red" href="./admin/mail/themes/update/{{$row->id}}">
								<i class="fa fa-edit" type="button" ></i>
							</a>
							<a class="btn default btn-xs red" onclick="return confirm('Are you sure you want to delete this Theme?')" href="./admin/mail/themes/delete/{{$row->id}}">
								<i class="fa fa-trash" type="button"></i>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>



    </div>
</div>



@stop



@section('js')
<!--<script>-->
<!--	$(document).ready(function() {
		var name, header, footer;

		$('#add').click(function(e) {
			e.preventDefault();
			var level = $(this).attr('data-level');
			if (level === 'name') {
				// name=$('#name').val();
				$('#theme_name').hide();
				$('#theme_header').show();
				$(this).attr('data-level', 'header');

			} else if (level === 'header') {
				//header=$('#header').val();
				$('#theme_header').hide();
				$('#theme_footer').show();
				$(this).attr('data-level', 'footer');
				$(this).attr('type', 'submit');
				$(this).html('complete');
			} else if (level === 'footer') {
				$('#add-theme').submit();
			}
		});
	});-->
<!--</script>-->

<?//= Mediasci\Cms\Http\Controllers\component\Froala\FroalaEditor::init_js() ?>
@stop

