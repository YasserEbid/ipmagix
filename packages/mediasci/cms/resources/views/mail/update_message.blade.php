@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>Edit Message</h1>

	<ul class="breadcrumb">
		<li><a href="<?php echo route('mail.messages'); ?>">messages</a></li>
	</ul>
</div>
<!-- ./page title -->

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>
<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        <form method="post">

					@include("cms::filter_update",['gender_id' => $message->gender_id , 'lifestyle_id' => $message->lifestyle_id , 'location_id' => $message->location_id , 'nationality_id' => $message->nationality_id])


            <div class="form-group">
                <label>Subject</label>
                <input type='text' name='subject' value='{{$message->subject}}' class="form-control" required=""/>
            </div>
            <br/>
            <div class="form-group">
                <textarea class="form-control summernote" name="content"  cols="50" rows="6" value=""  >{{$message->content}}</textarea>
            </div>
            <br/>

            <button type="submit" class="btn btn-primary">Save</button>

        </form>
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop

@section('js')
<?//=  \Mediasci\Cms\Http\Controllers\component\Froala\FroalaEditor::init_js()?>
@stop
