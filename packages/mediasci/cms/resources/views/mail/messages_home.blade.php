@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::messages.index_title'); ?></h1>

	  <ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		
	</ul>
</div>                        
<!-- ./page title -->
<div class="wrapper wrapper-white">
    <div class="row">
        <a href='<?= route('mail.messages.create') ?>' class="btn btn-primary">
			Add Message
        </a>

        <div class="table-responsive">
        <!--<table class="table table-bordered table-striped table-sortable">-->
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th>Subject</th>
						<th>Actions</th>
					</tr>
				</thead>                               
				<tbody id='table-data'>
					@foreach($messages as $row)
					<tr>
						<td>{{$row->subject}}</td>

						<td>

							<a href="./admin/mail/message/update/{{$row->id}}" class="btn default btn-xs red">
								 <i class="fa fa-edit"></i> 
							</a>
							<a href="./admin/mail/message/delete/{{$row->id}}" onclick="return confirm('Are you sure you want to delete this Massege?')" class="btn default btn-xs red">
								 <i class="fa fa-trash"></i> 
							</a>
							<a href="./admin/mail/statistics/{{$row->id}}" class="btn default btn-xs red">
								 <i class="fa fa-bar-chart"></i> 
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>



    </div>
</div>



@stop


@section('js')
<script>
	$(document).ready(function() {
		var name, header, footer;

		$('#add').click(function(e) {
			e.preventDefault();
			var level = $(this).attr('data-level');
			if (level === 'name') {
				// name=$('#name').val();
				$('#theme_name').hide();
				$('#theme_header').show();
				$(this).attr('data-level', 'header');

			} else if (level === 'header') {
				//header=$('#header').val();
				$('#theme_header').hide();
				$('#theme_footer').show();
				$(this).attr('data-level', 'footer');
				$(this).attr('type', 'submit');
				$(this).html('complete');
			} else if (level === 'footer') {
				$('#add-theme').submit();
			}
		});
	});
</script>

<?= Mediasci\Cms\Http\Controllers\component\Froala\FroalaEditor::init_js() ?>
@stop

