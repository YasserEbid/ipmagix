@extends('cms::layouts.master')

@section('content')
<!-- page title -->
 <div class="page-title">
    <h1>{{trans('cms::cvs.cvs')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<div class="wrapper wrapper-white">

    @if(Session::has('n'))
    <?php
    $a = [];
    $a = session()->pull('n');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    <?php session()->forget('n');?>
    @endif @if(Session::has('m'))
    <?php
    $a = [];
    $a = session()->pull('m');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    <?php session()->forget('m');?>
    @endif
<?php if($cvs->count() > 0){?>
    <div class="page-subtitle">
        <h3></h3>
        <a class="btn btn-primary" href="{{URL('admin/exportcv')}}">Export</a>
    </div>
    <?php }?>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('ID', 'id') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name', 'name') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Email', 'email') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Phone', 'phone') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Title', 'title') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Cover Letter', 'cover_letter') ?></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($cvs as $cv)
                    <td>{{$cv->id}}</td>
                    <td>{{$cv->name}}</td>
                    <td>{{$cv->email}}</td>
                    <td>{{$cv->phone}}</td>
                    <td>{{$cv->job_title}}</td>
                    <td>{{$cv->cover_letter}}</td>

                    <td>
                        <a href="{{URL('admin/jobcv/'.$cv->id)}}" class="btn green">
                            <i class="fa fa-eye"></i> {{trans('cms::cvs.show')}}
                        </a>

                        <a id="del" href="{{URL('admin/jobcv/delete/'.$cv->id)}}" onclick="return confirm('are you sure you want to delete this CV?')" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a></td>
                </tr>
                @endforeach
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
              <?php  $search_query = \Illuminate\Support\Facades\Input::except('page');
                    $cvs->appends($search_query); ?>
            {!! $cvs->links()!!}
        </div>
    </div>

    @endsection
