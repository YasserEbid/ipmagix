@extends('cms::layouts.master')

@section('content')
<div class="page-title">
    <h1>Show Job CV</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
     <li> <a href="{{URL('/admin/jobcv')}}">{{trans('cms::cvs.cvs')}}</a></li>
    </ul>
</div>
<div class="wrapper wrapper-white">
     <div class="page-subtitle">
    </div>
    <div class="form-group">
         <a href="{{URL('admin/jobcv')}}"><button type="button" class="btn btn-primary">{{trans('cms::cvs.back')}}</button></a>
     </div>
    <div class="table-responsive">
      <table class="table table-hover">
          <thead>
              <tr>
                  <th>{{trans('cms::cvs.field')}}</th>
                  <th>{{trans('cms::cvs.desc')}}</th>
              </tr>
          </thead>
          <tbody>
            <tr>
                <td>{{trans('cms::cvs.name')}}</td>
                <td>{{$cv->name}}</td>
            </tr>
            <tr>
              <td>{{trans('cms::cvs.email')}}</td>
              <td>{{$cv->email}}</td>
          </tr>
            @if($cv->apply_type == 0)
                <tr>
                  <td>{{trans('cms::cvs.phone')}}</td>
                  <td>{{$cv->phone}}</td>
               </tr>
               <tr>
                 <td>{{trans('cms::cvs.cover_letter')}}</td>
                 <td>{{$cv->cover_letter}}</td>
               </tr>
              @endif
              <tr>
                  <td>{{trans('cms::cvs.title')}}</td>
                  <td>{{$cv->job_title}}</td>
              </tr>

                <tr>
                  @if($cv->apply_type == 0)
                    <td>{{trans('cms::cvs.cv')}}</td>
                    <td>  <a href="{{URL('uploads/cvs/'.$cv->cv)}}" >{{trans('cms::cvs.download')}}</a></td>
                  @else
                    <td>Linked in Profile</td>
                    <td><a href="{{$cv->cv}}" target="_blank">{{$cv->name}}</a></td>
                  @endif
              </tr>
              @if($cv->career_id)
                <tr>
                  <td>{{trans('cms::cvs.job')}}</td>
                  <td>{{$cv->career->careerlang('en')->title}}</td>
               </tr>
              @endif
      </table>
  </div>
</div>
@endsection
