@extends('cms::layouts.master')

@section('content')
<div class="wrapper wrapper-white">

    <div class="page-subtitle">
        <h3>{{trans('cms::volunteer.volunteer')}}</h3>
    </div> <div class="form-group">
        <a href="{{URL('admin/partnership-requests')}}"><button type="button" class="btn btn-primary">{{trans('cms::volunteer.back')}}</button></a>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>{{trans('cms::volunteer.field')}}</th>
                    <th>{{trans('cms::volunteer.value')}}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        {{trans('cms::volunteer.name')}}
                    </td>
                    <td>
                        {{$volunteer->fullname}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{trans('cms::volunteer.company_name')}}
                    </td>

                    <td>   {{$volunteer->company_name}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{trans('cms::volunteer.mail')}}
                    </td>

                    <td>   {{$volunteer->email}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{trans('cms::volunteer.phone')}}
                    </td>
                    <td>
                      {{$volunteer->phone}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{trans('cms::volunteer.country')}}
                    </td>
                    <td>
                        {{$volunteer->countryName}}

                    </td>
                </tr>
                <tr>
                    <td>
                        {{trans('cms::volunteer.message')}}
                    </td>

                    <td>    {{$volunteer->message}}
                    </td>
                </tr>

        </table>
    </div>
</div>
@endsection
