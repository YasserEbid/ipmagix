@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::volunteer.volunteer')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<div class="wrapper wrapper-white">

    @if(Session::has('n'))
    <?php
    $a = [];
    $a = session()->pull('n');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    <?php session()->forget('n');?>
    @endif
    <div class="page-subtitle">
        <h3></h3>
        <a class="btn btn-primary" href="{{URL('admin/exportvolunteer')}}">Export</a>
    </div>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>

                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name', 'name') ?></th>
                    <th>{{trans('cms::volunteer.company_name')}}</th>
                    <th>{{trans('cms::volunteer.mail')}}</th>
                    <th>{{trans('cms::volunteer.phone')}}</th>
                    <th>{{trans('cms::volunteer.country')}}</th>
                    <th>{{trans('cms::volunteer.message')}}</th>
                    <th>{{trans('cms::volunteer.show')}}</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($volunteers as $volunteer)
                    <td>{{$volunteer->fullname}}</td>
                    <td>
                        {{$volunteer->company_name}}
                    </td>
                    <td>
                        {{$volunteer->email}}
                    </td>
                    <td>
                        {{$volunteer->phone}}
                    </td>
                    <td>
                        {{$volunteer->countryName}}
                    </td>
                    <td>
                        {{$volunteer->message}}
                    </td>
                    <td><a id="del" href="{{URL('admin/partnership-requests/'.$volunteer->id)}}" class="btn red">
                      <i class="fa fa-eye"></i> {{trans('cms::volunteer.show')}}
                    </a>
                    </td>
                    <td>
                        <a id="del" href="{{URL('admin/partnership-requests/delete/'.$volunteer->id)}}" onclick="return confirm('Are you sure you want to delete this Partner?')"  class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a></td>
                </tr>
                @endforeach
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
            <?php $search_query = \Illuminate\Support\Facades\Input::except('page');
            $volunteers->appends($search_query);
            ?>
            {!! $volunteers->links()!!}
        </div>
    </div>

    @endsection
