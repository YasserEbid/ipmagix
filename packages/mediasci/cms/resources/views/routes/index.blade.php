@extends('cms::layouts.master')

@section('content')
<?php

use Mediasci\Cms\Models\Routes; ?>
<!-- page title -->
<div class="page-title">
    <h1><?php echo trans('cms::routes.index_title'); ?></h1>


    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
        <li><?php echo trans('cms::routes.index_title'); ?></li>
    </ul>
</div>
<!-- ./page title -->

<!-- data table -->
<div class="wrapper wrapper-white" style="padding: 10px;">

	<div class="row">
		<?php if (session('message')) { ?>
			<div class="alert alert-success">
				<?php echo session('message'); ?>
			</div>
		<?php } ?>
    <?php session()->forget('message');?>
	</div>

    <div class="row">
		<?php echo Form::open(['route' => 'routes.index', 'method' => 'post', 'id' => "validate", 'role' => "form"]); ?>

			<?php
			foreach ($routeCollection as $route) {
				if ($route->getName() != '') {
					?>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label><?php echo trans('cms::routes.route_name'); ?>: </label>
							<input readonly type="text" value="<?php echo $route->getName(); ?>" class="form-control" name="route[]"/>
						</div>
					</div>
					<div class="col-md-3">
						<?php
							$conditions = [
								'routes.route_name' => $route->getName()
							];
							$current_route = Routes::find_by($conditions);
						?>
						<div class="form-group">
							<label><?php echo trans('cms::routes.conv_name'); ?>: <span><?php echo trans('cms::routes.Required'); ?>  </span></label>
							<input type="text" value="<?php echo isset($current_route->conventional_name) ? $current_route->conventional_name : ''; ?>" class="form-control" name="route_conventional_name[]" data-validation="required" />
						</div>
					</div>
					<div class="col-md-2">
						<?php
								$conditions = [
									'routes.route_name' => $route->getName()
								];
								$current_route = Routes::find_by($conditions);
						?>
						<div class="form-group">
							<label><?= trans('cms::routes.group_name'); ?></label>
							<select class="form-control selectpicker" data-validation="required" name="<?php echo str_replace(".", "_", $route->getName()) ?>_select">
								<option value=""><?php echo trans('cms::routes.plz_select'); ?></option>
								<?php foreach ($groups as $group) { ?>
									<option value="<?php echo $group->group_id; ?>" <?php echo(isset($current_route->group_id) && $current_route->group_id == $group->group_id) ? 'selected' : '' ?> ><?php echo $group->group_name ?></option>
								<?php } ?>
							</select>
						</div>

					</div>

					<div class="col-md-3">
						<?php
						$conditions = [
							'routes.route_name' => $route->getName()
						];
						$current_route = Routes::find_by($conditions);
						?>
						<div class="form-group row">
							<label><?php echo trans('cms::routes.route_type'); ?>:  </label>
							<br/>
							<div class="checkbox checkbox-inline">
								<input type="checkbox" id="check_<?php echo $route->getName(); ?>" value="1" name="<?php echo str_replace(".", "_", $route->getName()); ?>" <?php echo (isset($current_route->route_type) && $current_route->route_type == 1) ? 'checked' : ''; ?>/>
								<label for="check_<?php echo $route->getName(); ?>"><?= trans('cms::routes.general_route'); ?></label>
							</div>
						</div>

					</div>
				</div>
					<?php
				}
			}
			?>


		<div class="pull-left margin-top-10">

			<button class="btn btn-danger" type="submit"><?php echo trans('cms::routes.save'); ?></button>
		</div>
	<?php echo Form::close(); ?>
    </div>
</div>
<!-- ./data table -->
<script> $.validate();</script>
@stop
