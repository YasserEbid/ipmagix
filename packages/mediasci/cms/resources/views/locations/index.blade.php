@extends('cms::layouts.master')

@section('content')

<!-- page title -->
<div class="page-title col-md-12">
    <h1>{{trans('cms::locations.locations')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<!-- ./page title -->
<div class="wrapper wrapper-white">

    @if(Session::has('fail'))
    <?php $a = [];
    $a = session()->pull('fail');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
<?php session()->forget('fail'); ?>
    @endif
    @if(Session::has('success'))
    <?php $a = [];
    $a = session()->pull('success');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
<?php session()->forget('success'); ?>
    @endif

    <div class="form-group">
        <a href="{{URL('admin/locations/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::locations.new')}}</button></a>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>{{trans('cms::locations.city')}}</th>
                    <th>{{trans('cms::locations.address')}}</th>
                    <th>{{trans('cms::locations.phone')}}</th>
                    <th>{{trans('cms::locations.email')}}</th>
                    <th><?= trans('cms::locations.longitude'); ?></th>
                    <th><?= trans('cms::locations.latitude'); ?></th>
                    <th>{{trans('cms::locations.actions')}}</th>
                </tr>
            </thead>
            <tbody>
                @if(empty($locations))
                <tr>
                    <td>No Locations Found.</td>
                </tr>
                @else
                @foreach($locations as $location)
<?php //dd($location->locationslang(Config::get("app.locale"))->first()->city); ?>
                <tr>
                  <td>{{$location->city}}</td>
                    <td>{{$location->address}}</td>
                    <td>{{$location->phone}}</td>
                    <td>{{$location->email}}</td>
                    <td>{{$location->longitude}}</td>
                    <td>{{$location->latitude}}</td>
                    <td>
                        <a href="{{URL('admin/locations/update/'.$location->id)}}" class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a id="del" href="{{URL('admin/locations/delete/'.$location->id)}}" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <div class="btn-group" role="group">
            @unless(empty($locations))
            {!! $locations->links()!!}
            @endunless
        </div>
    </div>
</div>
@endsection
