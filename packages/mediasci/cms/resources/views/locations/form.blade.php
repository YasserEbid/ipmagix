@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1><?php echo trans('cms::locations.index_title'); ?></h1>


    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
        <li><a href="<?php echo url('admin/locations'); ?>"><?php echo trans('cms::locations.index_title'); ?></a></li>
        <li><?php echo $current_location ? trans('cms::locations.edit') : trans('cms::locations.create'); ?></li>
    </ul>
</div>
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <div class="row">
        <div class="page-subtitle">
            <h3><?php echo $current_location ? trans('cms::locations.edit') : trans('cms::locations.create'); ?></h3>
        </div>
    </div>
    <?php if (isset($errors)) { ?>
        <?php if (count($errors) > 0) { ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $error) { ?>
                        <li><?php echo $error; ?></li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    <?php } ?>

    <?php /* if (session('message')) { ?>
      <div class="alert alert-success">
      <?php echo session('message'); ?>
      </div>
      <?php } */ ?>
    @if(Session::has('fail'))
    <?php
    $a = [];
    $a = session()->pull('fail');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    @endif
    @if(Session::has('success'))
    <?php
    $a = [];
    $a = session()->pull('success');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    @endif
    <?php //$form_route = ($current_location ? ['services.update', $current_location->id] : ['services.create']); ?>
    <?php echo Form::open([/* 'route' => $form_route, */ 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'post_form']); ?>
    <?php if ($current_location) : ?>
        <!-- <input type="hidden" value="<?php //echo $current_location->id; ?>" name="location_lang_id" />
        <input type="hidden" value="<?php //echo $current_location->lang; ?>" name="lang" /> -->
    <?php endif; ?>

    <?php
    $app_locales = $languages;
    $app_locale = Config::get("app.locale");
    ?>

    <div class="wrapper-white col-sm-8 pull-left">
        <div class="widget-tabbed margin-top-30">
            <ul class="widget-tabs widget-tabs-three">
                <?php foreach ($app_locales as $key => $locale) : ?>
                    <?php $active_tab = ($locale->lang == $app_locale ? 'active' : ''); ?>
                    <li class="<?php echo $active_tab; ?>"><a href="#location_<?php echo $locale->lang; ?>"><?php echo trans('cms::locations.locale_' . $locale->lang); ?></a></li>
                <?php endforeach; ?>
            </ul>
            <?php foreach ($app_locales as $key => $locale) :
              ?>
                <div class="widget-tab <?php echo $active_tab; ?>" id="location_<?php echo $locale->lang; ?>">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="city_<?php echo $locale->lang; ?>"><?php echo trans('cms::locations.city'); ?></label>
                        <div class="col-md-10">
                            <input id="city_<?php echo $locale->lang; ?>" type='text'  name="city_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('city_' . $locale->lang, (isset($locations_langs) ? $locations_langs[$locale->lang]->city : '')); ?>' placeholder="<?php echo trans('cms::locations.city'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="address_<?php echo $locale->lang; ?>"><?php echo trans('cms::locations.address'); ?></label>
                        <div class="col-md-10">
                            <input id="address_<?php echo $locale->lang; ?>" type='text'  name="address_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('address_' . $locale->lang, (isset($locations_langs) ? $locations_langs[$locale->lang]->address : '')); ?>' placeholder="<?php echo trans('cms::locations.address'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="phone_<?php echo $locale->lang; ?>"><?php echo trans('cms::locations.phone'); ?></label>
                        <div class="col-md-10">
                            <input id="phone_<?php echo $locale->lang; ?>" type='text' name="phone_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('phone_' . $locale->lang, (isset($locations_langs) ? $locations_langs[$locale->lang]->phone : '')); ?>' placeholder="<?php echo trans('cms::locations.phone'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="email_<?php echo $locale->lang; ?>"><?php echo trans('cms::locations.email'); ?></label>
                        <div class="col-md-10">
                            <input id="email_<?php echo $locale->lang; ?>" type='email'  name="email_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('email_' . $locale->lang, (isset($locations_langs) ? $locations_langs[$locale->lang]->email : '')); ?>' placeholder="<?php echo trans('cms::locations.email'); ?>">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>


            <div class="form-group">
                <label class="col-md-2 control-label" for="longitude"><?php echo trans('cms::locations.longitude'); ?></label>
                <div class="col-md-10">
                    <input type='text' id="longitude" name="longitude" class="form-control"  value='<?php echo old('longitude', (isset($current_location->location->longitude) ? $current_location->location->longitude : '')); ?>' placeholder="<?php echo trans('cms::locations.longitude'); ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="latitude"><?php echo trans('cms::locations.latitude'); ?></label>
                <div class="col-md-10">
                    <input type='text' id="latitude" name="latitude" class="form-control"  value='<?php echo old('latitude', (isset($current_location->location->latitude) ? $current_location->location->latitude : '')); ?>' placeholder="<?php echo trans('cms::locations.latitude'); ?>">
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper wrapper-white col-sm-8 pull-left">
        <div class="form-group">
            <div class="col-md-offset-1 col-md-8">
                <button type="submit" class="btn btn-primary"><?php echo trans('cms::locations.save'); ?></button>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop
