@extends('cms::layouts.master')
@section('content')
<?php
$current_lang = Config::get('app.locale');
?>
<div class="page-title">
    <h1>{{trans('cms::careers.new')}}</h1>
    <p>{{trans('cms::careers.newmessage')}}</p>
<?php
if(session('validate_errors') != null)
  $validate_errors = session('validate_errors');
?>
    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="{{URL('admin/careers')}}">{{trans('cms::careers.careers')}}</a></li>

    </ul>
</div>

<div class="col-md-12" style="">

  <div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div>

<?php if(isset($validate_errors)){?>
    <div class="alert alert-danger alert-dismissible" role="alert">
      <?php foreach($validate_errors as $error){echo $error;}?>

      </div>
      <?php }?>
  </div>
        <div class="wrapper wrapper-white">
            <div class="wrapper">
            <div class="row">

                <form method="post" id="create_form" role="form" action="{{URL('admin/careers/create')}}">
                    <div class='col-md-12'>

                        <div class="tabs">
                                        <ul class="nav nav-tabs nav-tabs-arrowed" role="tablist">
                                          <?php foreach($languages as $language){?>
                  <li class="<?= ($language->lang == $current_lang)? "active":""?>"><a href="#tab1-{{$language->lang}}" role="tab" data-toggle="tab" aria-expanded="<?= ($language->lang == $current_lang)? "false":"true"?>">{{$language->name}}</a></li>
                  <?php }?>


                                        </ul>
                                        <div class="panel-body tab-content">
                                          <?php foreach($languages as $language){?>
                                            <div class="tab-pane <?= ($language->lang == $current_lang)? "active":""?>" id="tab1-{{$language->lang}}">
                                                <div class="col-md-12" >

                                                    <div class="col-md-6" >
                                                 <div class="form-group">
                    <label>Title: <span></span></label>
                    <input type="text" class="form-control" name="{{$language->lang}}_title" required="" minlength="3" value=""/>
                </div>
                                                    </div>
                                                      <div class="col-md-6" >
                                                 <div class="form-group">
                    <label>Location: <span></span></label>
                    <input type="text" class="form-control" name="{{$language->lang}}_location" required="" minlength="3" value=""/>
                </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">

                    <label>Summery: <span></span></label>
                    <textarea name="{{$language->lang}}_summery" class="form-control summernote" cols="50" rows="4"></textarea>

                                                </div>
                                                <div class="col-md-12">
                                                    <label >Description: <span></span></label>
                                                     <textarea class="form-control summernote" name="{{$language->lang}}_description" cols="50" rows="6"></textarea>
                                                </div>

                                            </div>
                                            <?php }?>

                                        </div>


                                    </div>
                        <div class="col-md-12">
                             <div class="form-group">
                    <label> {{trans('cms::careers.category')}}: <span></span></label>
                    <select class="form-control selectpicker" name="cat_id">
                        @foreach($category as $cat)
                        <option value="{{$cat->id}}">{{$cat->catlang('en')->title}}</option>
                        @endforeach
                    </select>
                             </div>
                        </div>

                    </div>

                    <div class="pull-left margin-top-10">
                <!--<button class="btn btn-warning hide-prompts" type="button">Hide prompts</button>-->
                <button class="btn btn-primary" type="submit" id='btn-submit'>{{trans('cms::careers.submit')}}</button>
            </div>

                </form></div>
            </div>
        </div>

        @endsection
