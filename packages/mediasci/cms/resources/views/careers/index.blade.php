@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1><?php echo trans('cms::careers.careers'); ?></h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>

</div>
<!-- ./page title -->
<!-- page title -->
<div class="wrapper wrapper-white">
    @if(Session::has('n'))
    <?php
    $a = [];
    $a = session()->pull('n');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    <?php session()->forget('n'); ?>
    @endif @if(Session::has('m'))
    <?php
    $a = [];
    $a = session()->pull('m');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    <?php session()->forget('m'); ?>
    @endif

    <div class="form-group">
        <a href="{{URL('admin/careers/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::careers.new')}}</button></a>
    </div>
    <!--                            <div class="page-subtitle">
                                                                    <h3>{{trans('cms::careers.careers')}}</h3>
                                                                     </div>-->

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>

                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Title', 'title') ?></th>

                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($careers as $career)
                    <td>{{$career->careerlang('en')->title}}</td>

                    <td>
                        <a href="{{URL('admin/careers/update/'.$career->id)}}" class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>

                        <a id="del" onclick="return confirm('Are you sure you want to delete this career?')" href="{{URL('admin/careers/delete/'.$career->id)}}" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a></td>
                </tr>
                @endforeach
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
            <?php $search_query = \Illuminate\Support\Facades\Input::except('page');
            $careers->appends($search_query);
            ?>
            {!! $careers->links()!!}
        </div>
    </div>

    @endsection
