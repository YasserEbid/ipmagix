@extends('cms::layouts.master')
@section('content')
<?php
$current_lang = Config::get('app.locale');
?>
<div class="page-title">
    <h1>{{trans('cms::menus.edit')}}</h1>


    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="{{URL('admin/menus')}}">{{trans('cms::menus.menus')}}</a></li>

    </ul>
</div>

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>
        <div class="wrapper wrapper-white">
            <div class="wrapper">
            <div class="row">
                <form method="post" id="create_form" role="form" action="{{URL('admin/menus/update/'.$menu->id)}}">
                    <div class='col-md-12'>

                        <div class="tabs">
                                        <ul class="nav nav-tabs nav-tabs-arrowed" role="tablist">
                                          <?php foreach($languages as $language){?>
                  <li class="<?= ($language->lang == $current_lang)? "active":""?>"><a href="#tab1-{{$language->lang}}" role="tab" data-toggle="tab" aria-expanded="<?= ($language->lang == $current_lang)? "false":"true"?>">{{$language->name}}</a></li>
                  <?php }?>

                                        </ul>
                                        <div class="panel-body tab-content">
                                          <?php foreach($languages as $language){?>
                                            <div class="tab-pane <?= ($language->lang == $current_lang)? "active":""?>" id="tab1-{{$language->lang}}">
                                                <div class="col-md-12" >

                                                    <div class="col-md-6" >
                                                 <div class="form-group">
                    <label>name: <span></span></label>
                    <input type="text" class="form-control" name="{{$language->lang}}_name" required="" minlength="3" value="{{$menu->menulinklang($language->lang)->name}}"/>
                </div>
                                                    </div>

                                                </div>


                                            </div>
                                            <?php }?>

                                        </div>


                                    </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                             <div class="form-group">
                    <label> {{trans('cms::menus.master')}}: <span></span></label>
                    <select class="form-control selectpicker" name="menu" >
                        @foreach($menus as $mainmen)
                        @if($mainmen->id==$menu->menu_id)
                        <option value="{{$mainmen->id}}" selected>{{$mainmen->menulang('en')->name}}</option>

                        @else

                        <option value="{{$mainmen->id}}">{{$mainmen->menulang('en')->name}}</option>
                        @endif
                        @endforeach
                    </select>
                             </div>
                        </div>
                               <div class="col-md-3">


                             <div class="form-group">
                    <label> {{trans('cms::menus.main')}}: <span></span></label>

                    <select class="form-control selectpicker" name="mainmenu" >

                      <option value=""></option>
                           @foreach($mainmenus as $mainmenu)
                          @if($mainmenu->id==$menu->parent_id)
                          <option value="{{$mainmenu->id}}" selected=>{{$mainmenu->menulinklang('en')->name}}</option>

                        @else

                        <option value="{{$mainmenu->id}}">{{$mainmenu->menulinklang('en')->name}}</option>
                        @endif
                        @endforeach

                    </select>
                             </div>

                        </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                     <div class="form-group">
                    <label>Link URL: <span></span></label>
                    <input type="text" class="form-control" name="link" required="" minlength="3" value="{{$menu->link_url}}"/>

                                                 </div>
                            </div>
                            <div class="col-md-4">
                                     <div class="form-group">
                    <label>Order: <span></span></label>
                    <input type="text" class="form-control" name="order" required="" minlength="1" value="{{$menu->order}}"/>

                                                 </div>
                            </div>
                            <div class="col-md-4">
                                  <div class="form-group">
                               <label> Activation<span></span></label><br>
                               @if($menu->active==1)
                               <input type="checkbox"   name="active" value="1" checked />
                               @else
                               <input type="checkbox"   name="active" value="1" />
                               @endif
                            </div>
                                </div>
                        </div>
                    </div>

                    <div class="pull-left margin-top-10">
                <!--<button class="btn btn-warning hide-prompts" type="button">Hide prompts</button>-->
                <button class="btn btn-primary" type="submit" id='btn-submit'>{{trans('cms::careers.submit')}}</button>
            </div>

                </form></div>
            </div>
        </div>

        @endsection
