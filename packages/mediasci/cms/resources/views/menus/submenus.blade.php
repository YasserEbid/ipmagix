@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>{{$menu->menulinklang('en')->name}}</h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
                <li><a href="{{URL('admin/menus')}}"><?php echo trans('cms::menus.menus'); ?></a></li>
                  <li><a href="{{URL('admin/menus/'.$menu->menu->id)}}"><?php echo trans('cms::menus.main'); ?></a></li>
                
		
	</ul>
</div>                        
<!-- ./page title -->
<!-- page title -->
<div class="wrapper wrapper-white">
        <div class="form-group">
        <a href="{{URL('admin/menus/create/'.$menu->menu->id.'/'.$menu->id)}}"><button type="button" class="btn btn-primary">{{trans('cms::menus.new')}}</button></a>
    </div>
    @if(Session::has('n'))
	<?php
	$a = [];
	$a = session()->pull('n');
	?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    @endif @if(Session::has('m'))
	<?php
	$a = [];
	$a = session()->pull('m');
	?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    @endif


    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>

                    <th>{{trans('cms::menus.title')}}</th>

                   

                    

                    
                    <th >Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   
                    @foreach($mainmenus as $menus)
                    @if($menus->menulinklang('en'))
                    <td>{{$menus->menulinklang('en')->name}}</td>
                   
                    <td>
                    <a href="./admin/menus/update/{{$menus->id}}"  class="btn default btn-xs red">
                                            <i class="fa fa-edit" type="button" ></i>
                                        </a>
                                        <a href="./admin/menus/delete/{{$menus->id}}"  class="btn default btn-xs red" onclick="return confirm('Are you sure you want to delete this Menu?')">
                                            <i class="fa fa-trash"></i>
                                        </a>
                    </td>
                </tr>
                @endif
                @endforeach
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
          
        </div>
    </div>                   

    @endsection