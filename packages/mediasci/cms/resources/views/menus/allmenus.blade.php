@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>{{trans('cms::menus.menus')}}</h1>

<ul class="breadcrumb">
            <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		
		
	</ul>
	
</div>                        
<!-- ./page title -->
<!-- page title -->
<div class="wrapper wrapper-white">
    
    @if(Session::has('n'))
	<?php
	$a = [];
	$a = session()->pull('n');
	?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    @endif @if(Session::has('m'))
	<?php
	$a = [];
	$a = session()->pull('m');
	?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    @endif


    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>

                    <th>{{trans('cms::menus.title')}}</th>

                   

                    <th>Actions</th>

                    
                
                </tr>
            </thead>
            <tbody>
                <tr>
                   
                    @foreach($menus as $menu)
                    @if($menu->menulang('en'))
                    <td>{{$menu->menulang('en')->name}}</td>
                    <td>
                        <a id="del" href="{{URL('admin/menus/'.$menu->id)}}"  class="btn">
                            <i class="fa fa-eye"></i>  Show Menus
                        </a>
                    </td>
                   
                  
                </tr>
                @endif
                @endforeach
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
          
        </div>
    </div>                   

    @endsection