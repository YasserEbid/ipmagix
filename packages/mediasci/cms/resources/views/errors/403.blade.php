<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- meta section -->
        <title>Mediasci CMS</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >
		<script type="text/javascript">
				setTimeout( function() {
					 history.go(-1);
				}, 10000);
		 </script>
        <link rel="icon" href="{{URL::asset('assets/dashboard/img/favicon.ico')}}" type="image/x-icon" >
        <!-- /meta section -->        

        <!-- css styles -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/dashboard/css/default-blue-white.css')}}" id="dev-css">
        <!-- ./css styles -->                               

        <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/dashboard/css/dev-other/dev-ie-fix.css')}}">
        <![endif]-->

        <!-- javascripts -->
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/modernizr/modernizr.js')}}"></script>
        <!-- ./javascripts -->

        <style>.dev-page{visibility: hidden;}</style>
    </head>
    <body>

        <!-- page wrapper -->
        <div class="dev-page dev-page-login dev-page-login-v2">

            <div class="dev-page-login-block">
                <a class="dev-page-login-block__logo">Can not access</a>
                <div class="dev-page-login-block__form">
                    <div class="title">you aren't<strong> authorized </strong>to access this page</div>
					<div class="form-group no-border margin-top-20">
						<button class="btn btn-success btn-block" onclick="history.go(-1);" >Back</button>
					</div>              

                </div>
                <div class="dev-page-login-block__footer">
                    © 2015 <strong>Mediasci</strong>. All rights reserved.
                </div>
            </div>

        </div>
        <!-- ./page wrapper -->                

        <!-- javascript -->
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/jquery/jquery.min.js')}}"></script>       
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/bootstrap/bootstrap.min.js')}}"></script>        
        <!-- ./javascript -->
    </body>
</html>






