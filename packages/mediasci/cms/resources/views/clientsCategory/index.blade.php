@extends('cms::layouts.master')

@section('content')
<?php
$app_locales = $languages;
$app_locale = Config::get("app.locale");
?>
<!-- page title -->
<div class="page-title">
	<h1>Clients Category</h1>

<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
	</ul>

</div>
<!-- page title -->
<div class="wrapper wrapper-white">
	@if(Session::has('clientCategory_created'))
	<?php
	$a = [];
	$a = session()->pull('clientCategory_created');
	?>
	<div class="alert alert-success" role="alert">{{$a[0]}} </div>
	@endif
	<?php session()->forget('clientCategory_created');?>
    <div class="form-group">
      <a href="<?php echo route('clientsCategory.create'); ?>">
				<button type="button" class="btn btn-primary">{{trans('cms::clientsCategory.new')}}</button></a>
    </div>
    <!-- table content -->
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="col-md-3"><?=\Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name' , 'name')?></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
              @foreach($categories as $category)
                <tr>
                    <td>
											@foreach($category->clientCategoryLangs as $lang)
												@if($lang->lang == Config::get("app.locale"))
													{{$lang->name}}
												@endif
											@endforeach
										</td>
                    <td>
                        <a href="{{URL('admin/clientsCategory/update/'.$category->id)}}" class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a id="del" href="{{URL('admin/clientsCategory/delete/'.$category->id)}}" onclick="return confirm('Are you sure you want to delete this Category?')" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
              @endforeach
        </table>
    </div>
</div>




<!--  append page number to url-->
<div class="col-md-12"
     <div class="btn-group" role="group">
         <?php  $search_query = \Illuminate\Support\Facades\Input::except('page');
                $categories->appends($search_query); ?>
        {!! $categories->links()!!}
    </div>
</div>

@endsection
