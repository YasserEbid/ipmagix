@extends('cms::layouts.master')

@section('content')
<?php
$app_locales = $languages;
$app_locale = Config::get("app.locale");
?>
<div class="page-title">
    <h1>{{trans('cms::clientsCategory.edit')}}</h1>
    <p>{{trans('cms::clientsCategory.editmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="./admin/clientsCategory">{{trans('cms::clientsCategory.clientsCategory')}}</a></li>

    </ul>
</div>

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
        {{trans('cms::clientsCategory.confirm')}}
    </div></div>

<div class="wrapper wrapper-whit">
<div class="row">
    <div class="col-md-12">

        <form action="" method="POST" id="clientsCategory_form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="widget-tabbed margin-top-30">
              <ul class="widget-tabs widget-tabs-three">
                <?php foreach($app_locales as $key => $locale) : ?>
                  <?php $active_tab = ($locale['lang'] == $app_locale ? 'active' : ''); ?>
                  <li class="<?php echo $active_tab; ?>"><a href="#clientCat__<?php echo $locale['lang']; ?>"><?php echo trans('cms::questionsAnswers.locale_'.$locale['lang']); ?></a></li>
                <?php endforeach; ?>
              </ul>
              <?php foreach($app_locales as $key => $locale) : ?>
                <div class="widget-tab <?php echo $active_tab; ?>" id="clientCat__<?php echo $locale['lang']; ?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label" for="name_<?php echo $locale["lang"]; ?>"><?php if($locale["lang"] == "en"){echo trans('cms::categories.englishname');}else{echo trans('cms::categories.arabicname');} ?></label>
                    <div class="col-md-10">
                      <input id="name_<?php echo $locale["lang"]; ?>" type='text'  name="name_<?php echo $locale["lang"]; ?>" class="form-control"
                      value='@foreach($category->clientCategoryLangs as $lang) @if($lang->lang == $locale['lang']) {{ $lang->name }} @endif @endforeach' placeholder="<?php echo trans('cms::categories.name'); ?>">
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
      </div>
            <div class="pull-left margin-top-20">
                <button type="submit" class="btn btn-primary">Update</button>
                <a class="btn btn-link pull-right" href="{{ route('clientsCategory.index') }}"></a>
            </div>
        </form>

    </div>
</div>
</div>

@endsection
