@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>Contact Us Leads</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<div class="wrapper wrapper-white">
  <div class="page-subtitle">
			<h3></h3>
			<a class="btn btn-primary" href="{{URL('admin/exportcontact-us')}}">Export</a>
	</div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name', 'name') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Email', 'email') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Phone', 'phone') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Subject', 'subject') ?></th>
                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Message', 'message') ?></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(empty($all_requests))
                <tr>
                    <td>No contact Requests Found.</td>
                </tr>
                @else
                @foreach($all_requests as $request)
                <tr>
                    <td>{{$request->name}}</td>
                    <td>{{$request->email}}</td>
                    <td>{{$request->phone}}</td>
                    <td>{{$request->subject}}</td>
                    <td>{{$request->message}}</td>
                    <td>
                        <a id="del" href="{{URL('admin/contact-us/delete/'.$request->id)}}" onclick="return confirm('Are you sure you want to delete this request?')" class="btn red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-12">

    </div>
</div>
@endsection
