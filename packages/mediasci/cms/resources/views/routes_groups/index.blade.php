@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?= trans('cms::routes_groups.index_title'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><?php echo trans('cms::routes_groups.index_title'); ?></li>
	</ul>
</div>
<!-- ./page title -->

<div class="wrapper wrapper-white" style="margin-bottom: 10px;">
	<div class="row">
		<div class="page-subtitle">
			<h3><?php echo $current_group ? trans('cms::routes_groups.edit') : trans('cms::routes_groups.create'); ?></h3>
		</div>
	</div>

	<div class="row">
		<?php if (isset($errors)) { ?>
			<?php if (count($errors) > 0) { ?>
				<div class="alert alert-danger">
					<ul>
						<?php foreach ($errors->all() as $error) { ?>
							<li><?php echo $error; ?></li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		<?php } ?>

		<?php if (session('message')) { ?>
			<div class="alert alert-success">
				<?php echo session('message'); ?>
			</div>
		<?php } ?>
		<?php session()->forget('message');?>
	</div>

	<?php $form_route = ($current_group ? ['routes_groups.edit', $current_group->group_id] : ['routes_groups.create']) ?>
	<?php echo Form::open(['route' => $form_route, 'method' => 'post']); ?>
	<div class="row">
		<div class="col-md-2" style="padding: 0 10px 0 0;">
			<div class="form-group">
				<label><?php echo trans('cms::routes_groups.group_name'); ?></label>
			</div>
		</div>
		<div class="col-md-5" style="padding: 0 10px 0 0;">
			<div class="form-group">
				<input value="<?php echo old('group_name', ($current_group ? $current_group->group_name : '')); ?>" type="text" name="group_name" placeholder="<?php trans('cms::routes_groups.group_name'); ?>" class="form-control">
			</div>
		</div>
		<div class="col-md-5" >
			<div class="form-group">
				<button class="btn btn-primary" type="submit"><?php echo trans('cms::routes_groups.save'); ?></button>
			</div>
		</div>
	</div>
	<?php echo Form::close(); ?>
</div>


<!-- data table -->
<div class="wrapper wrapper-white">
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><?php echo trans('cms::routes_groups.group_id'); ?></th>
					<th><?php echo trans('cms::routes_groups.group_name'); ?></th>
					<th><?php echo trans('cms::routes_groups.actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($groups)) { ?>
					<?php foreach ($groups as $group) { ?>
						<tr>
							<td><?php echo $group->group_id; ?></td>
							<td><?php echo $group->group_name; ?></td>
							<td>
								<a  class="btn default btn-xs red" href="<?php echo URL::to('/admin/routes_groups/' . $group->group_id . '/edit'); ?>" ><i class="fa fa-edit"></i>  </a>
								<a  class="btn default btn-xs red" href="<?php echo URL::to('/admin/routes_groups/' . $group->group_id . '/delete'); ?>" ><i class="fa fa-trash-o"></i>  </a>
							</td>
						</tr>
						<?php } ?>
				<?php } else { ?>
					<tr>
						<td style="text-align: center;" colspan="3"><?php echo trans('cms::users.no_roles'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!-- ./data table -->
@stop
