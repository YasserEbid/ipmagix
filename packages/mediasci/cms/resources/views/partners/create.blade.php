@extends('cms::layouts.master')

@section('content')

<?php
$app_locales = $languages;
$app_locale = Config::get("app.locale");
?>
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::partners.new')}}</h1>
    <p>{{trans('cms::partners.newmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="./admin/partners">{{trans('cms::partners.partners')}}</a></li>
    </ul>
</div>

<!-- <div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div> -->
<!-- ./page title -->
<!-- form -->
<div class="wrapper wrapper-white">


	<div class="row">
		<?php if (isset($errors)) { ?>
			<?php if (count($errors) > 0) { ?>
				<div class="alert alert-danger">
					<ul>
						<?php foreach ($errors->all() as $error) { ?>
							<li><?php echo $error; ?></li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		<?php } ?>

		@if(Session::has('fail'))
		<?php
		$a = [];
		$a = session()->pull('fail');
		?>
		<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
		@endif
		@if(Session::has('success'))
		<?php
		$a = [];
		$a = session()->pull('m');
		?>
		<div class="alert alert-success" role="alert">{{$a[0]}} </div>
		@endif
	</div>

	<?php //$form_route = ($category ? ['categories.edit', $category->cat_id] : ['categories.create', 'site_id' => $request->input('site_id')])   ?>
	<?php echo Form::open([/* 'route' => $form_route, */ 'method' => 'post', 'id' => 'partner_form']); ?>
		<input type="hidden" value="" name="featured_image" id="banner_id" />

	<div class="row">


		<div class="wrapper-white col-sm-12 pull-left">
			<div class="widget-tabbed margin-top-30">
				<ul class="widget-tabs widget-tabs-three">
					<?php foreach($app_locales as $key => $locale) : ?>
						<?php $active_tab = ($locale['lang'] == $app_locale ? 'active' : ''); ?>
						<li class="<?php echo $active_tab; ?>"><a href="#category__<?php echo $locale['lang']; ?>"><?php echo trans('cms::questionsAnswers.locale_'.$locale['lang']); ?></a></li>
					<?php endforeach; ?>
				</ul>
				<?php foreach($app_locales as $key => $locale) : ?>
					<div class="widget-tab <?php echo $active_tab; ?>" id="category__<?php echo $locale['lang']; ?>">
						<div class="form-group">
							<label class="col-md-2 control-label" for="name_<?php echo $locale["lang"]; ?>"><?php if($locale["lang"] == "en"){echo trans('cms::partners.englishname');}else{echo trans('cms::partners.arabicname');} ?></label>
							<div class="col-md-10">
								<input id="name_<?php echo $locale["lang"]; ?>" type='text'  name="name_<?php echo $locale["lang"]; ?>" class="form-control"
								value='' placeholder="<?php if($locale["lang"] == "en"){echo trans('cms::partners.englishname');}else{echo trans('cms::partners.arabicname');} ?>" required="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label" for="description_<?php echo $locale->lang; ?>"><?php if($locale["lang"] == "en"){echo trans('cms::partners.english_description');}else{echo trans('cms::partners.arabic_description');} ?></label>
							<div class="col-md-10">
								<textarea id="description_<?php echo $locale->lang; ?>" class="form-control summernote" rows="5" name="description_<?php echo $locale->lang; ?>" placeholder="<?php echo trans('cms::categories.description'); ?>" required>
								</textarea>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="col-md-6" style="padding: 0 10px 0 0;">
		  <div class="form-group">
		  <label><?php echo trans('cms::partners.type'); ?></label>
		  <select class="form-control" name="type" required="">
		  <option value="">Choose Type</option>
      <option value="c">Channel Partners</option>
      <option value="t">Technology Partners</option>
		  </select>
		  </div>
		  </div>
	</div>
	<div class="row">
		<div class="col-sm-6" >
			<div class="panel">
				<div class="panel-heading">
					<span class="panel-title">{{trans('cms::partners.featured_image')}}</span>
				</div>
				<div class="panel-body" id="image-container">
					<div class="wrapper row">
						<a href="javascript:void(0)" class="btn btn-default set-category-thumbnail">{{Lang::get('cms::categories.set_image')}}</a>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-md-6" style="padding: 0 10px 0 0;">
			<button class="btn btn-primary" type="submit"><?php echo trans('cms::partners.submit'); ?></button>
		</div>
	</div>
	<?php echo Form::close(); ?>
</div>
<!-- ./form -->
<script>
	$(function() {
		$(".set-category-thumbnail").filemanager({
			types: "png|jpg|jpeg|gif|bmp",
			done: function(files) {
				if (files.length) {
					var formContainer = $('#partner_form');
					var imageContainer = $('#image-container');
					var file = files[0];
					formContainer.find('#banner_id').val(file.media_id);
					imageContainer.find('img.banner-preview').remove();
					imageContainer.prepend($('<img>', {
						'class': 'banner-preview col-sm-12',
						'src': file.media_thumbnail
					}));
				}
			},
			error: function(media_path) {
				alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
			}
		});
	});
</script>
@stop
