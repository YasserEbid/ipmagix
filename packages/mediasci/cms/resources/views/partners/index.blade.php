@extends('cms::layouts.master')

@section('content')
<?php
$app_locale = Config::get("app.locale");
?>
<!-- page title -->
<div class="page-title">
    <h1>Partners</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>

</div>
<!-- page title -->
<div class="wrapper wrapper-white">
    @if(Session::has('partner_created'))
    <?php
    $a = [];
    $a = session()->pull('partner_created');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    @endif
    <?php session()->forget('partner_created'); ?>
    <div class="form-group">
        <a href="<?php echo route('partners.create'); ?>"><button type="button" class="btn btn-primary"> {{trans('cms::partners.new')}}</button></a>
    </div>
    <!-- table content -->
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="col-md-2"><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name', 'name') ?></th>
                    <th class="col-md-2">Logo Image</th>
                    <th class="col-md-2">Description</th>
                    <th class="col-md-2">Type</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($partners as $partner)
                    <td>{{$partner->partnerlang($app_locale)->name}}</td>
                    <td>
                        <img src="@if($partner->media){{url('uploads/'.$partner->media->media_path)}}@endif" class="img img-circle" alt="" width="100" height="100"/>
                        </th>
                    <td>{!!$partner->partnerlang($app_locale)->description!!}</td>
                    <td>@if($partner->type == 't') Technology Partners @else Channel Partner @endif</td>
                    <td>
                        <a href="{{URL('admin/partners/edit/'.$partner->id)}}" class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a id="del" href="{{URL('admin/partners/delete/'.$partner->id)}}" onclick="return confirm('Are you sure you want to delete this partner?')" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
        </table>
    </div>
</div>











<!--  append page number to url-->
<div class="col-md-12"
     <div class="btn-group" role="group">
        <?php
        $search_query = \Illuminate\Support\Facades\Input::except('page');
        $partners->appends($search_query);
        ?>
        {!! $partners->links()!!}
    </div>
</div>

@endsection
