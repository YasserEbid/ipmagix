@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo $current_category ? trans('cms::faqCategories.edit') : trans('cms::faqCategories.create'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><a href="<?php echo url('admin/faqCategories'); ?>"><?php echo trans('cms::faqCategories.index_title'); ?></a></li>
		<li><?php echo $current_category ? trans('cms::faqCategories.edit') : trans('cms::faqCategories.create'); ?></li>
	</ul>
</div>
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">

	<?php if (isset($errors)) { ?>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error) { ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php } ?>

	@if(Session::has('fail'))
		<?php $a = [];
		$a = session()->pull('fail'); ?>
		<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
	@endif
	@if(Session::has('success'))
	<?php $a = [];
		$a = session()->pull('success'); ?>
		<div class="alert alert-success" role="alert">{{$a[0]}} </div>
	@endif
	<?php //$form_route = ($current_category ? ['faqCategories.update', $current_category->id] : ['faqCategories.create']); ?>
	<?php echo Form::open( [/*'route' => $form_route,*/ 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'post_form']); ?>
	<?php if($current_category ) : ?>
		<input type="hidden" value="<?php echo $current_category->id; ?>" name="service_lang_id" />
		<input type="hidden" value="<?php echo $current_category->lang; ?>" name="lang" />
	<?php endif; ?>

	<?php
	   $app_locales = $languages;
		// $app_locales = Config::get("app.locales");
		$app_locale = Config::get("app.locale");
	?>

	<div class="wrapper-white col-sm-8 pull-left">

			<?php foreach($app_locales as $key => $locale) : ?>
				<div class="widget-tab" id="city_<?php echo $locale; ?>">
					<div class="form-group">
						<label class="col-md-2 control-label" for="name_<?php echo $locale; ?>"><?php echo trans('cms::faqCategories.name_'.$locale); ?></label>
						<div class="col-md-10">
							<input id="name_<?php echo $locale; ?>" type='text'  name="name_<?php echo $locale; ?>" class="form-control"  value='<?php echo old('name_'.$locale, (isset ($category_langs) ? $category_langs[$locale]->name : '')); ?>' placeholder="<?php echo trans('cms::faqCategories.name_'.$locale); ?>">
						</div>
					</div>


				</div>
			<?php endforeach; ?>
			<div class="widget-tab" id="category_active">
				<div class="form-group">
					<label class="col-md-2 control-label" for="active"><?php echo trans('cms::faqCategories.active'); ?></label>
					<div class="col-md-10">
						<?= Form::checkbox('active', '1',(isset($current_category->faqCategory) && $current_category->faqCategory->active?true:false),['id' => 'active']);?>
					</div>
				</div>
			</div>

	</div>


	<div class="wrapper wrapper-white col-sm-8 pull-left">
		<div class="form-group">
			<div class="col-md-offset-1 col-md-8">
				<button type="submit" class="btn btn-primary"><?php echo trans('cms::faqCategories.save'); ?></button>
			</div>
		</div>
	</div>
	<?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop
