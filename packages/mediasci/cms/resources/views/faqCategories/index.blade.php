
@extends('cms::layouts.master')

@section('content')
	@if(Session::has('fail'))
		<?php $a = [];
		$a = session()->pull('fail'); ?>
		<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
	@endif
	@if(Session::has('success'))
	<?php $a = [];
		$a = session()->pull('success'); ?>
		<div class="alert alert-success" role="alert">{{$a[0]}} </div>
	@endif
		<?php session()->flush();?>
	<!-- page title -->
	<div class="wrapper wrapper-white">
		<div class="page-subtitle">
			<h3>{{trans('cms::faqCategories.faqCategories')}}</h3>
		</div>
		<div class="form-group">
			<a href="{{URL('admin/faqCategories/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::faqCategories.new')}}</button></a>
		</div>
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>{{trans('cms::faqCategories.name')}}</th>
						<th><?= trans('cms::faqCategories.active'); ?></th>
						<th><?= trans('cms::faqCategories.created_at'); ?></th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@if(empty($faqCategories))
						<tr>
							<td>No FAQ Categories Found.</td>
						</tr>
					@else
						@foreach($faqCategories as $category)
							<tr>
								<td>{{$category->faqCategoryLang('en')->name}}</td>
								<td>{{($category->active)?"Yes":"No"}}</td>
								<td>{{$category->created_at}}</td>
								<td>
									<a href="{{URL('admin/faqCategories/update/'.$category->id)}}" class="btn default btn-xs red">
										<i class="fa fa-edit"></i> {{trans('cms::faqCategories.update')}}
									</a>
									<a id="del" href="{{URL('admin/faqCategories/delete/'.$category->id)}}" onclick="return confirm('Are you sure you want to delete this FAQ Category?')" class="btn default btn-xs red">
										<i class="fa fa-trash"></i> {{trans('cms::faqCategories.delete')}}
									</a>
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<div class="col-md-12">
			<div class="btn-group" role="group">
				@unless(empty($faqCategories))
					{!! $faqCategories->links()!!}
				@endunless
			</div>
		</div>
	</div>
@endsection
