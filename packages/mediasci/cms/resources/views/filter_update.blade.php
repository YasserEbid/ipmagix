      <div class="form-group col-md-6">
          <?=  \Mediasci\Cms\Http\Controllers\component\Component::gender($gender_id)?>
      </div>
      <br/>
      <div class="form-group col-md-6">
          <?=  \Mediasci\Cms\Http\Controllers\component\Component::lifestyle($lifestyle_id)?>
      </div>
      <br/>
      <div class="form-group col-md-6">
          <?=  \Mediasci\Cms\Http\Controllers\component\Component::location($location_id)?>
      </div>
      <br/>
      <div class="form-group col-md-6">
          <?=  \Mediasci\Cms\Http\Controllers\component\Component::nationality($nationality_id)?>
      </div>
      <br/>
