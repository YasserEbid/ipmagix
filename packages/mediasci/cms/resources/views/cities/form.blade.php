@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::cities.index_title'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><a href="<?php echo url('admin/cities'); ?>"><?php echo trans('cms::cities.index_title'); ?></a></li>
		<li><?php echo $current_city ? trans('cms::cities.edit') : trans('cms::cities.create'); ?></li>
	</ul>
</div>
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
	<div class="row">
		<div class="page-subtitle">
			<h3><?php echo $current_city ? trans('cms::cities.edit') : trans('cms::cities.create'); ?></h3>
		</div>
	</div>
	<?php if (isset($errors)) { ?>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error) { ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php } ?>

	<?php /*if (session('message')) { ?>
		<div class="alert alert-success">
			<?php echo session('message'); ?>
		</div>
	<?php } */?>
	@if(Session::has('fail'))
		<?php $a = [];
		$a = session()->pull('fail'); ?>
		<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
	@endif
	@if(Session::has('success'))
	<?php $a = [];
		$a = session()->pull('success'); ?>
		<div class="alert alert-success" role="alert">{{$a[0]}} </div>
	@endif
	<?php //$form_route = ($current_city ? ['cities.update', $current_city->id] : ['cities.create']); ?>
	<?php echo Form::open( [/*'route' => $form_route,*/ 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'post_form']); ?>
	<?php if($current_city ) : ?>
		<input type="hidden" value="<?php echo $current_city->id; ?>" name="city_lang_id" />
		<input type="hidden" value="<?php echo $current_city->lang; ?>" name="lang" />
	<?php endif; ?>

	<?php
		$app_locales = $languages;
		$app_locale = Config::get("app.locale");
	?>

	<div class="wrapper-white col-sm-8 pull-left">
		
			<?php foreach($app_locales as $key => $locale) : ?>
				<div class="widget-tab" id="city_<?php echo $locale['lang']; ?>">
					<div class="form-group">
						<label class="col-md-2 control-label" for="name_<?php echo $locale['lang']; ?>"><?php echo trans('cms::cities.name_'.$locale['lang']); ?></label>
						<div class="col-md-10">
							<input id="name_<?php echo $locale['lang']; ?>" type='text'  name="name_<?php echo $locale['lang']; ?>" class="form-control"  value='<?php echo old('name_'.$locale['lang'], (isset ($city_langs) ? $city_langs[$locale['lang']]->name : '')); ?>' placeholder="<?php echo trans('cms::cities.name'); ?>">
						</div>
					</div>


				</div>
			<?php endforeach; ?>
			<div class="widget-tab" id="city_active">
				<div class="form-group">
					<label class="col-md-2 control-label" for="active"><?php echo trans('cms::cities.active'); ?></label>
					<div class="col-md-10">
						<?= Form::checkbox('active', '1',(isset($current_city->city) && $current_city->city->active?true:false),['id' => 'active']);?>
					</div>
				</div>
			</div>

	</div>


	<div class="wrapper wrapper-white col-sm-8 pull-left">
		<div class="form-group">
			<div class="col-md-offset-1 col-md-8">
				<button type="submit" class="btn btn-primary"><?php echo trans('cms::cities.save'); ?></button>
			</div>
		</div>
	</div>
	<?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->

<script>
$(function() {

});
</script>
@stop
