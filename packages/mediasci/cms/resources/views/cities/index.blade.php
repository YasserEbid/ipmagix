
@extends('cms::layouts.master')

@section('content')
	@if(Session::has('fail'))
		<?php $a = [];
		$a = session()->pull('fail'); ?>
		<div class="alert alert-danger" role="alert">{{$a[0]}} </div>
		<?php session()->forget('fail');?>
	@endif
	@if(Session::has('success'))
	<?php $a = [];
		$a = session()->pull('success'); ?>
		<div class="alert alert-success" role="alert">{{$a[0]}} </div>
		<?php session()->forget('success');?>
	@endif
	
	<!-- page title -->
	<div class="wrapper">
		<div class="page-subtitle">
			<h3>{{trans('cms::cities.cities')}}</h3>
		</div>
		<div class="form-group">
			<a href="{{URL('admin/cities/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::cities.new')}}</button></a>
		</div>
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>{{trans('cms::cities.name')}}</th>
						<th><?= trans('cms::cities.active'); ?></th>
						<th><?= trans('cms::cities.created_at'); ?></th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@if(empty($cities))
						<tr>
							<td>No cities Found.</td>
						</tr>
					@else
						@foreach($cities as $city)
							<tr>
								<td>{{$city->cityLang('en')->name}}</td>
								<td>{{($city->active)?"Yes":"No"}}</td>
								<td>{{$city->created_at}}</td>
								<td>
									<a href="{{URL('admin/cities/update/'.$city->id)}}" class="btn default btn-xs red">
										<i class="fa fa-edit"></i>
									</a>
									<a id="del" onclick="return confirm('Are you sure you want to delete this city?')" href="{{URL('admin/cities/delete/'.$city->id)}}" class="btn default btn-xs red">
										<i class="fa fa-trash"></i>
									</a>
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
		<div class="col-md-12">
			<div class="btn-group" role="group">
				@unless(empty($cities))
					{!! $cities->links()!!}
				@endunless
			</div>
		</div>
	</div>
@endsection
