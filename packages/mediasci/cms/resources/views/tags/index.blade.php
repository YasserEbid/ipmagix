@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::tags.index_title'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><?php echo trans('cms::tags.index_title'); ?></li>
	</ul>
</div>
<!-- ./page title -->


<div class="wrapper wrapper-white" style="margin-bottom: 10px;">
	<div class="row">
		<div class="page-subtitle">
			<h3><?php echo $current_tag ? trans('cms::tags.edit') : trans('cms::tags.create'); ?></h3>
		</div>
	</div>

	<div class="row">
		<?php if (isset($errors)) { ?>
			<?php if (count($errors) > 0) { ?>
				<div class="alert alert-danger">
					<ul>
						<?php foreach ($errors->all() as $error) { ?>
							<li><?php echo $error; ?></li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		<?php } ?>

		<?php if (session('message')) { ?>
			<div class="alert alert-success">
				<?php echo session('message'); ?>
			</div>
		<?php } ?>
		<?php session()->forget('message');?>
	</div>

	<?php $form_route = ($current_tag ? ['tags.edit', $current_tag->tag_id] : ['tags.create']) ?>
	<?php echo Form::open(['route' => $form_route, 'method' => 'post']); ?>
	<div class="row">
		<div class="col-md-2" style="padding: 0 10px 0 0;">
			<div class="form-group">
				<label><?php echo trans('cms::tags.tag_name'); ?></label>
			</div>
		</div>
		<div class="col-md-5" style="padding: 0 10px 0 0;">
			<div class="form-group">
				<input value="<?php echo old('tag_name', ($current_tag ? $current_tag->tag_name : '')); ?>" type="text" name="tag_name" placeholder="<?php echo trans('cms::tags.tag_name'); ?>" class="form-control">
			</div>
		</div>
		<div class="col-md-5" >
			<div class="form-group">
				<button class="btn btn-primary" type="submit"><?php echo trans('cms::tags.save'); ?></button>
			</div>
		</div>
	</div>
	<?php echo Form::close(); ?>
</div>

<!-- data table -->
<div class="wrapper wrapper-white">
	<div class="dataTables_length">
		<div>
			<label><?php echo trans('cms::tags.show_per_page'); ?> </label>
			<select class='per_page form-control'>
				<option value="10" <?php echo ($request->input('per_page') == 10) ? 'selected' : '' ?> >10</option>
				<option value="25" <?php echo ($request->input('per_page') == 25) ? 'selected' : '' ?> >25</option>
				<option value="50" <?php echo ($request->input('per_page') == 50) ? 'selected' : '' ?> >50</option>
				<option value="100" <?php echo ($request->input('per_page') == 100) ? 'selected' : '' ?> >100</option>
			</select>
		</div>
	</div>

	<div class="dataTables_filter">
		<?php echo Form::open(['route' => ['tags.index'], 'method' => 'get', 'id' => 'tags_search_form']); ?>
		<label>Search:<input type="search" value="<?php echo $request->input('q'); ?>" name="q" class="form-control search_q"></label>
		<?php echo Form::close(); ?>
	</div>

	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><?php echo trans('cms::tags.tag_id'); ?></th>
					<th><?php echo trans('cms::tags.tag_name'); ?></th>
					<th><?php echo trans('cms::users.actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($tags)) { ?>
					<?php foreach ($tags as $tag) { ?>
						<tr>
							<td><?php echo $tag->tag_id; ?></td>
							<td><?php echo $tag->tag_name; ?></td>
							<td>
								<a href="<?php echo route('tags.edit', ['id' => $tag->tag_id]); ?>" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
								<a href="<?php echo route('tags.delete', ['id' => $tag->tag_id]); ?>" onclick="return confirm('<?php echo trans('cms::tags.confirm_delete'); ?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td style="text-align: center;" colspan="3"><?php echo trans('cms::tags.no_tags'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php
		if ($tags) {
			echo $tags->appends($request->except('page'))->links();
		}
		?>
	</div>
</div>
<!-- ./data table -->
<?php
$params_per_page = count($request->except(['per_page'])) ? http_build_query($request->except(['per_page'])) : '';

$params_search = count($request->except(['q'])) ? http_build_query($request->except(['q'])) : '';
?>
<script>
	$(document).ready(function () {
		$('.per_page').change(function () {
			location.href = '<?php echo route('tags.index'); ?>?<?php echo $params_per_page ? $params_per_page . '&' : ''; ?>per_page=' + $(this).val();
		});


		$('#tags_search_form').submit(function (e) {
			var q = $(this).find('.search_q');

			if (q.length > 0) {
				if (q.val().trim() == '') {
					location.href = '<?php echo route('tags.index'); ?><?php echo $params_search ? '?' . $params_search : ''; ?>';
					return false;
				}
			} else {
				return false;
			}
		});

	});
</script>
@stop
