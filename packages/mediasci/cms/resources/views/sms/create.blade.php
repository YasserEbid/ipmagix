@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::sms.index_title'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><?php echo trans('cms::sms.index_title'); ?></li>
	</ul>
</div>
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
	<div class="row">
		<div class="page-subtitle">
			<h3><?php echo $current_sms ? trans('cms::sms.edit') : trans('cms::sms.create'); ?></h3>
		</div>
	</div>
	<?php if (isset($errors)) { ?>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error) { ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if (session('message')) { ?>
		<div class="alert alert-success">
			<?php echo session('message'); ?>
		</div>
	<?php } ?>
	<?php $form_route = ($current_sms ? ['sms.edit', $current_sms->sms_id] : ['sms.create']); ?>
	<?php echo Form::open(['route' => $form_route, 'method' => 'post', 'class' => "form-horizontal"]); ?>
	<input type="hidden" value="<?php echo $request->input('site_id'); ?>" name="site_id" />
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::sms.users'); ?></label>
		<div class="col-md-10">

			<?php foreach ($users as $user) { ?>
					<div class="checkbox checkbox-inline col-md-3">
						<input type="checkbox" id="check_<?php echo $user->user_id; ?>" value="<?php echo $user->user_id; ?>" name="users[]" <?php echo (isset($current_sms) && in_array($user->user_id, $selected_users)) ? 'checked' : ''; ?>/>
						<label for="check_<?php echo $user->user_id; ?>"><?php echo $user->full_name ?></label>
					</div>
			<?php } ?>

		</div>

	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::sms.sms_message'); ?></label>
		<div class="col-md-8">
			<textarea name="sms_message" placeholder="<?php echo trans('cms::sms.sms_message'); ?>" class="form-control"><?php echo old('sms_message', ($current_sms ? $current_sms->sms_message : '')); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::sms.sent_date'); ?></label>
		<div class="col-md-8">
			<?php
				$sent_date='';
				if($current_sms ) {
					$the_date = strtotime($current_sms->sent_date);
					date_default_timezone_get();
					$sent_date=  str_replace(array('UTC','EET'),'T',date("Y-m-dTH:i",$the_date));
				}
			?>
			<input  type="datetime-local"  name="sent_date"  class="form-control"  value='<?php echo old('sent_date', ($current_sms ? $sent_date : '')); ?>' placeholder="<?php echo trans('cms::sms.sent_date'); ?>">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-8">
			<button type="submit" class="btn btn-default"><?php echo trans('cms::sms.save'); ?></button>
		</div>
	</div>
	<?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop
