@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::comments.index_title'); ?></h1>
	

	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><?php echo trans('cms::comments.index_title'); ?></li>
	</ul>
</div>                        
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
	<div class="row">
		<div class="page-subtitle">
			<h3><?php echo $current_comment ? trans('cms::comments.edit') : trans('cms::comments.create'); ?></h3>
		</div>
	</div>
	<?php if (isset($errors)) { ?>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error) { ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if (session('message')) { ?>
		<div class="alert alert-success">
			<?php echo session('message'); ?>
		</div>
	<?php } ?>
	<?php $form_route = ($current_comment ? ['comments.edit', $current_comment->comment_id] : ['comments.create']); ?>
	<?php $readonly = ($current_comment ? 'readonly' : ''); ?>
	<?php echo Form::open(['route' => $form_route,'method' => 'post', 'class' => "form-horizontal"]); ?> 
	<input type="hidden" value="<?php echo $request->input('post_id'); ?>" name="post_id" />
	<input type="hidden" value="<?php echo $request->input('site_id'); ?>" name="site_id" />
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::comments.full_name'); ?></label>
		<div class="col-md-8">
			<input type='text' <?php echo $readonly; ?> name="full_name" class="form-control" data-validation='required' value='<?php echo old('full_name', ($current_comment ? $current_comment->name : '')); ?>' placeholder="<?php echo trans('cms::comments.full_name'); ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::comments.email'); ?></label>
		<div class="col-md-8">
			<input type='text' name="email" <?php echo $readonly; ?> class="form-control" data-validation='email' value='<?php echo old('email', ($current_comment ? $current_comment->email : '')); ?>' placeholder="<?php echo trans('cms::comments.email'); ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::comments.content'); ?></label>
		<div class="col-md-8">
			<textarea class="form-control" rows="5" name="content" placeholder="Something about you"><?php echo old('full_name', ($current_comment ? $current_comment->comment_content : '')); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-8">
			<button type="submit" class="btn btn-default"><?php echo trans('cms::comments.save'); ?></button>
		</div>
	</div>
	<?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
<script> $.validate(); </script>
@stop