@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?= trans('cms::comments.index_title'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?= trans('cms::common.dashboard'); ?></a></li>
		<li><?= trans('cms::comments.index_title'); ?></li>
	</ul>
</div>
<!-- ./page title -->

<!-- data table -->
<div class="wrapper wrapper-white">
	<div class="row">
		<?php if (session('message')) { ?>
			<div class="alert alert-success">
				<?php echo session('message'); ?>
			</div>
		<?php } ?>
		<?php session()->forget('message');?>
	</div>
	<div class="row">

		<div class="col-md-3">
			<label><?php echo trans('cms::comments.show_per_page'); ?> </label>
			<select class='per_page form-control'>
				<option value="10" <?php echo ($request->input(['per_page']) == 10) ? 'selected' : '' ?> >10</option>
				<option value="25" <?php echo ($request->input(['per_page']) == 25) ? 'selected' : '' ?> >25</option>
				<option value="50" <?php echo ($request->input(['per_page']) == 50) ? 'selected' : '' ?> >50</option>
				<option value="100" <?php echo ($request->input(['per_page']) == 100) ? 'selected' : '' ?> >100</option>
			</select>
		</div>

		<div class="col-md-3">
			<label><?php echo trans('cms::comments.filter_by_status'); ?> </label>
			<select class='status form-control'>
				<option value="" ><?php echo trans('cms::comments.select_filter'); ?></option>
				<option value="1" <?php echo ($request->input(['status']) == 1) ? 'selected' : '' ?> ><?php echo trans('cms::comments.published'); ?></</option>
				<option value="2" <?php echo ($request->input(['status']) == 2) ? 'selected' : '' ?> ><?php echo trans('cms::comments.waiting_for_revision'); ?></option>
				<option value="3" <?php echo ($request->input(['status']) == 3) ? 'selected' : '' ?> ><?php echo trans('cms::comments.spammed'); ?></option>
				<option value="4" <?php echo ($request->input(['status']) == 4) ? 'selected' : '' ?> ><?php echo trans('cms::comments.deleted'); ?></option>
			</select>
		</div>

		<div class="col-md-3">
			<label><?php echo trans('cms::comments.filter_by_site'); ?> </label>
			<select class='site form-control'>
				<?php foreach ($sites as $site) { ?>
					<option value="<?php echo $site->site_id; ?>" <?php echo ($request->input(['site_id']) == $site->site_id) ? 'selected' : '' ?> ><?php echo $site->site_name; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-3">
			<label><?php echo trans('cms::comments.search_by_keyword'); ?> </label>
			<input class="search_q form-control" name="q" id="comments_search_form" value="<?php echo $request->input(['q']); ?>"/>
		</div>
	</div>
	<br />
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><?php echo trans('cms::comments.comment_id'); ?></th>
					<th><?php echo trans('cms::comments.post_title'); ?></th>
					<th><?php echo trans('cms::comments.comment_title'); ?></th>
					<th><?php echo trans('cms::comments.status_name'); ?></th>
					<th><?php echo trans('cms::comments.comment_date'); ?></th>
					<th><?php echo trans('cms::users.actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($comments)) { ?>
					<?php foreach ($comments as $comment) { ?>
						<tr>
							<td><?php echo $comment->comment_id; ?></td>
							<td><?php echo $comment->title; ?></td>
							<td><?php echo $comment->comment_content; ?></td>
							<td><?php echo trans('cms::comments.'.$comment->status_name); ?></td>
							<td><?php echo $comment->comment_date ?></td>
							<?php if ($comment->status_id == 4) { ?>
								<td>
									<a data-toggle="tooltip" title="<?php echo trans('cms::comments.edit_tool_tip'); ?>" href="<?php echo route('comments.edit', ['id' => $comment->comment_id, 'post_id' => $comment->post_id, 'site_id' => $comment->site_id]); ?>" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
									<a data-toggle="tooltip" title="<?php echo trans('cms::comments.hard_delete_tool_tip'); ?>" href="<?php echo route('comments.hard_delete', ['id' => $comment->comment_id]); ?>" onclick="return confirm('<?php echo trans('cms::comments.confirm_hard_delete'); ?>')" class="btn default btn-xs red"><i class="fa fa-remove"></i></a>
									<a data-toggle="tooltip" title="<?php echo trans('cms::comments.publish_tool_tip'); ?>" <?php echo $comment->status_id == 1 ? 'disabled style="color:red;"' : ''; ?> href="<?php echo route('comments.publish', ['id' => $comment->comment_id]); ?>" onclick="return confirm('<?php echo trans('cms::comments.confirm_publish'); ?>')" class="btn default btn-xs red"><i class="fa fa-check"></i></a>
								</td>
							<?php } else { ?>
								<td>
									<a data-toggle="tooltip" title="<?php echo trans('cms::comments.edit_tool_tip'); ?>" href="<?php echo route('comments.edit', ['id' => $comment->comment_id, 'post_id' => $comment->post_id, 'site_id' => $comment->site_id]); ?>" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
									<a data-toggle="tooltip" title="<?php echo trans('cms::comments.soft_delete_tool_tip'); ?>" <?php echo $comment->status_id == 4 ? 'disabled style="color:red;"' : ''; ?> href="<?php echo route('comments.soft_delete', ['id' => $comment->comment_id]); ?>" onclick="return confirm('<?php echo trans('cms::comments.confirm_delete'); ?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a>
									<a data-toggle="tooltip" title="<?php echo trans('cms::comments.spam_tool_tip'); ?>" <?php echo $comment->status_id == 3 ? 'disabled style="color:red;"' : ''; ?> href="<?php echo route('comments.spam', ['id' => $comment->comment_id]); ?>" onclick="return confirm('<?php echo trans('cms::comments.confirm_spam'); ?>')" class="btn default btn-xs red"><i class="fa fa-ban"></i></a>
									<a data-toggle="tooltip" title="<?php echo trans('cms::comments.publish_tool_tip'); ?>" <?php echo $comment->status_id == 1 ? 'disabled style="color:red;"' : ''; ?> href="<?php echo route('comments.publish', ['id' => $comment->comment_id]); ?>" onclick="return confirm('<?php echo trans('cms::comments.confirm_publish'); ?>')" class="btn default btn-xs red"><i class="fa fa-check"></i></a>
								</td>
							<?php } ?>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td style="text-align: center;" colspan="6"><?php echo trans('cms::comments.no_comments'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php
		if ($comments) {
			echo $comments->appends($request->except('page'))->links();
		}
		?>
	</div>
</div>
<!-- ./data table -->
<?php
$params_per_page = count($request->except(['per_page'])) ? http_build_query($request->except(['per_page'])) : '';

$params_status = count($request->except(['status'])) ? http_build_query($request->except(['status'])) : '';

$params_site_id = count($request->except(['site_id'])) ? http_build_query($request->except(['site_id'])) : '';

$params_search = count($request->except(['q'])) ? http_build_query($request->except(['q'])) : '';
?>
<script>
	$(document).ready(function () {

		$('.per_page').change(function () {
			location.href = '<?php echo route('comments.index'); ?>?<?php echo $params_per_page ? $params_per_page . '&' : ''; ?>per_page=' + $(this).val();
		});

		$('.status').change(function () {
			location.href = '<?php echo route('comments.index'); ?>?<?php echo $params_status ? $params_status . '&' : ''; ?>status=' + $(this).val();
		});

		$('.site').change(function () {
			location.href = '<?php echo route('comments.index'); ?>?<?php echo $params_site_id ? $params_site_id . '&' : ''; ?>site_id=' + $(this).val();
		});

		$('#comments_search_form').keypress(function (e) {

			var key = e.which;

			if (key == 13) {
				location.href = '<?php echo route('comments.index'); ?>?<?php echo $params_search ? $params_search . '&' : ''; ?>q=' + $(this).val().trim();
			}

		});

	});
</script>
@stop
