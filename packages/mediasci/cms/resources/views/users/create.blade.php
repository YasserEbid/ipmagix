@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>Add User</h1>

	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
                <li><a href="{{URL('admin/user')}}"><?php echo trans('cms::users.index_title'); ?></a></li>
	</ul>
</div>                        
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
	<?php if (isset($errors)) { ?>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error) { ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if (session('message')) { ?>
		<div class="alert alert-danger">
			<?php echo session('message'); ?>
		</div>
	<?php } ?>
	<?php echo Form::open(['method' => 'post', 'class' => "form-horizontal"]); ?> 
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::users.full_name'); ?></label>
		<div class="col-md-8">
			<input type='text' name="full_name" class="form-control" data-validation='required' value='' placeholder="<?php echo trans('cms::users.full_name'); ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::users.email'); ?></label>
		<div class="col-md-8">
			<input type='text' name="email" class="form-control" data-validation='email' value='' placeholder="<?php echo trans('cms::users.email'); ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::users.mobile'); ?></label>
		<div class="col-md-8">
			<input type='text' name="mobile" class="form-control" data-validation='required' value='' placeholder="<?php echo trans('cms::users.mobile'); ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::users.password'); ?></label>
		<div class="col-md-8">
			<input type='password' name="password" class="form-control"  data-validation="length" data-validation-length="min6" value='' placeholder="<?php echo trans('cms::users.password'); ?>">
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::users.role'); ?></label>
		<div class="col-md-8">
			<select name="role_id" class="form-control" data-validation="required">
				<option value=""><?= trans('cms::users.choose_role'); ?></option>
				<?php foreach ($roles as $role){ ?>
					<option value="<?php echo $role->role_id; ?>" ><?php echo $role->role_name; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-8">
			<button type="submit" class="btn btn-primary"><?php echo trans('cms::users.save'); ?></button>
		</div>
	</div>
	<?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
<script> $.validate(); </script>
@stop