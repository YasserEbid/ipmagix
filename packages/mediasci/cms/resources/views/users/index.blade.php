@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>
		<?= trans('cms::users.index_title'); ?>
	</h1>

<ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>

</div>
<div class="form-group">
	<a href="<?php echo route('user.create'); ?>" style="margin-left: 25px;" class="btn btn-primary"><?= trans('cms::users.create'); ?></a>
</div>
<!-- ./page title -->

<!-- data table -->
<div class="wrapper wrapper-white">
	<div class="row">
		<?php if (session('message')) { ?>
			<div class="alert alert-success">
				<?php echo session('message'); ?>
			</div>
		<?php } ?>
		<?php session()->forget('message');?>
	</div>
	<div class="dataTables_length">
		<div>
			<label><?php echo trans('cms::users.show_per_page'); ?> </label>
			<select class='per_page form-control'>
				<option value="10" <?php echo ($request->input(['per_page'])==10)?'selected':'' ?> >10</option>
				<option value="25" <?php echo ($request->input(['per_page'])==25)?'selected':'' ?> >25</option>
				<option value="50" <?php echo ($request->input(['per_page'])==50)?'selected':'' ?> >50</option>
				<option value="100" <?php echo ($request->input(['per_page'])==100)?'selected':'' ?> >100</option>
			</select>
		</div>
	</div>

	<div class="dataTables_filter">
		<?php echo Form::open(['route' => ['user.index'], 'method' => 'get', 'id' => 'users_search_form']); ?>
		<label>Search:<input type="search" value="<?php echo $request->input('q'); ?>" name="q" class="form-control search_q"></label>
		<?php echo Form::close(); ?>
	</div>

	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><?= trans('cms::users.user_id'); ?></th>
					<th><?= trans('cms::users.user_name'); ?></th>
					<th><?= trans('cms::users.privilege'); ?></th>
					<th>Points</th>
					<th><?= trans('cms::users.actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($users)) { ?>
					<?php foreach ($users as $user) { ?>
						<tr>
							<td><?php echo $user->user_id; ?></td>
							<td><?php echo $user->full_name; ?></td>
							<td><?php echo \Roles::find_by(['role_id' => $user->role_id])->role_name; ?></td>
							<td>{{$user->points}}</td>
							<td>
								<a class="btn default btn-xs red" href="<?php echo URL::to('/admin/user/' . $user->user_id . '/edit'); ?>" ><i class="fa fa-edit"></i>  </a>
								<a class="btn default btn-xs red" href="<?php echo URL::to('/admin/user/' . $user->user_id . '/delete'); ?>" onclick="return confirm('Are you sure you want to delete this User?')" ><i class="fa fa-trash-o"></i>  </a>
							</td>
						</tr>
					<?php }
					} else {
					?>
					<tr>
						<td style="text-align: center;" colspan="4"><?php echo trans('cms::users.no_users'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php
		if ($users) {
			echo $users->appends($request->except('page'))->links();
		}
		?>
	</div>
</div>
<!-- ./data table -->
<?php
$params_per_page = count($request->except(['per_page'])) ? http_build_query($request->except(['per_page'])) : '';

$params_search = count($request->except(['q'])) ? http_build_query($request->except(['q'])) : '';
?>
<script>
    $(document).ready(function () {
        $('.per_page').change(function () {
            location.href = '<?php echo route('user.index'); ?>?<?php echo $params_per_page ? $params_per_page . '&' : ''; ?>per_page=' + $(this).val();
        });


        $('#users_search_form').submit(function (e) {
            var q = $(this).find('.search_q');

            if (q.length > 0) {
                if (q.val().trim() == '') {
                    location.href = '<?php echo route('user.index'); ?><?php echo $params_search ? '?' . $params_search : ''; ?>';
                    return false;
                }
            } else {
                return false;
            }
        });

    });
</script>
@stop
