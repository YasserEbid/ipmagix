@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
  <!-- <h1><?php //echo  trans('cms::posts.post_tabs'); ?></h1> -->
<h1><?php echo $posts_langs->post_title ." Tabs"; ?></h1>

</div>
 <div class="wrapper wrapper-white">
     <div class="form-group">
         <a href="{{URL('admin/poststabs/create/'.$post->post_id)}}"><button type="button" class="btn btn-primary">{{trans('cms::tabs.new')}}</button></a>
     </div>
     @if(Session::has('n'))
                <?php $a = [];
                $a = session()->pull('n'); ?>
                <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
                <?php session()->forget('n');?>
                @endif @if(Session::has('m'))
<?php $a = [];
$a = session()->pull('m'); ?>
                <div class="alert alert-success" role="alert">{{$a[0]}} </div>
                <?php session()->forget('m');?>
                @endif
                
                            <div class="page-subtitle">
                                <!-- <h3>{{$posts_langs->post_title}}</h3> -->
                                 </div>
<?php if($tabs->count() > 0) {?>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>

                                           <th><?=\Mediasci\Cms\Http\Controllers\component\Helpers::sort('Title' , 'title')?></th>
                                            <th>Slug</th>

                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                           @foreach($tabs as $tab )

                                            <td>{{$tab->tablang('en')->title}}</td>
                                            <td>{{$tab->slug}}</td>
                                            <td>
                                                    <a href="{{URL('admin/poststabs/update/'.$tab->id)}}" class="btn default btn-xs red">
                                                            <i class="fa fa-edit"></i>
                                                    </a>

                                                    <a id="del" onclick="return confirm('Are you sure you want to delete this Event Page?')" href="{{URL('admin/poststabs/delete/'.$tab->id)}}" class="btn default btn-xs red">
                                                            <i class="fa fa-trash"></i>
                                                    </a></td>
                                        </tr>
                                       @endforeach
                                </table>
                            </div>
                            <?php }else{
                              ?>
<h3>Sorry , there are no tabs for this post yet .</h3>
                              <?php
                            }?>
     <div class="col-md-12"
     <div class="btn-group" role="group">
         <?php  $search_query = \Illuminate\Support\Facades\Input::except('page');
                    $tabs->appends($search_query); ?>
                                               {!! $tabs->links()!!}
                                            </div>
                        </div>

@endsection
