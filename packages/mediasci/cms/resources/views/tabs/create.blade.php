@extends('cms::layouts.master')
@section('content')
<div class="page-title">
    <h1>{{trans('cms::posts.new_tab')}}</h1>
    <!-- <p>{{trans('cms::posts.newtabs')}}</p> -->

    <ul class="breadcrumb">
        <li><a href="{{URL('admin/dashboard')}}">{{trans('cms::common.dashboard')}}</a></li>


    </ul>
</div>
<?php
$current_lang = Config::get('app.locale');
?>
<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
        {{trans('cms::events.confirm')}}
    </div></div>
<div class="wrapper wrapper-white">
    <div class="wrapper">
        <div class="row">
            <form method="post" id="create_form" role="form" action="{{URL('admin/poststabs/create/'.$id)}}">
                <input type="hidden" name="post_id" value="{{$id}}">
                <div class="col-md-12">


                </div>
                <div class="clear-fix"></div>
                <div class='col-md-12'>
                    <div class="clear-fix"></div>

                    <div class="tabs">
                        <ul class="nav nav-tabs nav-tabs-arrowed" role="tablist">
                          <?php foreach($languages as $language){?>
                          <li class="<?= ($language->lang == $current_lang)? "active":""?>"><a href="#tab1-{{$language->lang}}" role="tab" data-toggle="tab" aria-expanded="<?= ($language->lang == $current_lang)? "false":"true"?>">{{$language->name}}</a></li>
                          <?php }?>
                            <!-- <li class="active"><a href="#tab1-second" role="tab" data-toggle="tab" aria-expanded="true">English</a></li>
                            <li class=""><a href="#tab1-first" role="tab" data-toggle="tab" aria-expanded="false">Arabic</a></li> -->
                        </ul>
                        <div class="panel-body tab-content">
                          <?php foreach($languages as $language){?>
                            <div class="tab-pane <?= ($language->lang == $current_lang)? "active":""?>" id="tab1-{{$language->lang}}">
                            <!-- <div class="tab-pane active" id="tab1-second"> -->
                                <div class="col-md-12" >
                                    <div class="form-group">
                                        <label>title: <span></span></label>
                                        <input type="text" class="form-control" name="{{$language->lang}}_title" required="" minlength="3" value=""/>
                                    </div>
                                </div>
                                   <div class="col-md-12">
                                                     <label>description: <span></span></label>
                                                     <textarea class="form-control summernote" name="{{$language->lang}}_desc" cols="50" rows="6"></textarea>
                                                </div>

                            </div>
<!--
                            <div class="tab-pane" id="tab1-first">
                                <div class="tab-pane " id="tab1-second">
                                    <div class="col-md-12" >

                                        <div class="form-group">
                                            <label>العنوان: <span></span></label>
                                            <input type="text" class="form-control" name="ar_title" required="" minlength="3" value=""/>

                                        </div>
                                    </div>
   <div class="col-md-12">
                                                     <label>الوصف: <span></span></label>
                                                     <textarea class="form-control summernote" name="ar_desc"  cols="50" rows="6"></textarea>
                                                </div>

                                </div> </div> -->
                                <?php }?>

                        </div>


                    </div>

                </div>
                <div class ="col-md-12"   >
                    <div class="form_group">
                           <label>slug: <span></span></label>
                           <input name="slug" type="text" class="form-control" required>
                    </div>
                </div>             <div class="pull-left margin-top-10">
                    <!--<button class="btn btn-warning hide-prompts" type="button">Hide prompts</button>-->
                    <button class="btn btn-primary" type="submit" id='btn-submit'>{{trans('cms::tabs.submit')}}</button>
                </div>
            </form>
          </div>
    </div>
</div>

@endsection
