@extends('cms::layouts.master')

@section('content')
<?php
$app_locale = Config::get("app.locale");
?>
<!-- page title -->
<div class="page-title">
	<h1>Feedback</h1>

<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

	</ul>

</div>
<!-- page title -->
<div class="wrapper wrapper-white">
	@if(Session::has('feedback_created'))
	<?php
	$a = [];
	$a = session()->pull('feedback_created');
	?>
	<div class="alert alert-success" role="alert">{{$a[0]}} </div>
	@endif
	<?php session()->forget('feedback_created');?>
    <div class="form-group">
      <a href="<?php echo route('feedback.create'); ?>"><button type="button" class="btn btn-primary"> {{trans('cms::feedback.new')}}</button></a>
    </div>
    <!-- table content -->
    <div class="table-responsive">
			<?php if($feedbacks->count() > 0){?>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="col-md-2"><?=\Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name' , 'name')?></th>
                    <th class="col-md-2">Image</th>
										<th class="col-md-2">Job</th>
                    <th class="col-md-2">Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($feedbacks as $feedback)
                    <td>{{$feedback->feedbacklang($app_locale)->name}}</td>
                    <td>
											<img src="@if($feedback->media){{url('uploads/'.$feedback->media->media_path)}}@endif" class="img img-circle" alt="" width="100" height="100"/>
										</td>
										<td>{{$feedback->feedbacklang($app_locale)->job}}</td>
                    <td>{{$feedback->feedbacklang($app_locale)->description}}</td>
                    <td>
                        <a href="{{URL('admin/feedback/edit/'.$feedback->id)}}" class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a id="del" href="{{URL('admin/feedback/delete/'.$feedback->id)}}" onclick="return confirm('Are you sure you want to delete this  feedback?')" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
        </table>
				<?php }else{
					?>
<h3>Sorry , there are no any feedbacks yet .</h3>
					<?php
				}?>
    </div>
</div>











<!--  append page number to url-->
<div class="col-md-12"
     <div class="btn-group" role="group">
         <?php  $search_query = \Illuminate\Support\Facades\Input::except('page');
                $feedbacks->appends($search_query); ?>
        {!! $feedbacks->links()!!}
    </div>
</div>

@endsection
