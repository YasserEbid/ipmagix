@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::role.index_title'); ?></h1>
<ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>

</div>
<!-- ./page title -->

<!-- data table -->
<div class="wrapper wrapper-white">
	<div class="form-group">
		<a href="<?php echo route('roles.create'); ?>" class="btn btn-primary"><?= trans('cms::role.create'); ?></a>
	</div>
	<div class="row">
		<?php if (session('message')) { ?>
			<div class="alert alert-success">
				<?php echo session('message'); ?>
			</div>
		<?php } ?>
		<?php session()->forget('message');?>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><?php echo trans('cms::role.role_id'); ?></th>
					<th><?php echo trans('cms::role.role_name'); ?></th>
					<th><?php echo trans('cms::role.actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($roles)) { ?>
					<?php foreach ($roles as $role) { ?>
						<tr>
							<td><?php echo $role->role_id ?></td>
							<td><?php echo $role->role_name; ?></td>
							<td>
								<a  class="btn default btn-xs red" href="<?php echo URL::to('/admin/roles/' . $role->role_id . '/edit'); ?>" ><i class="fa fa-edit"></i>  </a>
								<a  class="btn default btn-xs red" href="<?php echo URL::to('/admin/roles/' . $role->role_id . '/delete'); ?>" ><i class="fa fa-trash-o"></i>  </a>
							</td>
						</tr>
						<?php
					}
				} else {
					?>
					<tr>
						<td style="text-align: center;" colspan="3"><?php echo trans('cms::users.no_roles'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<!-- ./data table -->
@stop
