@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::role.create'); ?></h1>
	

	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
                <li><a href="{{URL('admin/roles')}}"><?php echo trans('cms::role.index_title'); ?></a></li>
	</ul>
</div>                        
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">


	<div class="wrapper">
		<div class="row">
			<?php echo Form::open(['method' => 'post', 'class' => "form-horizontal"]); ?> 
			<div class="col-md-12">
				<div class="page-subtitle">
					<h3><?php echo trans('cms::role.role'); ?><span> <?php echo trans('cms::role.name'); ?></span></h3>
					<input type="text" class="form-control" value="" name='role_name'>
	
				</div>

				<ul class="timeline-simple">
					<?php foreach ($groups as $group) { ?>
					<li>
						<div class="timeline-simple-wrap col-md-12 group-div">
							<h3>
								<input class="group-check" type="checkbox" />
								<span><?php echo $group->parent_name; ?></span>
							</h3>
							<?php foreach ($group->child as $child) { ?>
								
									<div class="col-md-4">
										<br/>                               
										<div class="checkbox checkbox-inline">
											<input class="route-check" type="checkbox" id="check_<?php echo $child->id ?>" value="<?php echo $child->id; ?>" name="<?php echo $child->name ?>"  />
											<label for="check_<?php echo $child->id; ?>"><?php echo $child->naming; ?></label>
										</div>
									</div> 
								
							<?php } ?>
						</div>
					</li>    
					<?php } ?>
				</ul>

			</div>    
			<div class="pull-left margin-top-10">
				<button class="btn btn-primary" type="submit"><?php echo trans('cms::role.save'); ?></button>
			</div>
	<!-- ./radio checkbox file buttons -->
		<?php echo Form::close(); ?>
		</div>                            
	</div>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
<script>
$(document).ready(function() {
	$.validate();
	$('.group-check').click(function() {
		var routes = $(this).parents('.group-div').find('.route-check');
		if ($(this).is(':checked')) {
			routes.prop('checked', true);
		} else {
			routes.prop('checked', false);
		}
	});
});
</script>
@stop