@extends('cms::layouts.master')
@section('content')

	<!-- page title -->
	<div class="page-title" id="tour-step-4">
		<h1 style="display: inline"><?= trans('cms::dashboard.title'); ?></h1>
		<button class="btn btn-primary hide" style="float: right">Save Sorting</button>
	</div>
	<!-- ./page title -->
	<div id="sortable">
		<div class="wrapper wrapper-white hide" title="summary" >
			<div class="row">
				<div class="col-md-4">
					<a href="javascript:void()" class="tile tile-primary"><span class="fa fa-laptop"></span></a>
				</div>
				<div class="col-md-4">
					<a href="javascript:void()" class="tile tile-default">
						<span><?php echo $total_posts; ?></span> <p><?= trans('cms::dashboard.total_posts'); ?></p>
					</a>
				</div>
				<div class="col-md-4">
					<a href="javascript:void()" class="tile tile-warning">
						<span><?php echo $total_users; ?></span> <p><?= trans('cms::dashboard.total_users'); ?></p>
					</a>
				</div>
				<div class="col-md-4 hide">
					<a href="javascript:void()" class="tile tile-danger">
						<span>6,432</span> <p><?= trans('cms::dashboard.visitors_today'); ?></p>
						<div class="informer informer-default dir-tr"><?= date('d.m.y'); ?></div>
					</a>
				</div>
			</div>
		</div>
		<div class="wrapper hide" title="recent_posts" >
			<div class="col-md-12">
				<div class="panel widget-support-tickets" id="dashboard-support-tickets">
					<div class="panel-heading">
						<span class="panel-title"><?= trans('cms::dashboard.recent_posts'); ?></span>
						<ul class="panel-btn">
						<li><a href="<?php echo route('posts.index'); ?>" class="btn btn-info"><span class="panel-title"><?= trans('cms::dashboard.show_all'); ?></span></a></li>
						</ul>
					</div> <!-- / .panel-heading -->
					<div class="panel-body tab-content-padding">
						<!-- Panel padding, without vertical padding -->
						<?php if ($recent_posts->count()) { foreach ($recent_posts as $post) { ?>
						<div class="ticket">
							<a href="<?php echo route('posts.edit', ['id' => $post->post_id, 'lang' => $post->lang ]); ?>" title="" class="ticket-title"><?php echo $post -> post_title; ?></a>
							<span class="ticket-info">
							<?= trans('cms::dashboard.by'); ?> <?php echo $post -> full_name; ?> <?= trans('cms::dashboard.at'); ?> <?php echo $post -> created_date; ?></span>
						</div> <!-- / .ticket -->
						<?php } } else { ?>
						<div class="ticket">
							<p><?= trans('cms::posts.no_posts'); ?></p>
						</div>
						<?php } ?>
					</div>
				</div><!-- / .panel-body -->
			</div>
		</div>
		<div class="wrapper hide" title="recent_users" >
			<div class="col-md-12">
				<div class="panel widget-support-tickets" id="dashboard-support-tickets">
					<div class="panel-heading">
						<span class="panel-title"><?= trans('cms::dashboard.recent_users'); ?></span>
						<ul class="panel-btn">
							<li><a href="<?php echo route('user.index'); ?>" class="btn btn-info"><span class="panel-title"><?= trans('cms::dashboard.show_all'); ?></span></a></li>
						</ul>
					</div> <!-- / .panel-heading -->
					<div class="panel-body tab-content-padding">
						<!-- Panel padding, without vertical padding -->
						<?php if ($recent_users->count()) { foreach ($recent_users as $user) { ?>
						<div class="ticket">
							<a href="<?php echo route('user.edit', ['id' => $user->user_id]); ?>" title="" class="ticket-title"><?php echo $user->full_name; ?></a>
						</div> <!-- / .ticket -->
						<?php } } else { ?>
						<div class="ticket">
							<p><?= trans('cms::users.no_users'); ?></p>
						</div>
						<?php } ?>
					</div>
				</div> <!-- / .panel-body -->
			</div>
		</div>
	</div>

	<?php 
		$array = '';
		if($dashboard_sort_items){
			foreach ($dashboard_sort_items as $dashboard_item){
				if($array!='') $array.=', ';
				$array.="'".$dashboard_item->item."'";
			}
		}
	?>
	@section('js')
	<script>
		$(function () {
			var orderArray = [<?=$array;?>];
			var listArray = $('#sortable > div');
			for (var i = 0; i < orderArray.length; i++) {
				$('#sortable').append($('div[title="'+orderArray[i]+'"]'));
			}
			
			$('#sortable > div').removeClass('hide');
        	$( "#sortable" ).sortable({
			    stop: function(event, ui) {
			    	save_sorting();
			    }
			});
        	$( "#sortable" ).disableSelection();
		});

		function save_sorting() {
			var data = new Array();
			$('.wrapper').each(function (key, value) {
			var parentTitle = $(this).attr("title");
				data.push(parentTitle);
			});
			$.ajax({
				url: '{{url("admin/dashboard/sort")}}',
				headers: {'X-CSRF-TOKEN': "<?php echo csrf_token(); ?>"},
				data: { items : data },
				type: 'POST',
				success: function (resp) {
					
				}
			});
		}
	</script>
	@stop
@stop