<div class="cinema"></div>
<div class="file_manager">

    <div class="file_manager_header">

        <ul class="file_manager_tabs nav nav-tabs nav-tabs-sm">
            <img class="media_loader" src="{{URL::asset('assets/dashboard/img/loader.gif')}}" />
            <li class="upload-area-list">
                <a data-toggle="tab" href="#upload-area">
                    <i class="fa fa-cloud-upload"></i>
                    <span class="hidden-xs"><?php echo trans("cms::media.upload_files"); ?></span>
                </a>
            </li>
            <li class="active">
                <a data-toggle="tab" href="#library-area">
                    <i class="fa fa-cloud"></i>
                    <span class="hidden-xs"><?php echo trans("cms::media.files"); ?></span>
                </a>
            </li>
            <!--            <li>
                            <a data-toggle="tab" href="#galleries-area">
                                <i class="fa fa-camera"></i>
                                <span class="hidden-xs"><?php echo trans("cms::media.galleries"); ?></span>
                                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#embed-settings">
                                <i class="fa fa-cog"></i>
                                <span class="hidden-xs"><?php echo trans("cms::media.settings"); ?></span>
                                            </a>
                        </li>-->

            <li class="search-area-list" class="hidden-xs">
                <form class="search_media" >
                    <div class="input-group">
                        <input class="form-control" placeholder="<?php echo trans("cms::media.search_media"); ?>" value="" id="file_query" name="q">
                        <span class="input-group-btn">
                            <button type="submit" class="btn">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </li>
        </ul>
    </div>

    <link rel="stylesheet" href="{{URL::asset('assets/dashboard/uploader/jquery.fileupload.css')}}" />

    <div class="file_manager_content">
        <div class="tab-content" style="padding:0">
            <div id="upload-area" class="tab-pane fade">

                <div class="container">

                    <div class="row">
                        <div class="col-md-6">

                            <div class="dropzone-box dz-clickable span6" id="dropzonejs-example" style="height:430px">
                                <div class="dz-default dz-message" style="padding-top: 0px;">
                                    <i class="fa fa-cloud-upload hidden-xs"></i>
                                    <?php echo trans("cms::media.drop_files_here"); ?><span class="dz-text-small">
                                        <span class="btn btn-primary btn-flat fileinput-button" style="position: relative; top: 7px;">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span><?php echo trans("cms::media.select_files"); ?></span>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input id="fileupload" type="file" name="files[]" multiple>
                                        </span>
                                    </span>

                                </div>

                            </div>
                            <!-- The global progress bar -->
                            <div id="progress" class="progress" style="background: none repeat scroll 0 0 #ccc;margin-top: -13px;opacity: 1;position: relative;z-index: 9999;">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>

                            <?php /* ?>
                              <!-- The container for the uploaded files -->
                              <div class="row">
                              <div class="col-md-11 current-uploading-file"></div>
                              <div class="col-md-1 text-right current-uploading-rate"></div>
                              </div>
                              <?php */ ?>

                        </div>

                        <div class="col-md-6">

                            <div class="panel panel-default widget-profile" style="height:430px">
                                <div class="panel-heading">
                                    <div class="widget-profile-bg-icon"><i class="fa fa-link" style="color: #dadada;"></i></div>
                                    <div class="widget-profile-header">
                                        <span><?php echo trans("cms::media.add_external_link"); ?></span><br>
                                    </div>
                                </div> <!-- / .panel-heading -->

                                <form id="mediaform" method="post">
                                    <div class="list-group" style="margin-top: 93px; padding: 10px;">

                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon">
                                                <i class="fa fa-link"></i>
                                            </span>
                                            <input style="text-align:left; direction: ltr" name="link" value="" class="form-control" placeholder="https://www.youtube.com/watch?v=5Y_driviBlw" />
                                        </div>

                                        <br/>

                                        <div class="input-group">
                                            <input type="submit" data-loading-text="<?php echo trans("cms::media.grabbing"); ?>" value="<?php echo trans("cms::media.save_to_media"); ?>" class="btn btn-primary btn-flat" />
                                        </div>

                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>

                    <?php /*  <div class="upload_errors"></div> */ ?>
                </div>
            </div> <!-- / .tab-pane -->
            <div id="library-area" class="tab-pane fade active in">
                <div class="filter-bar">

                    <a href="#" media-type="all" class="active"><i class="fa fa-home"></i></a>
                    <a href="#" media-type="image"><i class="fa fa-picture-o" title="picture"></i></a>
                    <a href="#" media-type="audio"><i class="fa fa-music" title="music"></i></a>
                    <a href="#" media-type="video"><i class="fa fa-film" title="film"></i></a>
                    <a href="#" media-type="pdf"><i class="fa fa-file-pdf-o" title="pdf"></i></a>
                    <a href="#" media-type="swf"><i class="fa fa-bolt" title="bolt"></i>
                    </a>

                </div>

                <div class="row media-wrapper non-editable" style="margin:0" >



                    <div class="col-md-8 col-sm-9 col-xs-12 media-grid-wrapper">
                        <div class="media-grid"></div>
                        <div class="media-chooseSize" style="display:none;">
                            <div>
                                <a class="resizeImage" id="medium_image" style="float:left;cursor: pointer; width:200px;height:200px; margin:10px;" data-imagewidth="653" data-imageheight="345">
                                    <span style="position: absolute;margin:80px 0 0 65px;background: #000;padding:10px;border-radius: 10px;color:#fff;">653 x 345</span>
                                    <img src="" width="200" height="200" style="opacity: 0.5;">
                                </a>
                                <a class="resizeImage" id="solitem_image" style="float:left;cursor: pointer; width:200px;height:200px; margin:10px;" data-imagewidth="600" data-imageheight="393">
                                    <span style="position: absolute;margin:80px 0 0 65px;background: #000;padding:10px;border-radius: 10px;color:#fff;">600 x 393</span>
                                    <img src="" width="200" height="200" style="opacity: 0.5;">
                                </a>
                                <a class="resizeImage" id="small_image" style="float:left;cursor: pointer; width:200px;height:200px; margin:10px;" data-imagewidth="450" data-imageheight="331">
                                    <span style="position: absolute;margin:80px 0 0 65px;background: #000;padding:10px;border-radius: 10px;color:#fff;">450 x 331</span>
                                    <img src="" width="200" height="200" style="opacity: 0.5;">
                                </a>
                                <a class="resizeImage" id="thumbnail_image" style="float:left;cursor: pointer; width:200px;height:200px; margin:10px;" data-imagewidth="150" data-imageheight="150">
                                    <span style="position: absolute;margin:80px 0 0 65px;background: #000;padding:10px;border-radius: 10px;color:#fff;">150 x 150</span>
                                    <img src="" width="200" height="200" style="opacity: 0.5;">
                                </a>
                                <a class="resizeImage" id="free_image" style="float:left;cursor: pointer; width:200px;height:200px; margin:10px;" data-imagewidth="0" data-imageheight="0">
                                    <span style="position: absolute;margin:80px 0 0 65px;background: #000;padding:10px;border-radius: 10px;color:#fff;">Free Resizing</span>
                                    <img src="" width="200" height="200" style="opacity: 0.5;">
                                </a>
                              </div>
                                    <br />
                                    <button type="button" class="btn btn-primary" id="cancel_sizes">Cancel</button>
                                    <!-- <button type="button" class="btn btn-info resizeImage" data-imagewidth="1000" data-imageheight="1000" style="display:block;margin-top:35px;margin-left:250px;">resize 1000,1000</button> -->
                                    <!--                            <button type="button" class="btn btn-info resizeImage" data-imagewidth="653" data-imageheight="345" style="display:block;margin-top:35px;margin-left:250px;">resize 653, 345</button>
                                                                <button type="button" class="btn btn-info resizeImage" data-imagewidth="600" data-imageheight="393" style="display:block;margin-top:35px;margin-left:250px;">resize 600, 393</button>
                                                                <button type="button" class="btn btn-info resizeImage" data-imagewidth="450" data-imageheight="331" style="display:block;margin-top:35px;margin-left:250px;">resize 450, 331</button>
                                                                <button type="button" class="btn btn-info resizeImage" data-imagewidth="150" data-imageheight="150" style="display:block;margin-top:35px;margin-left:250px;">resize 150, 150</button>
                                                                <button type="button" class="btn btn-info resizeImage" data-imagewidth="0" data-imageheight="0" style="display:block;margin-top:35px;margin-left:250px;">free resize</button>-->
                                    <div>
                                    </div>
                        </div>
                        <div class="media-edit" style="display:none;">
                                    <div class="img-container" style="height:100%;width:100%;">
                                        <img  id="image" style="max-width: 100%; width:100%;" />
                                    </div>
                                    <div class="clearfix"></div>

                                    <style>
                                        .cropper-canvas {opacity: 1;}
                                    </style>
                        </div>
                                <input type="hidden" class="media-grid-page" value="1" />
                                <input type="hidden" class="media-grid-type" value="all" />
                            </div>
                            <div class="col-md-3 col-sm-3 hidden-xs media-form-wrapper" style="background:#f1f1f1">
                                <br/>
                                <!-- Extra small tabs -->

                                <div class="row details-box">
                                    <div class="col-md-5 details-box-image">
                                        <img style="max-height:105px" class="img-rounded hidden-sm" src="" />
                                    </div>
                                    <div class="col-md-7 details-box-name">
                                        <div class="file_name" style="word-wrap: break-word;"></div>
                                        <div class="file_date"></div>
                                        <div class="file_size"></div>
                                        <div class="file_duration"></div>
                                        <br/>
                                    </div>
                                </div>

                                <br />
                                <div class="btn-group btn-group-justified">
                                    <a class="btn btn-primary btn-flat" href="javascript:void(0)" target="_blank" id="download_media"><?php echo trans("cms::media.preview"); ?></a>
                                    <a class="btn btn-info btn-flat" href="javascript:void(0)" id="edit_media"><?php echo trans("cms::media.edit"); ?></a>
                                    <a class="btn btn-danger btn-flat" href="javascript:void(0)" id="delete_media"><?php echo trans("cms::media.delete"); ?></a>
                                </div>

                                <br />
                                <form action="" method="post" class="media-form" >

                                    <input type="hidden" name="file_id" />
                                    <input type="hidden" id="file_provider" />
                                    <input type="hidden" id="file_provider_id" />
                                    <input type="hidden" id="file_type" />

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input readonly="" style="text-align:left; direction: ltr" name="url" name="file_url" id="file_url" value="" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input name="file_title" id="file_title" value="" class="form-control input-lg" value="" placeholder="<?php echo trans("cms::media.title"); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea style="height:60px" name="file_description" id="file_description" class="form-control input-lg" value="" placeholder="<?php echo trans("cms::media.description"); ?>" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom:0">
                                        <input type="submit" data-loading-text="<?php echo trans("cms::media.loading"); ?>" id="save_media" class="pull-right btn btn-flat btn-primary" value="<?php echo trans("cms::media.save"); ?>" />
                                    </div>
                                </form>


                            </div>
                            <div class="col-sm-3 preview-wrapper" style="display:none;">
                                <div class="img-preview preview-sm" style="width:150px;height:100px">
                                </div>
                                <style>
                                    .img-preview > img {
                                        max-width: 80%;
                                    }
                                </style>
                            </div>
                        </div>
                    </div>

                    <div id="galleries-area" class="tab-pane fade gallery_rows row"  style="margin: 0;">

                        <div class="row">
                            <div  class="col-md-3">

                                <form class="search_galleries">
                                    <div class="input-group">
                                        <input name="q" id="gallery_query" value="" placeholder="البحث فى الألبومات" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>

                                <div id="galleries-sidebar" page="1">



                                    <?php
                                    $galleries = [];
                                    ?>

                                    <?php if (count($galleries)) { ?>
                                        <?php foreach ($galleries as $gallery) { ?>
                                            <div class="col-md-12 gallery_row" gallery-type="<?php echo $gallery->media_type; ?>" gallery-id="<?php echo $gallery->gallery_id; ?>">
                                                <div class="gallery_image">

                                                    <?php if ($gallery->media_provider == "") { ?>
                                                        <img class="img-rounded" src="<?php echo thumbnail($gallery->media_path); ?>">
                                                    <?php } else { ?>
                                                        <?php if ($gallery->media_provider_image != "") { ?>
                                                            <img class="img-rounded" src="<?php echo $gallery->media_provider_image; ?>" />
                                                        <?php } else { ?>
                                                            <img class="img-rounded" src="default/soundcloud.png" />
                                                        <?php } ?>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                            <div class="gallery_details">
                                                <div class="gallery_details_name"><?php echo $gallery->gallery_name; ?></div>
                                                <div class="gallery_details_count">(<?php echo $gallery->media_count; ?>) <?php echo trans("cms::media.files"); ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } else { ?>
                                    <div><?php echo trans("cms::media.no_galleries"); ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- / .tab-pane -->

        </div>
        <div class="file_manager_footer row">

            <div class="col-md-12 col-xs-12 col-sm-12 pull-left text-left" style="margin: 0px 50px;">
                <button class="btn btn-danger btn-flat disabled hide" data-loading-text="<?php echo trans("cms::media.deleting_selected"); ?>" type="button" id="delete_selected_media" ><?php echo trans("cms::media.delete_selected"); ?></button>
                <button class="btn btn-primary btn-flat disabled" data-loading-text="<?php echo trans("cms::media.loading"); ?>" type="button" id="select_media"><?php echo trans("cms::media.select_media"); ?></button>
                <div class="col-md-9 docs-buttons" style="display:none;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" data-method="zoom" id="zoom_in" data-option="0.1" title="Zoom In">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, 0.1)">
                                <span class="fa fa-search-plus"></span></span></button>
                        <button type="button" class="btn btn-primary" data-method="zoom" id="zoom_out" data-option="-0.1" title="Zoom Out" >
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, -0.1)">
                                <span class="fa fa-search-minus"></span></span></button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, -10, 0)">
                                <span class="fa fa-arrow-left"></span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 10, 0)">
                                <span class="fa fa-arrow-right"></span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 0, -10)">
                                <span class="fa fa-arrow-up"></span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 0, 10)">
                                <span class="fa fa-arrow-down"></span>
                            </span>
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, -45)">
                                <span class="fa fa-rotate-left"></span></span></button>
                        <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, 45)">
                                <span class="fa fa-rotate-right"></span></span></button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;scaleX&quot;, -1)">
                                <span class="fa fa-arrows-h"></span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
                            <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;scaleY&quot;-1)">
                                <span class="fa fa-arrows-v"></span>
                            </span>
                        </button>
                    </div>
                    <button type="button" class="btn btn-primary" id="save_crop">Save Edit</button>
                    <button type="button" class="btn btn-primary" id="cancel_crop">Cancel</button>
                </div>
            </div>
        </div>

        <span class="file_manager_close">
            <i class="fa fa-times"></i>
        </span>
    </div>
