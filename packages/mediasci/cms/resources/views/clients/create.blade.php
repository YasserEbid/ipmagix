@extends('cms::layouts.master')

@section('content')
<?php
$app_locales = $languages;
$app_locale = Config::get("app.locale");
?>
<div class="page-title">
    <h1>{{trans('cms::clients.new')}}</h1>
    <p>{{trans('cms::clients.newmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="./admin/clients">{{trans('cms::clients.clients')}}</a></li>
    </ul>
</div>

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
        {{trans('cms::clients.confirm')}}
    </div></div>

<div class="wrapper wrapper-white">

<div class="row">
    <div class="col-md-12">
        <form action="" method="POST" id="client_form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="widget-tabbed margin-top-30">
              <ul class="widget-tabs widget-tabs-three">
                <?php foreach($app_locales as $key => $locale) : ?>
                  <?php $active_tab = ($locale['lang'] == $app_locale ? 'active' : ''); ?>
                  <li class="<?php echo $active_tab; ?>"><a href="#client__<?php echo $locale['lang']; ?>"><?php echo trans('cms::questionsAnswers.locale_'.$locale['lang']); ?></a></li>
                <?php endforeach; ?>
              </ul>
              <?php foreach($app_locales as $key => $locale) : ?>
                <div class="widget-tab <?php echo $active_tab; ?>" id="client__<?php echo $locale['lang']; ?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label" for="name_<?php echo $locale["lang"]; ?>"><?php if($locale["lang"] == "en"){echo trans('cms::categories.englishname');}else{echo trans('cms::categories.arabicname');} ?></label>
                    <div class="col-md-10">
                      <input id="name_<?php echo $locale["lang"]; ?>" type='text'  name="name_<?php echo $locale["lang"]; ?>" class="form-control"
                      value='<?php echo old('name_' . $locale["lang"], (isset($category_langs) ? $category_langs[$locale["lang"]]->name : '')); ?>' placeholder="<?php echo trans('cms::categories.name'); ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-2 control-label" for="brief_<?php echo $locale["lang"]; ?>"><?php if($locale["lang"] == "en"){echo trans('cms::clients.english_brief');}else{echo trans('cms::clients.arabic_brief');} ?></label>
                    <div class="col-md-10">
                      <textarea id="brief_<?php echo $locale["lang"]; ?>" name="brief_<?php echo $locale["lang"]; ?>" class="form-control" rows="5"></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-2 control-label" for="description_<?php echo $locale->lang; ?>"><?php if($locale["lang"] == "en"){echo trans('cms::categories.english_description');}else{echo trans('cms::categories.arabic_description');} ?></label>
                    <div class="col-md-10">
                      <textarea id="description_<?php echo $locale->lang; ?>" class="form-control summernote" rows="5" name="description_<?php echo $locale->lang; ?>" placeholder="<?php echo trans('cms::categories.description'); ?>" >
                      </textarea>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
              </div>
                <div class="form-group">
                  <label for="sel1">Select Type:</label>
                  <select class="form-control" name="category_id" id="sel1">
                    @foreach($categories as $category)
                    <option value="{{$category->clientCategory_id}}">{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-md-12 margin-top-20">
                    <div class="col-sm-6 pull-left">
                        <div class="panel" style='height: 120px; margin-bottom: 45px;'>
                            <div class="panel-heading">
                                <span class="panel-title">{{trans('cms::clients.featured_image')}}</span>
                            </div>
                            <div class="panel-body" id="image-container" style="width:100%; height: 100%; ">
                                <div class="row">
                                    <a href="#" class="btn btn-default set-partner-thumbnail">Choose Image</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">


                  </div>
            <div class="pull-left margin-top-20">
                <button type="submit" class="btn btn-primary">{{trans('cms::partners.submit')}}</button>
                <a class="btn btn-link pull-right" href="{{ route('partners.index') }}"></a>
            </div>
        </form>

    </div>
</div>
</div>


<script>

$(function() {
$(".set-partner-thumbnail").click(function(e) {
  e.preventDefault();
});
$(".set-partner-thumbnail").filemanager({
  types: "png|jpg|jpeg|gif|bmp",
  done: function(files) {
  if (files.length) {
    var formContainer = $('#client_form');
    var imageContainer = $('#image-container');
    var file = files[0];
    imageContainer.find($('a.featured-preview')).remove();
    $('#featured_image_id').remove();
    formContainer.append($('<input>', {
      'type': 'hidden',
      'name': 'featured_image',
      'id': 'featured_image_id',
      'value': file.media_id
    }));
    imageContainer.prepend($('<a>', {
      'href': '#',
      'class': 'featured-preview set-partner-thumbnail'
    }).append($('<img>', {
      'src': file.media_thumbnail,
      'width': '150px',
      'height': '100px',
    })));
  }
  },
  error: function(media_path) {
  alert(media_path + " <?php echo trans("cms::users.is_not_an_image") ?>");
}
  });
    });
</script>
@endsection
