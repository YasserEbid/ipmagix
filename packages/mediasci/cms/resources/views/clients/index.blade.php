@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>Clients</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
    </ul>

</div>
<!-- page title -->
<div class="wrapper wrapper-white">
    @if(Session::has('client_created'))
    <?php
    $a = [];
    $a = session()->pull('client_created');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    @endif
    <?php session()->forget('clientCategory_created'); ?>
    <div class="form-group">
        <a href="<?php echo route('clients.create'); ?>"><button type="button" class="btn btn-primary">
                {{trans('cms::clients.new')}}</button></a>
    </div>
    <!-- table content -->
    <?php if ($clients->count() > 0) { ?>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th class="col-md-2"><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name', 'name') ?></th>
                                                                                    <!-- <th class="col-md-2">description</th> -->
                        <th class="col-md-2">Logo Image</th>
                        <th class="col-md-2">Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @foreach($clients as $client)
                        <td>{{$client->name}}</td>
                        <td>
                            <img src="@if($client->media){{url('uploads/'.$client->media->media_path)}}@endif" class="img img-circle" alt="" width="100" height="100"/>
                        </td>
                        <td>{{$client->clientsCategory->name}} Clients</td>
                        <td>
                            <a href="{{URL('admin/clients/update/'.$client->id)}}" class="btn default btn-xs red">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a id="del" href="{{URL('admin/clients/delete/'.$client->id)}}" onclick="return confirm('Are you sure you want to delete this Client?')" class="btn default btn-xs red">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
            </table>
        <?php } else { ?>
            <h3>Sorry , there are no any Categories for clients yet .</h3>
        <?php } ?>
    </div>

</div>




<!--  append page number to url-->
<div class="col-md-12"
     <div class="btn-group" role="group">
        <?php
        $search_query = \Illuminate\Support\Facades\Input::except('page');
        $clients->appends($search_query);
        ?>
        {!! $clients->links()!!}
    </div>
</div>

@endsection
