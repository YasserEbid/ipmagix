@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::sms.edit'); ?></h1>
        <p>{{trans('cms::sms.editmessage')}}</p>
	<ul class="breadcrumb">
		<li><a href="{{ route('dashboard') }}"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><a href="{{ route('sms') }}"><?php echo trans('cms::sms.index_title'); ?></a></li>
		
	</ul>
</div>                        
<!-- ./page title -->

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
    {{trans('cms::careers.confirm')}}
    </div></div>
<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
   
    @if(Session::has('errors'))
    <div class="row">
        <div class="alert alert-error">
            <ul>
                @foreach(Session::get('errors') as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    <div class="row">
        <form method="post">
            @include('cms::partials.filter_update',['object'=>$sms])
            <br/>
            <div class="form-group">
                <label>{{trans('cms::sms.sms_message')}}</label>
                <textarea name="message" class="form-control" required="">{{$sms->message}}</textarea>
            </div>
            <br/>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Save" />
            </div>
        </form>
    </div>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop