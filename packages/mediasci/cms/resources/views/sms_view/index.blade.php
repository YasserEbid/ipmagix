@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1>SMS</h1>

	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

	</ul>
</div>
<!-- ./page title -->
<div class="wrapper wrapper-white">
    @if(Session::has('message'))
    <div class="row">
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    </div>
		<?php session()->forget('message');?>
    @endif

    <div class="row">
        <a href="{{route('sms.create')}}">
            <button class="btn btn-primary" type="button">Send Message</button>
        </a>

        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Message</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id='table-data'>
                @foreach($smss as $row)
                <tr>
                    <td>{{$row->message}}</td>

                    <td>

                        <a href="./admin/sms/update/{{$row->id}}" class="btn default btn-xs red">
                             <i class="fa fa-edit"></i>
                        </a>
                        <a href="./admin/sms/delete/{{$row->id}}" onclick="return confirm('Are you sure you want to delete this SMS?')" class="btn default btn-xs red">
                              <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>

@stop
