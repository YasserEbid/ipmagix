@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::sliders.sliders')}}</h1>
    <p>{{trans('cms::sliders.newmessage')}}</p>

    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>

    </ul>
</div>
<div class="wrapper wrapper-white">

    @if(Session::has('n'))
    <?php $a = [];
    $a = session()->pull('n');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    <?php session()->forget('n'); ?>
    @endif @if(Session::has('m'))
    <?php $a = [];
    $a = session()->pull('m');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
<?php session()->forget('m'); ?>
    @endif

    <div class="page-subtitle">
        <a href="{{URL('admin/sliders/create')}}"><button type="button" class="btn btn-primary">{{trans('cms::sliders.new')}}</button></a>
    </div>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>{{trans('cms::sliders.title')}}</th>
                    <th>{{trans('cms::sliders.description')}}</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sliders as $slider)
                @if($slider->sliderlang('en'))
                <tr>
                    <td>{{$slider->sliderlang('en')->title}}</td>
                    <td>{!!$slider->sliderlang('en')->description!!}</td>
                    <td>
                        <a href="{{URL('admin/sliders/update/'.$slider->id)}}"  class="btn default btn-xs red">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a  onclick="return confirm('Are you sure you want to delete this HomeBanner?')" id="del" href="{{URL('admin/sliders/delete/'.$slider->id)}}"  class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
            {!! $sliders->links()!!}
        </div>
    </div>

    @endsection
