@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?= trans('cms::trends.name'); ?>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?= trans('cms::common.dashboard'); ?></a></li>
		
	</ul>
</div>                        
<!-- ./page title -->

<!-- data table -->
<div class="wrapper wrapper-white">
    <div class="row">
        <form method="get">
            <div class="col-md-6">
                <input type="date" name="date" class="form-control" />
            </div>
            <div class="col-md-3">
                <button class="btn btn-primary" type="submit">Search</button>
            </div>
        </form>
        <hr>
        @if(is_object($trends))
            <?php $twitter=  $trends->twitter ? json_decode($trends->twitter) : [] ?>
            <?php $youtube=  $trends->youtube ? json_decode($trends->youtube) : [] ?>
        
        
        <div class="tabs">                            
            <ul class="nav nav-tabs nav-tabs-arrowed" role="tablist">
                <li class="active"><a href="#twitter" role="tab" data-toggle="tab" aria-expanded="true">Twitter</a></li>
                <li class=""><a href="#youtube" role="tab" data-toggle="tab" aria-expanded="false">Youtube</a></li>
                                <li class=""><a href="#google" role="tab" data-toggle="tab" aria-expanded="false">google</a></li>

            </ul>                         
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="twitter">
                    <div class="list-group">
                        @foreach($twitter as $row)
                            <a href="{{$row->url}}" class="list-group-item" target="_blank">{{$row->name}}</a>
                        @endforeach
                    </div>
                </div>
                <div class="tab-pane" id="youtube">
                    <div class="list-group">
                        @foreach($youtube as $row)
                            <a href="javascrip:void(0)" class="list-group-item">
                                    <iframe style="display: inline-block" width="196" height="110" src="https://www.youtube.com/embed/{{$row->id}}" frameborder="0" allowfullscreen></iframe>
                                    <span style="display: inline-block">{{$row->title}}</span>
                            </a>
                        @endforeach
                    </div>
                </div>     
                <div class="tab-pane" id="google">
                    <div class="list-group">
                     
                           <iframe scrolling="no" style="border:none;" width="250" height="413" src="https://www.google.com.sg/trends/topcharts/widget?cid=b460a8b3-c2a8-4170-9534-7a2baae37853&geo=&date=2015&vm=trendingchart&h=413"></iframe>
                    </div>
                </div> 
            </div>
        </div>
 
        @else
        @endif
    </div>
</div>
@stop