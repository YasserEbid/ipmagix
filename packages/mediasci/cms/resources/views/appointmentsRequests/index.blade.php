@extends('cms::layouts.master')

@section('content')

<div class="page-title">
    <h1>{{trans('cms::appointmentsRequests.appointmentsRequests')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<!-- page title -->
<div class="wrapper wrapper-white">
    @if(Session::has('fail'))
    <?php
    $a = [];
    $a = session()->pull('fail');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    @endif
    @if(Session::has('success'))
    <?php
    $a = [];
    $a = session()->pull('success');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    @endif


    <div class="page-subtitle">
        <h3></h3>
        <a class="btn btn-primary" href="{{URL('admin/exportrequest')}}">Export</a>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Product</th>
                    <th>Subject</th>
                    <th>Comment</th>
                    <th>Date</th>
                    <th>Source</th>
                    <th>Medium</th>
                    <th>Term</th>
                    <th>Content</th>
                    <th>Campaign</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(empty($appointmentsRequests))
                <tr>
                    <td>No appointments Requests Found.</td>
                </tr>
                @else
                @foreach($appointmentsRequests as $appointment)
                <tr>
                    <td>{{$appointment->name}}</td>
                    <td>{{$appointment->email}}</td>
                    <td>{{$appointment->product_title}}</td>
                    <td>{{$appointment->subject}}</td>
                    <td>{{substr($appointment->comment,0,50).".."}} <a href="JavaScript:void(0)" class="btn" style="color:red;" data-toggle="modal" data-target="#{{$appointment->id}}">Read More</a></td>
                    <td>{{$appointment->created_at}}</td>
                    <td>{{$appointment->utm_source}}</td>
                    <td>{{$appointment->utm_medium}}</td>
                    <td>{{$appointment->utm_term}}</td>
                    <td>{{$appointment->utm_content}}</td>
                    <td>{{$appointment->utm_campaign}}</td>
                    <td>
                        <a id="del" href="{{URL('admin/demoRequests/delete/'.$appointment->id)}}" onclick="return confirm('Are you sure you want to delete this request?')" class="btn red">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    @if(empty($appointmentsRequests))
    @else
    @foreach($appointmentsRequests as $appointment)
    <div id="{{$appointment->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <p style="font-size: 15px;">{{$appointment->comment}}</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @endif

    <div class="col-md-12">
        <div class="btn-group" role="group">
            <?php
            $search_query = \Illuminate\Support\Facades\Input::except('page');
            $appointmentsRequests->appends($search_query);
            ?>
            @unless(empty($appointmentsRequests))
            {!! $appointmentsRequests->links()!!}
            @endunless
        </div>
    </div>
</div>
@endsection
