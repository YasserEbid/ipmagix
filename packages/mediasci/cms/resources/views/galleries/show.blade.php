@extends("cms::layouts.master")

@section("content")

<div id="content-wrapper">
    <div class="page-header">
        <div class="row">
            <!-- Page header, center on small screens -->
            <h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-book page-header-icon"></i>&nbsp;&nbsp;<?php echo trans("cms::galleries.galleries") ?> (<?php echo $galleries->getTotal() ?>)</h1>

            <div class="col-xs-12 col-sm-8">
                <div class="row">
                    <hr class="visible-xs no-grid-gutter-h">
                    <!-- "Create project" button, width=auto on desktops -->
                    <div class="pull-right col-xs-12 col-sm-auto">
                        <a href="<?php echo route("galleries.create"); ?>" class="btn btn-primary btn-labeled" style="width: 100%;">
                            <span class="btn-label icon fa fa-plus"></span>
                            <?php echo trans("cms::galleries.add_new") ?>
                        </a>
                    </div>

                    <!-- Margin -->
                    <div class="visible-xs clearfix form-group-margin"></div>

                    <!-- Search field -->
                    <form action="" method="get" class="pull-right col-xs-12 col-sm-6">
                        <div class="input-group no-margin">
                            <span class="input-group-addon" style="border:none;background: #fff;background: rgba(0,0,0,.05);"><i class="fa fa-search"></i></span>
                            <input name="q" value="<?php echo Input::get("q"); ?>" type="text" placeholder="<?php echo trans("cms::galleries.search_galleries"); ?>" class="form-control no-padding-hr" style="border:none;background: #fff;background: rgba(0,0,0,.05);">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> <!-- / .page-header -->


    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>


    <div class="row">
        <div class="col-md-6" style="width: 60%">
            <div class="media_rows">

                <ul class="breadcrumb breadcrumb-no-padding">
                    <li><a href="#"><?php echo trans("cms::galleries.galleries"); ?></a></li>
                    <li><a href="#"><?php  echo @$row->gallery_name; ?> (<?php echo @count($gallery_media); ?>)</a></li>
                </ul>

                <?php if (count($gallery_media)) { ?>
                    <?php foreach ($gallery_media as $media) { ?>
                        <div class="media_row" >
                            <input type="hidden" name="media_id[]" value="<?php echo $media->media_id; ?>" />

                            <?php if ($media->media_provider == "") { ?>
                                <img src="<?php echo thumbnail($media->media_path); ?>">
                            <?php } else { ?>
                                <?php if ($media->media_provider_image != "") { ?>
                                    <img src="<?php echo $media->media_provider_image; ?>" />
                                <?php } else { ?>
                                    <img src="<?php echo assets("default/soundcloud.png"); ?>" />
                                <?php } ?>
                            <?php } ?>
                            <label><?php echo $media->media_title; ?></label>
                        </div>
                    <?php } ?>
                <?php }else{ ?>
                    No media found in <?php echo @$row->gallery_name; ?>
                <?php } ?>

            </div>
        </div>

        <div class="col-md-6" style="width: 40%">
            <form action="" method="post" class="action_form">


                <select  name="post_status" id="post_status" class="form-control per_page_filter">
                    <option value="" selected="selected">-- <?php echo trans("cms::galleries.per_page") ?> --</option>
                    <?php foreach (array(10, 20, 30, 40) as $num) { ?>
                        <option value="<?php echo $num; ?>" <?php if ($num == $per_page) { ?> selected="selected" <?php } ?>><?php echo $num; ?></option>
                    <?php } ?>
                </select>


                <div class="form-group">
                    <select name="action" class="form-control" style="width:auto; display: inline-block;">
                        <option value="-1" selected="selected"><?php echo trans("cms::galleries.bulk_actions") ?></option>
                        <option value="delete"><?php echo trans("cms::galleries.delete") ?></option>
                    </select>
                    <button type="submit" class="btn" style="margin-top:-5px"><?php echo trans("cms::galleries.apply") ?></button>
                </div>

                <div class="table-primary">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
                        <thead>
                            <tr>
                                <th style="width:35px"><input type="checkbox" class="check_all" name="ids[]" /></th>
                                <th><?php echo trans("cms::galleries.name"); ?></th>
                                <th><?php echo trans("cms::galleries.actions") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($galleries as $gallery) { ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="id[]" value="<?php echo $gallery->gallery_id; ?>" />
                                    </td>
                                    <td>
                                        <a href="<?php echo URL::to("/") ?>/admin/galleries/<?php echo $gallery->gallery_id; ?>">
                                            <?php echo $gallery->gallery_name; ?>
                                        </a>
                                    </td>
                                    <td class="center">
                                        <a href="<?php echo URL::to("/") ?>/admin/galleries/<?php echo $gallery->gallery_id; ?>/edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <a class="ask" message="<?php echo trans("cms::galleries.sure_delete") ?>" href="<?php echo URL::to("/") ?>ظadmin/galleries/delete?id[]=<?php echo $gallery->gallery_id; ?>">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>

                    <div class="table-footer clearfix">
                        <div class="DT-label">
                            <div class="dataTables_info" id="jq-datatables-example_info" role="alert" aria-live="polite" aria-relevant="all"><?php echo trans("cms::galleries.page") ?> <?php echo $galleries->getCurrentPage() ?> <?php echo trans("cms::galleries.of") ?> <?php echo $galleries->getLastPage() ?> </div>
                        </div>
                        <div class="DT-pagination">
                            <div class="dataTables_paginate paging_simple_numbers" id="jq-datatables-example_paginate">
                                <?php echo $galleries->appends(Input::all())->links(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>



    <!-- /11. $JQUERY_DATA_TABLES -->

    <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

</div>
</div> <!-- / #content-wrapper -->

<script>
    $(document).ready(function () {

        $("[name=check_all]").change(function () {
            if ($(this).is(":checked")) {
                $("input[type=checkbox]").attr("checked", "checked");
            } else {
                $("input[type=checkbox]").removeAttr("checked");
            }
        });

        $(".per_page_filter").change(function () {
            var base = $(this);
            var per_page = base.val();
            location.href = "<?php echo route("galleries.show") ?>?per_page=" + per_page;
        });

    });



</script>

@stop