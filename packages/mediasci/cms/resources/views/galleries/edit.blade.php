@extends("cms::layouts.master")

@section("content")
<?php

use Mediasci\Cms\Models\GalleryLang; ?>

<div id="content-wrapper">
    <div class="page-header">
        <div class="row">
            <!-- Page header, center on small screens -->
            <h1 class="col-md-6 col-xs-12 text-left" >
                <i class="fa fa-book page-header-icon"></i>&nbsp;&nbsp;<?php echo trans("cms::galleries.add_new_gallery") ?>
                &nbsp;&nbsp;<a href="<?php echo route("galleries.create"); ?>" class="btn btn-primary btn-labeled"><span class="btn-label icon fa fa-plus"></span><?php echo trans("cms::galleries.add_new") ?></a>
            </h1>
            <?php if ($gallery) { ?>
                <div class="col-md-6 col-xs-12 text-center" >
                    <?php langs(); ?>
                </div>
            <?php } ?>
        </div>
    </div> <!-- / .page-header -->
    <?php if (Session::has("message")) { ?>
        <div class="alert alert-success alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Session::get("message"); ?>
        </div>
    <?php } ?>

    <?php if ($errors->count() > 0) { ?>
        <div class="alert alert-danger alert-dark">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo implode(' <br /> ', $errors->all()) ?>
        </div>
    <?php } ?>

    <form action="" method="post" >
        <div class="row">
            <div class="col-md-12">
                <div class="panel colourable">

                    <div class="panel-body">

                        <div class="form-group" style="position: relative;" >
                            <?php
                            langs(function($code, $title) use($gallery) {
                                if ($gallery) {
                                    $ob = GalleryLang::lang($code)->where("gallery_id", $gallery->gallery_id)->first();
                                }
                                ?> 
                                <input name="gallery_name[<?php echo $code; ?>]" value="<?php echo @Input::old("gallery_name." . $code, $ob->gallery_name); ?>" class="form-control input-lg" value="" placeholder="<?php echo trans("cms::galleries.name") ?>" />
                            <?php }); ?>

                            <button type="button" class="add-media btn-primary btn btn-flat" id="add_media">
                                <i class="fa fa-camera"></i>
                                <?php echo trans("cms::galleries.add_media") ?>
                            </button>

                        </div>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            <input name="gallery_author" value="" class="form-control " value="<?php echo @Input::old("gallery_author", $ob->gallery_author); ?>" placeholder="<?php echo trans("cms::galleries.gallery_author") ?>" />    
                        </div>
                        <br/>
                        <div class="panel">
                            <div id="collapse-media" class="panel-collapse in">
                                <div class="panel-body">
                                    <div class="media_rows">
                                        <?php if ($gallery) { ?>
                                            <?php if (count($gallery_media)) { ?>
                                                <?php foreach ($gallery_media as $media) { ?>
                                                    <div class="media_row" >
                                                        <input type="hidden" name="media_id[]" value="<?php echo $media->media_id; ?>" />
                                                        <a href="#" class="media_row_delete">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                        <?php if ($media->media_provider == "") { ?>
                                                            <img src="<?php echo thumbnail($media->media_path); ?>">
                                                        <?php } else { ?>
                                                            <?php if ($media->media_provider_image != "") { ?>
                                                                <img src="<?php echo $media->media_provider_image; ?>" />
                                                            <?php } else { ?>
                                                                <img src="<?php echo assets("default/soundcloud.png"); ?>" />
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <label><?php echo $media->media_title; ?></label>
                                                    </div>
                                                <?php } ?>
                                                <div class="well text-center empty-content hidden"><?php echo trans("cms::galleries.no_media") ?></div>
                                            <?php } else { ?>
                                                <div class="well text-center empty-content"><?php echo trans("cms::galleries.no_media") ?></div>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <div class="well text-center empty-content"><?php echo trans("cms::galleries.no_media") ?></div>
                                        <?php } ?>
                                    </div>
                                </div> <!-- / .panel-body -->
                            </div> <!-- / .collapse -->
                        </div> <!-- / .panel -->

                    </div> <!-- / .panel-body -->
                </div>
                <div class="panel-footer" style="border-top: 1px solid #ececec; position: relative;">
                    <div class="form-group" style="margin-bottom:0">
                        <input type="submit" class="pull-left btn-flat btn btn-primary" value="<?php echo trans("cms::galleries.save_gallery") ?>" />
                    </div>
                </div>
            </div>
            <!-- /6. $EASY_PIE_CHARTS -->
        </div>
    </form>
    <!-- /9. $UNIQUE_VISITORS_STAT_PANEL -->

</div>
</div> <!-- / #content-wrapper -->
<script>
    $(document).ready(function () {
        $("#add_media").filemanager({
            /*types: "jpg|png|jpeg|bmp|gif",*/
            done: function (files) {
                if (files.length) {
                    $(".empty-content").addClass("hidden");
                    files.forEach(function (file) {

                        if (file.media_provider == "") {
                            var thumbnail = file.media_thumbnail;
                        } else {
                            var thumbnail = file.media_provider_image;
                        }

                        var html = '<div class="media_row" >'
                                + '<input type="hidden" name="media_id[]" value="' + file.media_id + '" />'
                                + '<a href="#" class="media_row_delete">'
                                + '<i class="fa fa-times"></i>'
                                + '</a>'
                                + '<img src="' + thumbnail + '">'
                                + '<label>' + file.media_title + '</label>'
                                + '</div>';

                        $(".media_rows").append(html);

                    });
                }

            },
            error: function (media_path) {
                alert(media_path + "<?php echo trans("cms::galleries.is_not_valid_image") ?>");
            }
        });
 

        $("body").on("click", ".media_row_delete", function () {
            var base = $(this);
            if (confirm("<?php echo trans("cms::galleries.sure_delete") ?>")) {
                base.parents(".media_row").slideUp(function () {
                    base.parents(".media_row").remove();

                    if ($(".media_rows .media_row").length == 0) {
                        $(".empty-content").removeClass("hidden");
                    }

                });
            }

        });
    });
</script>

@stop