<!DOCTYPE html>
<html lang="en">
    @include('cms::layouts/partials/head')
    <body>
        @include("cms::media.manager")
        <!-- page wrapper -->
        <div class="dev-page">

            <!-- page header -->
            <div class="dev-page-header">

                <div class="dph-logo">
                    <a href="<?php echo route('dashboard'); ?>">Dashboard</a>
                    <a class="dev-page-sidebar-collapse">
                        <div class="dev-page-sidebar-collapse-icon">
                            <span class="line-one"></span>
                            <span class="line-two"></span>
                            <span class="line-three"></span>
                        </div>
                    </a>
                </div>

                <ul class="dph-buttons pull-right">
                    <li class="dph-button-stuck">
                        <a href="#" class="dev-page-search-toggle">
                            <div class="dev-page-search-toggle-icon">
                                <span class="circle"></span>
                                <span class="line"></span>
                            </div>
                        </a>
                    </li>
                    <li><a href="javascript:void(0)" id="open_media_popup">media</a></li>
                    <?php
                    if ($countries) {
                        foreach ($countries as $country) {
                          // dd(session('country'));
                            ?>
                            <li><a href="{{url('admin/dashboard/changecountry/'.$country->id)}}" <?php if (Session::has('country') && Session::get('country') == $country->id) { echo "style='background-color:#f2b85c'";
                    } ?> id="country_{{$country->id}}" title="{{$country->Lang()->first()->name}}"><img src="{{URL::asset('assets/dashboard/img/'.$country->logo)}}" width="30" height="20"></a></li>
                            <?php
                        }
                    }
                    ?>
                            <li><a href="{{url("admin/logout")}}" title="sign out"><i class="fa fa-sign-out"></i></a></li>
                </ul>



            </div>
            <!-- ./page header -->

            <!-- page container -->
            <div class="dev-page-container">

                <!-- page sidebar -->
                @include('cms::layouts/partials/sidebar')
                <!-- ./page sidebar -->

                <!-- page content -->
                <div class="dev-page-content">
                    <!-- page content container -->
                    <div class="container">
                        @yield('content')
                    </div>
                    <!-- ./page content container -->

                </div>
                <!-- ./page content -->
            </div>
            <!-- ./page container -->



            <!-- page footer -->
            <div class="dev-page-footer"> <!-- dev-page-footer-closed dev-page-footer-fixed -->

            </div>
            <!-- ./page footer -->

            <!-- page search -->
            <div class="dev-search">
                <div class="dev-search-container">
                    <div class="dev-search-form">
                        <form action="{{url('admin/posts')}}" method="">
                            <div class="dev-search-field">
                                <input type="text" name="q" placeholder="Search..." value="">
                            </div>
                        </form>
                    </div>
                    <div class="dev-search-results"></div>
                </div>
            </div>
            <!-- page search -->
        </div>
        <!-- ./page wrapper -->
        @yield('modals')
        <!-- javascript -->

        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/modernizr/modernizr.js')}}"></script>
        <script src="./assets/dashboard/js/plugins/sweet-alert/dist/sweetalert.min.js"></script>
        <link href="./assets/dashboard/js/plugins/sweet-alert/dist/sweetalert.css" />

        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>

        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/bootstrap-select/bootstrap-select.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/tags-input/jquery.tagsinput.min.js')}}"></script>

        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/bootstrap/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/plugins/summernote/summernote.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/dev-layout-default.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/dev-app.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/uploader/jquery.ui.widget.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/uploader/jquery.fileupload.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/uploader/popup.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/cropper.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/dashboard/js/mailMassages.js')}}"></script>

<!--		<script src="{{URL::asset('assets/dashboard/js/demo.js')}}"></script>-->
                <!--<script src="{{URL::asset('assets/dashboard/js/dev-settings.js')}}"></script>-->
        <script src="{{URL::asset('assets/dashboard/js/plugins/ckeditor/ckeditor.js')}}"></script>

        <!-- javascript -->
        <script>
$(document).ready(function () {
    $(window).resize();
    $('[data-toggle="tooltip"]').tooltip();
});
        </script>

        @yield('js')

    </body>
</html>
