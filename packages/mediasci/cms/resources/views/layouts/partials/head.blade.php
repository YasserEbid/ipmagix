<head>
        <!-- meta section -->
        <title>Mediasci CMS</title>
        <base href='<?= URL::to('/public/') ?>' />
        <script>var base = '<?= URL::to('/public/') ?>';</script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" >

        <link rel="icon" href="{{URL::asset('assets/dashboard/img/favicon.ico')}}" type="image/x-icon" >
        <!-- ./meta section -->

        <!-- css styles -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/dashboard/css/default-blue-white.css')}}" id="dev-css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="{{URL::asset('assets/dashboard/css/cropper.css')}}">
		<!-- ./css styles -->

        <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/dashboard/css/dev-other/dev-ie-fix.css')}}">
        <![endif]-->

        <link href="{{URL::asset('assets/dashboard/css/widgets.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::asset('assets/dashboard/uploader/popup.ltr.css')}}" rel="stylesheet" type="text/css">

        <style>
            .dev-page{visibility: hidden;}
            .nav-tabs > li a {
    text-transform: capitalize;
}
        </style>

        <!-- javascripts -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- ./javascripts -->

        <script>
            baseURL = "<?php echo url('/') . '/admin/' ?>";
        </script>
    </head>
