<div class="dev-page-sidebar">
	<div class="profile profile-transparent">
		<div class="profile-image">
			<img src="{{URL::asset('assets/dashboard/img/user.png')}}">
		</div>
		<div class="profile-info">
			<?php if ($current_user): ?>
				<h4><?php echo $current_user->full_name; ?></h4>
				<span><?php echo $current_user->role_name; ?></span>
			<?php endif; ?>
		</div>
	</div>

	<ul class="dev-page-navigation">

		<li class="has-child">
			<a href="javascript:void(0);"><i class="fa fa-book"></i> <span><?php echo trans('cms::sidebar.content_managment'); ?></span></a>
			<ul style="max-height: 0px;">

		{{-- content management --}}

		<li>
			<a href="<?php echo route('sliders.index') ?>"><i class="fa fa-desktop"></i> <span><?= trans('cms::sidebar.sliders'); ?></span></a>
		</li>
<?php if (count($sites) > 1) { ?>
		<li class="has-child <?php echo (\Request::route()->getName() == 'categories.index') ? 'active' : '' ?>">
			<a href="#"><i class="fa fa-briefcase"></i> <span><?php echo trans('cms::sidebar.categories'); ?></span></a>
			<ul style="max-height: 0px;">
				<?php if ($sites) { ?>
					<?php foreach ($sites as $site) { ?>
						<li><a href="<?php echo route('categories.index', ['site_id' => $site->site_id]); ?>"><i class="fa fa-book"></i> <span><?php echo $site->site_name; ?></span></a></li>
					<?php } ?>
				<?php } ?>
			</ul>
		</li>
<?php }else{?>
					<?php foreach ($sites as $site) { ?>
						<li><a href="<?php echo route('categories.index', ['site_id' => $site->site_id]); ?>"><i class="fa fa-book"></i> <span><?php echo trans('cms::sidebar.categories'); ?></span></a></li>
					<?php } ?>
<?php }?>
<?php if (count($sites) > 1) { ?>
		<li class="has-child">
			<a href="#"><i class="fa fa-stack-exchange"></i> <span><?php echo trans('cms::sidebar.posts'); ?></span></a>
			<ul style="max-height: 0px;">
				<?php if ($sites) { ?>
					<?php foreach ($sites as $site) { ?>
						<li><a href="<?php echo route('posts.index', ['site_id' => $site->site_id]); ?>"><i class="fa fa-book"></i> <span><?php echo $site->site_name; ?></span></a></li>
					<?php } ?>
				<?php } ?>
			</ul>
		</li>
		<?php }else{?>
			<?php foreach ($sites as $site) { ?>
				<li><a href="<?php echo route('posts.index', ['site_id' => $site->site_id]); ?>"><i class="fa fa-book"></i> <span><?php echo trans('cms::sidebar.posts'); ?></span></a></li>
			<?php } ?>
		<?php }?>


				<li>
					<a href="<?php echo route('news.index',['site_id' => $site->site_id]) ?>"><i class="fa fa-user"></i> <span><?= trans('cms::sidebar.news'); ?></span></a>
				</li>

		<li class="has-child">
			<a href="javascript:void(0);"><i class="glyphicon glyphicon-bullhorn"></i> <span><?php echo trans('cms::sidebar.careersmanagement'); ?></span></a>
			<ul style="max-height: 0px;">
				<li><a href="<?php echo route('careers.index'); ?>"><i class="fa fa-book"></i> <span><?php echo trans('cms::sidebar.career'); ?></span></a></li>
				<li><a href="<?php echo route('careercategory.index'); ?>"><i class="fa fa-book"></i> <span><?php echo trans('cms::sidebar.careercategory'); ?></span></a></li>
			</ul>
		</li>

		<li><a href="<?php echo route('questionsAnswers.index') ?>"><i class="fa fa-info"></i> <span><?= trans('cms::sidebar.questionsAnswers'); ?></span></a></li>
		<?php /*?><li class="has-child">
			<a href="javascript:void(0);"><i class="fa fa-envelope"></i> <span><?php echo trans('cms::sidebar.questionsAnswers'); ?></span></a>
			<ul style="max-height: 0px;">
				<li><a href="<?php echo route('questionsAnswers.index') ?>"><i class="fa fa-info"></i> <span><?= trans('cms::sidebar.questionsAnswers'); ?></span></a></li>
				<li><a href="<?php echo route('faqCategories.index') ?>"><i class="fa fa-info"></i> <span><?= trans('cms::sidebar.faqCategories'); ?></span></a></li>
			</ul>
		</li><?php */?>
		<li>
			<a href="<?php echo route('locations'); ?>"><i class="fa fa-map-marker"></i> <span><?= trans('cms::sidebar.locations'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('polls.index'); ?>"><i class="fa fa-bullhorn"></i> <span><?= trans('cms::sidebar.polls'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('workflow'); ?>"><i class="fa fa-cube"></i> <span><?= trans('cms::sidebar.workflow'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('menus.index') ?>"><i class="fa fa-cogs"></i> <span><?= trans('cms::sidebar.menu'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('partners.index') ?>"><i class="fa fa-user"></i> <span><?= trans('cms::sidebar.partners'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('feedback.index') ?>"><i class="fa fa-comments"></i> <span><?= trans('cms::sidebar.feedback'); ?></span></a>
		</li>

		<li class="has-child">
			<a href="javascript:void(0);"><i class="fa fa-users"></i> <span>Clients Management</span></a>
			<ul style="max-height: 0px;">
				<li>
					<a href="<?php echo route('clients.index') ?>"><i class="fa fa-users"></i> <span><?= trans('cms::sidebar.clients'); ?></span></a>
				</li>
				<li>
					<a href="<?php echo route('clientsCategory.index') ?>"><i class="fa fa-user"></i> <span><?= trans('cms::sidebar.clients_category'); ?></span></a>
				</li>
			</ul>
		</li>



		{{-- content management - end --}}

			</ul>
		</li>

		<li class="has-child">
			<a href="javascript:void(0);"><i class="fa fa-money"></i> <span><?php echo trans('cms::sidebar.campaigns'); ?></span></a>
			<ul style="max-height: 0px;">

		{{-- campains management --}}

		<li class="has-child">
			<a href="javascript:void(0);"><i class="fa fa-envelope"></i> <span><?php echo trans('cms::sidebar.email_marketing'); ?></span></a>
			<ul style="max-height: 0px;">
				<li><a href="<?php echo route('mail.messages'); ?>"><i class="fa fa-comments"></i> <span><?php echo trans('cms::sidebar.messages'); ?></span></a></li>
				<li><a href="<?php echo route('mail.themes'); ?>"><i class="fa fa-coffee"></i> <span><?php echo trans('cms::sidebar.themes'); ?></span></a></li>
			</ul>
		</li>

		<li>
			<a href="<?php echo route('ads'); ?>"><i class="fa fa-bookmark"></i> <span><?= trans('cms::sidebar.ads'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('sms'); ?>"><i class="fa fa-envelope-o"></i> <span><?php echo trans('cms::sidebar.sms'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('trends'); ?>"><i class="fa fa-microphone"></i> <span><?= trans('cms::sidebar.trends'); ?></span></a>
		</li>

		{{-- campains management - end --}}

			</ul>
		</li>

		<ul class="dev-page-navigation">

		<li class="has-child">
			<a href="javascript:void(0);"><i class="fa fa-users"></i> <span><?php echo trans('cms::sidebar.users'); ?></span></a>
			<ul style="max-height: 0px;">

		{{-- users management --}}

		<li>
			<a href="<?php echo route('user.index'); ?>"><i class="fa fa-users"></i> <span><?= trans('cms::sidebar.user'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('roles.index'); ?>"><i class="fa fa-sitemap"></i> <span><?= trans('cms::sidebar.privilege'); ?></span></a>
		</li>

		{{-- users management - end --}}

			</ul>
		</li>

		<li class="has-child">
			<a href="javascript:void(0);"><i class="fa fa-user"></i> <span><?php echo trans('cms::sidebar.user_requests'); ?></span></a>
			<ul style="max-height: 0px;">

		{{-- users-requests management --}}


		<li>
			<a href="<?php echo route('appointmentsRequests.index') ?>"><i class="fa fa-mail-reply-all"></i> <span><?= trans('cms::sidebar.appointmentsRequests'); ?></span></a>
		</li>

		<li>
			<a href="<?php echo route('volunteer.index'); ?>"><i class="fa fa-leaf"></i> <span><?= trans('cms::sidebar.volunteer'); ?></span></a>
		</li>
		<li>
			<a href="<?php echo route('jobcv.index') ?>"><i class="fa fa-book"></i> <span><?= trans('cms::sidebar.jobcv'); ?></span></a>
		</li>
		<li>
			<a href="<?php echo route('contact.allRequests') ?>"><i class="fa fa-book"></i> <span><?= trans('cms::sidebar.contact'); ?></span></a>
		</li>
    <li>
			<a href="<?php echo route('subscribers.index') ?>"><i class="fa fa-users"></i> <span><?= trans('cms::sidebar.subscribe'); ?></span></a>
		</li>
		{{-- users-requests management - end --}}

			</ul>
		</li>
<li class="has-child">
    <a href="javascript:void(0);"><i class="fa fa-eye"></i> <span><?php echo trans('cms::sidebar.moniter'); ?></span></a>
    <ul style="max-height: 0px;">
		<li>
			<a href="<?php echo route('setting.index') ?>"><i class="fa fa-cogs"></i> <span><?= trans('cms::sidebar.site_settings'); ?></span></a>
		</li>
    <li>
			<a href="<?php echo route('dashboard.chartbeat') ?>"><i class="fa fa-bar-chart-o"></i> <span><?= trans('cms::sidebar.chartbeat'); ?></span></a>
		</li>
                <li>
			<a href="<?php echo route('mailsystem.index') ?>"><i class="fa fa-cogs"></i> <span>System Mail</span></a>
		</li>
    </ul>
    </li>
	</ul>

</div>
