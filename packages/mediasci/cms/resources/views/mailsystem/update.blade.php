@extends('cms::layouts.master')
@section('content')
<div class="page-title">
    <h1>Edit {{$mail->name}}</h1>


    <ul class="breadcrumb">
        <li><a href="./">{{trans('cms::common.dashboard')}}</a></li>
        <li><a href="{{URL('admin/mailsystem')}}">System Mail</a></li>
    
    </ul>
</div>

	
        <div class="wrapper wrapper-white">
            <div class="wrapper">
            <div class="row">
                <form method="post" id="create_form" role="form" action="{{URL('admin/mailsystem/update/'.$mail->id)}}">
                    <div class='col-md-12'>
              <div class='col-md-12'>
            <div class="form-group">
                                        <label>Subject: <span></span></label>                                      
                                        <input type="text" class="form-control" name="title" required="" minlength="3" value="{{$mail->title}}"/>                                    
                                    </div> 
                                                                   
                    </div>
                            <div class='col-md-12'>
            <div class="form-group">
                                        <label>Message: <span></span></label>                                      
                       <textarea class="form-control summernote" name="message" required cols="50" rows="6">{{$mail->message}}</textarea>                                
                                    </div> 
                                                                   
                    </div>
                                                
                    </div>
                      
                    <div class="pull-left margin-top-10">
                        <!--<button class="btn btn-warning hide-prompts" type="button">Hide prompts</button>-->
                        <button class="btn btn-primary" type="submit" id='btn-submit'>{{trans('cms::setting.submit')}}</button>
                    </div>
                    
                </form></div>
            </div>
        </div>
       
        @endsection
