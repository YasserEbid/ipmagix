@extends('cms::layouts.master')

@section('content')
<div class="page-title">
    <h1>Show Subscriber</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<div class="wrapper wrapper-white">

     <div class="page-subtitle">

                                 </div> <div class="form-group">
         <a href="{{URL('admin/subscribers')}}"><button type="button" class="btn btn-primary">{{trans('cms::subscribers.back')}}</button></a>
     </div>
    <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>

                                            <th>{{trans('cms::subscribers.fields')}}</th>

                                            <th>{{trans('cms::subscribers.desc')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>

                                            <td>
                                                {{trans('cms::subscribers.name')}}
                                            </td>

                                            <td>   {{$subscribe->name}}
                                                   </td>
                                        </tr>
                                        <tr>

                                            <td>
                                                {{trans('cms::subscribers.email')}}
                                            </td>

                                            <td>   {{$subscribe->email}}
                                                   </td>
                                        </tr>
                                            <?php $proberty=new \Mediasci\Cms\Models\UsersProperties; ?>
                                         @if($subscribe->gender_id)
                                        <tr>

                                            <td>
                                                {{trans('cms::subscribers.gender')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->gender_id)->name}}
                                                   </td>
                                        </tr>
                                 @endif
                                 @if($subscribe->nationality_id)
                                          <tr>

                                            <td>
                                                {{trans('cms::subscribers.nationality')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->nationality_id)->name}}
                                                   </td>
                                        </tr>
                                        @endif
                                 @if($subscribe->location_id)
                                          <tr>

                                            <td>
                                                {{trans('cms::subscribers.location')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->location_id)->name}}
                                                   </td>
                                        </tr>
                                        @endif
                                 @if($subscribe->type_visitor_id)
                                          <tr>

                                            <td>
                                                {{trans('cms::subscribers.visitor')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->type_visitor_id)->name}}
                                                   </td>
                                        </tr>
                                        @endif
                                 @if($subscribe->p_interests_id)
                                          <tr>

                                            <td>
                                                {{trans('cms::subscribers.pintrest')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->p_interests_id)->name}}
                                                   </td>
                                        </tr>
                                        @endif
                                 @if($subscribe->s_inerests_id)
                                          <tr>

                                            <td>
                                                {{trans('cms::subscribers.sinterest')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->s_inerests_id)->name}}
                                                   </td>
                                        </tr>
                                        @endif
                                 @if($subscribe->case_id)
                                 <tr>

                                            <td>
                                                {{trans('cms::subscribers.case')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->case_id)->name}}
                                                   </td>
                                        </tr>  <tr>
                                       @endif
                                 @if($subscribe->diabetes_id)
                                            <td>
                                                {{trans('cms::subscribers.diabetes')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->diabetes_id)->name}}
                                                   </td>
                                        </tr>
                                        @endif
                                 @if($subscribe->lifestyle_id)
                                        <tr>

                                            <td>
                                                {{trans('cms::subscribers.lifestyle')}}
                                            </td>


                                            <td> {{$proberty::find($subscribe->lifestyle_id)->name}}
                                                   </td>
                                        </tr>
                                        @endif
                                        <tr>

                                            <td>
                                                {{trans('cms::subscribers.phone')}}
                                            </td>


                                            <td> {{$subscribe->phone}}
                                                   </td>
                                        </tr>
                                          <tr>

                                            <td>
                                                {{trans('cms::subscribers.designation')}}
                                            </td>


                                            <td> {{$subscribe->designation}}
                                                   </td>
                                        </tr>
                                         <tr>

                                            <td>
                                                {{trans('cms::subscribers.company')}}
                                            </td>


                                            <td> {{$subscribe->company}}
                                                   </td>
                                        </tr>
                                         <tr>

                                            <td>
                                                {{trans('cms::subscribers.country')}}
                                            </td>


                                            <td> {{$subscribe->country}}
                                                   </td>
                                        </tr>
                                </table>
                            </div>
</div>
@endsection
