@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
    <h1>{{trans('cms::subscribers.subscriber')}}</h1>

    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>

    </ul>
</div>
<div class="wrapper wrapper-white">

    @if(Session::has('n'))
    <?php
    $a = [];
    $a = session()->pull('n');
    ?>
    <div class="alert alert-danger" role="alert">{{$a[0]}} </div>
    <?php session()->forget('n');?>
    @endif @if(Session::has('m'))
    <?php
    $a = [];
    $a = session()->pull('m');
    ?>
    <div class="alert alert-success" role="alert">{{$a[0]}} </div>
    <?php session()->forget('m');?>
    @endif
    

    <div class="page-subtitle">

        <a class="btn btn-primary" href="{{URL('admin/exportsubecribe')}}">Export</a>
    </div>

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>

                    <th><?= \Mediasci\Cms\Http\Controllers\component\Helpers::sort('Name', 'name') ?></th>

                    <th>{{trans('cms::subscribers.email')}}</th>


                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($subscribers as $subscribe)
                    <td>{{$subscribe->name}}</td>
                    <td>{{$subscribe->email}}</td>
                    <td>
                        <a href="{{URL('admin/subscribers/'.$subscribe->id)}}" class="btn green">
                            <i class="fa fa-eye"></i> {{trans('cms::subscribers.show')}}
                        </a>

                        <a id="del" href="{{URL('admin/subscribers/delete/'.$subscribe->id)}}" onclick="return confirm('are you sure you want to delete this Subscribers?')" class="btn default btn-xs red">
                            <i class="fa fa-trash"></i>
                        </a></td>
                </tr>
                @endforeach
        </table>
    </div>
    <div class="col-md-12"
         <div class="btn-group" role="group">
                   <?php  $search_query = \Illuminate\Support\Facades\Input::except('page');
                    $subscribers->appends($search_query); ?>
            {!! $subscribers->links()!!}
        </div>
    </div>

    @endsection
