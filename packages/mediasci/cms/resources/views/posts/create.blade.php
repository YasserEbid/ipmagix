@extends('cms::layouts.master')

@section('content')
<?php
  $url_type = 'posts';
  if (Request::is('admin/news/*')) {
    $url_type = 'news';
  }
 ?>
<!-- page title -->
<div class="page-title">

    <p><?php echo $current_post ? trans('cms::'.$url_type.'.editmessage') : trans('cms::'.$url_type.'.newmessage'); ?></p>
    <ul class="breadcrumb">
        <li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
        <li><a href="{{URL('admin/'.$url_type)}}"><?php echo trans('cms::'.$url_type.'.index_title'); ?></a></li>
    </ul>
</div>
<!-- ./page title -->

<div class="col-md-12" style=""><div class="alert alert-success alert-dismissible hide" role="alert">
        {{trans('cms::careers.confirm')}}
    </div></div>

<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
    <!--    @if($current_post)
            <div class="col-md-2  col-md-offset-11"> <a href="{{URL('admin/posts/tabs/'.$current_post->post_id)}}"> Post Tabs</a> </div>

        @endif-->
    <h1><?php echo $current_post ? trans('cms::'.$url_type.'.edit') : trans('cms::'.$url_type.'.create'); ?></h1>
    <?php if (isset($errors)) { ?>
        <?php if (count($errors) > 0) { ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $error) { ?>
                        <li><?php echo $error; ?></li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (session('message')) { ?>
        <div class="alert alert-success">
            <?php echo session('message'); ?>
        </div>
    <?php } ?>
    <?php $form_route = ($current_post ? ['posts.edit', $current_post->post_id] : ['posts.create']); ?>
    <?php echo Form::open(['route' => $form_route, 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'post_form']); ?>
    <input type="hidden" value="<?php echo $request->input('site_id'); ?>" name="site_id" />
    <?php if ($current_post) : ?>
        <input type="hidden" value="<?php echo $post_image ? $post_image->media_id : ''; ?>" name="featured_image_id" id="featured_image_id" />
        <input type="hidden" value="<?php echo $intro_post_image ? $intro_post_image->media_id : ''; ?>" name="intro_image_id" id="intro_image_id" />
        @if($url_type == 'posts')
          <input type="hidden" value="<?php echo $internal_post_image ? $internal_post_image->media_id : ''; ?>" name="internal_image_id" id="internal_image_id" />
          <input type="hidden" value="<?php echo $logo_image ? $logo_image->media_id : ''; ?>" name="logo_id" id="logo_id" />
          <input type="hidden" value="<?php echo $video_cover ? $video_cover->media_id : ''; ?>" name="video_cover_id" id="video_cover_id" />
          <input type="hidden" value="<?php echo $post_video ? $post_video->media_id : ''; ?>" name="featured_video_id" id="featured_video_id" />
        @elseif($url_type == 'news')
          <input type="hidden" value="<?php echo $home_post_image ? $home_post_image->media_id : ''; ?>" name="home_image_id" id="home_image_id" />
        @endif
    <?php else: ?>
        <input type="hidden" value="" name="featured_image_id" id="featured_image_id" />
        <input type="hidden" value="" name="intro_image_id" id="intro_image_id" />
        @if($url_type == 'posts')
          <input type="hidden" value="" name="internal_image_id" id="internal_image_id" />
          <input type="hidden" value="" name="logo_id" id="logo_id" />
          <input type="hidden" value="" name="video_cover_id" id="video_cover_id" />
          <input type="hidden" value="" name="featured_video_id" id="featured_video_id" />
        @elseif($url_type == 'news')
          <input type="hidden" value="" name="home_image_id" id="home_image_id" />
        @endif
    <?php endif; ?>

    <?php
    $app_locales = $languages;
    $app_locale = Config::get("app.locale");
    ?>

    <div class="wrapper-white col-sm-9 pull-left">
        <div class="form-group">
            <label class="col-md-1 control-label"><?php echo trans('cms::'.$url_type.'.slug'); ?></label>
            <div class="col-md-8">
                <input type='text'  name="post_slug" class="form-control"  value='<?php echo old('post_slug', ($current_post ? $current_post->slug : '')); ?>' placeholder="<?php echo trans('cms::posts.slug'); ?>">
            </div>
            <!--                    <div class="col-md-2">
            <?php // $checked = ($current_post ? $current_post->is_link : false) ? 'checked="checked"' : ''; ?>
                                    <label class=""><?php // echo trans('cms::posts.is_link');            ?></label>
                                    <input <?php // echo $checked;  ?> type="checkbox" name="is_link" style="margin-top: 10px;">
                                </div>-->
        </div>

        <div class="widget-tabbed margin-top-30">
            <ul class="widget-tabs widget-tabs-three">
                <?php foreach ($app_locales as $key => $locale) : ?>

                    <?php $active_tab = ($locale->lang == $app_locale ? 'active' : ''); ?>
                    <li class="<?php echo $active_tab; ?>"><a href="#post_<?php echo $locale->lang; ?>"><?php echo trans('cms::posts.locale_' . $locale->lang); ?></a></li>

                <?php endforeach; ?>
            </ul>

            <?php foreach ($app_locales as $key => $locale) : ?>
                <input type="hidden" value="<?php echo $current_post ? $post_langs[$locale->lang]->id : ''; ?>" name="post_lang_id_<?php echo $locale->lang; ?>" />
                <div class="widget-tab <?php echo $active_tab; ?>" id="post_<?php echo $locale->lang; ?>">
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.title'); ?></label>
                        <div class="col-md-10">
                            <input type='text'  name="post_title_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('post_title_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->post_title : '')); ?>' placeholder="<?php echo trans('cms::'.$url_type.'.title'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.subtitle'); ?></label>
                        <div class="col-md-10">
                            <input type='text'  name="post_subtitle_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('post_subtitle_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->post_subtitle : '')); ?>' placeholder="<?php echo trans('cms::'.$url_type.'.subtitle'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.brief'); ?></label>
                        <div class="col-md-10">
                            <input type='text'  name="post_brief_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('post_brief_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->post_excerpt : '')); ?>' placeholder="<?php echo trans('cms::'.$url_type.'.brief'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.post_content'); ?></label>
                        <div class="col-md-10">
                            <textarea class="form-control summernote" rows="5" name="post_content_<?php echo $locale->lang; ?>" placeholder="<?php echo trans('cms::'.$url_type.'.post_content'); ?>" ><?php echo old('post_content_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->post_content : '')); ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h1>Seo</h1>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.tags'); ?></label>
                        <div class="col-md-10">
                            <input type='text'  name="post_tags_<?php echo $locale->lang; ?>" class="form-control tags"  value='<?php echo old('post_tags_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->tags : '')); ?>' placeholder="<?php echo trans('cms::'.$url_type.'.tags'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.meta_title'); ?></label>
                        <div class="col-md-10">
                            <input type='text'  name="meta_title_<?php echo $locale->lang; ?>" class="form-control"  value='<?php echo old('meta_title_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->meta_title : '')); ?>' placeholder="<?php echo trans('cms::'.$url_type.'.meta_title'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.meta_keywords'); ?></label>
                        <div class="col-md-10">
                            <input type='text'  name="meta_keywords_<?php echo $locale->lang; ?>" class="form-control tags"  value='<?php echo old('meta_keywords_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->meta_keywords : '')); ?>' placeholder="<?php echo trans('cms::'.$url_type.'.meta_keywords'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"><?php echo trans('cms::'.$url_type.'.meta_description'); ?></label>
                        <div class="col-md-10">
                            <textarea name="meta_description_<?php echo $locale->lang; ?>" class="form-control" placeholder="<?php echo trans('cms::'.$url_type.'.meta_description'); ?>"><?php echo old('meta_description_' . $locale->lang, ($post_langs ? $post_langs[$locale->lang]->meta_description : '')); ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-12" >
                        <!--<input class="btn btn-primary" type='button' id='hideshow' value='Terms'>-->
                        <div class="form-group" style="display:none;" id="seocontent">
                            <p><?php echo old('post_slug', ($current_post ? $current_post->seo : '')); ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-sm-8 pull-left" style="margin-top: 20px;">
            <button type="submit" class="btn btn-primary"><?php echo trans('cms::'.$url_type.'.save'); ?></button>
        </div>
    </div>
    <div class="col-sm-3 pull-right">
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">
                        <?php echo trans('cms::'.$url_type.'.status'); ?>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <select class="form-control selectpicker" name="post_status" style="height:100px">
                            <option><?php echo trans('cms::'.$url_type.'.nothing'); ?></option>
                            <?php if (($post_statuses)) { ?>
                                <?php foreach ($post_statuses as $key => $status) { ?>
                                    <?php
                                    $selected = '';
                                    if ($current_post && $current_post->status == $key) {
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $status; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        @if($url_type == 'news')
          <input name="categories[]" value="@foreach($all_categories as $cat)@if($cat->name=='News'){{$cat->cat_id}}@endif @endforeach" type="hidden">
        @else
          <div class="col-sm-12">
              <div class="panel">
                  <div class="panel-heading">
                      <span class="panel-title">
                          <?php echo trans('cms::'.$url_type.'.categories'); ?>
                      </span>
                  </div>
                  <div class="panel-body">
                      <div class="row">
                          <select class="form-control selectpicker" name="categories[]" style="height:100px" multiple>
                              <?php if (($all_categories)) { ?>
                                  <?php foreach ($all_categories as $cat) { ?>
                                      <?php
                                      $selected = '';
                                      if ($post_categories && in_array($cat->cat_id, $post_categories)) {
                                          $selected = 'selected';
                                      }
                                      ?>
                                      <option <?php echo $selected; ?> value="<?php echo $cat->cat_id; ?>"><?php echo $cat->name; ?></option>
                                  <?php } ?>
                              <?php } ?>
                          </select>
                      </div>
                  </div>
              </div>
          </div>
        @endif
        <br/>
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::'.$url_type.'.featured_image')}}</span>
                </div>
                <div class="panel-body" id="image-container">
                    <?php if ($post_image) : ?>
                        <img class="featured-preview col-sm-12" src="<?php echo thumbnail($post_image->media_path) ?>">
                    <?php endif; ?>
                    <div class="wrapper row">
                        <a href="#" class="btn btn-default set-post-thumbnail">{{Lang::get('cms::'.$url_type.'.set_image')}}</a>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">{{trans('cms::'.$url_type.'.intro_image')}}</span>
                </div>
                <div class="panel-body" id="intro-image-container">
                    <?php if ($intro_post_image) : ?>
                        <img class="intro-preview col-sm-12" src="<?php echo thumbnail($intro_post_image->media_path) ?>">
                    <?php endif; ?>
                    <div class="wrapper row">
                        <a href="#" class="btn btn-default set-intro-post-thumbnail">{{Lang::get('cms::'.$url_type.'.set_image')}}</a>
                    </div>

                </div>
            </div>
        </div>
        @if($url_type == 'posts')
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">{{trans('cms::'.$url_type.'.internal_image')}}</span>
                    </div>
                    <div class="panel-body" id="internal-image-container">
                        <?php if ($internal_post_image) : ?>
                            <img class="internal-preview col-sm-12" src="<?php echo thumbnail($internal_post_image->media_path) ?>">
                        <?php endif; ?>
                        <div class="wrapper row">
                            <a href="#" class="btn btn-default set-internal-post-thumbnail">{{Lang::get('cms::'.$url_type.'.set_image')}}</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">{{trans('cms::'.$url_type.'.logo_image')}}</span>
                    </div>
                    <div class="panel-body" id="logo-container">
                        <?php if ($logo_image) : ?>
                            <img class="logo-preview col-sm-12" src="<?php echo thumbnail($logo_image->media_path) ?>">
                        <?php endif; ?>
                        <div class="wrapper row">
                            <a href="#" class="btn btn-default set-logo-thumbnail">{{Lang::get('cms::'.$url_type.'.set_image')}}</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">{{trans('cms::'.$url_type.'.video_cover')}}</span>
                    </div>
                    <div class="panel-body" id="video-cover-container">
                        <?php if ($video_cover) : ?>
                            <img class="video-cover-preview col-sm-12" src="<?php echo thumbnail($video_cover->media_path) ?>">
                        <?php endif; ?>
                        <div class="wrapper row">
                            <a href="#" class="btn btn-default set-video-cover">{{Lang::get('cms::'.$url_type.'.set_image')}}</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">{{trans('cms::'.$url_type.'.featured_video')}}</span>
                    </div>
                    <div class="panel-body" id="video-container">
                        <?php if ($post_video) : ?>
                            <img class="featured-preview col-sm-12" src="<?php echo $post_video->media_provider ? $post_video->media_provider_image : thumbnail($post_video->media_path); ?>">
                        <?php endif; ?>
                        <div class="wrapper row">
                            <a href="#" class="btn btn-default set-post-video">{{Lang::get('cms::'.$url_type.'.set_video')}}</a>
                        </div>

                    </div>
                </div>
            </div>
        @elseif($url_type == 'news')
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">{{trans('cms::news.home_image')}}</span>
                    </div>
                    <div class="panel-body" id="home-image-container">
                        <?php if ($home_post_image) : ?>
                            <img class="home-preview col-sm-12" src="<?php echo thumbnail($home_post_image->media_path) ?>">
                        <?php endif; ?>
                        <div class="wrapper row">
                            <a href="#" class="btn btn-default set-home-post-thumbnail">{{Lang::get('cms::'.$url_type.'.set_image')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <input type="text" value="{{$url_type}}" name="url_type"  hidden/>
    <?php echo Form::close(); ?>

</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->

<script>
    $(document).ready(function () {
        $('#hideshow').click(function () {
            $("#seocontent").toggle();
        });

    });
    $(function () {
        $('.set-post-thumbnail').click(function (e) {
            e.preventDefault();
        });
        $(".set-post-thumbnail").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#post_form');
                    var imageContainer = $('#image-container');
                    var file = files[0];
                    formContainer.find('#featured_image_id').val(file.media_id);
                    imageContainer.find('img.featured-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'featured-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });

        /*begin intro image*/
        $('.set-intro-post-thumbnail').click(function (e) {
            e.preventDefault();
        });
        $(".set-intro-post-thumbnail").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#post_form');
                    var imageContainer = $('#intro-image-container');
                    var file = files[0];
                    formContainer.find('#intro_image_id').val(file.media_id);
                    imageContainer.find('img.intro-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'intro-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });
        /*end intro image*/
        /*
         for internal image in th post
         */
        $('.set-internal-post-thumbnail').click(function (e) {
            e.preventDefault();
        });
        $(".set-internal-post-thumbnail").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#post_form');
                    var imageContainer = $('#internal-image-container');
                    var file = files[0];
                    formContainer.find('#internal_image_id').val(file.media_id);
                    imageContainer.find('img.internal-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'internal-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });
        /*
         End the internal image in the post
         */
        /*
         for logo in th post
         */
        $('.set-logo-thumbnail').click(function (e) {
            e.preventDefault();
        });
        $(".set-logo-thumbnail").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#post_form');
                    var imageContainer = $('#logo-container');
                    var file = files[0];
                    formContainer.find('#logo_id').val(file.media_id);
                    imageContainer.find('img.logo-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'logo-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });
        /*
         End the logo image in the post
         */
        /*
         for video cover in th post
         */
        $('.set-video-cover').click(function (e) {
            e.preventDefault();
        });
        $(".set-video-cover").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#post_form');
                    var imageContainer = $('#video-cover-container');
                    var file = files[0];
                    formContainer.find('#video_cover_id').val(file.media_id);
                    imageContainer.find('img.video-cover-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'video-cover-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });
        /*
         End the video cover in the post
         */

        $('.set-post-video').click(function (e) {
            e.preventDefault();
        });
        $(".set-post-video").filemanager({
            types: "video",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#post_form');
                    var videoContainer = $('#video-container');
                    var file = files[0];
                    formContainer.find('#featured_video_id').val(file.media_id);
                    videoContainer.find('img.featured-preview').remove();
                    videoContainer.prepend($('<img>', {
                        'class': 'featured-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_video") ?>");
            }
        });

        /*
         for home image in th post
         */
        $('.set-home-post-thumbnail').click(function (e) {
            e.preventDefault();
        });
        $(".set-home-post-thumbnail").filemanager({
            types: "png|jpg|jpeg|gif|bmp",
            done: function (files) {
                if (files.length) {
                    var formContainer = $('#post_form');
                    var imageContainer = $('#home-image-container');
                    var file = files[0];
                    formContainer.find('#home_image_id').val(file.media_id);
                    imageContainer.find('img.home-preview').remove();
                    imageContainer.prepend($('<img>', {
                        'class': 'home-preview col-sm-12',
                        'src': file.media_thumbnail
                    }));
                }
            },
            error: function (media_path) {
                alert(media_path + " <?php echo trans("cms::posts.is_not_an_image") ?>");
            }
        });
        /*
         End the home image in the post
         */

        $("#remove-video-thumbnail").click(function (e) {
            e.preventDefault();
            $("#featured_video").val("");
            $(this).hide();
            $("#video_thumb_wrap").hide();
            $(".set-post-video").show();
            $("#video_image_post").attr("src", '');
        });
    });
</script>

@stop
