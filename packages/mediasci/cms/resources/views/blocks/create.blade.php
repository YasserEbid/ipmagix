@extends('cms::layouts.master')

@section('content')

<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 35px; }
	#sortable li span { position: absolute; margin-left: -1.3em; }
</style>
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::blocks.index_title'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><?php echo trans('cms::bolcks.bolck_posts_index_title'); ?></li>
	</ul>
</div>                        
<!-- ./page title -->


<!-- Horizontal Form -->
<div class="wrapper wrapper-white">
	<?php if (isset($errors)) { ?>
		<?php if (count($errors) > 0) { ?>
			<div class="alert alert-danger">
				<ul>
					<?php foreach ($errors->all() as $error) { ?>
						<li><?php echo $error; ?></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if (session('message')) { ?>
		<div class="alert alert-danger">
			<?php echo session('message'); ?>
		</div>
	<?php } ?>
	<?php echo Form::open(['method' => 'post', 'class' => "form-horizontal",'id'=>'order_posts']); ?> 
	<div class="form-group">
		<input type="hidden" name="block_id" value="<?php echo $current_block->block_id?>" />
		<label class="col-md-2 control-label"><?php echo trans('cms::blocks.block_name'); ?></label>
		<div class="col-md-8">
			<input type='text'disabled class="form-control" value='<?php echo ($current_block ? $current_block->block_name : '' ); ?>' >
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo trans('cms::blocks.post_search'); ?></label>
		<div class="col-md-8">
			<input type='text' class="form-control" id="auto_compelete" >
		</div>
	</div>
	<?php if (count($current_block_posts)) { ?>
		<div class="form-group">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<ul id="sortable">
				<?php  foreach ($current_block_posts as $post) { ?>
						<li class="ui-state-default" id="<?php echo $post->post_id ?>"><span class="ui-icon ui-icon-arrowthick-2-n-s" ></span><?php echo $post->title; ?></li>
				<?php } ?>
				</ul>
			</div>
		</div>
	<?php } ?>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-8">
			<input type="hidden" name="posts_ids" value="" id="ordering" />
			<input type="submit" class="btn btn-default" value="<?php echo trans('cms::blocks.save'); ?>" >
		</div>
	</div>
	<?php echo Form::close(); ?>
 <script>
 $("#order_posts").submit(function(){
		var orders = [];
		$("#sortable li").each(function(i){
		   
				orders[i] = $(this).attr("id")
			
		});
		var myJSON = orders.join();
		$("#ordering").val(myJSON);
	});
 </script>
</div>
<!-- ./Horizontal Form -->

<!-- ./data table -->
@stop