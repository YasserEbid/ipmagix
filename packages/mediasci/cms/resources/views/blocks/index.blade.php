@extends('cms::layouts.master')

@section('content')
<!-- page title -->
<div class="page-title">
	<h1><?php echo trans('cms::blocks.index_title'); ?></h1>


	<ul class="breadcrumb">
		<li><a href="<?php echo route('dashboard'); ?>"><?php echo trans('cms::common.dashboard'); ?></a></li>
		<li><?php echo trans('cms::blocks.index_title'); ?></li>
	</ul>
</div>                        
<!-- ./page title -->


<div class="wrapper wrapper-white" style="margin-bottom: 10px;">
	<div class="row">
		<div class="page-subtitle">
			<h3><?php echo $current_block ? trans('cms::blocks.edit') : trans('cms::blocks.create'); ?></h3>
		</div>
	</div>

	<div class="row">
		<?php if (isset($errors)) { ?>
			<?php if (count($errors) > 0) { ?>
				<div class="alert alert-danger">
					<ul>
						<?php foreach ($errors->all() as $error) { ?>
							<li><?php echo $error; ?></li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		<?php } ?>

		<?php if (session('message')) { ?>
			<div class="alert alert-success">
				<?php echo session('message'); ?>
			</div>
		<?php } ?>
	</div>

	<?php $form_route = ($current_block ? ['blocks.edit', $current_block->block_id] : ['blocks.create']) ?>
	<?php echo Form::open(['route' => $form_route, 'method' => 'post']); ?> 
	<div class="row">
		<div class="col-md-3" style="padding: 0 10px 0 0;">
			<label><?php echo trans('cms::blocks.block_name'); ?></label>
			<div class="form-group">
				<input value="<?php echo old('block_name', ($current_block ? $current_block->block_name : '')); ?>" type="text" name="block_name" placeholder="<?php echo trans('cms::blocks.block_name'); ?>" class="form-control">
			</div>
		</div>
		<div class="col-md-3" style="padding: 0 10px 0 0;">
			<label><?php echo trans('cms::blocks.max_posts'); ?></label>
			<div class="form-group">
				<input value="<?php echo old('max_posts', ($current_block ? $current_block->max_posts : '')); ?>" type="text" name="max_posts" placeholder="<?php echo trans('cms::blocks.max_posts'); ?>" class="form-control">
			</div>
		</div>
		<div class="col-md-3" >
			<div class="form-group" style="margin-top: 21px;">
				<button class="btn btn-primary" type="submit"><?php echo trans('cms::blocks.save'); ?></button>
			</div>
		</div>
	</div>  
	<?php echo Form::close(); ?>
</div>

<!-- data table -->
<div class="wrapper wrapper-white">
	<div class="dataTables_length">
		<div>
			<label><?php echo trans('cms::blocks.show_per_page'); ?> </label>
			<select class='per_page form-control'>
				<option value="10" <?php echo ($request->input('per_page') == 10) ? 'selected' : '' ?> >10</option>
				<option value="25" <?php echo ($request->input('per_page') == 25) ? 'selected' : '' ?> >25</option>
				<option value="50" <?php echo ($request->input('per_page') == 50) ? 'selected' : '' ?> >50</option>
				<option value="100" <?php echo ($request->input('per_page') == 100) ? 'selected' : '' ?> >100</option>
			</select>
		</div>
	</div>

	<div class="dataTables_filter">
		<?php echo Form::open(['route' => ['blocks.index'], 'method' => 'get', 'id' => 'blocks_search_form']); ?>
		<label>Search:<input type="search" value="<?php echo $request->input('q'); ?>" name="q" class="form-control search_q"></label>
		<?php echo Form::close(); ?>
	</div>

	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th><?php echo trans('cms::blocks.block_id'); ?></th>
					<th><?php echo trans('cms::blocks.block_name'); ?></th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($blocks)) { ?>
					<?php foreach ($blocks as $block) { ?>
						<tr>
							<td><?php echo $block->block_id; ?></td>
							<td><?php echo $block->block_name; ?></td>
							<td>
								<a href="<?php echo route('blocks.edit', ['id' => $block->block_id , 'site_id'=>$request->input('site_id') ]); ?>" class="btn default btn-xs red"><i class="fa fa-edit"></i></a>
								<a href="<?php echo route('blocks.posts', ['id' => $block->block_id]); ?>" class="btn default btn-xs red"><i class="fa fa-eye"></i></a>
								<a href="<?php echo route('blocks.delete', ['id' => $block->block_id]); ?>" onclick="return confirm('<?php echo trans('cms::blocks.confirm_delete'); ?>')" class="btn default btn-xs red"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td style="text-align: center;" colspan="3"><?php echo trans('cms::blocks.no_blocks'); ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php
		if ($blocks) {
			echo $blocks->appends($request->except('page'))->links();
		}
		?>
	</div>
</div>
<!-- ./data table -->
<?php
$params_per_page = count($request->except(['per_page'])) ? http_build_query($request->except(['per_page'])) : '';

$params_search = count($request->except(['q'])) ? http_build_query($request->except(['q'])) : '';
?>
<script>
	$(document).ready(function () {
		$('.per_page').change(function () {
			location.href = '<?php echo route('blocks.index'); ?>?<?php echo $params_per_page ? $params_per_page . '&' : ''; ?>per_page=' + $(this).val();
		});


		$('#current_block').submit(function (e) {
			var q = $(this).find('.search_q');

			if (q.length > 0) {
				if (q.val().trim() == '') {
					location.href = '<?php echo route('blocks.index'); ?><?php echo $params_search ? '?' . $params_search : ''; ?>';
					return false;
				}
			} else {
				return false;
			}
		});

	});
</script>
@stop