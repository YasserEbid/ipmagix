$(document).ready(function() {
	setTimeout(function() {
		$('.cssload-loader').fadeOut();
	}, 3000);
	$(window).load(function() {
		$('.cssload-loader').fadeOut();
	});
	$('nav .fa').click(function() {
		var me = $(this);
		me.parent().find('> ul').slideToggle();
		if(me.hasClass('fa-angle-down'))
			me.removeClass('fa-angle-down').addClass('fa-angle-up');
		else
			me.removeClass('fa-angle-up').addClass('fa-angle-down');
	});
	// wow = new WOW({
	// 	boxClass:     'wow',      // default
	// 	animateClass: 'animated', // default
	// 	offset:       0,          // default
	// 	mobile:       true,       // default
	// 	live:         true        // default
	// })
	// wow.init();
	$(document).on('click', '.nav-toggle', function(e) {
		e.preventDefault();
		$(this).toggleClass('open');
		$('header nav').slideToggle();
	});
	new WOW().init();
	jQuery('.tp-banner').show().revolution({
		delay:7500,
		startwidth:1200,
		startheight:$(window).height(),
		minHeight: 400,
		hideThumbs:600,

		thumbWidth:80,
		thumbHeight:50,
		thumbAmount:5,

		navigationType:"bullet",
		navigationArrows:"0",
		navigationStyle:"preview4",

		touchenabled:"on",
		onHoverStop:"on",

		swipe_velocity: 0.7,
		swipe_min_touches: 1,
		swipe_max_touches: 1,
		drag_block_vertical: false,

		parallax:"mouse",
		parallaxBgFreeze:"on",
		parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

		keyboardNavigation:"off",

		navigationHAlign:"center",
		navigationVAlign:"bottom",
		navigationHOffset:0,
		navigationVOffset:20,

		soloArrowLeftHalign:"left",
		soloArrowLeftValign:"center",
		soloArrowLeftHOffset:20,
		soloArrowLeftVOffset:0,

		soloArrowRightHalign:"right",
		soloArrowRightValign:"center",
		soloArrowRightHOffset:20,
		soloArrowRightVOffset:0,

		shadow:0,
		fullWidth:"on",
		fullScreen:"off",

		spinner:"spinner4",

		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,

		shuffle:"off",

		autoHeight:"on",
		forceFullWidth:"on",

		hideThumbsOnMobile:"on",
		hideNavDelayOnMobile:1500,
		hideBulletsOnMobile:"on",
		hideArrowsOnMobile:"on",
		hideThumbsUnderResolution:0,

		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		startWithSlide:0,
		videoJsPath:"",
		fullScreenOffsetContainer: ".main-slider"
	});

	$('header .search-btn').click(function(e) {
		e.preventDefault();
		$('.search-container').addClass('active');
		$('.search-container input:text').blur();
	});
	$('.search-container input:text').keyup(function() {
		var me = $(this);
		if(me.val() != "")
			me.parent().addClass('active');
		else
			me.parent().removeClass('active');
	});
	$('.search-close').click(function(e) {
		e.preventDefault();
		$('.search-container').removeClass('active');
	});
	$('.tabs a').click(function(e) {
		e.preventDefault();
		var me = $(this);
		$('.tabs a').removeClass('active');
		$('.tabs-container > div').hide();

		me.addClass('active');
		$(me.attr('href')).show();
	});
	$('.accordion a').click(function(e) {
		e.preventDefault();
		var me = $(this);
		if(me.next().hasClass('active')){
			$('.accordion > div').slideUp().removeClass('active');
			$('.accordion a').removeClass('active');
		} else {
			$('.accordion > div').slideUp().removeClass('active');
			$('.accordion a').removeClass('active');
			me.next().slideDown().addClass('active');
			me.addClass('active');
		}
	});
	$('.news-slider').owlCarousel({
		//loop:true,
		margin:0,
		responsiveClass:true,
		dots: false,
		nav:true,
		navText: ['', ''],
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});
	$('.feedback-slider').owlCarousel({
		items: 1,
		dots: true,
		nav: false,
	});
	$(document).on('click', '.readMore', function(e) {
		e.preventDefault();
		$('html').css('overflow', 'hidden');
		var target = $(this).attr('href');
		$(target+'.popup').addClass('active');
		$('.case-details.popup').find('section').scrollTop(0);
	});
	$(document).on('click', '.pop-close', function(e) {
		e.preventDefault();
		$('html').css('overflow', 'auto');
		$(this).parent('.popup').removeClass('active');
	});

	$(document).on('click', '.find-us .locations a.btn', function(e) {
		e.preventDefault();
		var me = $(this);
		var target = me.attr('href');
		$('.location-info-content').removeClass('active');
		$('.find-us .locations a.btn').removeClass('active');
		$('.location-map').removeClass('active');
		me.addClass('active');
		$(target).addClass('active');
		$(target+'-map').addClass('active');
	});
	$(document).on('change', '#uploadFile', function() {
		if($(this).val() != "")
			$(this).parent().find('p').html($(this).val());
		else
			$(this).parent().find('p').html('No File Selected');
	});

	$(document).keyup(function(e) {
		if (e.keyCode == 27) { // escape key maps to keycode `27`
			$('.popup').removeClass('active');
			$('html').css('overflow', 'auto');
			$('header .search-container').removeClass('active');
		}
	});

	$(window).bind('scroll', function(e) {
		e.preventDefault();
		if($(window).scrollTop() > ($(window).height()/2))
			$('.go-top').show();
		else
			$('.go-top').hide();
	});

	$('.go-top').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: 0
		});
	});

	var scroll = 0;
	$(window).bind('scroll', function() {
		if(($(window).scrollTop() > scroll) && ($(window).scrollTop() > 50)) {
			$('header').addClass('fadeUp').removeClass('fadeDown');
		} else {
			$('header').addClass('fadeDown').removeClass('fadeUp');
		}
		scroll = $(window).scrollTop();
	});
	$('.video-over label').click(function() {
		$(this).parent('.video-over').fadeOut();
		$('#solution-video')[0].src += "&autoplay=1";
	});
	$(document).on('click', '.partners .partner a', function(e) {
		e.preventDefault();
		var me = $(this);
		if(!me.hasClass('more')) {
			me.addClass('more').html('Read Less <i class="fa fa-arrow-circle-right"></i>');
			me.parent().find('p').css('height', 'auto');
		} else {
			me.removeClass('more').html('Read More <i class="fa fa-arrow-circle-right"></i>');
			me.parent().find('p').css('height', '');
		}
	});

	$('.location-map')
		.click(function(){
			$(this).find('iframe').addClass('clicked')})
		.mouseleave(function(){
			$(this).find('iframe').removeClass('clicked')});

	$(document).on('click', '.request-demo .btn', function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#RequestDemo').offset().top
		});
	});
});
