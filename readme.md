## Mediasci CMS

### Installation

Install dependancies

```bash
$ composer install
```


Create new database: 

```bash
new_cms
```
Then execute sql script in database/new_cms.sql


### Dashboard

Login using:

```bash
username: admin@cms.com
password: 123456
```