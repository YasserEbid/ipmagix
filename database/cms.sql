-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2016 at 12:57 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE IF NOT EXISTS `blocks` (
  `block_id` int(11) NOT NULL,
  `block_name` varchar(50) NOT NULL,
  `site_id` int(11) NOT NULL,
  `max_posts` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`block_id`, `block_name`, `site_id`, `max_posts`) VALUES
(1, '79797', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blocks_posts`
--

CREATE TABLE IF NOT EXISTS `blocks_posts` (
  `block_id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `post_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(300) NOT NULL,
  `cat_slug` varchar(300) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `site_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_slug`, `parent_id`, `site_id`) VALUES
(4, 'asdasd', '', 0, 1),
(9, 'ouioioh', '', 4, 1),
(11, 'asd', '', 0, 1),
(12, 'asdada', '', 0, 1),
(13, 'bjl', '', 0, 1),
(14, 'kop', '', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `comment_status` int(11) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edited` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `post_id`, `user_id`, `site_id`, `name`, `email`, `comment_status`, `comment_content`, `comment_date`, `edited`) VALUES
(2, 1, NULL, 1, 'a', 'admin@cms.com', 2, 'a', '2016-05-24 17:40:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mail_list`
--

CREATE TABLE IF NOT EXISTS `mail_list` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diabetes_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `lifestyle_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `nationality_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `p_interests_id` int(11) NOT NULL,
  `s_interests_id` int(11) NOT NULL,
  `type_visitor_id` int(11) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mail_list`
--

INSERT INTO `mail_list` (`id`, `email`, `diabetes_id`, `gender_id`, `lifestyle_id`, `location_id`, `nationality_id`, `case_id`, `p_interests_id`, `s_interests_id`, `type_visitor_id`, `is_deleted`, `status`) VALUES
(1, 'elsayed_nofal@ymail.com', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mail_messages`
--

CREATE TABLE IF NOT EXISTS `mail_messages` (
  `id` int(11) NOT NULL,
  `subject` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `theme_id` int(11) NOT NULL,
  `diabetes_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `lifestyle_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `nationality_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `p_interests_id` int(11) NOT NULL,
  `s_interests_id` int(11) NOT NULL,
  `type_visitor_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `is_finished` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mail_messages`
--

INSERT INTO `mail_messages` (`id`, `subject`, `content`, `theme_id`, `diabetes_id`, `gender_id`, `lifestyle_id`, `location_id`, `nationality_id`, `case_id`, `p_interests_id`, `s_interests_id`, `type_visitor_id`, `site_id`, `start`, `end`, `is_finished`) VALUES
(1, 'new mail message', '<p style="text-align: center;"><span style="color: rgb(85, 57, 130);"><span style="background-color: rgb(255, 255, 255);"><span style="font-family: Georgia,serif;"><span style="font-size: 36px;">This is test mail to show how it send to user</span></span><span style="font-family: Georgia,serif;"><span style="font-size: 36px;">&nbsp;</span></span></span></span></p><p style="text-align: center;"><br></p><p style="text-align: left;"><span style="color: rgb(85, 57, 130);"><span style="background-color: rgb(255, 255, 255);"><span style="font-family: Georgia,serif;"><span style="font-size: 18px;">Hi everyone ,</span></span></span></span></p><p style="text-align: left;">I hope this email find you well , please sea the next image&nbsp;</p><p style="text-align: left;"><br></p><p><img class="fr-dib fr-rounded" src="./uploads/editor/4401322383.jpg" style="width: 179px;" alt="the induls king"></p><p>thanks all ,</p>', 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1467108929, 1467118863, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mail_themes`
--

CREATE TABLE IF NOT EXISTS `mail_themes` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` text COLLATE utf8_unicode_ci NOT NULL,
  `footer` text COLLATE utf8_unicode_ci NOT NULL,
  `complete` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mail_themes`
--

INSERT INTO `mail_themes` (`id`, `name`, `header`, `footer`, `complete`) VALUES
(6, 'imperial stander', '<p><img class="fr-dib" src="http://www.icldc.ae/sites/all/themes/boilerplate/images/logo.gif" style="width: 300px;"></p>', '<p><span style="color: rgb(41, 105, 176);"><span style="font-family: Impact,Charcoal,sans-serif;"><em><span style="background-color: rgb(247, 218, 100);">Copyright &copy; 2014, Imperial College London Diabetes Centre</span></em></span></span></p>', 0),
(7, 'test', '<p>FEFEFef</p>', '<p>fewFWFWE</p>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mail_track`
--

CREATE TABLE IF NOT EXISTS `mail_track` (
  `id` int(11) NOT NULL,
  `email_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `site_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mail_track`
--

INSERT INTO `mail_track` (`id`, `email_id`, `message_id`, `time`, `site_id`) VALUES
(1, 1, 1, 1467118864, 0);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `media_id` bigint(30) NOT NULL,
  `media_provider` varchar(25) DEFAULT NULL,
  `media_path` varchar(200) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_title` varchar(255) DEFAULT NULL,
  `media_description` text,
  `media_created_date` datetime NOT NULL,
  `media_duration` int(11) NOT NULL DEFAULT '0',
  `media_hash` varchar(255) NOT NULL,
  `media_width` int(11) NOT NULL,
  `media_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `message_id` int(11) NOT NULL,
  `message_title` varchar(250) NOT NULL,
  `message_content` text NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `site_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(30) NOT NULL,
  `finsihed` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_06_21_121741_mail_themes', 1),
('2016_06_21_121753_mail_lists', 1),
('2016_06_21_121841_mail_emails', 1),
('2016_06_21_121851_mail_messages', 1),
('2016_06_21_121902_mail_messages_status', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` bigint(30) NOT NULL,
  `title` varchar(300) NOT NULL,
  `sub_title` varchar(300) NOT NULL,
  `excerpt` text NOT NULL,
  `created_date` datetime NOT NULL,
  `meta_title` varchar(300) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_excerpt` text NOT NULL,
  `status` int(4) NOT NULL,
  `template_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `title`, `sub_title`, `excerpt`, `created_date`, `meta_title`, `meta_keywords`, `meta_excerpt`, `status`, `template_id`) VALUES
(1, 'a', '', '', '0000-00-00 00:00:00', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts_contents`
--

CREATE TABLE IF NOT EXISTS `posts_contents` (
  `post_id` bigint(30) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_media`
--

CREATE TABLE IF NOT EXISTS `posts_media` (
  `post_id` bigint(30) NOT NULL,
  `media_id` bigint(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(300) NOT NULL,
  `access_dashboard` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `access_dashboard`) VALUES
(1, 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles_routes`
--

CREATE TABLE IF NOT EXISTS `roles_routes` (
  `role_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `can_access` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_routes`
--

INSERT INTO `roles_routes` (`role_id`, `route_id`, `site_id`, `can_access`) VALUES
(1, 1, NULL, 1),
(1, 2, NULL, 1),
(1, 3, NULL, 1),
(1, 4, NULL, 1),
(1, 12, NULL, 1),
(1, 13, NULL, 1),
(1, 14, NULL, 1),
(1, 15, NULL, 1),
(1, 16, NULL, 1),
(1, 17, NULL, 1),
(1, 18, NULL, 1),
(1, 19, NULL, 1),
(1, 20, NULL, 1),
(1, 21, NULL, 1),
(1, 22, NULL, 1),
(1, 23, NULL, 1),
(1, 24, NULL, 1),
(1, 25, NULL, 1),
(1, 26, NULL, 1),
(1, 27, NULL, 1),
(1, 30, NULL, 1),
(1, 31, NULL, 1),
(1, 32, NULL, 1),
(1, 33, NULL, 1),
(1, 34, NULL, 1),
(1, 35, NULL, 1),
(1, 36, NULL, 1),
(1, 38, NULL, 1),
(1, 39, NULL, 1),
(1, 40, NULL, 1),
(1, 41, NULL, 1),
(1, 42, NULL, 1),
(1, 43, NULL, 1),
(1, 44, NULL, 1),
(1, 45, NULL, 1),
(1, 46, NULL, 1),
(1, 47, NULL, 1),
(1, 48, NULL, 1),
(1, 49, NULL, 1),
(1, 50, NULL, 1),
(1, 51, NULL, 1),
(1, 52, NULL, 1),
(1, 53, NULL, 1),
(1, 54, NULL, 1),
(1, 55, NULL, 1),
(1, 56, NULL, 1),
(1, 62, NULL, 1),
(1, 63, NULL, 1),
(1, 64, NULL, 1),
(1, 65, NULL, 1),
(1, 66, NULL, 1),
(1, 67, NULL, 1),
(1, 68, NULL, 1),
(1, 69, NULL, 1),
(1, 70, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `route_id` int(11) NOT NULL,
  `route_name` varchar(300) NOT NULL,
  `conventional_name` varchar(180) NOT NULL,
  `group_id` int(11) NOT NULL,
  `route_type` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`route_id`, `route_name`, `conventional_name`, `group_id`, `route_type`) VALUES
(1, 'dashboard', 'لوحة التحكم', 1, 1),
(2, 'categories.index', 'عرض التصنيفات', 2, 0),
(3, 'auth.login', 'تسجيل الدخول', 1, 1),
(4, 'auth.logout', 'تسجيل خروج', 1, 1),
(12, 'roles.create', 'إضافة صلاحية', 4, 0),
(13, 'roles.edit', 'تعديل صلاحية', 4, 0),
(14, 'routes.index', 'مسارات الموقع', 1, 0),
(15, 'routes_groups.index', 'تصنيف المسارات', 1, 0),
(16, 'routes_groups.edit', 'تعديل تصنيف المسارات', 1, 0),
(17, 'routes_groups.delete', 'حذف مسار', 1, 0),
(18, 'routes_groups.create', 'إضافة تصنيف المسارات', 1, 0),
(19, 'roles.index', 'الصلاحيات', 4, 0),
(20, 'roles.delete', 'حذف صلاحية', 4, 0),
(21, 'user.index', 'عرض المستخدمين', 1, 1),
(22, 'user.create', 'إضافة مستخدم جديد', 1, 1),
(23, 'user.edit', 'تعديل مستخدم', 1, 1),
(24, 'user.delete', 'حذف مستخدم', 5, 1),
(25, 'categories.create', 'اضافة تصنيف', 2, 0),
(26, 'categories.edit', 'تعديل تصنيف', 2, 0),
(27, 'categories.delete', 'حذف تصنيف', 2, 0),
(28, 'test.index', 'test route', 5, 0),
(29, 'test3.index', 'new test', 5, 0),
(30, 'tags.index', 'الوسوم', 1, 0),
(31, 'tags.create', 'إضافة وسم جديد', 1, 0),
(32, 'tags.edit', 'تعديل وسم', 1, 0),
(33, 'tags.delete', 'حذف وسم', 1, 0),
(34, 'comments.index', 'عرض التعليقات', 1, 0),
(35, 'comments.create', 'إضافة تعليق', 1, 0),
(36, 'comments.edit', 'تعديل تعليق', 1, 0),
(37, 'comments.delete', 'حذف تعليق', 1, 0),
(38, 'comments.spam', 'ابلاغ اساءة عن تعليق', 1, 0),
(39, 'comments.publish', 'نشر تعليق', 1, 0),
(40, 'comments.soft_delete', 'حذف تعليق الى سلة المهملات', 1, 0),
(41, 'comments.hard_delete', 'حذف التعليق نهائيا', 1, 0),
(42, 'blocks.index', 'blocks', 1, 0),
(43, 'blocks.create', 'create blocks', 1, 0),
(44, 'blocks.edit', 'edit blocks', 1, 0),
(45, 'blocks.delete', 'delete block', 1, 0),
(46, 'blocks.posts', 'uj', 1, 0),
(47, 'messages.index', 'd', 1, 0),
(48, 'messages.create', 'd', 1, 0),
(49, 'messages.edit', 'd', 1, 0),
(50, 'messages.delete', 'd', 1, 0),
(51, 'messages.cron', 'd', 1, 0),
(52, 'sms.index', 'asd', 1, 0),
(53, 'sms.create', 'asd', 1, 0),
(54, 'sms.edit', 'asd', 1, 0),
(55, 'sms.delete', 'asd', 1, 0),
(56, 'sms.cron', 'asd', 1, 0),
(57, 'debugbar.openhandler', 'asdfh', 2, 0),
(58, 'debugbar.clockwork', 'zdxfcgv', 2, 0),
(59, 'debugbar.assets.css', 'sfdgfh', 1, 0),
(60, 'debugbar.assets.js', 'zdxfcgvh', 1, 0),
(61, 'mail.test', 'mail test', 1, 0),
(62, 'mail.themes.delete', 'delete theme', 1, 0),
(63, 'mail.themes.update', 'update themes', 1, 0),
(64, 'mail.thems.show', 'show themes', 1, 0),
(65, 'mail.themes', 'themes', 1, 0),
(66, 'mail.messages.create', 'create mail message', 1, 0),
(67, 'mail.messages', 'mail message', 1, 0),
(68, 'mail.messages.update', 'update mail message', 1, 0),
(69, 'mail.messages.delete', 'delete mail message', 1, 0),
(70, 'trends', 'trends', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `routes_groups`
--

CREATE TABLE IF NOT EXISTS `routes_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `routes_groups`
--

INSERT INTO `routes_groups` (`group_id`, `group_name`) VALUES
(1, 'General Group'),
(2, 'Categories'),
(4, 'Privilege'),
(5, 'koko66');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `site_id` int(11) NOT NULL,
  `site_name` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`site_id`, `site_name`, `status`) VALUES
(1, 'site 1', 1),
(2, 'site 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
  `sms_id` int(11) NOT NULL,
  `sms_message` varchar(1000) NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `site_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(30) NOT NULL,
  `finsihed` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`sms_id`, `sms_message`, `sent_date`, `site_id`, `created_at`, `created_by`, `finsihed`) VALUES
(0, 'hello ', '2016-06-15 08:00:00', 1, '2016-06-08 03:07:15', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
(1, 'published'),
(2, 'waiting_revision'),
(3, 'spam'),
(4, 'deleted');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` bigint(20) NOT NULL,
  `tag_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `template_id` int(11) NOT NULL,
  `template_title` varchar(300) NOT NULL,
  `frontend_view` varchar(300) NOT NULL,
  `backend_view` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`template_id`, `template_title`, `frontend_view`, `backend_view`) VALUES
(1, 'a', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `trends`
--

CREATE TABLE IF NOT EXISTS `trends` (
  `date` date NOT NULL,
  `twitter` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `trends`
--

INSERT INTO `trends` (`date`, `twitter`, `youtube`) VALUES
('2016-07-10', '[{"name":"#\\u062e\\u062f\\u0639\\u0648\\u0643_\\u0641\\u0642\\u0627\\u0644\\u0648\\u0627","url":"http:\\/\\/twitter.com\\/search?q=%23%D8%AE%D8%AF%D8%B9%D9%88%D9%83_%D9%81%D9%82%D8%A7%D9%84%D9%88%D8%A7"},{"name":"#\\u0643\\u0644\\u0645\\u0647_\\u062a\\u0646\\u0647\\u064a_\\u0641\\u064a\\u0647\\u0627_\\u0646\\u0642\\u0627\\u0634\\u0643","url":"http:\\/\\/twitter.com\\/search?q=%23%D9%83%D9%84%D9%85%D9%87_%D8%AA%D9%86%D9%87%D9%8A_%D9%81%D9%8A%D9%87%D8%A7_%D9%86%D9%82%D8%A7%D8%B4%D9%83"},{"name":"#\\u062d\\u0642\\u064a\\u0642\\u0647","url":"http:\\/\\/twitter.com\\/search?q=%23%D8%AD%D9%82%D9%8A%D9%82%D9%87"},{"name":"#EURO2016","url":"http:\\/\\/twitter.com\\/search?q=%23EURO2016"},{"name":"#\\u0645\\u0627\\u0647\\u064a_\\u0627\\u0648\\u0644_\\u0633\\u064a\\u0627\\u0631\\u0647_\\u0642\\u062f\\u062a\\u0647\\u0627","url":"http:\\/\\/twitter.com\\/search?q=%23%D9%85%D8%A7%D9%87%D9%8A_%D8%A7%D9%88%D9%84_%D8%B3%D9%8A%D8%A7%D8%B1%D9%87_%D9%82%D8%AF%D8%AA%D9%87%D8%A7"},{"name":"\\u0627\\u0644\\u0645\\u0639\\u0627\\u0631\\u0636\\u0647 \\u0627\\u0644\\u0627\\u064a\\u0631\\u0627\\u0646\\u064a\\u0647","url":"http:\\/\\/twitter.com\\/search?q=%22%D8%A7%D9%84%D9%85%D8%B9%D8%A7%D8%B1%D8%B6%D9%87+%D8%A7%D9%84%D8%A7%D9%8A%D8%B1%D8%A7%D9%86%D9%8A%D9%87%22"},{"name":"\\u0631\\u0627\\u0634\\u062f \\u0627\\u0644\\u0645\\u0627\\u062c\\u062f","url":"http:\\/\\/twitter.com\\/search?q=%22%D8%B1%D8%A7%D8%B4%D8%AF+%D8%A7%D9%84%D9%85%D8%A7%D8%AC%D8%AF%22"},{"name":"Serena Williams","url":"http:\\/\\/twitter.com\\/search?q=%22Serena+Williams%22"},{"name":"Abdul Sattar Edhi","url":"http:\\/\\/twitter.com\\/search?q=%22Abdul+Sattar+Edhi%22"}]', '[{"title":"\\u0645\\u0633\\u0644\\u0633\\u0644 \\u0627\\u0644\\u0642\\u064a\\u0635\\u0631 - \\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 ( 30 ) \\u0648\\u0627\\u0644\\u0623\\u062e\\u064a\\u0631\\u0629 - \\u0628\\u0637\\u0648\\u0644\\u0629 \\u064a\\u0648\\u0633\\u0641 \\u0627\\u0644\\u0634\\u0631\\u064a\\u0641 - The Caesar Series HD Episode 30","id":"dHdkqv2SFZ4"},{"title":"\\u0645\\u0633\\u0644\\u0633\\u0644 \\u0646\\u064a\\u0644\\u0644\\u064a \\u0648\\u0634\\u0631\\u064a\\u0647\\u0627\\u0646 - \\u0627\\u0644\\u062d\\u0644\\u0642\\u0647 \\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0648\\u0646 \\u0648\\u0627\\u0644\\u0627\\u062e\\u064a\\u0631\\u0647 \\u0648\\u0627\\u0644\\u0636\\u064a\\u0641  \\"\\u0633\\u0645\\u064a\\u0631 \\u063a\\u0627\\u0646\\u0645\\"  | Nelly & Sherihan - Episode 30","id":"c0NXqfPdoOI"},{"title":"\\u0646\\u0627\\u0635\\u0631 \\u064a\\u0642\\u062a\\u0644 \\u0628\\u062f\\u0631 \\u0628\\u0639\\u062f \\u062e\\u0628\\u0631 \\u0642\\u062a\\u0644 \\u062a\\u0648\\u062d\\u0629 - \\u0645\\u0633\\u0644\\u0633\\u0644 \\u0627\\u0644\\u0627\\u0633\\u0637\\u0648\\u0631\\u0629 \\/ \\u0645\\u062d\\u0645\\u062f \\u0631\\u0645\\u0636\\u0627\\u0646","id":"Qahl787AYPU"},{"title":"\\u0627\\u0646\\u0641\\u0631\\u0627\\u062f \\u0628\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648.. \\u0627\\u0644\\u0633\\u064a\\u0633\\u0649 \\u064a\\u062a\\u062c\\u0648\\u0644 \\u0628\\u062f\\u0631\\u0627\\u062c\\u0629 \\u0641\\u0649 \\u0634\\u0648\\u0627\\u0631\\u0639 \\u0627\\u0644\\u0645\\u0639\\u0645\\u0648\\u0631\\u0629 \\u0648\\u0633\\u0637 \\u0627\\u0644\\u062c\\u0645\\u0627\\u0647\\u064a\\u0631","id":"H-YapyVdCY8"},{"title":"\\u0646\\u0627\\u0635\\u0631 \\u064a\\u0642\\u062a\\u0644 \\u0634\\u0647\\u062f \\u0628\\u0633\\u0628\\u0628 \\u0627\\u0646\\u062a\\u0642\\u0627\\u0645 \\u0633\\u0645\\u0627\\u062d \\u0645\\u0646\\u0647...","id":"tmZzPjIofcU"},{"title":"Sharmoofers - Hepta Song - 5,6 Hepta | \\u0634\\u0627\\u0631\\u0645\\u0648\\u0641\\u0631\\u0632- \\u0623\\u063a\\u0646\\u064a\\u0629 \\u0641\\u064a\\u0644\\u0645 \\u0647\\u064a\\u0628\\u062a\\u0627 \\u0666\\u060c\\u0665 \\u0647\\u064a\\u0628\\u062a\\u0627 - \\u0628\\u0627\\u0645 \\u0628\\u0627\\u0645 \\u0627\\u0632\\u064a\\u062c\\u0644\\u064a \\u0628\\u0627\\u0645 \\u0628\\u0627\\u0645","id":"JAQzzO4uSt8"},{"title":"\\u0641\\u062a\\u0627\\u0629 \\u062a\\u0635\\u0644\\u064a \\u0627\\u0644\\u0639\\u064a\\u062f \\u0628\\u0627\\u0644\\u0647\\u0648\\u062a \\u0634\\u0648\\u0631\\u062a \\u0648\\u0634\\u0627\\u0628 \\u064a\\u0635\\u0644\\u064a \\u0628\\u062c\\u0648\\u0627\\u0631 \\u0643\\u0644\\u0628\\u0647","id":"vESjtif0_1o"},{"title":"\\u0647\\u0627\\u0646\\u064a \\u0641\\u0649 \\u0627\\u0644\\u0627\\u062f\\u063a\\u0627\\u0644 | \\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u062b\\u0627\\u0645\\u0646\\u0629 \\u0648\\u0627\\u0644\\u0639\\u0634\\u0631\\u0648\\u0646 (28) | \\u0636\\u064a\\u0641 \\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u0641\\u0646\\u0627\\u0646 \\u0645\\u0635\\u0637\\u0641\\u0649 \\u0642\\u0645\\u0631 \\u0641\\u0649 \\u0623\\u062e\\u0637\\u0631 \\u062d\\u0644\\u0642\\u0627\\u062a \\u0627\\u0644\\u0628\\u0631\\u0646\\u0627\\u0645\\u062c","id":"9GHmTwFXbNA"},{"title":"\\u0645\\u0633\\u0644\\u0633\\u0644 \\u0648\\u0646\\u0648\\u0633 | \\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 \\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0648\\u0646 \\u0648\\u0627\\u0644\\u0627\\u062e\\u064a\\u0631\\u0629","id":"LRAjWFenPrI"},{"title":"\\u062d\\u0645\\u0627\\u0642\\u064a - \\u0627\\u0644\\u0644\\u0642\\u0627 - \\u0623\\u063a\\u0646\\u064a\\u0629 \\u0641\\u064a\\u0644\\u0645 \\"\\u0645\\u0646 30 \\u0633\\u0646\\u0629\\" | Hamaki - El Lo''a - \\"30 Years Ago\\" Movie Song","id":"9JZDkYHz7qw"}]'),
('2016-07-12', '[{"name":"#\\u0627\\u062e\\u0631\\u062c\\u0648_\\u0627\\u0644\\u0631\\u062c\\u0627\\u0644_\\u0645\\u0646_\\u0643\\u0648\\u0643\\u0628_\\u0627\\u0644\\u0627\\u0631\\u0636","url":"http:\\/\\/twitter.com\\/search?q=%23%D8%A7%D8%AE%D8%B1%D8%AC%D9%88_%D8%A7%D9%84%D8%B1%D8%AC%D8%A7%D9%84_%D9%85%D9%86_%D9%83%D9%88%D9%83%D8%A8_%D8%A7%D9%84%D8%A7%D8%B1%D8%B6"},{"name":"#\\u0627\\u0644\\u0628\\u0646\\u062a_\\u0627\\u0644\\u0645\\u062b\\u0627\\u0644\\u064a\\u0647_\\u0647\\u064a","url":"http:\\/\\/twitter.com\\/search?q=%23%D8%A7%D9%84%D8%A8%D9%86%D8%AA_%D8%A7%D9%84%D9%85%D8%AB%D8%A7%D9%84%D9%8A%D9%87_%D9%87%D9%8A"},{"name":"#\\u0627\\u0628\\u0648\\u0638\\u0628\\u064a","url":"http:\\/\\/twitter.com\\/search?q=%23%D8%A7%D8%A8%D9%88%D8%B8%D8%A8%D9%8A"},{"name":"#\\u0648\\u0638\\u064a\\u0641\\u0647_\\u0646\\u0641\\u0633\\u0643_\\u062a\\u0634\\u062a\\u063a\\u0644\\u0647\\u0627","url":"http:\\/\\/twitter.com\\/search?q=%23%D9%88%D8%B8%D9%8A%D9%81%D9%87_%D9%86%D9%81%D8%B3%D9%83_%D8%AA%D8%B4%D8%AA%D8%BA%D9%84%D9%87%D8%A7"},{"name":"#\\u0647\\u0644_\\u0644\\u064a_\\u0628\\u0631\\u0648\\u064a\\u0647_\\u062e\\u0644\\u0641\\u064a\\u0647_\\u062c\\u0648\\u0627\\u0644\\u0643","url":"http:\\/\\/twitter.com\\/search?q=%23%D9%87%D9%84_%D9%84%D9%8A_%D8%A8%D8%B1%D9%88%D9%8A%D9%87_%D8%AE%D9%84%D9%81%D9%8A%D9%87_%D8%AC%D9%88%D8%A7%D9%84%D9%83"},{"name":"Ronaldo","url":"http:\\/\\/twitter.com\\/search?q=Ronaldo"},{"name":"Theresa May","url":"http:\\/\\/twitter.com\\/search?q=%22Theresa+May%22"},{"name":"Pokemon Go","url":"http:\\/\\/twitter.com\\/search?q=%22Pokemon+Go%22"},{"name":"\\u0639\\u0645\\u0631 \\u0627\\u0644\\u0628\\u0639\\u062f","url":"http:\\/\\/twitter.com\\/search?q=%22%D8%B9%D9%85%D8%B1+%D8%A7%D9%84%D8%A8%D8%B9%D8%AF%22"}]', '[{"title":"\\u0627\\u0647\\u062f\\u0627\\u0641 \\u0645\\u0628\\u0627\\u0631\\u0627\\u0629 \\u0627\\u0644\\u0628\\u0631\\u062a\\u063a\\u0627\\u0644 \\u0648\\u0641\\u0631\\u0646\\u0633\\u0627 1-0 [\\u0643\\u0627\\u0645\\u0644\\u0629] \\u062a\\u0639\\u0644\\u064a\\u0642 \\u0639\\u0635\\u0627\\u0645 \\u0627\\u0644\\u0634\\u0648\\u0627\\u0644\\u064a - \\u0646\\u0647\\u0627\\u0626\\u064a \\u064a\\u0648\\u0631\\u0648 2016 \\u0628\\u0641\\u0631\\u0646\\u0633\\u0627 [10-7-2016] HD","id":"MvrNMIduYPM"},{"title":"\\u0646\\u0627\\u0635\\u0631 \\u064a\\u0642\\u062a\\u0644 \\u0628\\u062f\\u0631 \\u0628\\u0639\\u062f \\u062e\\u0628\\u0631 \\u0642\\u062a\\u0644 \\u062a\\u0648\\u062d\\u0629 - \\u0645\\u0633\\u0644\\u0633\\u0644 \\u0627\\u0644\\u0627\\u0633\\u0637\\u0648\\u0631\\u0629 \\/ \\u0645\\u062d\\u0645\\u062f \\u0631\\u0645\\u0636\\u0627\\u0646","id":"Qahl787AYPU"},{"title":"\\u0645\\u0633\\u0644\\u0633\\u0644 \\u0646\\u064a\\u0644\\u0644\\u064a \\u0648\\u0634\\u0631\\u064a\\u0647\\u0627\\u0646 - \\u0627\\u0644\\u062d\\u0644\\u0642\\u0647 \\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0648\\u0646 \\u0648\\u0627\\u0644\\u0627\\u062e\\u064a\\u0631\\u0647 \\u0648\\u0627\\u0644\\u0636\\u064a\\u0641  \\"\\u0633\\u0645\\u064a\\u0631 \\u063a\\u0627\\u0646\\u0645\\"  | Nelly & Sherihan - Episode 30","id":"c0NXqfPdoOI"},{"title":"Sharmoofers - Hepta Song - 5,6 Hepta | \\u0634\\u0627\\u0631\\u0645\\u0648\\u0641\\u0631\\u0632- \\u0623\\u063a\\u0646\\u064a\\u0629 \\u0641\\u064a\\u0644\\u0645 \\u0647\\u064a\\u0628\\u062a\\u0627 \\u0666\\u060c\\u0665 \\u0647\\u064a\\u0628\\u062a\\u0627 - \\u0628\\u0627\\u0645 \\u0628\\u0627\\u0645 \\u0627\\u0632\\u064a\\u062c\\u0644\\u064a \\u0628\\u0627\\u0645 \\u0628\\u0627\\u0645","id":"JAQzzO4uSt8"},{"title":"\\u0645\\u0633\\u0644\\u0633\\u0644 \\u0627\\u0644\\u0642\\u064a\\u0635\\u0631 - \\u0627\\u0644\\u062d\\u0644\\u0642\\u0629 ( 30 ) \\u0648\\u0627\\u0644\\u0623\\u062e\\u064a\\u0631\\u0629 - \\u0628\\u0637\\u0648\\u0644\\u0629 \\u064a\\u0648\\u0633\\u0641 \\u0627\\u0644\\u0634\\u0631\\u064a\\u0641 - The Caesar Series HD Episode 30","id":"dHdkqv2SFZ4"},{"title":"\\u0646\\u0627\\u0635\\u0631 \\u064a\\u0642\\u062a\\u0644 \\u0634\\u0647\\u062f \\u0628\\u0633\\u0628\\u0628 \\u0627\\u0646\\u062a\\u0642\\u0627\\u0645 \\u0633\\u0645\\u0627\\u062d \\u0645\\u0646\\u0647...","id":"tmZzPjIofcU"},{"title":"\\u0627\\u0646\\u0641\\u0631\\u0627\\u062f \\u0628\\u0627\\u0644\\u0641\\u064a\\u062f\\u064a\\u0648.. \\u0627\\u0644\\u0633\\u064a\\u0633\\u0649 \\u064a\\u062a\\u062c\\u0648\\u0644 \\u0628\\u062f\\u0631\\u0627\\u062c\\u0629 \\u0641\\u0649 \\u0634\\u0648\\u0627\\u0631\\u0639 \\u0627\\u0644\\u0645\\u0639\\u0645\\u0648\\u0631\\u0629 \\u0648\\u0633\\u0637 \\u0627\\u0644\\u062c\\u0645\\u0627\\u0647\\u064a\\u0631","id":"H-YapyVdCY8"},{"title":"\\u0633\\u062a\\u0648\\u062f\\u064a\\u0648 \\u0627\\u0644\\u062d\\u064a\\u0627\\u0629 - \\u0628\\u0643\\u0627\\u0621 \\u0627\\u0644\\u062d\\u0627\\u0631\\u0633 \\u0623\\u062d\\u0645\\u062f \\u0639\\u0627\\u062f\\u0644 \\u0639\\u0628\\u062f \\u0627\\u0644\\u0645\\u0646\\u0639\\u0645 \\u0639\\u0642\\u0628 \\u0627\\u0646\\u062a\\u0647\\u0627\\u0621 \\u0627\\u0644\\u0644\\u0642\\u0627\\u0621 .. \\u062f\\u0648\\u0646 \\u0633\\u0628\\u0628 !","id":"6nV7zIyqT50"},{"title":"\\u0641\\u062a\\u0627\\u0629 \\u062a\\u0635\\u0644\\u064a \\u0627\\u0644\\u0639\\u064a\\u062f \\u0628\\u0627\\u0644\\u0647\\u0648\\u062a \\u0634\\u0648\\u0631\\u062a \\u0648\\u0634\\u0627\\u0628 \\u064a\\u0635\\u0644\\u064a \\u0628\\u062c\\u0648\\u0627\\u0631 \\u0643\\u0644\\u0628\\u0647","id":"vESjtif0_1o"},{"title":"\\u062d\\u0645\\u0627\\u0642\\u064a - \\u0627\\u0644\\u0644\\u0642\\u0627 - \\u0623\\u063a\\u0646\\u064a\\u0629 \\u0641\\u064a\\u0644\\u0645 \\"\\u0645\\u0646 30 \\u0633\\u0646\\u0629\\" | Hamaki - El Lo''a - \\"30 Years Ago\\" Movie Song","id":"9JZDkYHz7qw"}]');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(30) NOT NULL,
  `email` varchar(300) NOT NULL,
  `full_name` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `full_name`, `password`, `mobile`, `role_id`) VALUES
(1, 'admin@cms.com', 'mostafa elbardeny', '4297f44b13955235245b2497399d7a93', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_messages`
--

CREATE TABLE IF NOT EXISTS `users_messages` (
  `message_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `sent` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_properties`
--

CREATE TABLE IF NOT EXISTS `users_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_properties`
--

INSERT INTO `users_properties` (`id`, `name`, `parent`) VALUES
(2, 'Abu Dhabi', 'Location'),
(3, 'Al Ain', 'Location'),
(4, 'Care givers', 'Types of visitors'),
(5, 'Current Patients', 'Types of visitors'),
(6, 'Public Health', 'Types of visitors'),
(7, 'Research Community', 'Types of visitors'),
(8, 'Physicians and other HCPs', 'Types of visitors'),
(9, 'ICLDC Employees', 'Types of visitors'),
(10, 'Food and Drink', 'Primary interests'),
(11, 'Events', 'Primary interests'),
(12, 'Health', 'Primary interests'),
(13, 'Hobbies and Interests', 'Primary interests'),
(14, 'Family and Parenting', 'Primary interests'),
(15, 'Education', 'Primary interests'),
(16, 'Careers', 'Primary interests'),
(17, 'diabetes type 1', 'Patients case'),
(18, 'diabetes type 2', 'Patients case'),
(19, 'Gestational Diabetes (GDM)', 'Patients case'),
(20, 'pre-diabetes', 'Patients case'),
(21, 'other diagnosis', 'Patients case'),
(22, 'other types of diabetes', 'Patients case'),
(23, 'Business', 'Secondary interests'),
(24, 'Beauty', 'Secondary interests'),
(25, 'Movies and television', 'Secondary interests'),
(26, 'Music and Radio', 'Secondary interests'),
(27, 'Books and Literature', 'Secondary interests'),
(28, 'adults with heart disease', 'Diabetes related complications'),
(29, 'adults with retinopathy detected', 'Diabetes related complications'),
(30, 'adults with peripheral vascular disease', 'Diabetes related complications'),
(31, 'adults with neuropathy', 'Diabetes related complications'),
(32, 'adults with nephropathy', 'Diabetes related complications'),
(33, 'Modern Traditionalists ', 'Lifestyle segments'),
(34, 'Cultured Progressives', 'Lifestyle segments'),
(35, 'Societal Strivers ', 'Lifestyle segments'),
(36, 'Abu Dhabi Nationals', 'Nationality'),
(37, 'Northern Emirates Nationals', 'Nationality'),
(38, 'GCC Expats', 'Nationality'),
(39, 'Western Expats', 'Nationality'),
(40, 'males', 'Gender'),
(41, 'females', 'Gender');

-- --------------------------------------------------------

--
-- Table structure for table `users_sms`
--

CREATE TABLE IF NOT EXISTS `users_sms` (
  `sms_id` int(11) NOT NULL,
  `user_id` bigint(30) NOT NULL,
  `sent` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_sms`
--

INSERT INTO `users_sms` (`sms_id`, `user_id`, `sent`) VALUES
(0, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`block_id`),
  ADD KEY `block_name` (`block_name`),
  ADD KEY `site_id` (`site_id`);

--
-- Indexes for table `blocks_posts`
--
ALTER TABLE `blocks_posts`
  ADD PRIMARY KEY (`block_id`,`post_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `cat_slug` (`cat_slug`(255)),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `name` (`name`),
  ADD KEY `email` (`email`(255)),
  ADD KEY `comment_status` (`comment_status`),
  ADD KEY `edited` (`edited`),
  ADD KEY `site_id` (`site_id`);

--
-- Indexes for table `mail_list`
--
ALTER TABLE `mail_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_messages`
--
ALTER TABLE `mail_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_themes`
--
ALTER TABLE `mail_themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_track`
--
ALTER TABLE `mail_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`),
  ADD KEY `media_provider` (`media_provider`),
  ADD KEY `media_provider_2` (`media_provider`),
  ADD KEY `media_type` (`media_type`),
  ADD KEY `media_created_date` (`media_created_date`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`),
  ADD UNIQUE KEY `msg_title` (`message_title`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `site_id` (`site_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `template_id` (`template_id`),
  ADD FULLTEXT KEY `title` (`title`);
ALTER TABLE `posts`
  ADD FULLTEXT KEY `sub_title` (`sub_title`);

--
-- Indexes for table `posts_contents`
--
ALTER TABLE `posts_contents`
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `posts_media`
--
ALTER TABLE `posts_media`
  ADD PRIMARY KEY (`post_id`,`media_id`),
  ADD KEY `media_id` (`media_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD KEY `access_dashboard` (`access_dashboard`);

--
-- Indexes for table `roles_routes`
--
ALTER TABLE `roles_routes`
  ADD PRIMARY KEY (`role_id`,`route_id`),
  ADD KEY `can_access` (`can_access`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `route_id` (`route_id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`route_id`),
  ADD KEY `route_name` (`route_name`(255)),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `route_type` (`route_type`);

--
-- Indexes for table `routes_groups`
--
ALTER TABLE `routes_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`site_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`sms_id`),
  ADD KEY `sms_message` (`sms_message`(255)),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`),
  ADD KEY `tag_name` (`tag_name`(255));

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`template_id`),
  ADD KEY `template_title` (`template_title`(255));

--
-- Indexes for table `trends`
--
ALTER TABLE `trends`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `email` (`email`(255)),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `users_messages`
--
ALTER TABLE `users_messages`
  ADD PRIMARY KEY (`message_id`,`user_id`),
  ADD KEY `sent` (`sent`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users_properties`
--
ALTER TABLE `users_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_sms`
--
ALTER TABLE `users_sms`
  ADD PRIMARY KEY (`sms_id`,`user_id`),
  ADD KEY `sent` (`sent`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mail_list`
--
ALTER TABLE `mail_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mail_messages`
--
ALTER TABLE `mail_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mail_themes`
--
ALTER TABLE `mail_themes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mail_track`
--
ALTER TABLE `mail_track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `media_id` bigint(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` bigint(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `route_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `routes_groups`
--
ALTER TABLE `routes_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `site_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_properties`
--
ALTER TABLE `users_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `templates` (`template_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `posts_contents`
--
ALTER TABLE `posts_contents`
  ADD CONSTRAINT `posts_contents_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts_media`
--
ALTER TABLE `posts_media`
  ADD CONSTRAINT `posts_media_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_media_ibfk_2` FOREIGN KEY (`media_id`) REFERENCES `media` (`media_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles_routes`
--
ALTER TABLE `roles_routes`
  ADD CONSTRAINT `roles_routes_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_routes_ibfk_3` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_routes_ibfk_4` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `users_messages`
--
ALTER TABLE `users_messages`
  ADD CONSTRAINT `users_messages_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `messages` (`message_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_messages_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_sms`
--
ALTER TABLE `users_sms`
  ADD CONSTRAINT `users_sms_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_sms_ibfk_2` FOREIGN KEY (`sms_id`) REFERENCES `sms` (`sms_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
