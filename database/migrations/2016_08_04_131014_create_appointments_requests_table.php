<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsRequestsTable extends Migration{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create('appointments_requests', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('subject');
			$table->string('phone');
			$table->string('email');
			$table->string('comment');
			$table->string('country')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::drop('appointments_requests');
	}
}
