<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users_properties')){
        Schema::create('users_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('parent', 255);
            $table->timestamps();
        });
        

        DB::table('users_properties')->insert(
            Array(
                Array('name' => 'Abu Dhabi', 'parent' => 'Location'),
                Array('name' => 'Al Ain', 'parent' => 'Location'),
                Array('name' => 'Care givers', 'parent' => 'Types of visitors'),
                Array('name' => 'Current Patients', 'parent' => 'Types of visitors'),
                Array('name' => 'Public Health', 'parent' => 'Types of visitors'),
                Array('name' => 'Research Community', 'parent' => 'Types of visitors'),
                Array('name' => 'Physicians and other HCPs', 'parent' => 'Types of visitors'),
                Array('name' => 'ICLDC Employees', 'parent' => 'Types of visitors'),
                Array('name' => 'Food and Drink', 'parent' => 'Primary interests'),
                Array('name' => 'Events', 'parent' => 'Primary interests'),
                Array('name' => 'Health', 'parent' => 'Primary interests'),
                Array('name' => 'Hobbies and Interests', 'parent' => 'Primary interests'),
                Array('name' => 'Family and Parenting', 'parent' => 'Primary interests'),
                Array('name' => 'Education', 'parent' => 'Primary interests'),
                Array('name' => 'Careers', 'parent' => 'Primary interests'),
                Array('name' => 'diabetes type 1', 'parent' => 'Patients case'),
                Array('name' => 'diabetes type 2', 'parent' => 'Patients case'),
                Array('name' => 'Gestational Diabetes (GDM)', 'parent' => 'Patients case'),
                Array('name' => 'pre-diabetes', 'parent' => 'Patients case'),
                Array('name' => 'other diagnosis', 'parent' => 'Patients case'),
                Array('name' => 'other types of diabetes', 'parent' => 'Patients case'),
                Array('name' => 'Business', 'parent' => 'Secondary interests'),
                Array('name' => 'Beauty', 'parent' => 'Secondary interests'),
                Array('name' => 'Movies and television', 'parent' => 'Secondary interests'),
                Array('name' => 'Music and Radio', 'parent' => 'Secondary interests'),
                Array('name' => 'Books and Literature', 'parent' => 'Secondary interests'),
                Array('name' => 'adults with heart disease', 'parent' => 'Diabetes related complications'),
                Array('name' => 'adults with retinopathy detected', 'parent' => 'Diabetes related complications'),
                Array('name' => 'adults with peripheral vascular disease', 'parent' => 'Diabetes related complications'),
                Array('name' => 'adults with neuropathy', 'parent' => 'Diabetes related complications'),
                Array('name' => 'adults with nephropathy', 'parent' => 'Diabetes related complications'),
                Array('name' => 'Modern Traditionalists ', 'parent' => 'Lifestyle segments'),
                Array('name' => 'Cultured Progressives', 'parent' => 'Lifestyle segments'),
                Array('name' => 'Societal Strivers ', 'parent' => 'Lifestyle segments'),
                Array('name' => 'Abu Dhabi Nationals', 'parent' => 'Nationality'),
                Array('name' => 'Northern Emirates Nationals', 'parent' => 'Nationality'),
                Array('name' => 'GCC Expats', 'parent' => 'Nationality'),
                Array('name' => 'Western Expats', 'parent' => 'Nationality'),
                Array('name' => 'males', 'parent' => 'Gender'),
                Array('name' => 'females', 'parent' => 'Gender')
            )
        );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_properties');
    }
}
