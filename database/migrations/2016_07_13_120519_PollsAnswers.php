<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PollsAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Polls_answers',function(Blueprint $table){
            if (!Schema::hasTable('Polls_answers')) {
                $table->integer('id',TRUE);
                $table->integer('user_id');
                $table->integer('poll_id');
                $table->integer('option_id');
                $table->foreign('option_id')
                          ->references('id')
                          ->on('polls_options')
                          ->onDelete('cascade');
                $table->foreign('poll_id')
                          ->references('id')
                          ->on('polls')
                          ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Polls_answers');
    }
}
