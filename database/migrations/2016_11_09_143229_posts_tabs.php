<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostsTabs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_tabs', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('post_id')->unsigned();
			//$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('noaction');
			$table->string('slug');
			
			$table->timestamps();
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('posts_tabs');
    }
}
