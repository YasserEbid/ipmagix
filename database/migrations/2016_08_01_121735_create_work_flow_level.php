<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkFlowLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('workflow_levels')){
            Schema::create('workflow_levels', function (Blueprint $table) {
                $table->integer('id',true);
                $table->integer('workflow_id');
                $table->string('name', 255);
                $table->string('url', 500);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workflow_levels');
    }
}
