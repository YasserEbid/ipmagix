<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenuLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_links', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('menu_id')->unsigned();
			$table->integer('parent_id')->nullable();
			$table->integer('active');
			$table->integer('order');
			$table->string('link_url');
			$table->integer('open_window');
            $table->timestamps();
        });
		
		DB::table('menu_links')->insert(
			array(
				/* Header Links */
				array('menu_id'=>1,'parent_id'=>null,'active'=>'1','order'=>1,'link_url'=>'about_us','open_window'=>0),
				array('menu_id'=>1,'parent_id'=>null,'active'=>'1','order'=>2,'link_url'=>'careers','open_window'=>0),
				array('menu_id'=>1,'parent_id'=>null,'active'=>'1','order'=>3,'link_url'=>'events','open_window'=>0),
				array('menu_id'=>1,'parent_id'=>null,'active'=>'1','order'=>4,'link_url'=>'news','open_window'=>0),
				/* Main Menu Links */
				array('menu_id'=>2,'parent_id'=>null,'active'=>'1','order'=>1,'link_url'=>'about_us','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>null,'active'=>'1','order'=>2,'link_url'=>'ourServices','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>null,'active'=>'1','order'=>3,'link_url'=>'healthcare','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>null,'active'=>'1','order'=>4,'link_url'=>'diabetes','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>null,'active'=>'1','order'=>5,'link_url'=>'research','open_window'=>0),
				/* Footer Links */
				array('menu_id'=>3,'parent_id'=>null,'active'=>'1','order'=>1,'link_url'=>'about_us','open_window'=>0),
				array('menu_id'=>3,'parent_id'=>null,'active'=>'1','order'=>2,'link_url'=>'contact_us','open_window'=>0),
				array('menu_id'=>3,'parent_id'=>null,'active'=>'1','order'=>3,'link_url'=>'careers','open_window'=>0),
				array('menu_id'=>3,'parent_id'=>null,'active'=>'1','order'=>4,'link_url'=>'news','open_window'=>0),
				array('menu_id'=>3,'parent_id'=>null,'active'=>'1','order'=>5,'link_url'=>'FAQ','open_window'=>0),
				
				//main menu submenus
				//about us sub links
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>1,'link_url'=>'about_us/p/whoWeAre','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>2,'link_url'=>'about_us/About_Mubadala_Healthcare','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>3,'link_url'=>'about_us/About_Imperial_College_London','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>4,'link_url'=>'about_us/Mission_&_Vision','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>5,'link_url'=>'about_us/Leadership_Team','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>6,'link_url'=>'about_us/doctors','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>7,'link_url'=>'about_us/Quality_And_Safety','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>8,'link_url'=>'about_us/Accreditations','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>9,'link_url'=>'about_us/Insurance','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>10,'link_url'=>'about_us/Outcomes_Report','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>5,'active'=>'1','order'=>11,'link_url'=>'about_us/UAE_Diabetes_Trends_And_Numbers','open_window'=>0),
				//our services sub links
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>1,'link_url'=>'ourServices#DIABETES','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>2,'link_url'=>'ourServices#ENDOCRINOLOGY','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>3,'link_url'=>'ourServices#HEART','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>4,'link_url'=>'ourServices#PODIATRY','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>5,'link_url'=>'ourServices#NEPHROLOGY','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>6,'link_url'=>'ourServices#OPHTHALMOLOGY','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>7,'link_url'=>'ourServices#DIETETIC','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>8,'link_url'=>'ourServices#LABORATORY','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>9,'link_url'=>'ourServices#RADIOLOGY','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>6,'active'=>'1','order'=>10,'link_url'=>'ourServices#PHARMACY','open_window'=>0),
				//our For Healthcare Professionals sub links
				array('menu_id'=>2,'parent_id'=>7,'active'=>'1','order'=>1,'link_url'=>'events/2/education','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>7,'active'=>'1','order'=>2,'link_url'=>'events/3/continuingmedicaleducation','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>7,'active'=>'1','order'=>3,'link_url'=>'subscribe','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>7,'active'=>'1','order'=>4,'link_url'=>'healthcare/p/resource','open_window'=>0),
				//our For Diabetes && Health Information sub links
				array('menu_id'=>2,'parent_id'=>8,'active'=>'1','order'=>1,'link_url'=>'diabetesPost/aboutDiabetes','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>8,'active'=>'1','order'=>2,'link_url'=>'diabetesPost/dietDiabetes','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>8,'active'=>'1','order'=>3,'link_url'=>'diabetesPost/excerciseDiabetes','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>8,'active'=>'1','order'=>4,'link_url'=>'diabetes','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>8,'active'=>'1','order'=>5,'link_url'=>'diabetes','open_window'=>0),
				//Research
				array('menu_id'=>2,'parent_id'=>9,'active'=>'1','order'=>1,'link_url'=>'research/p/ResearchOverview','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>9,'active'=>'1','order'=>2,'link_url'=>'research/p/Active-Projects','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>9,'active'=>'1','order'=>3,'link_url'=>'research/p/Resources','open_window'=>0),
				array('menu_id'=>2,'parent_id'=>9,'active'=>'1','order'=>4,'link_url'=>'volunteer','open_window'=>0),
			)
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_links');
    }
}
