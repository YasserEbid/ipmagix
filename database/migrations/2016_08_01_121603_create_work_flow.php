<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkFlow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('workflow')){
            Schema::create('workflow', function (Blueprint $table) {
                $table->integer('id',true);
                $table->string('title', 500);
                $table->integer('diabetes_id');
                $table->integer('gender_id');
                $table->integer('lifestyle_id');
                $table->integer('location_id');
                $table->integer('nationality_id');
                $table->integer('case_id');
                $table->integer('p_interests_id');
                $table->integer('s_interests_id');
                $table->integer('type_visitor_id');
                $table->integer('site_id');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workflow');
    }
}
