<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQuestionAnswersLangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up(){
		Schema::create('questions_answers_langs', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('question_answer_id')->unsigned();
			$table->string('question');
			$table->text('answer');
			$table->string('lang',2);
			$table->timestamps();
		});
		
		DB::table('questions_answers_langs')->insert(
			array(
				array(
					'question_answer_id'=>1,
					'question'=>'What are some of the symptoms that could indicate?',
					'answer'=>'Usual symptoms include thirst, passing urine frequently and tiredness. However, these symptoms often have other non-worrying causes. Many people have diabetes without having any symptoms and without knowing they have it. This is why it is so important for people to have screening tests if they are at risk.',
					'lang'=>'en'
				),
				array(
					'question_answer_id'=>1,
					'question'=>'ما هي بعض من الأعراض التي يمكن أن تشير إلى',
					'answer'=>'وتشمل الأعراض المعتادة والعطش، والتبول المتكرر والتعب. ومع ذلك، هذه الأعراض غالبا ما يكون لأسباب أخرى غير مقلقة. كثير من الناس لديهم مرض السكري من دون وجود أي أعراض ودون أن يعرفوا لديهم. هذا هو السبب في أنه من المهم جدا للناس ليكون فحوصات إذا كانت معرضة للخطر.',
					'lang'=>'ar'
				),
				array(
					'question_answer_id'=>2,
					'question'=>'What causes diabetes?',
					'answer'=>'Usual symptoms include thirst, passing urine frequently and tiredness. However, these symptoms often have other non-worrying causes. Many people have diabetes without having any symptoms and without knowing they have it. This is why it is so important for people to have screening tests if they are at risk.',
					'lang'=>'en'
				),
				array(
					'question_answer_id'=>2,
					'question'=>'ما الذي يسبب مرض السكري؟',
					'answer'=>'وتشمل الأعراض المعتادة والعطش، والتبول المتكرر والتعب. ومع ذلك، هذه الأعراض غالبا ما يكون لأسباب أخرى غير مقلقة. كثير من الناس لديهم مرض السكري من دون وجود أي أعراض ودون أن يعرفوا لديهم. هذا هو السبب في أنه من المهم جدا للناس ليكون فحوصات إذا كانت معرضة للخطر.',
					'lang'=>'ar'
				),
				array(
					'question_answer_id'=>3,
					'question'=>'How does being obese put one at risk for diabetes?',
					'answer'=>'Usual symptoms include thirst, passing urine frequently and tiredness. However, these symptoms often have other non-worrying causes. Many people have diabetes without having any symptoms and without knowing they have it. This is why it is so important for people to have screening tests if they are at risk.',
					'lang'=>'en'
				),
				array(
					'question_answer_id'=>3,
					'question'=>'كيف السمنة وضع واحد في خطر لمرض السكري؟',
					'answer'=>'وتشمل الأعراض المعتادة والعطش، والتبول المتكرر والتعب. ومع ذلك، هذه الأعراض غالبا ما يكون لأسباب أخرى غير مقلقة. كثير من الناس لديهم مرض السكري من دون وجود أي أعراض ودون أن يعرفوا لديهم. هذا هو السبب في أنه من المهم جدا للناس ليكون فحوصات إذا كانت معرضة للخطر.',
					'lang'=>'ar'
				),
				array(
					'question_answer_id'=>4,
					'question'=>'How many people have diabetes, worldwide, and where does the UAE rank?',
					'answer'=>'Usual symptoms include thirst, passing urine frequently and tiredness. However, these symptoms often have other non-worrying causes. Many people have diabetes without having any symptoms and without knowing they have it. This is why it is so important for people to have screening tests if they are at risk.',
					'lang'=>'en'
				),
				array(
					'question_answer_id'=>4,
					'question'=>'كم من الناس يعانون من مرض السكري، في جميع أنحاء العالم، وأين رتبة الإمارات العربية المتحدة؟',
					'answer'=>'وتشمل الأعراض المعتادة والعطش، والتبول المتكرر والتعب. ومع ذلك، هذه الأعراض غالبا ما يكون لأسباب أخرى غير مقلقة. كثير من الناس لديهم مرض السكري من دون وجود أي أعراض ودون أن يعرفوا لديهم. هذا هو السبب في أنه من المهم جدا للناس ليكون فحوصات إذا كانت معرضة للخطر.',
					'lang'=>'ar'
				),
			)
		);
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions_answers_langs');
    }
}
