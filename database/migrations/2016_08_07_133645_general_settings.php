<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeneralSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('value',255);
            
            $table->timestamps();
        });
        
        DB::table('general_settings')->insert(array(
            array('name'=>'phone','value'=>'01111111111'),
             array('name'=>'face','value'=>'https://www.facebook.com/DiabetesUAE?fref=ts'),
             array('name'=>'twitter','value'=>'https://twitter.com/Start_Walking'),
             array('name'=>'youtube','value'=>'https://www.youtube.com/user/DiabetesUAE'),
             array('name'=>'google','value'=>'https://plus.google.com/u/0/106439152555488576425/posts')
        )
                );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('general_settings');
    }
}
