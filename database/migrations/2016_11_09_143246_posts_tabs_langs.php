<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostsTabsLangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_tabs_langs', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('post_tab_id')->unsigned();
			//$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('noaction');
			$table->string('lang',2);
			$table->string('title');
			$table->text('description')->nullable();			
			$table->timestamps();
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('posts_tabs_langs');
    }
}
