<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('templates')){
            Schema::table('posts', function (Blueprint $table) {
                // $table->dropForeign(['template_id']);
                $table->dropColumn('template_id');
            });
            Schema::drop('templates');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
