<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('mail_list', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email');
                $table->integer('gender_id')->nullable();
                $table->integer('nationality_id')->nullable();
                $table->integer('location_id')->nullable();
                $table->integer('type_visitor_id')->nullable();
                $table->integer('p_interests_id')->nullable();
                $table->integer('case_id')->nullable();
                $table->integer('s_interests_id')->nullable();
                $table->integer('diabetes_id')->nullable();
                $table->integer('lifestyle_id')->nullable();
                
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('mail_list');
    }
}
