<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenusLangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus_langs', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('menu_id')->unsigned();
			$table->string('name');
			$table->string('lang',2);
            $table->timestamps();
        });
		
		DB::table('menus_langs')->insert(
			array(
				array(
					'menu_id'=>1,
					'name'=>'Header Menu',
					'lang'=>'en'
				),
				array(
					'menu_id'=>1,
					'name'=>'قائمة الرأس',
					'lang'=>'ar'
				),
				array(
					'menu_id'=>2,
					'name'=>'Main Menu',
					'lang'=>'en'
				),
				array(
					'menu_id'=>2,
					'name'=>'القائمة الرئيسية',
					'lang'=>'ar'
				),
				array(
					'menu_id'=>3,
					'name'=>'Footer Menu',
					'lang'=>'ar'
				),
				array(
					'menu_id'=>3,
					'name'=>'قائمة الذيل',
					'lang'=>'ar'
				),
			)
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus_langs');
    }
}
