<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareerLangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
             Schema::create('career_langs', function (Blueprint $table) {
                 $table->increments('id');
                 $table->string('title', 255);
                 $table->string('location', 255);
                 $table->text('summery');
                 $table->text('description');
                 $table->string('lang',2);
                  $table->integer('career_id')->nullable();
            $table->timestamps();
                 
             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('career_langs');
    }
}
