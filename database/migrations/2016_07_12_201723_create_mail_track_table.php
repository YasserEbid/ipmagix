<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTrackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        if(!Schema::hasTable('mail_track')){
            Schema::create('mail_track', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_id');
            $table->integer('message_id');
            $table->integer('time');
            $table->integer('site_id');
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mail_track', function (Blueprint $table) {
            //
        });
    }
}
