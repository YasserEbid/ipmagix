<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailSubscribers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        
         Schema::create('mail_subscribers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email');
                $table->integer('gender_id')->nullable();
                $table->integer('nationality_id')->nullable();
                $table->integer('location_id')->nullable();
                $table->integer('visitor_type')->nullable();
                $table->integer('primary_interest')->nullable();
                $table->integer('case_id')->nullable();
                $table->integer('secondary_interest')->nullable();
                $table->integer('complication_id')->nullable();
                $table->integer('style_id')->nullable();
                
         });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('mail_subscribers');
    }
}
