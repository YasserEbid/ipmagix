<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_langs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->string('lang', 10);
            $table->string('post_title', 255);
            $table->string('post_subtitle', 255);
            $table->text('post_excerpt');
            $table->text('post_content');
            $table->string('meta_title', 150);
            $table->string('meta_keywords', 150);
            $table->string('meta_description', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts_langs');
    }
}
