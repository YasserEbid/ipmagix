<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactFieldToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('general_settings', function (Blueprint $table) {
            $table->enum('type', ['string', 'text'])->default('string');
        });

        DB::table('general_settings')->insert(array(array(
            'name' => 'operation_hours',
            'type' => 'text',
            'display_name' => 'operation hours',
            'value' => '<h2>Operation Hours</h2><div><p>Sunday - Thursday</p><p>07:00am - 06:00pm</p></div><div><p>Friday Closed</p></div><div><p>Saturday (Only Abu Dhabi)</p><p>07:00am - 03:00pm</p></div><div><p>Email</p><p>info@icldc.ae</p></div>',
        )));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('general_settings', function (Blueprint $table) {
            //
        });
    }
}
