<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Careers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('careers', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('status');
           $table->integer('cat_id')->nullable();
           $table->integer('media_id')->nullable();
            $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('careers');
    }
}
