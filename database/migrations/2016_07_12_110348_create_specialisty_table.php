<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialistyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialisty', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lang');
            $table->timestamps();
        });

        DB::table('specialisty')->insert(
            Array(
                Array('name' => 'Cardiologist', 'lang' => 'en'),
                Array('name' => 'Diabetologist/Endocrinologist', 'lang' => 'en'),
                Array('name' => 'Family Medicine', 'lang' => 'en'),
                Array('name' => 'General Practice', 'lang' => 'en'),
                Array('name' => 'Internal medicine', 'lang' => 'en'),
                Array('name' => 'Nephrologist', 'lang' => 'en'),
                Array('name' => 'Ophthalmologist', 'lang' => 'en'),
                Array('name' => 'Paediatrician', 'lang' => 'en'),
                Array('name' => 'Pathologist', 'lang' => 'en'),
                Array('name' => 'Podiatrist', 'lang' => 'en'),
                Array('name' => 'Radiologist', 'lang' => 'en'),
                Array('name' => 'أمراض السكري و الغدد الصماء', 'lang' => 'ar'),
                Array('name' => 'أمراض القلب', 'lang' => 'ar'),
                Array('name' => 'أمراض الكلى', 'lang' => 'ar'),
                Array('name' => 'الأشعة', 'lang' => 'ar'),
                Array('name' => 'الأمراض الباطنية', 'lang' => 'ar'),
                Array('name' => 'المختبر الطبي', 'lang' => 'ar'),
                Array('name' => 'طب الأسرة', 'lang' => 'ar'),
                Array('name' => 'طب الأطفال', 'lang' => 'ar'),
                Array('name' => 'طب العيون', 'lang' => 'ar'),
                Array('name' => 'طب القدمين', 'lang' => 'ar'),
                Array('name' => 'ممارس عام', 'lang' => 'ar')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('specialisty');
    }
}
