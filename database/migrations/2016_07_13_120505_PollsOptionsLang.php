<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PollsOptionsLang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls_options_lang',function(Blueprint $table){
            if (!Schema::hasTable('polls_options_lang')) {
                $table->integer('id', TRUE);
                $table->string('option', 500);
                $table->string('lang', 255);
                $table->integer('option_id');
                $table->foreign('option_id')
                          ->references('id')
                          ->on('polls_options')
                          ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Polls_answers');
        Schema::dropIfExists('polls_options_lang');
    }
}
