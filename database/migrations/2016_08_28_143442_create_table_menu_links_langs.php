<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenuLinksLangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up(){
		Schema::create('menu_links_langs', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('menu_link_id')->unsigned();
			$table->string('name');
			$table->string('lang',2);
			$table->timestamps();
		});
		
		DB::table('menu_links_langs')->insert(
			array(
				/* Header Links Langs*/
				array('menu_link_id'=>1,'name'=>'About Us','lang'=>'en'),
				array('menu_link_id'=>1,'name'=>'من نحن','lang'=>'ar'),
				array('menu_link_id'=>2,'name'=>'Careers','lang'=>'en'),
				array('menu_link_id'=>3,'name'=>'الوظائف','lang'=>'ar'),
				array('menu_link_id'=>3,'name'=>'Events','lang'=>'en'),
				array('menu_link_id'=>3,'name'=>'الاحداث','lang'=>'ar'),
				array('menu_link_id'=>4,'name'=>'News','lang'=>'en'),
				array('menu_link_id'=>4,'name'=>'الاخبار','lang'=>'ar'),
				/* Main Menu Links Langs*/
				array('menu_link_id'=>5,'name'=>'About Us','lang'=>'en'),
				array('menu_link_id'=>5,'name'=>'من نحن','lang'=>'ar'),
				array('menu_link_id'=>6,'name'=>'Our Services','lang'=>'en'),
				array('menu_link_id'=>6,'name'=>'حدماتنا','lang'=>'ar'),
				array('menu_link_id'=>7,'name'=>'For Healthcare Professionals','lang'=>'en'),
				array('menu_link_id'=>7,'name'=>'لمتخصصي الرعاية الصحية','lang'=>'ar'),
				array('menu_link_id'=>8,'name'=>'Diabetes & Health Information','lang'=>'en'),
				array('menu_link_id'=>8,'name'=>'السكري وصحة المعلومات','lang'=>'ar'),
				array('menu_link_id'=>9,'name'=>'Research','lang'=>'en'),
				array('menu_link_id'=>9,'name'=>'الابحاث','lang'=>'ar'),
				/* Footer Links Langs*/
				array('menu_link_id'=>10,'name'=>'About Us','lang'=>'en'),
				array('menu_link_id'=>10,'name'=>'من نحن','lang'=>'ar'),
				array('menu_link_id'=>11,'name'=>'Contact Us','lang'=>'en'),
				array('menu_link_id'=>11,'name'=>'اتصل بنا','lang'=>'ar'),
				array('menu_link_id'=>12,'name'=>'Careers','lang'=>'en'),
				array('menu_link_id'=>12,'name'=>'الوظائف','lang'=>'ar'),
				array('menu_link_id'=>13,'name'=>'News','lang'=>'en'),
				array('menu_link_id'=>13,'name'=>'الاخبار','lang'=>'ar'),
				array('menu_link_id'=>14,'name'=>'FAQ\'s','lang'=>'en'),
				array('menu_link_id'=>14,'name'=>'الأسئلة الأكثر شيوعا','lang'=>'ar'),
				
				//main menu submenus
				//about us sub links
				array('menu_link_id'=>15,'name'=>'Who We Are','lang'=>'en'),
				array('menu_link_id'=>15,'name'=>'Who We Are','lang'=>'ar'),
				array('menu_link_id'=>16,'name'=>'About Mubadala','lang'=>'en'),
				array('menu_link_id'=>16,'name'=>'About Mubadala','lang'=>'ar'),
				array('menu_link_id'=>17,'name'=>'About Imperial College London','lang'=>'en'),
				array('menu_link_id'=>17,'name'=>'About Imperial College London','lang'=>'ar'),
				array('menu_link_id'=>18,'name'=>'Mission &amp; Vision','lang'=>'en'),
				array('menu_link_id'=>18,'name'=>'Mission &amp; Vision','lang'=>'ar'),
				array('menu_link_id'=>19,'name'=>'Leadership Team','lang'=>'en'),
				array('menu_link_id'=>19,'name'=>'Leadership Team','lang'=>'ar'),
				array('menu_link_id'=>20,'name'=>'Our Doctors','lang'=>'en'),
				array('menu_link_id'=>20,'name'=>'Our Doctors','lang'=>'ar'),
				array('menu_link_id'=>21,'name'=>'Quality and Safety','lang'=>'en'),
				array('menu_link_id'=>21,'name'=>'Quality and Safety','lang'=>'ar'),
				array('menu_link_id'=>22,'name'=>'Accreditations','lang'=>'en'),
				array('menu_link_id'=>22,'name'=>'Accreditations','lang'=>'ar'),
				array('menu_link_id'=>23,'name'=>'Insurance','lang'=>'en'),
				array('menu_link_id'=>23,'name'=>'Insurance','lang'=>'ar'),
				array('menu_link_id'=>24,'name'=>'Outcomes Report','lang'=>'en'),
				array('menu_link_id'=>24,'name'=>'Outcomes Report','lang'=>'ar'),
				array('menu_link_id'=>25,'name'=>'UAE Diabetes Trends And Numbers','lang'=>'en'),
				array('menu_link_id'=>25,'name'=>'UAE Diabetes Trends And Numbers','lang'=>'ar'),
				//our services sub links
				array('menu_link_id'=>26,'name'=>'Diabetes','lang'=>'en'),
				array('menu_link_id'=>26,'name'=>'داء السكري','lang'=>'ar'),
				array('menu_link_id'=>27,'name'=>'Endocrinology','lang'=>'en'),
				array('menu_link_id'=>27,'name'=>'أمراض الغدد الصماء','lang'=>'ar'),
				array('menu_link_id'=>28,'name'=>'Heart Disease Prevention','lang'=>'en'),
				array('menu_link_id'=>28,'name'=>'أمراض القلب منع (غير الغازية أمراض القلب)','lang'=>'ar'),
				array('menu_link_id'=>29,'name'=>'Podiatry','lang'=>'en'),
				array('menu_link_id'=>29,'name'=>'علاج الأرجل','lang'=>'ar'),
				array('menu_link_id'=>30,'name'=>'Nephrology','lang'=>'en'),
				array('menu_link_id'=>30,'name'=>'أمراض الكلى','lang'=>'ar'),
				array('menu_link_id'=>31,'name'=>'Ophthalmology','lang'=>'en'),
				array('menu_link_id'=>31,'name'=>'طب العيون','lang'=>'ar'),
				array('menu_link_id'=>32,'name'=>'Dietetic services','lang'=>'en'),
				array('menu_link_id'=>32,'name'=>'الخدمات الحمية','lang'=>'ar'),
				array('menu_link_id'=>33,'name'=>'Laboratory','lang'=>'en'),
				array('menu_link_id'=>33,'name'=>'مختبر','lang'=>'ar'),
				array('menu_link_id'=>34,'name'=>'Radiology','lang'=>'en'),
				array('menu_link_id'=>34,'name'=>'طب إشعاعي','lang'=>'ar'),
				array('menu_link_id'=>35,'name'=>'Pharmacy','lang'=>'en'),
				array('menu_link_id'=>35,'name'=>'صيدلية','lang'=>'ar'),
				
				//our For Healthcare Professionals sub links
				array('menu_link_id'=>36,'name'=>'Training','lang'=>'en'),
				array('menu_link_id'=>36,'name'=>'Training','lang'=>'ar'),
				array('menu_link_id'=>37,'name'=>'Continuing Medical Education','lang'=>'en'),
				array('menu_link_id'=>37,'name'=>'Continuing Medical Education','lang'=>'ar'),
				array('menu_link_id'=>38,'name'=>'Subscribe To Mailing List','lang'=>'en'),
				array('menu_link_id'=>38,'name'=>'Subscribe To Mailing List','lang'=>'ar'),
				array('menu_link_id'=>39,'name'=>'Resources','lang'=>'en'),
				array('menu_link_id'=>39,'name'=>'Resources','lang'=>'ar'),
				
				//For Diabetes && Health Information sub links
				array('menu_link_id'=>40,'name'=>'About Diabetes','lang'=>'en'),
				array('menu_link_id'=>40,'name'=>'About Diabetes','lang'=>'ar'),
				array('menu_link_id'=>41,'name'=>'Diet','lang'=>'en'),
				array('menu_link_id'=>41,'name'=>'Diet','lang'=>'ar'),
				array('menu_link_id'=>42,'name'=>'Exercise','lang'=>'en'),
				array('menu_link_id'=>42,'name'=>'Exercise','lang'=>'ar'),
				array('menu_link_id'=>43,'name'=>'Diabetes FAQs','lang'=>'en'),
				array('menu_link_id'=>43,'name'=>'Diabetes FAQs','lang'=>'ar'),
				array('menu_link_id'=>44,'name'=>'Diabetes. Knowledge. Action','lang'=>'en'),
				array('menu_link_id'=>44,'name'=>'Diabetes. Knowledge. Action','lang'=>'ar'),
				
				//Research
				array('menu_link_id'=>45,'name'=>'Overview','lang'=>'en'),
				array('menu_link_id'=>45,'name'=>'Overview','lang'=>'ar'),
				array('menu_link_id'=>46,'name'=>'Projects','lang'=>'en'),
				array('menu_link_id'=>46,'name'=>'Projects','lang'=>'ar'),
				array('menu_link_id'=>47,'name'=>'Publications','lang'=>'en'),
				array('menu_link_id'=>47,'name'=>'Publications','lang'=>'ar'),
				array('menu_link_id'=>48,'name'=>'Volunteer For A Research Project','lang'=>'en'),
				array('menu_link_id'=>48,'name'=>'Volunteer For A Research Project','lang'=>'ar'),
			)
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_links_langs');
    }
}
