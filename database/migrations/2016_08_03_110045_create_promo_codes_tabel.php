<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoCodesTabel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('promo_codes', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('code', 255);
			$table->integer('times')->unsigned();
			$table->integer('amount')->unsigned();
			$table->timestamp('expired_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('promo_codes');
	}

}
