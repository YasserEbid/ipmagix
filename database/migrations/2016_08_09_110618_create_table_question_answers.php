<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQuestionAnswers extends Migration{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up(){
		Schema::create('questions_answers', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('active');
			$table->timestamps();
		});
		
		DB::table('questions_answers')->insert(
			array(
				array(
					'active'=>'1'
				),
				array(
					'active'=>'1'
				),
				array(
					'active'=>'1'
				),
				array(
					'active'=>'1'
				),
			)
		);
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down(){
		Schema::drop('questions_answers');
	}
}
