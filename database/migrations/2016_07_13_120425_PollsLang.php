<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PollsLang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls_lang',function(Blueprint $table){
            if (!Schema::hasTable('polls_lang')) {
                $table->integer('id',TRUE);
                $table->integer('poll_id');
                $table->text('question');
                $table->string('lang', 255);
                $table->foreign('poll_id')
                      ->references('id')
                      ->on('polls')
                      ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Polls_answers');
        Schema::dropIfExists('polls_options_lang');
        Schema::dropIfExists('polls_options');
        Schema::dropIfExists('polls_lang');
       // Schema::drop('polls_lang');
    }
}
