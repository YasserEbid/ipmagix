<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PollsOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls_options',function(Blueprint $table){
            if (!Schema::hasTable('poll_options')) {
                $table->integer('id',TRUE);
                $table->integer('poll_id');
                $table->foreign('poll_id')
                          ->references('id')
                          ->on('polls')
                          ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Polls_answers');
        Schema::dropIfExists('polls_options_lang');
        Schema::dropIfExists('polls_options');
    }
}
