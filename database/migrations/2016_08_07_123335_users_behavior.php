<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersBehavior extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users_behavior')){
            Schema::create('users_behavior',function(Blueprint $table){
                $table->integer('id', true);
                $table->integer('user_id');
                $table->integer('diabetes_id');
                $table->integer('gender_id');
                $table->integer('lifestyle_id');
                $table->integer('location_id');
                $table->integer('nationality_id');
                $table->integer('case_id');
                $table->integer('p_interests_id');
                $table->integer('s_interests_id');
                $table->integer('type_visitor_id');
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_behavior');
    }
}
