<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RegisteredInformations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('registered_informations', function (Blueprint $table) {
            $table->increments('id');
			
			$table->string('name');
                        $table->date('birth_date');
                        $table->string('gender');
                        $table->string('title');
                        $table->string('email');
                        $table->string('phone');
                        $table->string('langs');
                        $table->integer('event_id')->nullable();
                        $table->string('cv_name');
                        $table->string('certificat_name');
                        $table->string('achieved_name')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registered_informations');
    }
}
