<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('mail_messages')){
        Schema::create('mail_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 500);
            $table->text('content');
            $table->integer('theme_id');
            $table->integer('diabetes_id');
            $table->integer('gender_id');
            $table->integer('lifestyle_id');
            $table->integer('location_id');
            $table->integer('nationality_id');
            $table->integer('case_id');
            $table->integer('p_interests_id');
            $table->integer('s_interests_id');
            $table->integer('type_visitor_id');
            $table->integer('site_id');
            $table->integer('start');
            $table->integer('end');
            $table->tinyInteger('is_finished');
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mail_messages');
    }
}
