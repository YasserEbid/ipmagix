<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('trends')){
            Schema::create('trends', function (Blueprint $table) {
                $table->increments('id');
                $table->datetime('date');
                $table->text('twitter');
                $table->text('youtube');
                $table->timestamps();
                $table->index('date');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trends');
    }
}
