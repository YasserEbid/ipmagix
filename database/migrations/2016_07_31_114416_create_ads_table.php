<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('ads')){
            Schema::create('ads', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('diabetes_id');
                $table->integer('gender_id');
                $table->integer('lifestyle_id');
                $table->integer('location_id');
                $table->integer('nationality_id');
                $table->integer('case_id');
                $table->integer('p_interests_id');
                $table->integer('s_interests_id');
                $table->integer('type_visitor_id');
                $table->integer('is_deleted');
                $table->integer('site_id');
                $table->tinyInteger('type')->comment = '1 image , 2 html';
                $table->integer('media_id');
                $table->string('link');
                $table->string('subject');
                $table->text('html');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
