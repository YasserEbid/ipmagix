<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
		Schema::create('menus', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('active');
			$table->timestamps();
		});
		
		DB::table('menus')->insert(
			array(
				array(
					'active'=>'1'
				),
				array(
					'active'=>'1'
				),
				array(
					'active'=>'1'
				),
			)
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}
