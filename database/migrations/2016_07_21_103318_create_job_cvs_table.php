<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_cvs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('job_title', 255);
            $table->string('email', 255);
            $table->string('phone', 255);
            $table->string('cv', 255);
            $table->integer('cover_letter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_cvs');
    }
}
