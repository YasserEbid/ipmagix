<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('new_sms')){
            Schema::create('new_sms',function(Blueprint $table){
                $table->integer('id', true);
                $table->text('message');
                $table->integer('diabetes_id');
                $table->integer('gender_id');
                $table->integer('lifestyle_id');
                $table->integer('location_id');
                $table->integer('nationality_id');
                $table->integer('case_id');
                $table->integer('p_interests_id');
                $table->integer('s_interests_id');
                $table->integer('type_visitor_id');
                $table->integer('site_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('new_sms');
    }
}
