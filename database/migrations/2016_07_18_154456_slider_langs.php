<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SliderLangs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { Schema::create('slider_langs', function (Blueprint $table) {
            $table->increments('id');
                 $table->string('title');
                    $table->string('lang',2);
                  $table->integer('slider_id')->unsigned();
            $table->foreign('slider_id')->references('id')->on('sliders');
            $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               Schema::drop('slider_langs');

        //
    }
}
