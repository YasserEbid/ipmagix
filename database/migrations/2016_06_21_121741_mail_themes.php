<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailThemes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('mail_themes')){
            Schema::create('mail_themes',function(Blueprint $table){

                $table->increments('id');
                $table->string('name', 255);
                $table->text('header');
                $table->text('footer');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mail_themes');
    }
}
