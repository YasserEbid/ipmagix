<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class Polls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls',function(Blueprint $table){
            if (!Schema::hasTable('polls')) {
                $table->integer('id',TRUE);
                $table->integer('time');
                $table->date('date');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Polls_answers');
        Schema::dropIfExists('polls_options_lang');
        Schema::dropIfExists('polls_options');
        Schema::dropIfExists('polls_lang');
        Schema::dropIfExists('polls');
       // Schema::drop('polls');
    }
}
