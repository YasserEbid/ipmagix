<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_langs', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('city_id')->unsigned();
			$table->string('name');
			$table->string('lang',2);
            $table->timestamps();
        });
		
		DB::table('cities_langs')->insert(
			array(
				array(
					'city_id'=>1,
					'name'=>'Abu Dhabi',
					'lang'=>'en'
				),
				array(
					'city_id'=>1,
					'name'=>'ابو ظبى',
					'lang'=>'ar'
				),
				array(
					'city_id'=>2,
					'name'=>'Al Ain',
					'lang'=>'en'
				),
				array(
					'city_id'=>2,
					'name'=>'العين',
					'lang'=>'ar'
				),
			)
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cities_langs');
    }
}
