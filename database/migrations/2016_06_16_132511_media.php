<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('posts_media');
        Schema::drop('media');

        Schema::create('media', function (Blueprint $table) {
            $table->increments('media_id');
            $table->string('media_provider');
            $table->string('media_provider_id');
            $table->string('media_provider_image');
            $table->string('media_path');
            $table->string('media_type');
            $table->string('media_title');
            $table->text('media_description');
            $table->string('media_duration');
            $table->string('video_id');
            $table->text('vides_thumbnail');
            $table->dateTime('media_created_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('media');
    }
}
