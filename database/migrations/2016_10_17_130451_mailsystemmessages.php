<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mailsystemmessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {Schema::create('mailsystem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('title',255);
            $table->text('message');
            
            $table->timestamps();
        });
        
        DB::table('mailsystem')->insert(array(
            array('name'=>'subscribe','title'=>'Thank you for your interest','message'=>'Thank you ,You are now subscribed to our mailing list.'),
               array('name'=>'volunteer','title'=>'Thank you for taking the initiative to Volunteer','message'=>'Thank you We will contact you if you are a suitable candidate for any of our research projects.'),
        
              array('name'=>'requestanappointment','title'=>'You Have Requested an Appointment at ICLDC.','message'=>'You have successfully Requested an appointment. A representative from ICLDC will contact you within 24 hours'),
               array('name'=>'career','title'=>'Thank you for applying ','message'=>'Thank you"."\r\n"."Your CV is now registered on our system and our HR team will be in touch if your CV is shortlisted.')
        )
                );
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('mailsystem');
    }
}
