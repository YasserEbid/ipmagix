<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Volunteers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('volunteers', function (Blueprint $table) {
             $table->increments('id');
             $table->string('fullname');
             $table->string('company_name');
             $table->string('email');
             $table->string('phone');
             $table->string('message');
             $table->string('country');
             $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('volunteers');
    }
}
