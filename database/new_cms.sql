-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2016 at 12:09 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `block_id` int(11) NOT NULL,
  `block_name` varchar(50) NOT NULL,
  `site_id` int(11) NOT NULL,
  `max_posts` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`block_id`, `block_name`, `site_id`, `max_posts`) VALUES
(1, '79797', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blocks_posts`
--

CREATE TABLE `blocks_posts` (
  `block_id` int(11) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `post_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(300) NOT NULL,
  `cat_slug` varchar(300) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `site_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_slug`, `parent_id`, `site_id`) VALUES
(4, 'asdasd', '', 0, 1),
(9, 'ouioioh', '', 4, 1),
(11, 'asd', '', 0, 1),
(12, 'asdada', '', 0, 1),
(13, 'bjl', '', 0, 1),
(14, 'kop', '', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `comment_status` int(11) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edited` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `post_id`, `user_id`, `site_id`, `name`, `email`, `comment_status`, `comment_content`, `comment_date`, `edited`) VALUES
(2, 1, NULL, 1, 'a', 'admin@cms.com', 2, 'a', '2016-05-24 17:40:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `media_id` bigint(30) NOT NULL,
  `media_provider` varchar(25) DEFAULT NULL,
  `media_path` varchar(200) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_title` varchar(255) DEFAULT NULL,
  `media_description` text,
  `media_created_date` datetime NOT NULL,
  `media_duration` int(11) NOT NULL DEFAULT '0',
  `media_hash` varchar(255) NOT NULL,
  `media_width` int(11) NOT NULL,
  `media_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `message_title` varchar(250) NOT NULL,
  `message_content` text NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `site_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(30) NOT NULL,
  `finsihed` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` bigint(30) NOT NULL,
  `title` varchar(300) NOT NULL,
  `sub_title` varchar(300) NOT NULL,
  `excerpt` text NOT NULL,
  `created_date` datetime NOT NULL,
  `meta_title` varchar(300) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_excerpt` text NOT NULL,
  `status` int(4) NOT NULL,
  `template_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `title`, `sub_title`, `excerpt`, `created_date`, `meta_title`, `meta_keywords`, `meta_excerpt`, `status`, `template_id`) VALUES
(1, 'a', '', '', '0000-00-00 00:00:00', '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts_contents`
--

CREATE TABLE `posts_contents` (
  `post_id` bigint(30) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts_media`
--

CREATE TABLE `posts_media` (
  `post_id` bigint(30) NOT NULL,
  `media_id` bigint(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(300) NOT NULL,
  `access_dashboard` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `access_dashboard`) VALUES
(1, 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles_routes`
--

CREATE TABLE `roles_routes` (
  `role_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `site_id` int(11) DEFAULT NULL,
  `can_access` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_routes`
--

INSERT INTO `roles_routes` (`role_id`, `route_id`, `site_id`, `can_access`) VALUES
(1, 1, NULL, 1),
(1, 2, NULL, 1),
(1, 3, NULL, 1),
(1, 4, NULL, 1),
(1, 12, NULL, 1),
(1, 13, NULL, 1),
(1, 14, NULL, 1),
(1, 15, NULL, 1),
(1, 16, NULL, 1),
(1, 17, NULL, 1),
(1, 18, NULL, 1),
(1, 19, NULL, 1),
(1, 20, NULL, 1),
(1, 21, NULL, 1),
(1, 22, NULL, 1),
(1, 23, NULL, 1),
(1, 24, NULL, 1),
(1, 25, NULL, 1),
(1, 26, NULL, 1),
(1, 27, NULL, 1),
(1, 30, NULL, 1),
(1, 31, NULL, 1),
(1, 32, NULL, 1),
(1, 33, NULL, 1),
(1, 34, NULL, 1),
(1, 35, NULL, 1),
(1, 36, NULL, 1),
(1, 38, NULL, 1),
(1, 39, NULL, 1),
(1, 40, NULL, 1),
(1, 41, NULL, 1),
(1, 42, NULL, 1),
(1, 43, NULL, 1),
(1, 44, NULL, 1),
(1, 45, NULL, 1),
(1, 46, NULL, 1),
(1, 47, NULL, 1),
(1, 48, NULL, 1),
(1, 49, NULL, 1),
(1, 50, NULL, 1),
(1, 51, NULL, 1),
(1, 52, NULL, 1),
(1, 53, NULL, 1),
(1, 54, NULL, 1),
(1, 55, NULL, 1),
(1, 56, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `route_id` int(11) NOT NULL,
  `route_name` varchar(300) NOT NULL,
  `conventional_name` varchar(180) NOT NULL,
  `group_id` int(11) NOT NULL,
  `route_type` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`route_id`, `route_name`, `conventional_name`, `group_id`, `route_type`) VALUES
(1, 'dashboard', 'لوحة التحكم', 1, 1),
(2, 'categories.index', 'عرض التصنيفات', 2, 0),
(3, 'auth.login', 'تسجيل الدخول', 1, 1),
(4, 'auth.logout', 'تسجيل خروج', 1, 1),
(12, 'roles.create', 'إضافة صلاحية', 4, 0),
(13, 'roles.edit', 'تعديل صلاحية', 4, 0),
(14, 'routes.index', 'مسارات الموقع', 1, 0),
(15, 'routes_groups.index', 'تصنيف المسارات', 1, 0),
(16, 'routes_groups.edit', 'تعديل تصنيف المسارات', 1, 0),
(17, 'routes_groups.delete', 'حذف مسار', 1, 0),
(18, 'routes_groups.create', 'إضافة تصنيف المسارات', 1, 0),
(19, 'roles.index', 'الصلاحيات', 4, 0),
(20, 'roles.delete', 'حذف صلاحية', 4, 0),
(21, 'user.index', 'عرض المستخدمين', 1, 1),
(22, 'user.create', 'إضافة مستخدم جديد', 1, 1),
(23, 'user.edit', 'تعديل مستخدم', 1, 1),
(24, 'user.delete', 'حذف مستخدم', 5, 1),
(25, 'categories.create', 'اضافة تصنيف', 2, 0),
(26, 'categories.edit', 'تعديل تصنيف', 2, 0),
(27, 'categories.delete', 'حذف تصنيف', 2, 0),
(28, 'test.index', 'test route', 5, 0),
(29, 'test3.index', 'new test', 5, 0),
(30, 'tags.index', 'الوسوم', 1, 0),
(31, 'tags.create', 'إضافة وسم جديد', 1, 0),
(32, 'tags.edit', 'تعديل وسم', 1, 0),
(33, 'tags.delete', 'حذف وسم', 1, 0),
(34, 'comments.index', 'عرض التعليقات', 1, 0),
(35, 'comments.create', 'إضافة تعليق', 1, 0),
(36, 'comments.edit', 'تعديل تعليق', 1, 0),
(37, 'comments.delete', 'حذف تعليق', 1, 0),
(38, 'comments.spam', 'ابلاغ اساءة عن تعليق', 1, 0),
(39, 'comments.publish', 'نشر تعليق', 1, 0),
(40, 'comments.soft_delete', 'حذف تعليق الى سلة المهملات', 1, 0),
(41, 'comments.hard_delete', 'حذف التعليق نهائيا', 1, 0),
(42, 'blocks.index', 'blocks', 1, 0),
(43, 'blocks.create', 'create blocks', 1, 0),
(44, 'blocks.edit', 'edit blocks', 1, 0),
(45, 'blocks.delete', 'delete block', 1, 0),
(46, 'blocks.posts', 'uj', 1, 0),
(47, 'messages.index', 'd', 1, 0),
(48, 'messages.create', 'd', 1, 0),
(49, 'messages.edit', 'd', 1, 0),
(50, 'messages.delete', 'd', 1, 0),
(51, 'messages.cron', 'd', 1, 0),
(52, 'sms.index', 'asd', 1, 0),
(53, 'sms.create', 'asd', 1, 0),
(54, 'sms.edit', 'asd', 1, 0),
(55, 'sms.delete', 'asd', 1, 0),
(56, 'sms.cron', 'asd', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `routes_groups`
--

CREATE TABLE `routes_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `routes_groups`
--

INSERT INTO `routes_groups` (`group_id`, `group_name`) VALUES
(1, 'General Group'),
(2, 'Categories'),
(4, 'Privilege'),
(5, 'koko66');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `site_id` int(11) NOT NULL,
  `site_name` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`site_id`, `site_name`, `status`) VALUES
(1, 'site 1', 1),
(2, 'site 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE `sms` (
  `sms_id` int(11) NOT NULL,
  `sms_message` varchar(1000) NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `site_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(30) NOT NULL,
  `finsihed` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`sms_id`, `sms_message`, `sent_date`, `site_id`, `created_at`, `created_by`, `finsihed`) VALUES
(0, 'hello ', '2016-06-15 08:00:00', 1, '2016-06-08 03:07:15', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
(1, 'published'),
(2, 'waiting_revision'),
(3, 'spam'),
(4, 'deleted');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` bigint(20) NOT NULL,
  `tag_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `template_id` int(11) NOT NULL,
  `template_title` varchar(300) NOT NULL,
  `frontend_view` varchar(300) NOT NULL,
  `backend_view` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`template_id`, `template_title`, `frontend_view`, `backend_view`) VALUES
(1, 'a', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(30) NOT NULL,
  `email` varchar(300) NOT NULL,
  `full_name` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `full_name`, `password`, `mobile`, `role_id`) VALUES
(1, 'admin@cms.com', 'mostafa elbardeny', 'e10adc3949ba59abbe56e057f20f883e', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_messages`
--

CREATE TABLE `users_messages` (
  `message_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `sent` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_sms`
--

CREATE TABLE `users_sms` (
  `sms_id` int(11) NOT NULL,
  `user_id` bigint(30) NOT NULL,
  `sent` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_sms`
--

INSERT INTO `users_sms` (`sms_id`, `user_id`, `sent`) VALUES
(0, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`block_id`),
  ADD KEY `block_name` (`block_name`),
  ADD KEY `site_id` (`site_id`);

--
-- Indexes for table `blocks_posts`
--
ALTER TABLE `blocks_posts`
  ADD PRIMARY KEY (`block_id`,`post_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `cat_slug` (`cat_slug`(255)),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `name` (`name`),
  ADD KEY `email` (`email`(255)),
  ADD KEY `comment_status` (`comment_status`),
  ADD KEY `edited` (`edited`),
  ADD KEY `site_id` (`site_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`),
  ADD KEY `media_provider` (`media_provider`),
  ADD KEY `media_provider_2` (`media_provider`),
  ADD KEY `media_type` (`media_type`),
  ADD KEY `media_created_date` (`media_created_date`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`),
  ADD UNIQUE KEY `msg_title` (`message_title`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `site_id` (`site_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
ALTER TABLE `posts` ADD FULLTEXT KEY `title` (`title`);
ALTER TABLE `posts` ADD FULLTEXT KEY `sub_title` (`sub_title`);

--
-- Indexes for table `posts_contents`
--
ALTER TABLE `posts_contents`
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `posts_media`
--
ALTER TABLE `posts_media`
  ADD PRIMARY KEY (`post_id`,`media_id`),
  ADD KEY `media_id` (`media_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD KEY `access_dashboard` (`access_dashboard`);

--
-- Indexes for table `roles_routes`
--
ALTER TABLE `roles_routes`
  ADD PRIMARY KEY (`role_id`,`route_id`),
  ADD KEY `can_access` (`can_access`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `route_id` (`route_id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`route_id`),
  ADD KEY `route_name` (`route_name`(255)),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `route_type` (`route_type`);

--
-- Indexes for table `routes_groups`
--
ALTER TABLE `routes_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`site_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`sms_id`),
  ADD KEY `sms_message` (`sms_message`(255)),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`),
  ADD KEY `tag_name` (`tag_name`(255));

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`template_id`),
  ADD KEY `template_title` (`template_title`(255));

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `email` (`email`(255)),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `users_messages`
--
ALTER TABLE `users_messages`
  ADD PRIMARY KEY (`message_id`,`user_id`),
  ADD KEY `sent` (`sent`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users_sms`
--
ALTER TABLE `users_sms`
  ADD PRIMARY KEY (`sms_id`,`user_id`),
  ADD KEY `sent` (`sent`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `media_id` bigint(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` bigint(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `routes_groups`
--
ALTER TABLE `routes_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts_contents`
--
ALTER TABLE `posts_contents`
  ADD CONSTRAINT `posts_contents_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts_media`
--
ALTER TABLE `posts_media`
  ADD CONSTRAINT `posts_media_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_media_ibfk_2` FOREIGN KEY (`media_id`) REFERENCES `media` (`media_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles_routes`
--
ALTER TABLE `roles_routes`
  ADD CONSTRAINT `roles_routes_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_routes_ibfk_3` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_routes_ibfk_4` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `users_messages`
--
ALTER TABLE `users_messages`
  ADD CONSTRAINT `users_messages_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `messages` (`message_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_messages_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_sms`
--
ALTER TABLE `users_sms`
  ADD CONSTRAINT `users_sms_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_sms_ibfk_2` FOREIGN KEY (`sms_id`) REFERENCES `sms` (`sms_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
