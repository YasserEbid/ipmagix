<?php

return [

    /*
    |--------------------------------------------------------------------------
    | App cache keys
    |--------------------------------------------------------------------------
    |   'controller route' => 'cache keys'
    /   example: 'posts' => 'posts_menu,posts_all'
    */

    'names' => [
        'cities' => 'cities',
        'posts' => 'aboutus',
        'locations' => 'locations',
        'setting' => 'site_settings',
        'services' => 'specialities,services_menu',
        'sliders' => 'sliders',
        'menus' => 'menusLinks',
    ]

];
