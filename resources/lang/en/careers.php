<?php

return[
    'career'=>'Careers',
    'home'=>'Home',
    'suburcv'=>'Submit Your CV',
    'jobtitle'=>'Job Title',
    'loc'=>'Location',
    'created'=>'Date Posted',
    'apply'=>'Apply Now',
    'uploadcv'=>'Upload Your CV',
    'submit'=>'Submit',
    'notify'=>'you must upload Your CV',
    'error'=>'Please upload valid CV',
    'success'=>'	
Thank you
Your CV is now registered on our system and our HR team will be in touch if your CV is shortlisted.',
    'interst'=>'Vacancies of Interest'
];
