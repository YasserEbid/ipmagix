<?php

return [

	/*
	  |--------------------------------------------------------------------------
	  | All website lines
	  |--------------------------------------------------------------------------
	  |
	  | The following language lines are used by the paginator library to build
	  | the simple pagination links. You are free to change them to anything
	  | you want to customize your views to better match your application.
	  |
	 */

	'lang' => 'العربية',
	'Careers' => 'Careers',
	'home' => 'Home',
	'Events' => 'Events',
	'UpcomingEvents' => 'Upcoming Events',
	'Newss' => 'News',
	'Request_an' => 'Request an',
	'Thank_You' => 'Thank You',
	'Success_Request' => 'Thank you for submitting you request. A representative from ICLDC will contact you within 24 hours.',
	'Success_Subscribe' => 'Thank you.
You are now subscribed to our mailing list',
	'Appointment' => 'Appointment',
	'Find_a' => 'Find a',
	'Doctor' => 'Doctor',
	'OurDoctors' => 'Our Doctors',
	'Find' => 'Find',
	'Us' => 'Us',
	'Search' => 'Search',
	'Findad' => 'Find a Doctor',
	'searchad' => 'Search for a doctor through one of the below fields:',
	'Viewad' => 'View All Doctors',
	'ByInsSpe' => 'By Speciality',//'By Institute/ Speciality',
	'SelectInsSpe' => 'Select a Speciality',//'Select an Institute/ Speciality',
	'ByFirLas' => 'By Name',//'By First Name/ Last Name',
	'Searchbyname' => 'Search by name',
	'ByConDisKey' => 'By Condition/ Disease/ Keyword',
	'ByAlph' => 'By Alphabet',//'By Alphabet (Last Name)',
	'ChooseLetter' => 'Choose letter',
	'AboutUs' => 'About Us',
	'News' => 'News',
	'LatestNews' => 'Latest News',
	'Whoweare' => 'Who we are?',
	'Mission&Vision' => 'Mission & Vision',
	'LeadershipTeam' => 'Leadership Team',
	'OurDoctors' => 'Our Doctors',
	'Accreditations' => 'Accreditations',
	'Billing&Insurance' => 'Billing & Insurance',
	'OutcomesReport' => 'Outcomes Report',
	'OurServices' => 'Our Services',
	'DIABETES' => 'DIABETES',
	'ENDOCRINOLOGY' => 'ENDOCRINOLOGY',
	'HEART' => 'HEART',
	'PODIATRY' => 'PODIATRY',
	'NEPHROLOGY' => 'NEPHROLOGY',
	'OPHTHALMOLOGY' => 'OPHTHALMOLOGY',
	'DIETETIC' => 'DIETETIC',
	'LABORATORY' => 'LABORATORY',
	'RADIOLOGY' => 'RADIOLOGY',
	'PHARMACY' => 'PHARMACY',
	'ForHealthcareProfessionals' => 'For Healthcare Professionals',
	'Diabetes&HealthInformation' => 'Diabetes & Health Information',
	'Research' => 'Research',
	'ContactUs' => 'Contact Us',
	'FAQ' => "FAQ's",
	'Sitemap' => 'Sitemap',
	'Legal' => 'Legal Disclaimer',
	'PrivacyPolicy' => 'Privacy Policy',
	'Copyright' => "Copyright © 2016, Imperial College London Diabetes Centre - by ",
	'Up' => 'Up',
	'Doctors'=>'Doctors',
	'evreSuccess'=>'Thank you for Registraion.',
	'evreFail'=>'Sorry , something went wrong. Please try again.',
	'evreExist'=>'This email is already registered before.',
	'RequestAnAppointment'=>'Request an Appointment',
	'promonotExist'=>'The promocode is wrong , Please try again or try another code .',
	'promoExpired'=>'This promocode is expired .',
	'nonews'=>'Sorry , it seems there are no news yet.',
	'find_us'=>'Find Us',
	'MediaSci'=>'MediaSci',
    'byloc'=>'By Location',
    'ain'=>'Al Ain',
    'dhabi'=>'Abu Dhabi'
];
