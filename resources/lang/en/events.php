<?php

return[
    'month'=>'',
    'upcoming'=>'Upcoming',
    'home'=>'Home',
    'events'=>'All Events',
    'country'=>'Country',
    'city'=>'City',
    'first_name'=>'First Name',
    'last_name'=>'Last Name',
    'email'=>'Email',
    'contact'=>'Contact',
    'promo_code'=>'Promo Code',
];

