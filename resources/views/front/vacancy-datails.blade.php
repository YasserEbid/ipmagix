@extends('front.layout')
@section('meta')
	<title>Vacancy Details</title>
@endsection
@section('content')
	<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
		<img src="{{url("assets/front/images/headers/vacancy.jpg")}}" alt="">
		<div>
			<h1>Vacancy Details</h1>
		</div>
	</section>
	@if(Session::has('linkedin_apply'))
	<?php $redirect_msg = session()->pull('linkedin_apply'); ?>
		<div class="alert alert-success">
			<strong>Success! </strong> {{$redirect_msg[0]}}
		 </div>
		 <?php session()->forget('linkedin_apply');?>
	@elseif(Session::has('already_registered'))
	<?php $info_msg = session()->pull('already_registered'); ?>
		<div class="alert alert-info">
			<strong>Success! </strong> {{$info_msg[0]}}
		 </div>
		 <?php session()->forget('already_registered');?>
	@endif
	<section class="vacancy-section">
		<div class="container">
			<div class="ip-list-item">
				<div class="row">
					<div class="col-md-10">
						<div class="clearfix">
							<h3>{{$career->title}}</h3>
							<label class="info">{{$career->cat_title}}</label>
						</div>
						<p class="location">{{$career->location}}</p>
					</div>
					<div class="col-md-2">
						<p class="vacancy-date">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$career->created_at)->format('F j Y') }}</p>
					</div>
				</div>
			</div>

			<div class="vacancy-details">
        {!!$career->description!!}

				<div class="btn-container">
					<a href="{{url("linkedIN/login")."/".$career->id}}" class="btn borderd">Apply With Linkedin</a>
					<a href="{{url("careers/apply")."/".$career->id}}" class="btn">Apply Now</a>
				</div>
			</div>
		</div>
	</section>
@stop
