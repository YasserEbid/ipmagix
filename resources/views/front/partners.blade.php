@extends('front.layout')
@section('meta')
<title>Technology Partners</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta property="og:title" content="Technology Partners">
<meta property="og:description" content="">
<meta property="twitter:title" content="Technology Partners">
<meta property="twitter:description" content="">
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url('assets/front/images/headers/partners.jpg')}}" alt="">
    <div>
        <h1>Technology Partners</h1>
    </div>
</section>
<?php if (isset($partners) && $partners->count() > 0) { ?>
    <section class="partners">
        <div class="container">
            <h2>Our Partners</h2>
            <div class="row">
                <?php
                $delay = 200;
                $partnercount = 1;
                ?>
                @foreach($partners as $key => $partner)
                <?php if ($partnercount % 2 != 0) {
                    ?>
                    <div class="row">
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="partner wow fadeInRight" data-wow-delay="{{$delay}}ms" data-wow-duration="1000ms">
                            <div class="image-container">
                                <img src="{{url('uploads/'.$partner->media->media_path)}}" alt="">
                            </div>
                            <div class="">
                                <h3>{{$partners_lang[$key]->name}} </h3>
                                 {!! $partners_lang[$key]->description !!}
                                <a href="#">Read More <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <?php if ($partnercount % 2 == 0) { ?>
                    </div>
                    <?php
                }
                $partnercount++;
                $delay += 200;
                ?>
                @endforeach
            </div>
        </div>
    </section>
<?php } ?>
@stop
