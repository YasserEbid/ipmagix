@extends('front.layout')
@section('meta')
<title>Become A Partner</title>
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url('assets/front/images/headers/become-partner.jpg')}}" alt="">
    <div>
        <h1>Become A Partner</h1>
    </div>
</section>

<section class="form-section">
    <div class="container">
        <h2>Let`s Work Together</h2>
        @if(Session::has('partnerRequest'))
        <?php $redirect_msg = session()->pull('partnerRequest'); ?>
        <div class="alert alert-success">
            <strong>Success! </strong> {{$redirect_msg[0]}}
        </div>
        <?php session()->forget('partnerRequest'); ?>
        @endif

        <form class="" action="{{url('partners/become-partner')}}" method="post" id="partner_request">
            <div class="form-group row">
                <div class="col-sm-6 @if ($errors->has('name')) has-error @endif">
                    <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Your Name">
                    @if ($errors->has('name')) <p class="help-block" style='color:red;'>{{ $errors->first('name') }}</p> @endif
                </div>
                <div class="col-sm-6 @if ($errors->has('companyName')) has-error @endif">
                    <input type="text" class="form-control" name="companyName" value="{{old('companyName')}}" placeholder="Company Name">
                    @if ($errors->has('companyName')) <p class="help-block" style='color:red;'>{{ $errors->first('companyName') }}</p> @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 @if ($errors->has('email')) has-error @endif">
                    <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email Address">
                    @if ($errors->has('email')) <p class="help-block" style='color:red;'>{{ $errors->first('email') }}</p> @endif
                </div>
                <div class="col-sm-6 @if ($errors->has('phone')) has-error @endif">
                    <input type="text" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Phone Number">
                    @if ($errors->has('phone')) <p class="help-block" style='color:red;'>{{ $errors->first('phone') }}</p> @endif
                </div>
            </div>
            <div class="form-group @if ($errors->has('country')) has-error @endif">
                <select class="form-control" name="countries_names">
                    <option value="0">Choose Country</option>
                    @foreach($country_names as $key => $country_name)
                    <option value="{{$country_name->id}}">{{$country_name->name}}</option>
                    @endforeach
                </select>
<!-- <input type="text" class="form-control" name="country" value="{{old('country')}}" placeholder="Country"> -->
                @if ($errors->has('country')) <p class="help-block" style='color:red;'>{{ $errors->first('country') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('subject')) has-error @endif">
                <input type="text" class="form-control" name="subject" value="{{old('subject')}}" placeholder="Subject">
                @if ($errors->has('subject')) <p class="help-block" style='color:red;'>{{ $errors->first('subject') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('comment')) has-error @endif">
                <textarea name="comment" rows="10" class="form-control" placeholder="Comment">{{old('comment')}}</textarea>
                @if ($errors->has('comment')) <p class="help-block" style='color:red;'>{{ $errors->first('comment') }}</p> @endif
            </div>
            <div class="btn-container">
                <input type="submit" class="btn" name="" value="Send Message">
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $("#partner_request").submit(function (e) {
        $("#partner_request .form-control").each(function () {
            if ($(this).val() == "") {
//                $(this).css("border-color", "red");
                if (!$(this).next("p").length) {
                    $(this).parent().append("<p style='color:red;'>This field is required.</p>");
                } else {
                    $(this).next("p").show();
                }
                e.preventDefault();
            }
        });
    });
    $("#partner_request .form-control").each(function () {
        $(this).keypress(function () {
//            $(this).css("border-color", "#e0e0e0");
            $(this).next("p").hide();
        });
    });
</script>
@stop
