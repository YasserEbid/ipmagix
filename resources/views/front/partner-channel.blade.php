@extends('front.layout')
@section('meta')
<title>Channel Partners</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta property="og:title" content="Channel Partners">
<meta property="og:description" content="">
<meta property="twitter:title" content="Channel Partners">
<meta property="twitter:description" content="">
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url('assets/front/images/headers/partners-channel.jpg')}}" alt="">
    <div>
        <h1>Channel Partners</h1>
    </div>
</section>

<section class="bio-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5 wow bounceInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                <h2>Customer Satisfaction Is Our Number One Target, But We Cannot Do It Without Our Channel Partners</h2>
            </div>
            <div class="col-md-6 col-md-offset-1 wow bounceInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                <p style="max-height:205px !important;">As part of a global network, we partner with world-class system integrators that indirectly sell our products. The IPMagiX Partner program is designed to reward your loyalty as an esteemed Business Partner. Our programs are designed to drive our mutual success in today’s increasingly competitive market, and our dedication begins with robust sales and marketing tools to better enable your business and training to increase your relevancy in the marketplace, and profitability programs to improve your bottom line and reward your loyalty.
                </p>
            </div>
        </div>
    </div>
</section>

<?php if (isset($partners) && $partners->count() > 0) { ?>
    <section class="partners gray">
        <div class="container">
            <h2>Our Partners</h2>
            <div class="row">
                <?php $delay = 200; ?>
                @foreach($partners as $key => $partner)
                <div class="col-md-6">
                    <div class="partner wow fadeInRight" data-wow-delay="{{$delay}}ms" data-wow-duration="1000ms">
                        <div class="image-container">
                            <img src="{{url('uploads/'.$partner->media->media_path)}}" alt="">
                        </div>
                        <div class="">
                            <h3>{{$partners_lang[$key]->name}} </h3>
                            <p> {!! $partners_lang[$key]->description !!} </p>
                            <a href="partner-channel.php">Read More <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <?php $delay += 200; ?>
                </div>
                @endforeach
            </div>
        </div>
    </section>
<?php } ?>

<section class="partners-work wow fadeIn" data-wow-delay="800ms" data-wow-duration="1000ms">
    <div class="container">
        <div class="text-center">
            <h2>Let's Work Together</h2>
            <p style="max-width:785px !important;">Since our business model entails rewarding partnerships, you profit a great deal from the packages and benefits that we offer, including but not limited to: sales incentive programs, deal closure support and POCs, endless revenue streams, dedicated BDM & support personnel, training sessions in sales, marketing & support, value added end-to-end solution benefits and joint marketing efforts. Join our partner program and be part of our innovative journey.
            </p>
            <a href="{{url('partners/become-partner')}}" class="btn borderd">Become A Partner</a>
        </div>
    </div>
</section>
@stop
