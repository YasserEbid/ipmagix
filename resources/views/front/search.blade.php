@extends('front.layout')
@section('meta')
<title>Search</title>
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url('assets/front/images/headers/search.jpg')}}" alt="">
    <div>
        <h1>Search</h1>
    </div>
</section>

<section class="search-result">
    <div class="container">
        <h2>Search Results For “{{$search_word}}”</h2>
        <div class="ip-list">
            @if(!empty($notfound))
            {!!$notfound!!}
            @else
            @foreach($posts as $key => $post)
            <div class="ip-list-item">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <h3>{{$post->post_title}}</h3>
                        </div>
                        <p class="section-found">In {{$post->parent_name}}</p>
                    </div>
                    <div class="col-sm-3">
                        <?php if($post->parent_name == "News"){?>
                        <a href="{{url('news-details/'.$post->post_id)}}" target="_blank" class="btn borderd">Read More</a>
                        <?php }else if($post->parent_name == "About us"){?>
                        <a href="{{url('about-us')}}" target="_blank" class="btn borderd">Read More</a>
                        <?php }else{?>
                        <a href="{{url($post->parent_name.'/solutions-details/'.$post->post_id)}}" target="_blank" class="btn borderd">Read More</a>
                        <?php }?>
                    </div>
                </div>
            </div>
            @endforeach
            @foreach($categories as $key => $category)
            <div class="ip-list-item">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="clearfix">
                            <h3>{{$category->name}}</h3>
                        </div>
                        <p class="section-found">In {{$category->parent}}</p>
                    </div>
                    <div class="col-sm-3">
                        <a href="{{url('solution/'.$category->cat_id)}}" target="_blank" class="btn borderd">Read More</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
@stop
