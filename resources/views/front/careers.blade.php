@extends('front.layout')
@section('meta')
<title>Careers</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta property="og:title" content="Careers">
<meta property="og:description" content="">
<meta property="twitter:title" content="Careers">
<meta property="twitter:description" content="">
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url("assets/front/images/headers/careers.jpg")}}" alt="">
    <div>
        <h1>Careers</h1>
    </div>
</section>

<section class="bio-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5 wow bounceInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                <h2>IPMagix employs talented and aspiring specialists who aim to create world-class products.</h2>
            </div>
            <div class="col-md-6 col-md-offset-1 wow bounceInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                <p style="max-height: 330px !important;">As a company, We innovate everywhere to create fresh ideas and possibilities. We make a meaningful difference that will benefit everyone - our people, our partners, and our customers, so talent and passion power everything we do.
 
We're looking for the kind of people who take smart risks, inspire their colleagues, and are committed to making an impact.
 
If you share our beliefs, let’s talk - We’re expanding our teams across the globe and we’re on the lookout for bright and talented individuals.</p>
            </div>
        </div>
    </div>
</section>

<section class="careers-section wow fadeIn" data-wow-delay="0ms" data-wow-duration="1000ms">
    <div class="container">
        <div class="row">
            <div class="col-md-6 wow bounceInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                <div class="careers-services">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="careers-service">
                                <img src="{{url("assets/front/images/icons/award.png")}}" alt="">
                                <h3>Fascinating Projects</h3>
                                <p>Our solutions change the way our customers work or live, but our edge doesn't come from technology alone.</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="careers-service">
                                <img src="{{url("assets/front/images/icons/crown.png")}}" alt="">
                                <h3>Boost Your Skills</h3>
                                <p>Whether you create technology solutions that redefine business or build connections that strengthen the community, you can make it happen at IPMagiX !</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="careers-service">
                                <img src="{{url("assets/front/images/icons/heart.png")}}" alt="">
                                <h3>Great work environment</h3>
                                <p>As an employee at IPMagiX, you will work with a team of dedicated professionals and continually challenged to achieve your best.</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="careers-service">
                                <img src="{{url("assets/front/images/icons/bowl.png")}}" alt="">
                                <h3>World Class Products</h3>
                                <p>We take pride in our employees’ creativity and relentless drive to innovate. In turn, our diverse team of leaders has made us a force in the industry</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 image-side wow bounceInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                <div class="image-container">
                    <img src="{{url("assets/front/images/careers/career.jpg")}}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="vacancies">
    <div class="container">
        <h2>Current Vacancies</h2>
        <div class="ip-list">
            <?php
            if ($careers->count() > 0) {
                foreach ($careers as $career) {
                    ?>
                    <div class="ip-list-item">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="clearfix">
                                    <h3>{{$career->title}}</h3>
                                    <label class="info">{{$career->cat_title}}</label>
                                </div>
                                <p class="location">{{$career->location}}</p>
                            </div>
                            <div class="col-md-2">
                                <a href="careers/show/{{$career->id}}" class="btn borderd">Read More</a>
                            </div>
                        </div>
                    </div>
                <?php
                }
            }else{
                ?>
            <p>Send us your CV at <a href="mailto:info@ipmagix.com">info@ipmagix.com</a></p>
                    <?php
            }
?>
        </div>
    </div>
</section>
@stop
