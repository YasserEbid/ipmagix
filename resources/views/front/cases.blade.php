@extends('front.layout')
@section('meta')
	<title>Cases</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="Cases">
	<meta property="og:description" content="">
	<meta property="twitter:title" content="Cases">
	<meta property="twitter:description" content="">
@endsection
@section('content')
	<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
		<img src="{{url("assets/front/images/headers/cases.jpg")}}" alt="">
		<div>
			<h1>Cases</h1>
		</div>
	</section>
	<section class="cases-list">
		<div class="container">
			<h2>Featured Cases</h2>
			<div class="cases-items">
				<div class="row">
					<?php if($cases->count() > 0){
						$delay = 100;
						foreach($cases as $case){
								if($delay == 400){
									$delay = 100;
								}
							?>
					<div class="col-md-3 col-sm-6 wow fadeInUp"  data-wow-delay="{{$delay}}ms" data-wow-duration="1000ms">
						<div class="case-item">
							<img src="{{url("uploads/".$case->media_path)}}" alt="">
							<h3>{{$case->name}}</h3>
							<p>{{$case->brief}}</p>
							<a href="#{{$case->id}}_case" class="readMore">Read More <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					</div>
					<?php $delay +=100; }}?>
				</div>
			</div>
		</div>
	</section>
<?php if($cases->count() > 0){
foreach($cases as $onecase){
	?>
	<div class="case-details popup" id="{{$onecase->id}}_case">
		<a href="#" class="pop-close"></a>
		{!!$onecase->description !!}
	</div>
	<?php }}?>
@stop
