@extends('front.layout')
@section('meta')
	<title>{{$category->Lang->name}}</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="{{$category->Lang->name}}">
	<meta property="og:description" content="">
	<meta property="twitter:title" content="{{$category->Lang->name}}">
	<meta property="twitter:description" content="">
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="@if(count($category->media) > 0) {{url('uploads/'.$category->media->media_path)}} @endif" alt="">
    <div>
        <h1>{{$category->Lang->name}}</h1>
    </div>
</section>

<section class="news-list">
    <div class="container">
        @foreach($posts as $key => $post)
        @if($post->lang->lang == $current_lang)
        <div class="news-item wow fadeIn" data-wow-delay="400ms" data-wow-duration="1000ms">
            <div class="row">
                <div class="col-md-7">
                    <div class="image-container">
                        <img src="@if(count($post->introImage) > 0) {{url('uploads/'.$post->introImage->media_path)}} @endif" alt="">
                    </div>
                </div>
                <div class="col-md-5">
                    <label class="news-date">
                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$post->created_date)->format('F j Y') }}
                    </label>
                    <h3>{{$post->lang->post_title}}</h3>
                    <p>{!!$post->lang->post_excerpt!!}</p>
                    <a href="{{ url('news-details/'.$post->post_id) }}">Read More <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</section>
@stop
