@extends('front.layout')
@section('meta')
<title>Request A Demo</title>
@endsection
@section('content')

<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url('assets/front/images/headers/request-demo.jpg')}}" alt="">
    <div>
        <h1>Discover More</h1>
    </div>
</section>

<section class="form-section">
    <div class="container">
        <h2>Request A Demo</h2>
        @if(session()->has('demoRequest'))
        <div class="alert alert-success">
            <strong>Success! </strong> {{session()->get('demoRequest')}}
            <!-- Google Code for IP Magix Conversions Conversion Page -->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 861403832;
            var google_conversion_language = "en";
            var google_conversion_format = "3";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "Q0z7CLj_lm4QuPXfmgM";
            var google_remarketing_only = false;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/861403832/?label=Q0z7CLj_lm4QuPXfmgM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript>
            <!-- Facebook Pixel Code -->
            <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '411868832538381'); // Insert your pixel ID here.
            fbq('track', 'PageView');
            </script>
            <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=411868832538381&ev=PageView&noscript=1"
            /></noscript>
            <!-- DO NOT MODIFY -->
            <!-- End Facebook Pixel Code -->
        </div>
        @elseif(session()->has('sentRequest'))
        <div class="alert alert-info">
            <strong>Success! </strong> {{session()->get('sentRequest')}}
        </div>
        @endif
        <form class="" action="{{url('demo-requests/'.$id)}}" method="post" id="demo_request">
            <div class="form-group row">
                <div class="col-sm-6 @if ($errors->has('name')) has-error @endif">
                    <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Your Name">
                    @if ($errors->has('name')) <p class="help-block"style='color:red;'>{{ $errors->first('name') }}</p> @endif
                </div>
                <div class="col-sm-6 @if ($errors->has('email')) has-error @endif">
                    <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email Address">
                    @if ($errors->has('email')) <p class="help-block" style='color:red;'>{{ $errors->first('email') }}</p> @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 @if ($errors->has('subject')) has-error @endif">
                    <input type="text" class="form-control" name="subject" value="{{old('subject')}}" placeholder="Subject">
                    @if ($errors->has('subject')) <p class="help-block" style='color:red;'>{{ $errors->first('subject') }}</p> @endif
                </div>
                <div class="col-sm-6 @if ($errors->has('product')) has-error @endif">
                    <select class="form-control" name="product_name">
                      <?php $no_repeat = []; ?>
                        @foreach($cats_products as $key => $cat_product)
                          @if(array_search(strtolower($cat_product->post_title),$no_repeat) === false)
                          <option value="{{$cat_product->post_id}}" @if($cat_product->post_id == $id) selected @endif >{{$cat_product->post_title}}</option>
                          <?php array_push($no_repeat, strtolower($cat_product->post_title)); ?>
                          @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group @if ($errors->has('comment')) has-error @endif">
                <textarea name="comment" rows="10" class="form-control" placeholder="Comment">{{old('comment')}}</textarea>
                @if ($errors->has('comment')) <p class="help-block" style='color:red;'>{{ $errors->first('comment') }}</p> @endif
            </div>
            <input hidden name="request_type" value="request_page">

            <input type="hidden" name="utm_source" value="@if(isset($_GET['utm_source'])) {{$_GET['utm_source']}} @else {{'organic'}} @endif">
            <input type="hidden" name="utm_medium" value="@if(isset($_GET['utm_medium'])) {{$_GET['utm_medium']}} @else {{'organic'}} @endif">
            <input type="hidden" name="utm_term" value="@if(isset($_GET['utm_term'])) {{$_GET['utm_term']}} @else {{'organic'}} @endif">
            <input type="hidden" name="utm_content" value="@if(isset($_GET['utm_content'])) {{$_GET['utm_content']}} @else {{'organic'}} @endif">
            <input type="hidden" name="utm_campaign" value="@if(isset($_GET['utm_campaign'])) {{$_GET['utm_campaign']}} @else {{'organic'}} @endif">
            <div class="btn-container">
                <input type="submit" class="btn" name="" value="Send Message">
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $("#demo_request").submit(function (e) {
        $("#demo_request .form-control").each(function () {
            if ($(this).val() == "") {
//                $(this).css("border-color", "red");
                if (!$(this).next("p").length) {
                    $(this).parent().append("<p style='color:red;'>This field is required.</p>");
                } else {
                    $(this).next("p").show();
                }
                e.preventDefault();
            }
        });
    });
    $("#demo_request .form-control").each(function () {
        $(this).keypress(function () {
//            $(this).css("border-color", "#e0e0e0");
            $(this).next("p").hide();
        });
    });
</script>
@stop
