@extends('front.layout')
@section('meta')
<title>Apply Now</title>
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url("assets/front/images/headers/apply.jpg")}}" alt="">
    <div>
        <h1>Apply Now</h1>
    </div>
</section>

<section class="form-section">
    <div class="container">
        <h2>Join Our Fabulous Team</h2>
        <?php if (session("message")) { ?>
            <div class="alert alert-success">
                <strong>Success!</strong> {{session("message")}}
            </div>
        <?php } elseif (session("error")) { ?>
            <div class="alert alert-danger">
                <strong>Alert!</strong> {{session("error")}}
            </div>
        <?php } ?>
        <form class="" id="apply_r" action="careers/apply/{{$id}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="vacancy_id" value="{{$id}}">
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="name" value="" placeholder="Your Name">
                    @if ($errors->has('name')) <p class="help-block" style='color:red;'>{{ $errors->first('name') }}</p> @endif
                </div>
                <div class="col-sm-6">
                    <input type="email" class="form-control" name="email" value="" placeholder="Email Address">
                    @if ($errors->has('email')) <p class="help-block" style='color:red;'>{{ $errors->first('email') }}</p> @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" value="" placeholder="Phone Number">
                    @if ($errors->has('phone')) <p class="help-block" style='color:red;'>{{ $errors->first('phone') }}</p> @endif
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="job_title" value="" placeholder="Job Title">
                    @if ($errors->has('job_title')) <p class="help-block" style='color:red;'>{{ $errors->first('job_title') }}</p> @endif
                </div>
            </div>
            <div class="form-group">
                <textarea name="cover_letter" rows="10" class="form-control" placeholder="Cover Letter"></textarea>
                @if ($errors->has('cover_letter')) <p class="help-block" style='color:red;'>{{ $errors->first('cover_letter') }}</p> @endif
            </div>
            <div class="form-group uploader clearfix">
                <label for="uploadFile" class="btn">Upload CV</label>
                <p>No File Selected</p>
                <input id="uploadFile" class="form-control hide" type="file" name="cv">
                @if ($errors->has('cv')) <p class="help-block" style='color:red;'>{{ $errors->first('cv') }}</p> @endif
            </div>
            <div class="btn-container">
                <input type="submit" class="btn" name="" value="Send Application">
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $("#apply_r").submit(function (e) {
        $("#apply_r .form-control").each(function () {
            if ($(this).val() == "") {
//                $(this).css("border-color", "red");
                if (!$(this).next("p").length) {
                    $(this).parent().append("<p style='color:red;'>This field is required.</p>");
                } else {
                    $(this).next("p").show();
                }
                e.preventDefault();
            }
        });
    });
    $("#apply_r .form-control").each(function () {
        $(this).keypress(function () {
//            $(this).css("border-color", "#e0e0e0");
            $(this).next("p").hide();
        });
    });
</script>
@stop
