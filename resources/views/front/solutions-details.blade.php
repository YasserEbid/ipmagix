@extends('front.layout')
@section('meta')
<title>{{$posts_lang->post_title}}</title>
<meta name="description" content="{{$posts_lang->meta_description}}">
<meta name="keywords" content="{{$posts_lang->meta_keywords}}">
<meta property="og:title" content="{{$posts_lang->meta_title}}">
<meta property="og:description" content="{{$posts_lang->meta_description}}">
<meta property="twitter:title" content="{{$posts_lang->meta_title}}">
<meta property="twitter:description" content="{{$posts_lang->meta_description}}">
@endsection
@section('content')
<?php if(isset($_GET["utm_source"])){$campaign_parameters = "?utm_source=".$_GET["utm_source"]."&utm_medium=".$_GET["utm_medium"]."&utm_term=".$_GET["utm_term"]."&utm_content=".$_GET["utm_content"]."&utm_campaign=".$_GET["utm_campaign"];}else{$campaign_parameters ="";}?>
<section class="request-demo">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 text-left">
                <h2>Want To Discover More?</h2>
            </div>
            <div class="col-sm-7 text-center">
                <p>If you would like to receive a demo of the product, request it below or reach out to us if you have any inquiries.</p>
            </div>
            <div class="col-sm-2 text-right">
                <a href="{{url('demo-requests/'.$post->post_id).$campaign_parameters}}" class="btn">Request A Demo</a>
            </div>
        </div>
    </div>
</section>
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="@if(count($post->media) > 0) {{url('uploads/'.$post->media->media_path)}} @endif" alt="">
    <div>
        <h1>{{$posts_lang->post_title}}</h1>
    </div>
</section>

<section class="bio-section">
    <div class="container">
        <div class="row">
            <?php if ($post->logo_id == "" || $post->logo_id == 0 || is_null($post->logo_id)) {
                ?>
                <div class="col-md-5 wow bounceInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <h2>{{$posts_lang->post_subtitle}}</h2>
                </div>
                <div class="col-md-6 col-md-offset-1 wow bounceInRight" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <p>{!!$posts_lang->post_content!!}</p>
                </div>
            <?php } else { ?>
                <div class="col-md-2 wow bounceInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <div class="image-container">
                        <img src="{{url('uploads/'.$post->logoImage->media_path)}}" alt="">
                    </div>
                </div>
                <div class="col-md-4 wow bounceInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <h2>{{$posts_lang->post_subtitle}}</h2>
                </div>
                <div class="col-md-6 wow bounceInRight" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <p>{!!$posts_lang->post_content!!}</p>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

@if(count($post->video) > 0)
<section class="video-section">
    <div class="container">
        <h2>{{$posts_lang->post_title}} Solution</h2>
        <div class="video-container wow fadeIn" data-wow-delay="300ms" data-wow-duration="1000ms">
            <div class="video-holder">
                <div class="video-over">
                    <img src="@if(count($post->videoCover) > 0) {{url('uploads/'.$post->videoCover->media_path)}} @endif" alt="">
                    <label></label>
                </div>
                <iframe id="solution-video" src="{{str_replace("watch?v=","embed/",$post->video->media_path)."?rel=0"}}" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@endif

<section id="WhyChoose" class="solution-details">
    <div class="container">
        <h2>Why Choose {{$posts_lang->post_title}}?</h2>
        <div class="why">
            <div class="row">
                <div class="col-md-6 wow bounceInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <img src="@if(count($post->internalImage) > 0) {{url('uploads/'.$post->internalImage->media_path)}} @endif" alt="">
                </div>
                <div class="col-md-6 wow bounceInRight" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <div class="tabs clearfix">
                        @foreach($tab_lang as $key => $tab)
                        @if($key == 0)
                        <a href="#{{$tab->title}}" class="active">{{$tab->title}}</a>
                        @else
                        <a href="#{{$tab->title}}">{{$tab->title}}</a>
                        @endif
                        @endforeach
                        <!-- <a href="#Benefits" class="active">Benefits</a>
                        <a href="#Features">Features</a> -->
                    </div>
                    <div class="tabs-container">
                        @foreach($tab_lang as $key => $tab)
                        @if($key == 0)
                        <div id="{{$tab->title}}" class="tab-content active">
                            <!-- <h3>posts_lang->post_title Great tab->title</h3> -->
                            @else
                            <div id="{{$tab->title}}" class="tab-content">
                                <!-- <h3>posts_lang->post_title Great tab->title</h3> -->
                                @endif
                                {!! $tab->description !!}

                            </div>
                            <!-- </div> -->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            </section>

            <section id="RequestDemo" class="discover">
                <div class="container wow fadeIn" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <h2>Want To Discover More?</h2>
                    <p>If you would like to receive a demo of the product, request it below or reach out to us if you have any inquiries.</p>
                    <!-- <div class="btn-container">
                        <a href="{{url('demo-requests/'.$post->post_id).$campaign_parameters}}" class="btn borderd">Request A Demo</a>
                        <a href="contact-us" class="btn">Let’s Talk</a>
                    </div> -->
                    <section class="form-section">
                      @if(session()->has('demoRequest'))
                      <div class="alert alert-success">
                          <strong>Success! </strong> Your request has been sent seccessfully.
                          <!-- Google Code for IP Magix Conversions Conversion Page -->
                        <script type="text/javascript">
                          /* <![CDATA[ */
                          var google_conversion_id = 861403832;
                          var google_conversion_language = "en";
                          var google_conversion_format = "3";
                          var google_conversion_color = "ffffff";
                          var google_conversion_label = "Q0z7CLj_lm4QuPXfmgM";
                          var google_remarketing_only = false;
                          /* ]]> */
                          </script>
                          <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                          </script>
                          <noscript>
                          <div style="display:inline;">
                          <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/861403832/?label=Q0z7CLj_lm4QuPXfmgM&amp;guid=ON&amp;script=0"/>
                          </div>
                          </noscript>
                          <!-- Facebook Pixel Code -->
                          <script>
                          !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                          n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                          n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                          t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                          document,'script','https://connect.facebook.net/en_US/fbevents.js');
                          fbq('init', '411868832538381'); // Insert your pixel ID here.
                          fbq('track', 'PageView');
                          </script>
                          <noscript><img height="1" width="1" style="display:none"
                          src="https://www.facebook.com/tr?id=411868832538381&ev=PageView&noscript=1"
                          /></noscript>
                          <!-- DO NOT MODIFY -->
                          <!-- End Facebook Pixel Code -->
                      </div>
                      @elseif(session()->has('sentRequest'))
                      <div class="alert alert-warning">
                          <strong>Worning! </strong> {{session()->get('sentRequest')}}
                      </div>
                      @endif
                      <form class="" action="{{url('demo-requests/'.$posts_lang->post_id)}}" method="post" id="demo_request">
                        <div class="form-group row">
                          <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Your Name">
                          </div>
                          <div class="col-sm-6">
                            <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email Address">
                          </div>
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" name="subject" value="{{old('subject')}}" placeholder="Subject">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" name="company" value="{{old('company')}}" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                          <textarea name="comment" rows="10" class="form-control" placeholder="Comment">{{old('comment')}}</textarea>
                        </div>
                        <input hidden name="product_name" value="{{$posts_lang->post_id}}">
                        <input hidden name="request_type" value="post_details">

                        <input type="hidden" name="utm_source" value="@if(isset($_GET['utm_source'])) {{$_GET['utm_source']}} @else {{'organic'}} @endif">
                        <input type="hidden" name="utm_medium" value="@if(isset($_GET['utm_medium'])) {{$_GET['utm_medium']}} @else {{'organic'}} @endif">
                        <input type="hidden" name="utm_term" value="@if(isset($_GET['utm_term'])) {{$_GET['utm_term']}} @else {{'organic'}} @endif">
                        <input type="hidden" name="utm_content" value="@if(isset($_GET['utm_content'])) {{$_GET['utm_content']}} @else {{'organic'}} @endif">
                        <input type="hidden" name="utm_campaign" value="@if(isset($_GET['utm_campaign'])) {{$_GET['utm_campaign']}} @else {{'organic'}} @endif">
                        <div class="btn-container">
                          <input type="submit" class="btn" name="" value="Send Message">
                        </div>
                      </form>
                    </section>
                </div>
            </section>
            <?php
            if (count($posts_ids) > 0) {
                $index = array_search($post->post_id, $posts_ids);
            }
            ?>
            <section class="solution-nav">
                <div class="container">
                    <div class="solution-nav-container">
                        <div class="row">
                            <div class="col-xs-5 text-left">
                                <?php if (isset($index) && $index > 0) { ?>
                                    <a href="<?= url($cat_name->name.'/solutions-details/' . $posts_ids[$index - 1]) ?>" class="prev"><i class="circle"><i class="fa arrow fa-angle-left"></i></i><span>Previous</span></a>
                                <?php } ?>
                            </div>
                            <div class="col-xs-2 text-center">
                                <a href="<?= url('solution/' . $cat_name->cat_id) ?>" title="{{$cat_name->name}}">
                                    <table>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </a>
                            </div>
                            <div class="col-xs-5 text-right">
                                <?php if (isset($index) && (count($posts_ids) - 1) - $index > 0) { ?>
                                    <a href="<?= url($cat_name->name.'/solutions-details/' . $posts_ids[$index + 1]) ?>" class="next"><span>Next</span><i class="circle"><i class="fa arrow fa-angle-right"></i></i></a>
                                        <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <script type="text/javascript">
                $("#demo_request").submit(function (e) {
                    $("#demo_request .form-control").each(function () {
                        if ($(this).val() == "") {
                            if (!$(this).next("p").length) {
                                $(this).parent().append("<p style='color:red;'>This field is required.</p>");
                            } else {
                                $(this).next("p").show();
                            }
                            e.preventDefault();
                        }
                    });
                });
                $("#demo_request .form-control").each(function () {
                    $(this).keypress(function () {
                        $(this).next("p").hide();
                    });
                });
            </script>
            @stop
