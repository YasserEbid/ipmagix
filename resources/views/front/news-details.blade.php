@extends('front.layout')
@section('meta')
	<title>{{$post_lang->post_title}}</title>
	<meta name="description" content="{{$post_lang->meta_description}}">
	<meta name="keywords" content="{{$post_lang->meta_keywords}}">
	<meta property="og:title" content="{{$post_lang->meta_title}}">
	<meta property="og:description" content="{{$post_lang->meta_description}}">
	<meta property="twitter:title" content="{{$post_lang->meta_title}}">
	<meta property="twitter:description" content="{{$post_lang->meta_description}}">
@endsection
@section('content')
	<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
		<img src="@if(count($post->media) > 0) {{url('uploads/'.$post->media->media_path)}} @endif" alt="">
		<div></div>
	</section>

	<section class="news-details-section">
		<div class="container">
			<div class="news-details">
				<label class="news-date">
					{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$post->created_date)->format('F j Y') }}
				</label>
				<h3>{{$post_lang->post_title}}</h3>

          {!!$post_lang->post_content!!}
			</div>
		</div>
	</section>
@stop
