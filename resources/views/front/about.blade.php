@extends('front.layout')
@section('meta')
	<title>{{$aboutPost_lang->post_title}}</title>
	<meta name="description" content="{{$aboutPost_lang->meta_description}}">
	<meta name="keywords" content="{{$aboutPost_lang->meta_keywords}}">
	<meta property="og:title" content="{{$aboutPost_lang->meta_title}}">
	<meta property="og:description" content="{{$aboutPost_lang->meta_description}}">
	<meta property="twitter:title" content="{{$aboutPost_lang->meta_title}}">
	<meta property="twitter:description" content="{{$aboutPost_lang->meta_description}}">
@endsection
@section('content')
<?php if(isset($aboutPost_lang)){?>
	<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
		<img src="{{url('uploads/'.$about_post->media->media_path)}}" alt="">
		<div>
			<h1>{{$aboutPost_lang->post_title}}</h1>
		</div>
	</section>

	<section class="bio-section">
		<div class="container">
			<div class="row">
				<div class="col-md-5 wow bounceInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
					<h2>{{$aboutPost_lang->post_subtitle}}</h2>
				</div>
				<div class="col-md-6 col-md-offset-1 wow bounceInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
					<p>{!!$aboutPost_lang->post_excerpt!!}</p>
				</div>
			</div>
		</div>
	</section>
	{!!$aboutPost_lang->post_content!!}
	<?php }else{?>
		<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
			<img src="{{url("assets/front/images/headers/about.jpg")}}" alt="">
			<div>
				<h1>About Us</h1>
			</div>
		</section>
		<?php }?>
@stop
