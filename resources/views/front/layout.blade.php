<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <base href='<?= URL::to('./') ?>' />
        <script>var base = '<?= URL::to('./') ?>';</script>
        <meta charset="utf-8">
        @yield('meta')
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{url("assets/front/css/animate.css")}}">
        <link rel="stylesheet" href="{{url("assets/front/css/revolution-slider.css")}}">
        <link rel="stylesheet" href="{{url("assets/front/css/slider/owl.carousel.min.css")}}">
        <link href="{{url("assets/front/css/screen.css")}}" media="all" rel="stylesheet" type="text/css" />
        <!--<link href="{{url("assets/front/css/myscreen.css")}}" media="all" rel="stylesheet" type="text/css" />-->

        <link href="{{url("assets/front/images/logo.png")}}" rel="icon">

        <script src="{{url("assets/front/js/jquery-1.11.3.min.js")}}"></script>
        <script src="{{url("assets/front/js/revolution.min.js")}}"></script>
        <script src="{{url("assets/front/js/wow.min.js")}}"></script>
        <script src="{{url("assets/front/js/owl.carousel.min.js")}}"></script>
        <script src="{{url("assets/front/js/custom.js")}}"></script>

                <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
                <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
    </head>
    <body>

        <div class="cssload-loader">
            <div class="cssload-square cssload-square--main">
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
                <div class="cssload-square cssload-square--mini"></div>
            </div>
        </div>
    <header>
        <div class="container">
            <a href="{{url(".")}}" class="logo"><img src="{{url("assets/front/images/logo.png")}}" alt="IPMagiX"></a>
            <a href="#" class="nav-toggle"><span></span></a>
            <div class="search">
                <a href="#" class="search-btn"><i class="fa fa-search"></i></a>
            </div>
            <nav>
                <ul>
                    <?php for ($mcount = 0; $mcount < count($menusLinks[1]); $mcount++) {
                        ?>
                        <li><a href="{{($menusLinks[1][$mcount]['link_url'] == "")?"JavaScript::void(0)":$menusLinks[1][$mcount]['link_url']}}">{{$menusLinks[1][$mcount]['name']}}</a>
                            <?php
                            if (isset($menusLinks[1][$mcount]['subLinks'])) {
                                ?>
                                <i class="fa fa-angle-down"></i>
                                <ul>
                                    <?php for ($scount = 0; $scount < count($menusLinks[1][$mcount]['subLinks']); $scount++) { ?>
                                        <li>
                                            <a href="{{($menusLinks[1][$mcount]['subLinks'][$scount]['link_url'] == "")?"JavaScript::void(0)":$menusLinks[1][$mcount]['subLinks'][$scount]['link_url']}}">{{$menusLinks[1][$mcount]['subLinks'][$scount]['name']}}</a>
                                            <?php if (isset($menusLinks[1][$mcount]['subLinks'][$scount]['subLinks'])) {
                                                ?>
                                                <i class="fa fa-angle-down"></i>
                                                <ul>
                                                    <?php for ($sscount = 0; $sscount < count($menusLinks[1][$mcount]['subLinks'][$scount]['subLinks']); $sscount++) { ?>
                                                        <li><a href="{{($menusLinks[1][$mcount]['subLinks'][$scount]['subLinks'][$sscount]['link_url'] == "")?"JavaScript::void(0)":$menusLinks[1][$mcount]['subLinks'][$scount]['subLinks'][$sscount]['link_url']}}">{{$menusLinks[1][$mcount]['subLinks'][$scount]['subLinks'][$sscount]['name']}}</a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php }
                                            ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </li>
                    <?php }
                    ?>

                    <!-- end menu for loop -->
                </ul>
            </nav>
        </div>
    </header>
	<div class="search-container">
		<a href="#" class="search-close"></a>
		<div class="search-form">
			<label for="">Search:</label>
			<form action="{{url('search/')}}" method="get">
				<input type="text" name="searchText" id="searchText" value="">
				<!-- <input type="text" id="public_path" value="{{url('')}}" hidden> -->
				<button type="submit" name="searchbtn" id="searchbtn"><i class="fa fa-search"></i></button>
			</form>
		</div>
	</div>
    @yield('content')
    <footer>
        <div class="container">
            <div class="top-footer">
                <div class="row">
                    <div class="col-md-5" >
                        <h3>About IPMagiX</h3>

                        <p>{{$site_settings["About"]}}</p>
                    </div>
                    <div class="col-md-4" >
                        <h3>Helpful Links</h3>

                        <div class="row">
                            <div class="col-xs-6">
                                <ul>
                                    <li><a href="{{url("./")}}"><i class="fa fa-angle-right"></i> Home</a></li>
                                    <li><a href="{{url("cases")}}"><i class="fa fa-angle-right"></i> Cases</a></li>
                                    <li><a href="{{url("about-us")}}"><i class="fa fa-angle-right"></i> About</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-6">
                                <ul>
                                    <li><a href="{{url("partners/technology")}}"><i class="fa fa-angle-right"></i> Partners</a></li>
                                    <li><a href="{{url("news")}}"><i class="fa fa-angle-right"></i> News</a></li>
                                    <li><a href="{{url("contact-us")}}"><i class="fa fa-angle-right"></i> Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h3>Stay Up To Date</h3>

                        <form id="subscribeform" action="" method="post">
                            <p id="submessage" style="display:none;"></p>
                            <input type="text" name="name" value="" placeholder="Your Name" id="subscribername"/>
                            <input type="text" name="email" value="" placeholder="Email Address" id="subscriberemail"/>
                            <input type="submit" value="Subscribe" class="btn" />
                        </form>
                    </div>
                </div>
            </div>

            <div class="bottom-footer">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li><a href="{{$site_settings["Facebook"]}}" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{$site_settings["Twitter"]}}" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{$site_settings["Youtube"]}}" target="_blank" class="googlePlus"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="{{$site_settings["Linked_in"]}}" target="_blank" class="linkedIn"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <p>© <?php echo date("Y"); ?> IPMagix. Powered by <a href="http://media-sci.com/" target="_blank">MediaSci</a> <a href="http://media-sci.com/" target="_blank"><img src="{{url('assets/front/images/media-sci.png')}}" alt=""></a></p>
                    </div>
                </div>
            </div>
        </div>


    </footer>
    <a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
    <script type="text/javascript">
            $("#subscribeform").submit(function (e) {
                e.preventDefault();
                var subscriber_name = $("#subscribername").val();
                var subscriber_email = $("#subscriberemail").val();
                if (subscriber_name == "" || subscriber_email == "") {
                    $("#submessage").text("Name and Email are required .");
                    $("#submessage").fadeIn(500);
                    return false;
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: './subscribe',
                    data: {name: subscriber_name, email: subscriber_email},
                    method: "POST",
                    timeout: 10000,
                    beforeSend: function (xhr) {
                    },
                    success: function (res) {
                        if (res == 0) {
                            $("#submessage").text("Sorry , this email already subscribed.");
                            $("#submessage").fadeIn(500);
                        } else {
                            $("#submessage").text(res);
                            $("#submessage").fadeIn(500);
                        }
                        $("#subscribeform input[type=text]").each(function () {
                            $(this).val("");
                        });
                    }
                });
            });

    </script>
</body>
</html>
