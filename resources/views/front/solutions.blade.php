@extends('front.layout')
@section('meta')
<title>{{$category->Lang->name}}</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta property="og:title" content="{{$category->Lang->name}}">
<meta property="og:description" content="">
<meta property="twitter:title" content="{{$category->Lang->name}}">
<meta property="twitter:description" content="">
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="@if(count($category->media) > 0) {{url('uploads/'.$category->media->media_path)}} @endif" alt="">
    <div>
        <h1>{{$category->Lang->name}}</h1>
    </div>
</section>
<section class="bio-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5 wow bounceInLeft" data-wow-delay="0ms" data-wow-duration="1000ms">
                <h2>{{$category->Lang->sub_title}}</h2>
            </div>
            <div class="col-md-6 col-md-offset-1 wow bounceInRight" data-wow-delay="0ms" data-wow-duration="1000ms">
                {!!$category->Lang->description!!}
            </div>
        </div>
    </div>
</section>

@if(count($category->video) > 0)
<section class="video-section">
    <div class="container">
        <h2>IPMagiX {{$category->Lang->name}} Solution</h2>
        <div class="video-container wow fadeIn" data-wow-delay="300ms" data-wow-duration="1000ms">
            <div class="video-holder">
                <div class="video-over">
                    <img src="@if(count($category->videoCover) > 0) {{url('uploads/'.$category->videoCover->media_path)}} @endif" alt="">
                    <label></label>
                </div>
                <iframe id="solution-video" src="{{str_replace("watch?v=","embed/",$category->video->media_path)."?rel=0"}}" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@endif


<section class="solutions-items">
    <div class="container">
        <?php
        $display = 0;
        if (count($posts) > 0) {
            ?>
            <h2>{{$category->Lang->name}} Products</h2>
            <div class="solutions-list">
                <?php
                foreach ($posts as $key => $post) {
                    if ($post->lang->lang == $current_lang) {
                        if ($display == 0) {
                            ?>
                            <div class="solution-item left wow bounceInLeft" data-wow-delay="400ms" data-wow-duration="1000ms">
                                <?php
                                $display = 1;
                            } else {
                                ?>
                                <div class="solution-item right wow bounceInRight" data-wow-delay="400ms" data-wow-duration="1000ms">
                                    <?php
                                    $display = 0;
                                }
                                ?>
                                <div class="row">
                                    <?php if($post->logo_id != "" &&$post->logo_id != 0 && !is_null($post->logo_id)) { ?>
                                        <div class="col-md-4">
                                            <img src="@if(count($post->logoImage) > 0) {{url('uploads/'.$post->logoImage->media_path)}} @endif" alt="">
                                        </div>
                                        <div class="col-md-4">
                                            <img src="@if(count($post->introImage) > 0) {{url('uploads/'.$post->introImage->media_path)}} @endif" alt="">
                                        </div>
                                        <div class="col-md-4">
                                            <h3>{{$post->lang->post_title}}</h3>
                                            <p>{!!substr($post->lang->post_excerpt,0,112)."..."!!}</p>
                                            <a href="{{ url($category->Lang->name.'/solutions-details/'.$post->post_id) }}" class="btn">Read More</a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-md-6">
                                            <img src="@if(count($post->introImage) > 0) {{url('uploads/'.$post->introImage->media_path)}} @endif" alt="">
                                        </div>
                                        <div class="col-md-6">
                                            <h3>{{$post->lang->post_title}}</h3>
                                            <p>{!!substr($post->lang->post_excerpt,0,112)."..."!!}</p>
                                            <a href="{{ url($category->Lang->name.'/solutions-details/'.$post->post_id) }}" class="btn">Read More</a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>

            </div>
        </div>
</section>
@stop
