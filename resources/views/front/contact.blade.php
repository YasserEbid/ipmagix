@extends('front.layout')
@section('meta')
<title>Contact Us</title>
@endsection
@section('content')
<section class="header wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
    <img src="{{url("assets/front/images/headers/contact.jpg")}}" alt="">
    <div>
        <h1>Contact Us</h1>
    </div>
</section>

<section class="form-section">
    <div class="container">
        <h2>Drop Us A Line</h2>
        @if(isset($conmessage))
        <div class="alert alert-success">
            {!! $conmessage !!}
        </div>
        @endif
        <form class="" action="" method="post" id="contactform">
            <div class="form-group contactmessage">
                <p id="conmessage" class="contactmessage" style="display:none;"></p>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="name" value="" placeholder="Your Name">
                    @if ($errors->has('name')) <p class="help-block" style='color:red;'>{{ $errors->first('name') }}</p> @endif
                </div>
                <div class="col-sm-6">
                    <input type="email" class="form-control" name="email" value="" placeholder="Email Address">
                    @if ($errors->has('email')) <p class="help-block" style='color:red;'>{{ $errors->first('email') }}</p> @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="phone" value="" placeholder="phone">
                    @if ($errors->has('phone')) <p class="help-block" style='color:red;'>{{ $errors->first('phone') }}</p> @endif
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="subject" value="" placeholder="Subject">
                    @if ($errors->has('subject')) <p class="help-block" style='color:red;'>{{ $errors->first('subject') }}</p> @endif
                </div>
            </div>
            <div class="form-group">
                <textarea name="message" rows="10" class="form-control" placeholder="Message"></textarea>
                @if ($errors->has('message')) <p class="help-block" style='color:red;'>{{ $errors->first('message') }}</p> @endif
            </div>

            <div class="btn-container">
                <input type="submit" class="btn" name="" value="Send Message">
            </div>
        </form>
    </div>
</section>
<?php if ($locations->count() > 0) { ?>
    <section class="find-us">
        <div class="container">
            <h2>Find Us There</h2>
            <div class="row">
                <div class="col-md-4 locations wow fadeIn" data-wow-delay="200ms" data-wow-duration="1000ms">
                    <?php
                    $clo = 1;
                    foreach ($locations as $location) {
                        ?>
                        <a href="#{{$location->city}}" class="btn <?= ($clo == 1) ? "active" : "'" ?>">{{$location->city}}</a>
                        <?php
                        $clo++;
                    }
                    ?>
                </div>
                <?php
                $clo2 = 1;
                foreach ($locations as $one) {
                    ?>
                    <div id="{{$one->city}}" class="col-md-4 location-info-content <?= ($clo2 == 1) ? "wow fadeIn active" : "" ?>" data-wow-delay="400ms" data-wow-duration="1000ms">
                        <div class="location-info">
                            <div class="address">
                                <h4>Address</h4>
                                <p>{{$one->address}}</p>
                            </div>
                            <div class="phone">
                                <h4>Phone</h4>
                                <p>{{$one->phone}}</p>
                            </div>
                            <div class="email">
                                <h4>Email</h4>
                                <p><a href="mailto:{{$one->email}}">{{$one->email}}</a></p>
                            </div>
                        </div>
                    </div>
                    <?php
                    $clo2++;
                }
                ?>
                <div class="col-md-4">
                    <div class="location-info wow fadeIn" data-wow-delay="600ms" data-wow-duration="1000ms">
                        <div class="email">
                            <h4>Support</h4>
                            <p><a href="mailto:{{$site_settings["Support"]}}">{{$site_settings["Support"]}}</a></p>
                            <h4>Sales</h4>
                            <p><a href="mailto:{{$site_settings["Sales"]}}">{{$site_settings["Sales"]}}</a></p>
                            <h4>Inquiry</h4>
                            <p><a href="mailto:{{$site_settings["Inquiry"]}}">{{$site_settings["Inquiry"]}}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $clo3 = 1;
    foreach ($locations as $onelocation) {
        if ($onelocation->city == "Cairo") {
            ?>
            <section id="{{$onelocation->city}}-map" class="location-map <?= ($clo3 == 1) ? "active" : "'" ?> wow fadeIn" data-wow-delay="400ms" data-wow-duration="1000ms">
                <iframe src="https://www.google.com/maps/embed/v1/place?q=<?= $onelocation->longitude ?>,<?= $onelocation->latitude ?>&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"  frameborder="0" style="border:0" allowfullscreen></iframe>
            </section>
            <?php
            $clo3++;
        }
    }
}
?>

<script type="text/javascript">
    $("#contactform").submit(function (e) {
        $("#contactform .form-control").each(function () {
            if ($(this).val() == "") {
//                $(this).css("border-color", "red");
                if (!$(this).next("p").length) {
                    $(this).parent().append("<p style='color:red;'>This field is required.</p>");
                } else {
                    $(this).next("p").show();
                }
                e.preventDefault();
            }
        });
    });
    $("#contactform .form-control").each(function () {
        $(this).keypress(function () {
//            $(this).css("border-color", "#e0e0e0");
            $(this).next("p").hide();
        });
    });
</script>

@stop
