@extends('front.layout')
@section('meta')
<title>IPMagiX</title>
@endsection
@section('content')
<section class="main-slider">
    <div class="tp-banner-container">
        <div class="tp-banner ">
            <ul>
                <?php
                if (isset($sliders) && $sliders->count() > 0) {
                    foreach ($sliders as $slider) {
                        ?>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="./uploads/{{$slider->media_path}}"  data-saveperformance="off"  data-title="Your Lead Power">
                            <img src="{{url('/uploads/'.$slider->media_path)}}"  alt=""  data-bgposition="center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            
                            <div class="hidden-xs hidden-sm">
                                <div class="tp-caption lfb tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="-80"
                                     data-speed="1500"
                                     data-start="500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="slide-title"><h2>{{$slider->title}}</h2></div></div>

                                <div class="tp-caption lfb tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="-20"

                                     data-start="1000"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="slide-text">{!!$slider->description!!}</div></div>

                                <div class="tp-caption lfb tp-resizeme"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="50"
                                     data-speed="1500"
                                     data-start="1500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div>
                                        <a href="about-us" class="btn">Explore</a>
                                    </div></div>
                            </div>
                            
                            <div class="hidden-lg hidden-md">
                                <div class="tp-caption"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="-200"
                                     data-speed="1500"
                                     data-start="500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="slide-title"><h2>{{$slider->title}}</h2></div></div>

                                <div class="tp-caption"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="-20"

                                     data-start="1000"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div class="slide-text">{!!$slider->description!!}</div></div>

                                <div class="tp-caption"
                                     data-x="center" data-hoffset="0"
                                     data-y="center" data-voffset="200"
                                     data-speed="1500"
                                     data-start="1500"
                                     data-easing="easeOutExpo"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-elementdelay="0.01"
                                     data-endelementdelay="0.3"
                                     data-endspeed="1200"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><div>
                                        <a href="about-us" class="btn">Explore</a>
                                    </div></div>
                            </div>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>

            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>

<section class="solutions-section">
    <div class="container">
        <h2>Our Solutions</h2>
        <div class="tabs">
            <a href="#Industry" class="active">Industry</a>
            <a href="#Technology">Technology</a>
        </div>

        <div class="tabs-container">
            <div id="Industry">
                <div class="row">
                    <?php
                    if (isset($ind_solutions) && $ind_solutions->count() > 0) {
                        $delay = 1;
                        foreach ($ind_solutions as $ind_solution) {
                            ?>
                            <div class="col-sm-4 wow fadeInUp" data-wow-delay="<?php
                            if ($delay == 1) {
                                echo 0;
                            } else if ($delay == 2) {
                                echo "200ms";
                            } else if ($delay == 3) {
                                echo "400ms";
                            }
                            ?>" data-wow-duration="1000ms">
                                <a href="{{url("solution/$ind_solution->cat_id")}}">
                                    <div class="icon-container">
                                        <img src="{{url('uploads/'.$ind_solution->media_path)}}" alt="">
                                        <?php if (isset($ind_solution->icon_active_path)) { ?>
                                            <img class="active" src="{{url('uploads/'.$ind_solution->icon_active_path)}}" alt="">
                                        <?php } ?>
                                    </div>
                                    <h3>{{$ind_solution->name}}</h3>
                                    <p>{{$ind_solution->sub_title}}</p>
                                </a>
                            </div>
                            <?php
                            $delay++;
                        }
                    }
                    ?>
                </div>
            </div>

            <div id="Technology">
                <div class="row">
                    <?php
                    if (isset($tech_solutions) && $tech_solutions->count() > 0) {
                        $tdelay = 1;
                        foreach ($tech_solutions as $tech_solution) {
                            ?>
                            <div class="col-sm-4 wow fadeInUp" data-wow-delay="<?php
                            if ($tdelay == 1) {
                                echo 0;
                            } else if ($tdelay == 2) {
                                echo "200ms";
                            } else if ($tdelay == 3) {
                                echo "400ms";
                            }
                            ?>" data-wow-duration="1000ms">
                                <a href="{{url("solution/$tech_solution->cat_id")}}">
                                    <div class="icon-container">
                                        <img src="{{url('/uploads/'.$tech_solution->media_path)}}" alt="">
                                        <?php if (isset($tech_solution->icon_active_path)) { ?>
                                            <img class="active" src="{{url('uploads/'.$tech_solution->icon_active_path)}}" alt="">
                                        <?php } ?>
                                    </div>
                                    <h3>{{$tech_solution->name}}</h3>
                                    <p>{{$tech_solution->sub_title}}</p>
                                </a>
                            </div>
                            <?php
                            $tdelay++;
                        }
                    }
                    ?>
                </div>
            </div>

        </div>

    </div>
</section>

<section class="samrt-solution-section wow bounceInRight" data-wow-delay="500ms" data-wow-duration="1000ms">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>We Provide Smart Solutions</h2>
                <p>With innovation and passion, we create unified communications solutions using state-of-the-art technologies.</p>
            </div>
            <div class="col-md-4 text-center">
                <a href="./about-us" class="btn">Read More</a>
                <a href="./contact-us" class="btn">Contact Us</a>
            </div>
        </div>
    </div>
</section>

<section class="recent-cases-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 wow bounceInLeft" data-wow-delay="500ms" data-wow-duration="1000ms">
                <img src="{{url('/assets/front/images/recent.jpg')}}" alt="">
            </div>
            <div class="col-sm-6 wow bounceInRight" data-wow-delay="500ms" data-wow-duration="1000ms">
                <div class="recent-cases">
                    <h2>Recent Cases</h2>
                    <div class="container-fluid">
                        <?php
                        if ($cases->count() > 0) {
                            $casecount = 1;
                            $case_delay = 0;
                            foreach ($cases as $case) {
                                if ($casecount % 2 != 0) {
                                    ?>
                                    <div class="row">
                                    <?php } ?>
                                    <div class="col-md-6 wow fadeInUp" data-wow-delay="{{$case_delay}}ms" data-wow-duration="1000ms">
                                        <div class="logo-container">
                                            <img src="{{url('/uploads/'.$case->media_path)}}" alt="">
                                        </div>
                                        <h3>{{$case->name}}</h3>
                                        <p>{{$case->brief}}</p>
                                    </div>
                                    <?php if ($casecount % 2 == 0) { ?>
                                    </div>
                                    <?php
                                }
                                $casecount++;
                                $case_delay = $case_delay + 200;
                            }
                        }
                        ?>
                    </div>
                    <div class="">
                        <a href="./cases" class="btn">View All</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="news-section wow slideInUp" data-wow-delay="400ms" data-wow-duration="1000ms">
    <h2>Latest News</h2>
    <?php if (isset($news) && $news) { ?>
        <div class="news-slider owl-carousel">
            <?php foreach ($news as $new) { ?>
                <div class="news-item">
                    <img src="{{url('/uploads/'.$new->media_path)}}" alt="">
                    <a href="./news-details/{{$new->post_id}}" class="news-item-content">
                        <span>{{date("F d , Y",strtotime($new->created_date))}}</span>
                        <h3>{{$new->post_title}}</h3>
                    </a>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</section>

<?php if (1!= 1 && $feedbacks->count() > 0) { ?>
    <section class="clients-feedback wow fadeIn" data-wow-delay="400ms" data-wow-duration="1000ms">
        <div class="container">
            <h2>Clients Feedback</h2>
            <div class="feedback-slider owl-carousel">
                <?php foreach ($feedbacks as $feedback) { ?>
                    <div class="feedback">
                        <div class="client-image">
                            <img src="{{url('/uploads/'.$feedback->media_path)}}" alt="">
                        </div>
                        <h3>{{$feedback->name}}</h3>
                        <h4>{{$feedback->job}}</h4>
                        {!!$feedback->description!!}
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>


<section class="home-partners">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <img src="{{url("assets/front/images/logos/cisco1.jpg")}}" alt="">
            </div>
            <div class="col-sm-3">
                <img src="{{url("assets/front/images/logos/microsoft.jpg")}}" alt="">
            </div>
            <div class="col-sm-3">
                <img src="{{url("assets/front/images/logos/cisco.jpg")}}" alt="">
            </div>
            <div class="col-sm-3">
                <img src="{{url("uploads/1602674278193388792.png")}}" alt="" width="171" height="56">
            </div>
        </div>
    </div>
</section>
@stop
