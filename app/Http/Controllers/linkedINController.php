<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Job_cv;
use App\Models\Career_lang;
use DB;
use Socialite;
use Illuminate\Support\Facades\Input;

class linkedINController extends Controller {

      /**
      * Redirect the user to the GitHub authentication page.
      *
      * @return Response
      */
     public function redirectToProvider($id)
     {
         session(['career_id' => $id]);
         return Socialite::driver('linkedin')->redirect();
     }

     /**
      * Obtain the user information from GitHub.
      *
      * @return Response
      */
     public function handleProviderCallback()
     {
        if (isset($_GET['error'])) {
          return redirect('careers/show/'.session('career_id'));
        }else {
          $user = Socialite::driver('linkedin')->user();
         //  dd([$user->name,$user->email,$user->user['publicProfileUrl'],session('career_id')]);
         $user_data = Job_cv::where('email',$user->email)->where('career_id',session('career_id'))->first();
          if ($user_data) {
            session()->push('already_registered',"You already registered for this this job before.");
          }else {
            $jobcv = new Job_cv();
            $jobcv->name = $user->name;
            $jobcv->email = $user->email;
           //  $jobcv->phone = Input::get("phone");
            $jobcv->job_title = Career_lang::where('career_id',session('career_id'))->where('lang',\Config::get("app.locale"))->first()->title;
            $jobcv->apply_type = 1;
            $jobcv->cv = $user->user['publicProfileUrl'];
            $jobcv->career_id = session('career_id');
            $jobcv->save();
            session()->push('linkedin_apply',"Your request has been sent seccessfully.");
          }
          return redirect('careers/show/'.session('career_id'));
        }

     }











  // end of class
}
