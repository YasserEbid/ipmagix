<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Partner;
use DB;
use Illuminate\Support\Facades\Input;

class PartnersController extends Controller {

  public function getPartners($type){
    $partners = new Partner();
    if ($type == 'technology') {
      $partners = $partners::where('type','t')->where('country_id',session('current_countryCode'))->get();
    }elseif($type == 'channel') {
      $partners = $partners::where('type','c')->where('country_id',session('current_countryCode'))->get();
    }else {
      return response()->view('errors.404', [], 404);
    }
    $data = [];
    if (is_object($partners) && $partners->count() > 0) {
        $partners_lang = [];
        foreach ($partners as $key => $partner) {
          $partners_lang[$key] = $partner->partnerlang(\Config::get("app.locale"));
        }
        $data['partners'] = $partners;
        $data['partners_lang'] = $partners_lang;
    }
    if ($type == 'technology') {
      return view('front.partners',$data);
    }elseif ($type == 'channel') {
      return view('front.partner-channel',$data);
    }

  }









  //// end of class
}
