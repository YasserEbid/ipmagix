<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Client;
use DB;
use Illuminate\Support\Facades\Input;

class CasesController extends Controller {

  public function showCases(){
    $data["cases"] = Client::join("media","clients.media_id","=","media.media_id")
    ->leftJoin("clients_langs","clients.id","=","clients_langs.client_id")
    ->where('clients.country_id',session('current_countryCode'))
     ->where("clients_langs.lang",\Config::get('app.locale'))->orderBy("clients.id","desc")->get();
    return view("front.cases",$data);
  }



// end of controller
}
