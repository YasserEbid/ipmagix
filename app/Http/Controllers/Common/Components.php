<?php

namespace App\Http\Controllers\Common;

class Components {

	public static function specialities() {
		$html = '<label>' . trans('home.ByInsSpe') . '</label>';
		$html.='<select class="form-control" name="insspe">';
		$html.='<option value="">' . trans('home.SelectInsSpe') . '</option>';

		$specialities = \Cache::remember('specialities', Helpers::$cache_time, function() {
			return \App\Models\services::where('in_menu',1)->with('lang')->get();
		});
		foreach ($specialities as $row) {
			if (isset($_GET['insspe'])) {
				$html.='<option value="' . $row->id . '" ' . Helpers::selected($_GET['insspe'], $row->id) . '>' . $row->lang->title . '</option>';
			} else {
				$html.='<option value="' . $row->id . '" >' . $row->lang->title . '</option>';
			}
		}
		$html.='</select>';
		return $html;
	}

}
