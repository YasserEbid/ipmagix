<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Intervention\Image\Image;
use Intervention\Image\Facades\Image;

class Helpers extends Controller
{
    static $cache_time;

    public static function selected($option1, $option2) {	
        if ($option1 == $option2) {
            return "selected='selected'";
        }
    }
    
    public static function checked($option1,$option2){
        if($option1==$option2){
            return "checked='checked'";
        }
    }
    
    
    public static function createThumb($path,$image,$dest,$width=150,$height=150){
        $img = Image::make($path.'/'.$image)->resize($width, $height);
        $img->save($dest.'/'.$image);
        return $dest.'/'.$image;
    }
    
    public static function clearHtml($text){
        // Strip HTML Tags
        $clear = strip_tags($text);
        // Clean up things like &amp;
        $clear = html_entity_decode($clear);
        // Strip out any url-encoded stuff
        $clear = urldecode($clear);
        // Replace non-AlNum characters with space
        $clear = preg_replace('/[^A-Za-z0-9#_]/', ' ', $clear);
        // Replace Multiple spaces with single space
        $clear = preg_replace('/ +/', ' ', $clear);
        // Trim the string of leading/trailing space
        $clear = trim($clear);
        return $clear;
    }
    
    public static function UploadProfilePicture($image){
        
        $image_directory=date('Y-m');
        if (!file_exists('./uploads/'.$image_directory)) {
            mkdir('./uploads/'.$image_directory, 0755, true);
        }
        
        if (!file_exists('./uploads/thumbs/small/'.$image_directory)) {
            mkdir('./uploads/thumbs/small/'.$image_directory, 0755, true);
        }
        
        if (!file_exists('./uploads/thumbs/'.$image_directory)) {
            mkdir('./uploads/thumbs/'.$image_directory, 0755, true);
        }
        
        if (!file_exists('./uploads/thumbs/medium/'.$image_directory)) {
            mkdir('./uploads/thumbs/medium/'.$image_directory, 0755, true);
        }
        
        
        rename('./uploads/temp/'.$image, './uploads/'.$image_directory."/".$image);
        Helpers::createThumb('./uploads/'.$image_directory."/", $image, './uploads/thumbs/medium/'.$image_directory, 400,250);
        Helpers::createThumb('./uploads/'.$image_directory."/", $image, './uploads/thumbs/small/'.$image_directory, 50,50);
        Helpers::createThumb('./uploads/'.$image_directory."/", $image, './uploads/thumbs/'.$image_directory, 160,160);
        
        return $image_directory."/".$image;
    }
    
    public static function UploadImage($image){
        
        $image_directory=date('Y-m');
        if (!file_exists('./uploads/'.$image_directory)) {
            mkdir('./uploads/'.$image_directory, 0755, true);
        }
        
        if (!file_exists('./uploads/thumbs/small/'.$image_directory)) {
            mkdir('./uploads/thumbs/small/'.$image_directory, 0755, true);
        }
        
        if (!file_exists('./uploads/thumbs/'.$image_directory)) {
            mkdir('./uploads/thumbs/'.$image_directory, 0755, true);
        }
        
        if (!file_exists('./uploads/thumbs/medium/'.$image_directory)) {
            mkdir('./uploads/thumbs/medium/'.$image_directory, 0755, true);
        }
        
        
        rename('./uploads/temp/'.$image, './uploads/'.$image_directory."/".$image);
        Helpers::createThumb('./uploads/'.$image_directory."/", $image, './uploads/thumbs/medium/'.$image_directory, 400,250);
        Helpers::createThumb('./uploads/'.$image_directory."/", $image, './uploads/thumbs/small/'.$image_directory, 50,50);
        Helpers::createThumb('./uploads/'.$image_directory."/", $image, './uploads/thumbs/'.$image_directory, 160,160);
        
        return $image_directory."/".$image;
    }
    
    public static function DeleteImage($image){
        if(file_exists('./uploads/'.$image)){
            unlink('./uploads/'.$image);
        }
        
        if(file_exists('./uploads/thumbs/'.$image)){
            unlink('./uploads/thumbs/'.$image);
        }
        
        if(file_exists('./uploads/'.$image)){
            unlink('./uploads/'.$image);
        }
        
        if(file_exists('./uploads/'.$image)){
            unlink('./uploads/'.$image);
        }
        
    }
    
    /* this function return how muh time ago */
    public static function humanTiming($time) {

		$time = time() - $time; // to get the time since that moment
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'year',
			2592000 => 'month',
			604800 => 'week',
			86400 => 'day',
			3600 => 'hour',
			60 => 'minute',
			1 => 'second'
		);

		foreach ($tokens as $unit => $text) {
			if ($time < $unit)
				continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
		}
	}
        
   public static function getClientIp() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
    
    public static function limitString($string='', $limit=0)
    {
        // return $string;
        return preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $limit));
    }
    public static function highlight($text,$input)
    {
     //return marked text
        return str_ireplace($input,'<b class="bg-info">'.ucfirst($input).'</b>',Helpers::clearHtml($text));
    }
}
