<?php

namespace App\Http\Controllers\Common;

class CountryIP {

    static function get_IP() {
        $ip = $_SERVER['REMOTE_ADDR'];
        // $jsonData = file_get_contents('http://freegeoip.net/json/'.$ip);
        // $countryInfo = json_decode($jsonData, true);
        if (!@ file_get_contents('http://freegeoip.net/json/' . $ip)) {
            $geoplugin_jsonData = file_get_contents('http://www.geoplugin.net/json.gp/' . $ip);
            $countryInfo = json_decode($geoplugin_jsonData, true);
            $countryInfo['ip'] = $countryInfo['geoplugin_request'];
        } else {
            $freegeoip_jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
            $countryInfo = json_decode($freegeoip_jsonData, true);
        }
        return $countryInfo['ip'];
    }

    static function get_Symbol() {
        $ip = $_SERVER['REMOTE_ADDR'];
        // $ip="34.248.100.250"; //irelandland
        //  $ip="163.121.103.122"; //egypt
        //  $ip="212.138.92.10"; //KSA
        // $ip="83.110.250.231"; //UAE
        $countryInfo = [];
        if (!@ file_get_contents('http://freegeoip.net/json/' . $ip) && !@ file_get_contents('http://www.geoplugin.net/json.gp/' . $ip)) {
            return "EG";
        } else {
            if (!@ file_get_contents('http://freegeoip.net/json/' . $ip)) {
                $geoplugin_jsonData = file_get_contents('http://www.geoplugin.net/json.gp/' . $ip);
                $countryInfo = json_decode($geoplugin_jsonData, true);
                $countryInfo['country_code'] = $countryInfo['geoplugin_countryCode'];
            } else {
                $freegeoip_jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
                $countryInfo = json_decode($freegeoip_jsonData, true);
            }
        }
        // dd($countryInfo);
        return $countryInfo['country_code'];
    }

    static function get_Name() {
        $ip = $_SERVER['REMOTE_ADDR'];
        // $jsonData = file_get_contents('http://freegeoip.net/json/'.$ip);
        // $countryInfo = json_decode($jsonData, true);
        if (!@ file_get_contents('http://freegeoip.net/json/' . $ip)) {
            $geoplugin_jsonData = file_get_contents('http://www.geoplugin.net/json.gp/' . $ip);
            $countryInfo = json_decode($geoplugin_jsonData, true);
            $countryInfo['country_name'] = $countryInfo['geoplugin_countryName'];
        } else {
            $freegeoip_jsonData = file_get_contents('http://freegeoip.net/json/' . $ip);
            $countryInfo = json_decode($freegeoip_jsonData, true);
        }
        return $countryInfo['country_name'];
    }

}
