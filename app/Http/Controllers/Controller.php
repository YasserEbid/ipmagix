<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\Models\countries;
use Carbon\Carbon;
use Mediasci\Cms\Models\Cities;
use App\Models\Event;
use App\Http\Controllers\Common\Helpers;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct() {
        $this->data = [];
        // $this->cache_time = Helpers::$cache_time = Carbon::now()->addWeeks(2);
        $this->cache_time = 1;
        $this->getCountryName();
        $this->getSiteSettings();
        $this->getMenuslinks();
    }

    function getSiteSettings() {
        $this->data['site_settings'] = \Setting::getAll();
        view()->share($this->data);
    }

    public function getCountryName() {
        $current_country = \App\Http\Controllers\Common\CountryIP::get_Symbol();
        
        if(request('cn'))
            $current_country = request('cn') ;
        else
            $current_country = \App\Http\Controllers\Common\CountryIP::get_Symbol();
        
        $check_exist = countries::where('code', $current_country)->first();
        if ($check_exist) {
            return session(['current_countryCode' => $check_exist->id]);
        } else {
            // dd(countries::where('code','EG')->first()->id);
            return session(['current_countryCode' => countries::where('code', 'EG')->first()->id]);
        }
    }

    public function getMenuslinks() {
        $menusLinks = \Cache::remember('menusLinks', $this->cache_time, function() {
                    $menusLinks = array();
                    $menu_links = DB::table('menu_links')
                            ->join('menu_links_langs', 'menu_links.id', '=', 'menu_links_langs.menu_link_id')
                            ->select('menu_links.*', 'menu_links_langs.name')
                            ->where('parent_id', NULL)
                            ->where('menu_links.country_id', session('current_countryCode'))
                            ->where('active', '1')
                            ->where('lang', \Config::get("app.locale"))
                            ->orderBy('menu_links.order', 'asc')
                            ->get();
                    if ($menu_links) {

                        foreach ($menu_links as $key => $link) {
                            $menusLinks[$link->menu_id][$key]['id'] = $link->id;
                            $menusLinks[$link->menu_id][$key]['menu_id'] = $link->menu_id;
                            $menusLinks[$link->menu_id][$key]['link_url'] = $link->link_url;
                            $menusLinks[$link->menu_id][$key]['name'] = $link->name;

                            $sub_menu_links = DB::table('menu_links')
                                    ->join('menu_links_langs', 'menu_links.id', '=', 'menu_links_langs.menu_link_id')
                                    ->select('menu_links.*', 'menu_links_langs.name')
                                    ->where('menu_links.country_id', session('current_countryCode'))
                                    ->where('active', '1')
                                    ->where('parent_id', $link->id)
                                    ->where('lang', \Config::get("app.locale"))
                                    ->orderBy('menu_links.order', 'asc')
                                    ->get();
                            if (count($sub_menu_links)) {
                                foreach ($sub_menu_links as $k => $sublink) {
                                    $menusLinks[$link->menu_id][$key]['subLinks'][$k]['id'] = $sublink->id;
                                    $menusLinks[$link->menu_id][$key]['subLinks'][$k]['menu_id'] = $sublink->menu_id;
                                    $menusLinks[$link->menu_id][$key]['subLinks'][$k]['link_url'] = $sublink->link_url;
                                    $menusLinks[$link->menu_id][$key]['subLinks'][$k]['name'] = $sublink->name;


                                    $sub_sub_menu_links = DB::table('menu_links')
                                            ->join('menu_links_langs', 'menu_links.id', '=', 'menu_links_langs.menu_link_id')
                                            ->select('menu_links.*', 'menu_links_langs.name')
                                            ->where('menu_links.country_id', session('current_countryCode'))
                                            ->where('active', '1')
                                            ->where('parent_id', $sublink->id)
                                            ->where('lang', \Config::get("app.locale"))
                                            ->orderBy('menu_links.order', 'asc')
                                            ->get();
                                    if (count($sub_sub_menu_links)) {
                                        foreach ($sub_sub_menu_links as $kk => $subsublink) {
                                            $menusLinks[$link->menu_id][$key]['subLinks'][$k]['subLinks'][$kk]['id'] = $subsublink->id;
                                            $menusLinks[$link->menu_id][$key]['subLinks'][$k]['subLinks'][$kk]['menu_id'] = $subsublink->menu_id;
                                            $menusLinks[$link->menu_id][$key]['subLinks'][$k]['subLinks'][$kk]['link_url'] = $subsublink->link_url;
                                            $menusLinks[$link->menu_id][$key]['subLinks'][$k]['subLinks'][$kk]['name'] = $subsublink->name;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return $menusLinks;
                });
        //echo "<pre>";print_r($menusLinks);die;
        $this->data['menusLinks'] = $menusLinks;
        // dd($menusLinks);
        view()->share($this->data);
    }

}
