<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests;
use Request;
use App\Models\Career;
use App\Models\Job_cv;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class CareersController extends Controller {

    public function showCareers() {
        $data["careers"] = Career::
                        leftJoin("career_langs", "careers.id", "=", "career_langs.career_id")
                        ->leftJoin("careers_categories", "careers.cat_id", "=", "careers_categories.id")
                        ->leftJoin("careers_category_langs", "careers_categories.id", "=", "careers_category_langs.cat_id")
                        ->select("careers.id", "career_langs.title",'careers.country_id',"career_langs.location", "careers_category_langs.title as cat_title")
                        ->where('careers.country_id',session('current_countryCode'))
                        ->where("career_langs.lang", \Config::get('app.locale'))->where("careers_category_langs.lang", \Config::get('app.locale'))->orderBy("career_langs.created_at", "desc")->get();
        return view("front.careers", $data);
    }

    public function show($id) {
        $data["career"] = Career::
                        leftJoin("career_langs", "careers.id", "=", "career_langs.career_id")
                        ->leftJoin("careers_categories", "careers.cat_id", "=", "careers_categories.id")
                        ->leftJoin("careers_category_langs", "careers_categories.id", "=", "careers_category_langs.cat_id")
                        ->select("careers.id", "careers.created_at", "career_langs.title",'careers.country_id', "career_langs.location", "career_langs.description", "careers_category_langs.title as cat_title")
                        ->where('careers.country_id',session('current_countryCode'))
                        ->where("career_langs.lang", \Config::get('app.locale'))->where("careers.id", $id)->where("careers_category_langs.lang", \Config::get('app.locale'))->orderBy("career_langs.created_at", "desc")->first();
        return view("front.vacancy-datails", $data);
    }

    public function apply($id) {
        $data["id"] = $id;
        if (Request::isMethod('post')) {

            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|min:11|max:15',
                'job_title' => 'required',
                'cover_letter' => 'required',
                'cv' => 'required',
            ];
            $validator = \Validator::make(Request::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $file_ectentions = array("pdf", "doc", "docx");
            $extension = strtolower(Input::file('cv')->getClientOriginalExtension());
            if (!in_array($extension, $file_ectentions)) {
                return redirect('careers/apply/' . $id)->with("error", "Sorry , The file must be pdf or word .");
            } else {
                $check = Job_cv::where('email', Input::get("email"))->first();
                if (is_object($check) && $check->count() > 0) {
                    return redirect('careers/apply/' . $id)->with("error", "Sorry , This email is already exist .");
                } else {
                    if (!file_exists('./uploads/cvs')) {
                        mkdir('./uploads/cvs', 0755, true);
                    }
                    $destinationPath = 'uploads/cvs'; // upload path
                    $fileName = random_int(1, 5000) * microtime() . '.' . $extension; // renameing image
                    Input::file('cv')->move($destinationPath, $fileName); // uploading file to given path


                    $jobcv = new Job_cv();
                    $jobcv->name = Input::get("name");
                    $jobcv->email = Input::get("email");
                    $jobcv->phone = Input::get("phone");
                    $jobcv->job_title = Input::get("job_title");
                    $jobcv->cover_letter = Input::get("cover_letter");
                    $jobcv->apply_type = 0;
                    $jobcv->cv = $fileName;
                    $jobcv->career_id = $id;
                    $jobcv->save();
                    return redirect('careers/apply/' . $id)->with("message", "Thank you for applying on this job.");
                }
            }
        }
        return view("front.apply", $data);
    }

// end of controller
}
