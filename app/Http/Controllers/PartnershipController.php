<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Volunteer;
use DB;
use Request;
use Illuminate\Support\Facades\Input;

class PartnershipController extends Controller {

  public function partnerRequest(){
    $data = [];
    if (Request::isMethod('post')) {
      $rules = [
        'name'            => 'required',
        'email'           => 'required|email',
        'phone'           => 'required|min:11|max:11',
        'countries_names' => 'required',
        'subject'         => 'required',
        'comment'         => 'required',
        'companyName'     => 'required'
      ];
      $validator = \Validator::make(Request::all(), $rules);
      if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
      }
      $newPartner = new Volunteer();
      $newPartner['fullname'] = Input::get('name');
      $newPartner['company_name'] = Input::get('companyName');
      $newPartner['country'] = Input::get('countries_names');
      $newPartner['email'] = Input::get('email');
      $newPartner['phone'] = Input::get('phone');
      $newPartner['subject'] = Input::get('subject');
      $newPartner['message'] = Input::get('comment');
      $newPartner->save();
      session()->push('partnerRequest',"Your request has been sent successfully.");
    }
    $data['country_names'] = $country_names = DB::table('countries_names')->get();
    return view('front.become-partner',$data);
  }



  // edn of class
}
