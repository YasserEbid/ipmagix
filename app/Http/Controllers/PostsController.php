<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Categories;
use App\Models\posts_categories;
use App\Models\Categories_langs;
use App\Models\posts;
use App\Models\posts_langs;
use App\Models\Search;
use DB;
use Illuminate\Support\Facades\Input;

class PostsController extends Controller {

    public function postDetails($category_name, $id) {
        $current_lang = \Config::get("app.locale");
        $data = [];
        $data['post'] = $post = posts::where('post_id', $id)->where('country_id', session('current_countryCode'))->first();
        if (is_object($post) && $post->count() > 0) {
            $data["post_cat"] = $post_cat = posts_categories::where("post_id", $id)->get();
            $data['cat_name'] = [];
            $postsids = [];
            foreach ($post_cat as $key => $cat) {
                $cat_name = Categories_langs::where("cat_id", $cat->cat_id)->where('name', $category_name)->first();
                if (is_object($cat_name) && $cat_name->count() > 0) {
                    $data['cat_name'] = $cat_name;
                    $postsids = posts_categories::where("cat_id", $cat_name->cat_id)->select("post_id")->get();
                }
            }
            $data["posts_ids"] = array();
            if ($postsids->count() > 1) {
                foreach ($postsids as $one) {
                    $data["posts_ids"][] = $one->post_id;
                }
            }
            $data['posts_lang'] = $post->lang()->where('lang', $current_lang)->first();
            $post_tabs = $post->postTabs;
            $tab_lang = [];
            foreach ($post_tabs as $key => $tab) {
                $tab_lang[$key] = $tab->lang()->where('lang', $current_lang)->first();
            }
            $data['tab_lang'] = $tab_lang;
        } else {
            $data["message"] = "Sorry, This product is not found.";
            return view('errors.404', $data);
        }

        return view("front.solutions-details", $data);
    }

    public function aboutContent() {
        $data = [];
        $current_lang = \Config::get("app.locale");
        $category = Categories::where('cat_slug', 'about_us')->where('country_id', session('current_countryCode'))->first();
        if (is_object($category)) {
            $about_post = $category->posts()->first();
            if (is_object($about_post)) {
                $aboutPost_lang = $about_post->lang()->where('lang', $current_lang)->first();
                $data['about_post'] = $about_post;
                $data['aboutPost_lang'] = $aboutPost_lang;
            }
        }
        return view("front.about", $data);
    }

    public function findResult() {
        $data = [];
        $search_word = trim(Input::get('searchText'));
        $posts = posts_langs::join('posts_categories', 'posts_langs.post_id', '=', 'posts_categories.post_id')
                ->join('categories', 'posts_categories.cat_id', '=', 'categories.cat_id')
                //  ->join('categories_langs','categories.cat_id','=','categories_langs.cat_id')
                ->where('categories.country_id', session('current_countryCode'))
                ->where('posts_langs.post_title', 'like', '%' . $search_word . '%')
                ->where("posts_langs.lang", \Config::get("app.locale"))
                ->get();
        foreach ($posts as $key => $post) {
            $post->parent_name = Categories_langs::where('cat_id', $post->cat_id)->where('lang', \Config::get("app.locale"))->first()->name;
        }
        $categories = Categories::leftJoin('categories_langs', 'categories.cat_id', '=', 'categories_langs.cat_id')
                ->where('name', 'like', '%' . $search_word . '%')
                ->where('categories.country_id', session('current_countryCode'))
                ->where('parent_id', '<>', 0)
                ->where("categories_langs.lang", \Config::get("app.locale"))
                ->get();
        if ($categories->count() > 0) {
            foreach ($categories as $category) {
                $category->parent = Categories_langs::where('cat_id', $category->parent_id)->first()->name;
            }
        }
        if ($posts->count() > 0 || $categories->count() > 0) {
            $data['posts'] = $posts;
            $data['categories'] = $categories;
        } else {
            $data['notfound'] = "NOTHING FOUND<p>Sorry, but nothing matched your search terms. Please check your spelling or try again with some different keywords.<p>";
            // return view('front.search',);
        }
        $data['search_word'] = $search_word;
        return view('front.search', $data);
    }

    public function newsPost() {
        $current_lang = \Config::get("app.locale");
        $data = [];
        $category = Categories::where('cat_slug', 'news')->where('country_id', session('current_countryCode'))->first();
        if (is_object($category) && $category->count() > 0) {
            $data['category'] = $category;
            $data['posts'] = $category->posts;
            $data['current_lang'] = $current_lang;
            return view("front.news", $data);
        } else {
            return view("front.news");
            //  return response()->view('errors.404', [], 404);
        }
    }

    public function news_details($id) {
        $data = [];
        $post = posts::find($id);
        $data['post'] = $post;
        if ($post) {
            $data['post_lang'] = $post->lang()->where('lang', \Config::get("app.locale"))->first();
            return view("front.news-details", $data);
        } else {
            return response()->view('errors.404', [], 404);
        }
    }

// end of controller
}
