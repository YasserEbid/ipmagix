<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests;
use Request;
use App\Models\Event_category;
use App\Models\Categories;
use App\Models\Client;
use App\Models\ClientsLangs;
use App\Models\SiteLocations;
use App\Models\posts;
use App\Models\Feedback;
use App\Models\posts_categories;
use App\Models\Search;
use App\Models\sliders;
use App\Models\Contactus;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\UsersProperties;
use Illuminate\Support\Facades\Session;
use Mediasci\Cms\Models\MailList;

class HomeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['sliders'] = sliders::leftJoin("media", "sliders.media_id", "=", "media.media_id")->leftJoin("slider_langs", "sliders.id", "=", "slider_langs.slider_id")
                ->where("slider_langs.lang", \Config::get('app.locale'))
                ->where('country_id', session('current_countryCode'))
                ->get();

        $indcategory = Categories::where("cat_slug", "Industry")->first();
        if (is_object($indcategory) && $indcategory->count() > 0) {
            $ind_id = $indcategory->cat_id;

            $data["ind_solutions"] = $ind_solutions = Categories::leftJoin("media", "categories.icon_id", "=", "media.media_id")
                            ->leftJoin("categories_langs", "categories.cat_id", "=", "categories_langs.cat_id")
                            ->where("categories_langs.lang", \Config::get('app.locale'))
                            ->where('categories.country_id', session('current_countryCode'))
                            ->where("categories.parent_id", $ind_id)->get();
            if ($ind_solutions->count() > 0) {
                foreach ($ind_solutions as $solution) {
                    if (!is_null($solution->icon_id_active) && $solution->icon_id_active != 0) {
                        $solution->icon_active_path = \Media::where("media_id", $solution->icon_id_active)->first()->media_path;
                    }
                }
            }
        }

        $techcategory = Categories::where("cat_slug", "Technology")->first();
        //  dd([$indcategory,$techcategory]);
        if (is_object($techcategory) && $techcategory->count() > 0) {
            $tech_id = $techcategory->cat_id;
            $data["tech_solutions"] = $tech_solutions = Categories::leftJoin("media", "categories.icon_id", "=", "media.media_id")
                            ->leftJoin("categories_langs", "categories.cat_id", "=", "categories_langs.cat_id")
                            ->where("categories_langs.lang", \Config::get('app.locale'))
                            ->where('categories.country_id', session('current_countryCode'))
                            ->where("categories.parent_id", $tech_id)->get();
            if ($tech_solutions->count() > 0) {
                foreach ($tech_solutions as $tsolution) {
                    if (!is_null($tsolution->icon_id_active)) {
                        $tsolution->icon_active_path = \Media::where("media_id", $tsolution->icon_id_active)->first()->media_path;
                    }
                }
            }
        }
        $data["cases"] = Client::leftJoin("media", "clients.media_id", "=", "media.media_id")
                        ->leftJoin("clients_langs", "clients.id", "=", "clients_langs.client_id")
                        ->where('clients.country_id', session('current_countryCode'))
                        ->where("clients_langs.lang", \Config::get('app.locale'))->orderBy("clients.id", "desc")->take(4)->get();

        $news_category = Categories::where("cat_slug", "news")
                        ->where('categories.country_id', session('current_countryCode'))->first();

        if (is_object($news_category) && $news_category->count() > 0) {
            $category_id = $news_category->cat_id;
            $data["news"] = posts_categories::join("posts", "posts_categories.post_id", "=", "posts.post_id")
                    ->leftJoin("posts_langs", "posts.post_id", "=", "posts_langs.post_id")
                    ->leftJoin("media", "posts.home_image_id", "=", "media.media_id")
                    ->where("posts_categories.cat_id", $category_id)
                    ->where('posts.country_id', session('current_countryCode'))
                    ->take(10)
                    ->orderBy("posts.post_id", "desc")
                    ->select("posts.created_date", "media.media_path", "posts.post_id", "posts.slug", "posts_langs.post_title")
                    ->get();
        }

        $data["feedbacks"] = Feedback::leftJoin("media", "feedback.media_id", "=", "media.media_id")
                ->leftJoin("feedback_lang", "feedback.id", "=", "feedback_lang.feedback_id")
                ->where("feedback_lang.lang", \Config::get('app.locale'))
                ->where('feedback.country_id', session('current_countryCode'))
                ->orderBy("feedback.id", "desc")
                ->select("media.media_path", "feedback.id", "feedback_lang.name", "feedback_lang.job", "feedback_lang.description")
                ->get();

        return view("front.index", $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function subscribe() {
        if (Input::has('email')) {
            $email = Input::get('email');
            $data = MailList::where('email', $email)->get();

            if (!$data->isEmpty()) {
                return 0;
            } else {
                $subscribe = new MailList();
                $subscribe->name = Input::get('name');
                $subscribe->email = Input::get('email');
                $subscribe->save();
                return "Thank you for your interest in finding out more about IpMagix. Welcome to the community.";
            }
        }
    }

    public function search() {
        $data = Search::findByTerm(Input::get('term'));
        return view('front.search', $data);
    }

    public function contactus() {
        $data = [];
        if (Request::isMethod('post')) {
            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required|min:11|max:15',
                'subject' => 'required',
                'message' => 'required',
            ];
            $validator = \Validator::make(Request::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $subscribe = new Contactus();
            $subscribe->name = Input::get('name');
            $subscribe->email = Input::get('email');
            $subscribe->phone = Input::get('phone');
            $subscribe->subject = Input::get('subject');
            $subscribe->message = Input::get('message');
            $subscribe->save();
            $data["conmessage"] = "<strong>Thank you ,</strong> We will contact with you as soon as possible.";
        }
        $data["locations"] = SiteLocations::leftJoin("site_locations_lang", "site_locations.id", "=", "site_locations_lang.location_id")
                        ->where("site_locations_lang.lang", \Config::get('app.locale'))->get();
        return view('front.contact', $data);
    }

}
