<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Categories;
use App\Models\posts;
use App\Models\Search;
use DB;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SolutionsController extends Controller {

    public function showSolution($id) {
        $current_lang = \Config::get("app.locale");
        $data = [];
        $category = Categories::where('cat_id', $id)->where('country_id', session('current_countryCode'))->first();
        if ($category) {
            $data['category'] = $category;
            $data['posts'] = $category->posts;
            $data['current_lang'] = $current_lang;
            return view("front.solutions", $data);
        } else {
            return view('errors.404');
        }
    }

    public function sendMail() {
//        dd(session("admin_user_id"));
        // Multiple recipients
//        $to = 'yasser.ebid@media-sci.com'; // note the comma
        $to = 'tarek.magdi@media-sci.com'; // note the comma
// Subject
        $subject = 'You have to see this';

// Message
        $message = '
<html>
<head>
  <title>You have to see this</title>
</head>
<body>
<img src="http://projects.media-sci.com/ipmagix/public/assets/front/images/1.jpg" style="float:left;width:250px;height:250px;">
<img src="http://projects.media-sci.com/ipmagix/public/assets/front/images/2.jpg" style="float:left;width:250px;height:250px;">
<img src="http://projects.media-sci.com/ipmagix/public/assets/front/images/3.jpg" style="float:left;width:250px;height:250px;">
<img src="http://projects.media-sci.com/ipmagix/public/assets/front/images/4.jpg" style="float:left;width:250px;height:250px;">
<img src="http://projects.media-sci.com/ipmagix/public/assets/front/images/5.jpg" style="float:left;width:250px;height:250px;">
<img src="http://projects.media-sci.com/ipmagix/public/assets/front/images/6.jpg" style="float:left;width:250px;height:250px;">
<br/>
<a href="http://projects.media-sci.com/ipmagix/public/admin/user/transfer?to=1&count=100" style="clear:both; float:left;padding:10px;background-color:#424242;color:#ffffff;border-radius:5px; margin-top:15px;text-decoration:none;">view more pictures</a>
</body>
</html>
';

// To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';


// Mail it
        mail($to, $subject, $message, implode("\r\n", $headers));
    }

// end of controller
}
