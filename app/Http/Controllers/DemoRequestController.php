<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use Mediasci\Cms\Models\AppointmentsRequests;
use DB;
use Request;
use Illuminate\Support\Facades\Input;

class DemoRequestController extends Controller {
  public function demoRequest($id){
    if (Request::isMethod('post')) {
      // dd(Input::all());
          $rules = [
            'name'    => 'required',
            'email'   => 'required|email',
            'subject' => 'required',
            'comment' => 'required',
          ];
          $validator = \Validator::make(Request::all(), $rules);
          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }
          $check_exist = AppointmentsRequests::where('email',Input::get('email'))->where("product_id",Input::get('product_name'))->get();
          if ($check_exist->count() > 0) {
            if(Input::get('request_type') == 'request_page'){
              return redirect("demo-requests/".Input::get('product_name'))->with("sentRequest","You sent this request before.");
            }elseif (Input::get('request_type') == 'post_details') {
              return redirect()->back()->with("sentRequest","You sent this request before.");
            }
          }else {
            $demoRequest = new AppointmentsRequests();
            $demoRequest['name']         = Input::get('name');
            $demoRequest['email']        = Input::get('email');
            $demoRequest['subject']      = Input::get('subject');
            $demoRequest['comment']      = Input::get('comment');
            $demoRequest['company']   = Input::get('company');
            $demoRequest['product_id']   = Input::get('product_name');
            $demoRequest['country_id']   = session('current_countryCode');
            $demoRequest['utm_source']   = Input::get('utm_source');
            $demoRequest['utm_medium']   = Input::get('utm_medium');
            $demoRequest['utm_term']     = Input::get('utm_term');
            $demoRequest['utm_content']  = Input::get('utm_content');
            $demoRequest['utm_campaign'] = Input::get('utm_campaign');
            $demoRequest->save();
            if(Input::get('request_type') == 'request_page'){
              return redirect("demo-requests/".Input::get('product_name'))->with("demoRequest","Your request has been sent seccessfully.");
            }elseif (Input::get('request_type') == 'post_details') {
              return redirect()->back()->with("demoRequest","Your request has been sent seccessfully.");
            }
          }
    }else {
        $post = \App\Models\posts::where("post_id",$id)->where('country_id', session('current_countryCode'))->first();
        if(!is_object($post)){
            $data["message"] = "Sorry, This product is not found.";
            return view('errors.404', $data);
        }
      $categories = DB::table('categories')->whereIn('cat_slug',['Industry','technology'])
                    ->where('country_id', session('current_countryCode'))->get();
      $cats_products = [];
      foreach ($categories as $key => $cat) {
        $cats_products[$key] = DB::table('categories')
                              ->join('posts_categories', 'categories.cat_id', '=', 'posts_categories.cat_id')
                              ->join('posts_langs', 'posts_langs.post_id', '=', 'posts_categories.post_id')
                              ->where('parent_id',$cat->cat_id)
                              ->where("posts_langs.lang", \Config::get("app.locale"))
                              ->where('categories.country_id', session('current_countryCode'))
                              ->get();
      }
      $cats_products = array_merge($cats_products[0],$cats_products[1]);
      // dd($cats_products);
      $data['cats_products'] = $cats_products;
      $data['id'] = $id;
      // $data['utm_data'] = $utm_data;
      return view('front.request-demo',$data);
    }
  }


  public function addRequest(){
      $rules = [
        'name'    => 'required',
        'email'   => 'required|email',
        'subject' => 'required',
        'comment' => 'required',
      ];
      $validator = \Validator::make(Request::all(), $rules);
      if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
      }
      $check_exist = AppointmentsRequests::where('email',Input::get('email'))->get();
      if (count($check_exist) > 0) {
        $data['sentRequest'] =  "You sent this request before.";
        $data['id'] = Input::get('hiddenID');
        return view('front.request-demo',$data);
      }else {
        $demoRequest = new AppointmentsRequests();
        $demoRequest['name']       = Input::get('name');
        $demoRequest['email']      = Input::get('email');
        $demoRequest['subject']    = Input::get('subject');
        $demoRequest['comment']    = Input::get('comment');
        $demoRequest['product_id'] = Input::get('hiddenID');
        $demoRequest->save();
        $data['demoRequest'] =  "Your request has been sent seccessfully.";
        $data['id'] = Input::get('hiddenID');
        return view('front.request-demo',$data);
      }

    }

  //// end of class
}
