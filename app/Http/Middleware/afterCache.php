<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;

class afterCache {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $response = $next($request);
        if (count($_POST) <= 0) {
            $key = $this->makeCacheName($request->fullUrl());

            if (!Cache::has($key)) {
                Cache::put($key, $response->getContent(), 60 * 24 * 100);
            }
        }
        return $next($request);
    }

    private function makeCacheName($url) {
        return "route_" . md5($url . "_" . session("current_countryCode"));
    }

}
