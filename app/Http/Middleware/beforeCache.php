<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;

class beforeCache {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (count($_POST) <= 0) {
            $key = $this->makeCacheName($request->fullUrl());
            if (Cache::has($key)) {
                return response(Cache::get($key));
            } else {
                return $next($request);
            }
        } else {
            return $next($request);
        }
    }

    private function makeCacheName($url) {
        return "route_" . md5($url . "_" . session("current_countryCode"));
    }

}
