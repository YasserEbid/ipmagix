<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::group(['middleware' => ['web', 'beforecache', 'aftercache']], function () {
    Route::any('/admin', function() {
        return redirect()->route('auth.login');
    });

    Route::get('/db_migrate', function() {
        \Artisan::call('migrate', ["--force" => true]);
    });

    Route::get('/lang', function() {
        $poll = Mediasci\Cms\Models\Polls::find(1);
        dd($poll->chooseLang('en')->lang()->get()->toArray());
        dd($poll->allLang()->lang()->where('lang', 'ar')->get()->toArray());
    });

    Route::any('/trend', function() {
        $data = Twitter::getTrendsPlace(['id' => 1940119]);
        $i = 0;
        foreach ($data[0]->trends as $row) {
            if ($i >= 9)
                break;
            $trend_row['name'] = $row->name;
            $trend_row['url'] = $row->url;
            $output[] = $trend_row;
            $i++;
        }
        if (!is_object($trend = \Mediasci\Cms\Models\Trends::where('date', date('Y-m-d', time()))->first())) {
            $trend = new \Mediasci\Cms\Models\Trends();
            $trend->date = date('Y-m-d', time());
        }
        $trend->twitter = json_encode($output);
        $trend->save();
    });

    Route::get('/', "HomeController@index");
    Route::get('solution/send', "SolutionsController@sendMail");
    Route::get('solution/{id}', "SolutionsController@showSolution");
    Route::get('about-us', "PostsController@aboutContent");

    Route::get('cases', "CasesController@showCases");
    Route::any("contact-us", "HomeController@contactus");

    Route::get('partners/{type}', 'PartnersController@getPartners');

    Route::get('news', 'PostsController@newsPost');
    Route::get('news-details/{id}', 'PostsController@news_details');

    Route::any('careers', "CareersController@showCareers");
    Route::any('careers/show/{id}', "CareersController@show");
});
Route::group(['middleware' => ['web']], function () {
    Route::get('{category_name}/solutions-details/{id}', "PostsController@postDetails");
    Route::any('partners/become-partner', 'PartnershipController@partnerRequest');
    Route::any('demo-requests/{id}', 'DemoRequestController@demoRequest');
    Route::get('search', 'PostsController@findResult');
    Route::post('contactus', "HomeController@savecontact");
    Route::any('careers/apply/{id}', "CareersController@apply");
    Route::get('linkedIN/login/{id}', 'linkedINController@redirectToProvider');
    Route::get('linkedIN/redirect-back', 'linkedINController@handleProviderCallback');
    Route::post('subscribe', "HomeController@subscribe");
});

Route::get('/cache_flush', function() {
    \Cache::flush();
});


/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

// Route::group(['middleware' => ['web']], function () {
	//
// });
