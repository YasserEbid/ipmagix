<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sliders extends BaseModel
{
	
     protected $table = 'sliders';
	 public $timestamps = false;
	 protected $primaryKey = 'id';

function Lang(){
	return $this->hasOne('App\Models\slider_langs', 'slider_id', 'id');
}
function media(){
	return $this->hasOne('App\Models\media', 'media_id', 'media_id');
}
}