<?php

namespace App\Models;

class Workflow extends BaseModel {

    public $table = 'workflow';
    protected $guarded=['id'];
   
    public function levels(){
        return $this->hasMany('Mediasci\Cms\Models\WorkflowLevels', 'workflow_id');
    }
}
