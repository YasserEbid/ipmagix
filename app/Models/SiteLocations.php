<?php

namespace App\Models;

class SiteLocations extends BaseModel {

	public $table = 'site_locations';
	public $timestamps = false;
	protected $primaryKey = 'id';

	function Lang() {
		return $this->hasOne('App\Models\LocationsLangs', 'location_id', 'id');
	}
}
