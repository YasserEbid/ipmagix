<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Languages;

class slider_langs extends BaseModel
{
	use Languages;
     protected $table = 'slider_langs';
	 public $timestamps = false;
	 protected $primaryKey = 'id';
}
