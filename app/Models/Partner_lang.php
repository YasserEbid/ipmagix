<?php
namespace App\Models;

class Partner_lang extends BaseModel {
    protected $table='partners_lang';
    protected $fillable=['name','description','partner_id','lang','time'];
    public $timestamps = false;
    public function partner(){
        return $this->belongsTo('Mediasci\Cms\Models\Partner','partner_id');
    }
}
