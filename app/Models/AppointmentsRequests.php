<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentsRequests extends BaseModel {

	protected $table='appointments_requests';

	public static function get_attributes_names() {

		$names = [
			'first_name' => trans('cms::cities.first_name'),
			'last_name' => trans('cms::cities.last_name'),
			'email' => trans('cms::cities.email'),
			'city_id'=>trans('cms::cities.city_id'),
			'specialisty_id'=>trans('cms::cities.specialisty_id'),
			'visit_reason'=>trans('cms::cities.visit_reason'),
			'visit_date'=>trans('cms::cities.visit_date'),
			'visit_time'=>trans('cms::cities.visit_time'),
			'created_at'=>trans('cms::cities.created_at'),
			'updated_at'=>trans('cms::cities.updated_at'),
		];

		return $names;
	}
}
