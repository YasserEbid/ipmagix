<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event_category extends BaseModel
{
    protected $table='event_categories';
     function Lang(){
	return $this->hasOne('App\Models\Event_category_lang', 'event_category_id', 'id');
}
public function event(){
    return $this->hasMany('App\Models\Event', 'eventcat_id', 'id');
}
function media(){
	return $this->hasOne('App\Models\media', 'media_id', 'media_id');
}
}