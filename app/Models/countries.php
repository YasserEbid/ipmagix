<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class countries extends BaseModel {

	protected $table = 'countries';
	public $timestamps = false;
	protected $primaryKey = 'id';

	function Lang() {
		return $this->hasOne('App\Models\countries_langs', 'country_id', 'id');
	}

}
