<?php

namespace App\Models\Traits;

/**
 * Description of lang
 *
 * @author Elsayed Nofal
 */
trait Languages {
    
    
    public static $allLanguage = false;
    //public $all_lang=false;
    public function newQuery()
    {
        $query = parent::newQuery();
        
        if(!parent::$all_lang){
            $query->where('lang',  config('app.locale'));
        }else{
            parent::$all_lang=false;
        }
        return $query;
    }
    
    

}
