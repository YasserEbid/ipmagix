<?php
namespace App\Models;

class Feedback_lang extends BaseModel {
    protected $table='feedback_lang';
    protected $fillable=['name','description','feedback_id','lang','job'];
    public $timestamps = false;
    public function feedback(){
        return $this->belongsTo('Mediasci\Cms\Models\Feedback','feedback_id');
    }
}
