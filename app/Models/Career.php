<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Career extends BaseModel
{
    protected $table='careers';
    function Lang(){
	return $this->hasOne('App\Models\Career_lang', 'career_id', 'id');
}
public function category(){
    return $this->belongsTo('App\Models\Careers_category','cat_id');
}
function job(){
    return $this->hasMany('App\Models\Job_cv','career_id');
}

}