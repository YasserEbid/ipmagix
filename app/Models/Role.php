<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Role
 *
 * @author Darsh
 */
class Role extends Model {

	protected $table = 'roles';
	public $timestamps = false;
	protected $primaryKey = 'role_id';

}
