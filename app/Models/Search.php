<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Search extends BaseModel {

    public static function findByTerm($term = '') {
        $lang = \Config::get("app.locale");
        $data = [];
        if (isset($term) && !empty($term)) {
            $data['posts'] = \DB::table('posts')
                    ->join('posts_langs', 'posts.post_id', '=', 'posts_langs.post_id')
                    ->leftJoin('posts_categories', 'posts.post_id', '=', 'posts_categories.post_id')
                    ->leftJoin('categories', 'categories.cat_id', '=', 'posts_categories.cat_id')->where('posts.status', '1')
                    ->select('posts.*', 'posts_langs.post_title', 'posts_langs.post_subtitle', 'posts_langs.post_content', 'categories.cat_slug');
            //->where('status','1');
            $data['posts'] = $data['posts']->where(function($q) use (&$term, &$lang) {
                        $q->where('lang', '=', $lang);
                        $q->where(function ( $q ) use (&$term) {

                            $q->where('post_title', 'like', "%" . $term . "%");
                            $q->orwhere('post_content', 'like', "%" . $term . "%");
                            $q->orwhere('slug', 'like', "%" . $term . "%");
                        });
                    })
                    //->orderBy('posts.post_id', 'asc')
                    ->groupBy('posts.post_id')
                    ->get();
            //echo "<pre>";print_r($data['posts']);die;
            $data['doctors'] = \DB::table('doctors')
                    ->join('doctor_langs', 'doctors.id', '=', 'doctor_langs.doctor_id')
                    ->select('doctors.*', 'doctor_langs.name', 'doctor_langs.description', 'doctor_langs.lang_spoken');
            //->where('lang',\Config::get("app.locale"));
            $data['doctors'] = $data['doctors']->where(function($q) use (&$term, &$lang) {
                        $q->where('lang', '=', $lang);
                        $q->where(function ( $q ) use (&$term) {
                            $q->where('name', 'like', "%" . $term . "%");
                            $q->orwhere('description', 'like', "%" . $term . "%");
                        });
                    })
                    ->groupBy('doctors.id')
                    ->get();

            $data['services'] = \DB::table('services')
                    ->join('services_langs', 'services.id', '=', 'services_langs.service_id')
                    ->select('services.*', 'services_langs.title', 'services_langs.description');
            //->where('active','1');
            //->where('lang',\Config::get("app.locale"));
            $data['services'] = $data['services']->where(function($q) use (&$term, &$lang) {
                        $q->where('lang', '=', $lang);
                        $q->where(function ( $q ) use (&$term) {
                            $q->where('title', 'like', "%" . $term . "%");
                            $q->orwhere('description', 'like', "%" . $term . "%");
                            $q->orwhere('alias', 'like', "%" . $term . "%");
                        });
                    })
                    ->groupBy('services.id')
                    ->get();

            $data['events'] = \DB::table('events')
                    ->join('event_langs', 'events.id', '=', 'event_langs.event_id')
                    ->select('events.*', 'event_langs.title', 'event_langs.description');
            //->where('lang',\Config::get("app.locale"));
            $data['events'] = $data['events']->where(function($q) use (&$term, &$lang) {
                        $q->where('lang', '=', $lang);
                        $q->where(function ( $q ) use (&$term) {
                            $q->where('title', 'like', "%" . $term . "%");
                            $q->orwhere('description', 'like', "%" . $term . "%");
                            $q->orwhere('slug', 'like', "%" . $term . "%");
                        });
                    })
                    ->groupBy('events.id')
                    ->get();

            $data['categories'] = \DB::table('categories')
                    ->join('categories_langs', 'categories.cat_id', '=', 'categories_langs.cat_id')
                    ->select('categories.*', 'categories_langs.name');
            $data['categories'] = $data['categories']->where(function($q) use (&$term, &$lang) {
                        $q->where('lang', '=', $lang);
                        $q->where(function ( $q ) use (&$term) {
                            $q->where('name', 'like', "%" . $term . "%");
                            $q->orwhere('cat_slug', 'like', "%" . $term . "%");
                        });
                    })
                    ->groupBy('categories.cat_id')
                    ->get();
        }
        return $data;
    }

}
