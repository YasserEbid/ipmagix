<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Languages;
class Event_lang extends BaseModel
{
	use Languages;
    protected $table='event_langs';
    public $timestamps = false;
	 protected $primaryKey = 'id';
    
}