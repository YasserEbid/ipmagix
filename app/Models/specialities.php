<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class specialities extends Model
{
   	protected $table = 'specialisty';
	public $timestamps = false;
	protected $primaryKey = 'id';
}
