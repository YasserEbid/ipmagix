<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends BaseModel
{
    protected $table='general_settings';

    public static function getAll(){
        $site_settings = [];
        $settings=self::get();
        foreach ($settings as $key => $setting) {
        	$site_settings[$setting->name] = $setting->value;
        }
        
        return $site_settings;
    }

    public static function setting($name){
        $setting=self::where('name',$name)->first();
        
        return $setting->value;
    }
    
}