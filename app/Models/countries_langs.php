<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Languages;
class countries_langs extends BaseModel
{
	use Languages;
     protected $table = 'countries_langs';
	 public $timestamps = false;
	 protected $primaryKey = 'id';
}
