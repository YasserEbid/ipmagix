<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class media extends BaseModel
{
     protected $table = 'media';
	 public $timestamps = false;
	 protected $primaryKey = 'media_id';
}
