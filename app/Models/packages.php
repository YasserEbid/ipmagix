<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class packages extends BaseModel {

	protected $table = 'packages';
	public $timestamps = false;
	protected $primaryKey = 'id';
function event(){
    return $this->belongsToMany('App\Models\Event', 'events_packages', 'package_id', 'event_id');
}
function events(){
    return $this->hasMany('App\Models\Event','package_id');
}
}
