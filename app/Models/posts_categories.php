<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class posts_categories extends BaseModel {

	protected $table = 'posts_categories';
	public $timestamps = false;
	protected $primaryKey = 'id';

	function category() {
		return $this->belongsTo('App\Models\categories', 'cat_id', 'id');
	}

	function post() {
		return $this->belongsTo('App\Models\posts', 'post_id', 'post_id');
	}

}
