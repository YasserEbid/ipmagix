<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends BaseModel {

	protected $table = 'categories';
	public $timestamps = false;
	protected $primaryKey = 'cat_id';

	function Lang() {
  	return $this->hasOne('App\Models\Categories_langs', 'cat_id', 'cat_id');
	}

	function postsIds(){
		return $this->hasMany('App\Models\posts_categories', 'cat_id', 'cat_id')->orderBy('sort')->orderBy('post_id');
	}

	function posts(){
		return $this->belongsToMany('App\Models\posts', 'posts_categories', 'cat_id', 'post_id')->orderBy('posts_categories.sort')->orderBy('posts.post_id');
	}

	function media() {
		return $this->belongsTo('App\Models\media', 'banner_id', 'media_id');
	}

	function video() {
		return $this->belongsTo('App\Models\media', 'video_id', 'media_id');
	}

	function videoCover() {
		return $this->belongsTo('App\Models\media', 'video_cover_id', 'media_id');
	}

}
