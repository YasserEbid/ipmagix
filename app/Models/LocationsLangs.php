<?php

namespace App\Models;
use App\Models\Traits\Languages;

class LocationsLangs extends BaseModel {

	use Languages;

	protected $table = 'site_locations_lang';
	public $timestamps = false;
	protected $primaryKey = 'id';

}
