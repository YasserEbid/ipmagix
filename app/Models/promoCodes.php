<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class promoCodes extends BaseModel {

	protected $table = 'promo_codes';
	public $timestamps = false;
	protected $primaryKey = 'id';

}
