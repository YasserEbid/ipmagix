<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event_page extends BaseModel
{
    protected $table='event_pages';
    public $timestamps = false;
	 protected $primaryKey = 'id';
    function media(){
	return $this->hasOne('App\Models\media', 'media_id', 'media_id');
}
function Lang(){
	return $this->hasOne('App\Models\Event_pages_lang', 'eventpage_id', 'id');
}
public function event(){
    return $this->belongsTo('App\Model\Event', 'event_id','id');
}
}