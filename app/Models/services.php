<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class services extends BaseModel {

	protected $table = 'services';
	public $timestamps = false;
	protected $primaryKey = 'id';

	function Lang() {
		return $this->hasOne('App\Models\services_langs', 'service_id', 'id');
	}

}
