<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Languages;

class posts_langs extends BaseModel
{
	use Languages;
   protected $table = 'posts_langs';
	 public $timestamps = false;
	 protected $primaryKey = 'id';


}
