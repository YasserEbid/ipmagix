<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class services_langs extends BaseModel
{
	
     protected $table = 'services_langs';
	 public $timestamps = false;
	 protected $primaryKey = 'id';
}