<?php

namespace App\Models;

class ClientsLangs extends BaseModel {

	protected $table='clients_langs';
	public function client(){
		 return $this->belongsTo('Mediasci\Cms\Models\ClientsLangs','client_id','id');
	}


}
