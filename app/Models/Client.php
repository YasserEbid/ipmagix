<?php
namespace App\Models;


class Client extends BaseModel
{

protected $table='clients';

  public function media(){
     return $this->hasOne('Mediasci\Cms\Models\Media','media_id','media_id');
  }
  public function clientsCategory(){
     return $this->belongsTo('Mediasci\Cms\Models\ClientsCategory','category_id','id');
  }
  public function clientsLangs(){
     return $this->hasMany('Mediasci\Cms\Models\ClientsLangs','client_id','id');
  }
}
