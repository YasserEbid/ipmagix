<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Languages;
class Categories_langs extends BaseModel
{
	use Languages;
   protected $table = 'categories_langs';
	 public $timestamps = false;
	 protected $primaryKey = 'id';
	 function parent() {
		 return $this->hasOne('App\Models\Categories', 'parent_id', 'cat_id');
	 }
}
