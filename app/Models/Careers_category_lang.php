<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Languages;

class Careers_category_lang extends BaseModel
{ 
   use Languages;
     protected $table = 'careers_category_langs';
	 public $timestamps = false;
	 protected $primaryKey = 'id';
}
