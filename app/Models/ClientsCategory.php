<?php
namespace App\Models;


class ClientsCategory extends BaseModel
{
protected $table='clientsCategory';
  public function clients(){
     return $this->hasMany('Mediasci\Cms\Models\Client','category_id','id');
  }
  public function clientCategoryLangs(){
		 return $this->hasMany('Mediasci\Cms\Models\ClientsCategoryLangs','clientCategory_id','id');
	}
}
