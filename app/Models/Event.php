<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends BaseModel
{
    protected $table='events';
    public $timestamps = false;
	 protected $primaryKey = 'id';
    function media(){
	return $this->hasOne('App\Models\media', 'media_id', 'media_id');
}
    function Lang(){
	return $this->hasOne('App\Models\Event_lang', 'event_id', 'id');
}
public function eventpage(){
    return $this->hasMany('App\Models\Event_page', 'event_id', 'id');
}
public function category(){
    return $this->belongsTo('App\Models\Event_category', 'eventcat_id');
}
function packages(){
    return $this->belongsToMany('App\Models\packages', 'events_packages', 'event_id', 'package_id');
}
function package(){
    return $this->belongsTo('App\Models\packages', 'package_id');
}
}
