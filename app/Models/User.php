<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

	protected $table = 'users';
	public $timestamps = false;
	protected $primaryKey = 'user_id';

	public function role() {
		return $this->hasOne('App\Models\Role', 'role_id', 'role_id');
	}

}
