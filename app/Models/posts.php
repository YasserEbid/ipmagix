<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class posts extends BaseModel {

	protected $table = 'posts';
	public $timestamps = false;
	protected $primaryKey = 'post_id';

	function Lang() {
		return $this->hasOne('App\Models\posts_langs', 'post_id', 'post_id');
	}

	function categoriesIds() {
		return $this->hasMany('App\Models\posts_categories', 'post_id', 'post_id');
	}

	function media() {
		return $this->hasOne('App\Models\media', 'media_id', 'image_id');
	}

	function categories() {
		return $this->belongsToMany('App\Models\Categories', 'posts_categories', 'post_id', 'cat_id');
	}

	function link($category = null) {
		$post_link = '';
		if ($this->is_link) {
			$post_link = $this->slug;
		} else {
			if ($this->site_id == 2) {
				$post_link .= 'diabetes/';
			}
			if ($category) {
				$post_link .= $category->cat_slug.'/';
			} else {
				$category = $this->categories->first();
				if ($category) {
					$post_link .= $category->cat_slug.'/';
				} else {
					// fixed slug to be changed
					$post_link .= 'about_us'.'/';
				}
			}
			$post_link .= $this->slug;
		}
		return url($post_link);
	}
        public function tab(){
            return $this->hasMany('App\Models\Posttabs', 'post_id');
  }

	function internalImage() {
		return $this->hasOne('App\Models\media', 'media_id', 'internal_image_id');
	}
	function logoImage() {
		return $this->hasOne('App\Models\media', 'media_id', 'logo_id');
	}

	function video() {
		return $this->belongsTo('App\Models\media', 'video_id', 'media_id');
	}

	function videoCover() {
		return $this->hasOne('App\Models\media', 'media_id', 'video_cover_id');
	}

	function homeImage() {
		return $this->hasOne('App\Models\media', 'media_id', 'home_image_id');
	}

	function introImage() {
		return $this->hasOne('App\Models\media', 'media_id', 'intro_image_id');
	}

	function postTabs(){
		return $this->hasMany('App\Models\Posttabs', 'post_id', 'post_id');
	}



}
