<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posttabs extends BaseModel
{
    protected $table='posts_tabs';
    function Lang(){
	return $this->hasOne('App\Models\Posttablang', 'post_tab_id', 'id');
}
public function post(){
    return $this->belongsTo('App\Models\posts','post_id');
}

}