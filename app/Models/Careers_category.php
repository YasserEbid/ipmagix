<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Careers_category extends BaseModel
{
    protected $table='careers_categories';
    
    function Lang(){
		return $this->hasOne('App\Models\Careers_category_lang', 'cat_id', 'id');
	}
	
	function media(){
		return $this->hasOne('App\Models\media', 'media_id', 'media_id');
	}
	
	function career(){
	    return $this->hasMany('App\Models\Career','cat_id');
	}

	public function careers()
    {
        return $this->belongsToMany('App\Models\Career', 'career_careers_category', 'careers_category_id', 'career_id');
    }
}
