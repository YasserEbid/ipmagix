<?php
namespace App\Models;


class Feedback extends BaseModel
{
  protected $table = 'feedback';
    protected $fillable = ['media_id'];
  public function media(){
     return $this->hasOne('Mediasci\Cms\Models\Media','media_id','media_id');
  }
  public function feedbacklang($lang){
      return $this->hasMany('Mediasci\Cms\Models\Feedback_lang', 'feedback_id')->where('lang',$lang)->first();
  }
}
